import matplotlib.pyplot as plt
import mplhep as hep
import uproot
import matplotlib.gridspec as gridspec
import numpy as np
import matplotlib.patches as patches
import matplotlib.path as path
import hist
from hist import Hist
#from pathlib import Path
#from pathlib import PathPatch
#DY.root  EWK_LLJJ_MLL.root  SingleMuon.root

def make_ratio(hnum, hden):
  wnum, bins = hnum.to_numpy()
  wden, bins = hden.to_numpy()

  numerr = np.sqrt(hnum.variances())
  
  #wratio = np.divide(wnum, wden) - 1
  wratio = np.divide(wnum, wden)
  print('data')
  print(wnum)
  print('mc')
  print(wden)
  ratioerr = np.divide(numerr, wden)
  bincenters = (bins[:-1] + bins[1:]) / 2
  return wratio, bins, ratioerr

def make_ratio_array(hnum, hden):
  wnum = hnum
  wden = hden
  

  #wratio = np.divide(wnum, wden) - 1
  wratio = np.divide(wnum, wden)
  print('data')
  print(wnum)
  print('mc')
  print(wden)
  
  return wratio

def systematics(mc_nom_file, sys_up_file, sys_down_file):
    sys_up_f = 0
    sys_down_f = 0
    for mc_nom, sys_up_value, sys_down_value in zip(mc_nom_file, sys_up_file, sys_down_file):
        mc_nom = hist.Hist(mc_nom)
        sys_up_value = hist.Hist(sys_up_value)
        sys_down_value = hist.Hist(sys_down_value)
        sys_up_temp = ((sys_up_value.values() - mc_nom.values())**2)
        sys_down_temp = ((sys_down_value.values() - mc_nom.values())**2)
        sys_up_f = sys_up_temp + sys_up_f
        sys_down_f = sys_down_temp + sys_down_f
    sys_up = np.sqrt(sys_up_f)
    sys_down = np.sqrt(sys_down_f)
    
    return sys_up, sys_down

def total_systematics(sys_up, sys_down):
    sys_up_f = 0
    sys_down_f = 0
    for sys_up_partial, sys_down_partial in zip(sys_up, sys_down):
        sys_up_temp = (sys_up_partial**2)
        sys_down_temp = (sys_down_partial**2)
        sys_up_f = sys_up_temp + sys_up_f
        sys_down_f = sys_down_temp + sys_down_f
    total_sys_up = np.sqrt(sys_up_f)
    total_sys_down = np.sqrt(sys_down_f)
    return total_sys_up, total_sys_down

def systematics_ratio(sys_up, sys_down, mc_nom_stacked):
    ratio_up_sys = 1 + sys_up/mc_nom_stacked  
    ratio_down_sys = 1 - sys_down/mc_nom_stacked
    return ratio_up_sys, ratio_down_sys

# Load histograms

# General results directory
inputdir_MET =  '/nfs/dust/cms/user/moralfk/EWZjj_Pepper/hists/'
inputdir_noMET = '/nfs/dust/cms/user/moralfk/EWZjj_Pepper/ALL_30thjune_sys/'

# New file names
file_names = ['Dijet_cut_pv_npvs.root', 'Dijet_cut_pv_npvsGood.root', 'Dijet_cut_leading_jet_QGL.root', 'Dijet_cut_subleading_jet_QGL.root', 'Dijet_cut_R_pT_hard.root', 'Dijet_cut_z_star.root',
              'Dijet_cut_leading_muon_pt.root', 'Dijet_cut_leading_muon_abseta.root', 'Dijet_cut_Delta_R_jets_l_mu.root', 'Dijet_cut_Delta_R_jets_s_mu.root',
              'Dijet_cut_subleading_muon_pt.root', 'Dijet_cut_subleading_muon_abseta.root',
              'Dijet_cut_leading_jet_pt.root', 'Dijet_cut_leading_jet_abseta.root', 'Dijet_cut_subleading_jet_pt.root', 'Dijet_cut_subleading_jet_abseta.root', 
              'Dijet_cut_Dijet_pt.root', 'Dijet_cut_Dijet_abseta.root', 'Dijet_cut_Dijet_mass.root', 'Dijet_cut_Dijet_phi.root', 
              'Dijet_cut_number_of_jets.root', 'Dijet_cut_number_of_low_pT_jets.root', 'Dijet_cut_dphijj.root', 'Dijet_cut_detajj.root', 'Dijet_cut_detall.root', 'Dijet_cut_dphill.root',
              'Dijet_cut_Zboson_pt.root', 'Dijet_cut_Zboson_mass.root', 'Dijet_cut_Zboson_abseta.root', 'Dijet_cut_Zboson_phi.root', 
              'Dijet_cut_DphiZ_l_jet.root', 'Dijet_cut_DphiZ_s_jet.root', 'Dijet_cut_DphiZ_jets.root']

file_names_MET = ['MET filters_pv_npvs.root', 'MET filters_pv_npvsGood.root', 'MET filters_leading_jet_QGL.root', 'MET filters_subleading_jet_QGL.root', 'MET filters_R_pT_hard.root', 
              'MET filters_z_star.root',
              'MET filters_leading_muon_pt.root', 'MET filters_leading_muon_abseta.root', 'MET filters_Delta_R_jets_l_mu.root', 'MET filters_Delta_R_jets_s_mu.root',
              'MET filters_subleading_muon_pt.root', 'MET filters_subleading_muon_abseta.root',
              'MET filters_leading_jet_pt.root', 'MET filters_leading_jet_abseta.root', 'MET filters_subleading_jet_pt.root', 'MET filters_subleading_jet_abseta.root',
              'MET filters_Dijet_pt.root', 'MET filters_Dijet_abseta.root', 'MET filters_Dijet_mass.root', 'MET filters_Dijet_phi.root',
              'MET filters_number_of_jets.root', 'MET filters_number_of_low_pT_jets.root', 'MET filters_dphijj.root', 'MET filters_detajj.root', 'MET filters_detall.root', 'MET filters_dphill.root',
              'MET filters_Zboson_pt.root', 'MET filters_Zboson_mass.root', 'MET filters_Zboson_abseta.root', 'MET filters_Zboson_phi.root',
              'MET filters_DphiZ_l_jet.root', 'MET filters_DphiZ_s_jet.root', 'MET filters_DphiZ_jets.root']

samples = ['EWK_LLJJ_MLL-50_MJJ-120_TuneCP5_13TeV-madgraph-pythia8_dipole', 'WW_TuneCP5_13TeV-pythia8', 'WZ_TuneCP5_13TeV-pythia8', 'ZZ_TuneCP5_13TeV-pythia8',
           'ST_tW_top_5f_inclusiveDecays_TuneCP5_13TeV-powheg-pythia8', 'ST_t-channel_top_4f_InclusiveDecays_TuneCP5_13TeV-powheg-madspin-pythia8',
           'ST_tW_antitop_5f_inclusiveDecays_TuneCP5_13TeV-powheg-pythia8','ST_t-channel_antitop_4f_InclusiveDecays_TuneCP5_13TeV-powheg-madspin-pythia8',
           'ST_s-channel_4f_leptonDecays_TuneCP5_13TeV-amcatnlo-pythia8', 'TTTo2L2Nu_TuneCP5_13TeV-powheg-pythia8','TTToSemiLeptonic_TuneCP5_13TeV-powheg-pythia8',
           'DYJetsToLL_M-50_TuneCP5_13TeV-amcatnloFXFX-pythia8', 'SingleMuon' ]

for file_name, file_name_MET in zip(file_names, file_names_MET):

    mc_histograms = [None]*(len(samples)-1)
    mc_histograms_MET = [None]*(len(samples)-1)

    for i in range(len(mc_histograms)):
        open_file = uproot.open(inputdir_noMET + file_name)
        mc_histograms[i] = open_file[samples[i]].to_hist()
        data_histogram = open_file['SingleMuon'].to_hist()
        
        open_file_MET = uproot.open(inputdir_MET + file_name_MET)
        mc_histograms_MET[i] = open_file_MET[samples[i]].to_hist()
        data_histogram_MET = open_file_MET['SingleMuon'].to_hist()

    # Set style
    plt.style.use(hep.style.ROOT)
    plt.rcParams.update({'font.size': 14})   
    plt.rcParams.update({'axes.titlesize':20})
    plt.rcParams.update({'axes.labelsize':20})

    # Set stack and ratio plot
    fig, ax = plt.subplots(2, 1, gridspec_kw={'height_ratios': [2, 1], 'hspace':0}, sharex=True)
    scatter_ax = ax[0]
    # Calculate ratio Data/MC
    sum_mc_histos = sum(mc_histograms)
    wratio_noMET, bins_noMET, werr_noMET = make_ratio(data_histogram, sum_mc_histos)
    sum_mc_histos_MET = sum(mc_histograms_MET)
    wratio_MET, bins_MET, werr_MET = make_ratio(data_histogram_MET, sum_mc_histos_MET)
    hep.histplot(wratio_noMET, bins_noMET, yerr = (werr_noMET),ax = scatter_ax,  stack=False, histtype='errorbar', color='red', label = 'No MET filters')
    hep.histplot(wratio_MET, bins_MET, yerr = (werr_MET), ax= scatter_ax, stack=False, histtype='errorbar', color='blue', label = 'MET filters')
    scatter_ax.legend()

    ratio_axis = ax[1]
    ratio_axis.axhline(y=1, color='black')
    wratio_data, bins_data, werr_data = make_ratio(data_histogram_MET, data_histogram)
    wratio_mc, bins_mc, werr_mc = make_ratio(sum_mc_histos_MET, sum_mc_histos)
    wratio = make_ratio_array(wratio_MET, wratio_noMET)

    hep.histplot(wratio_data, bins_data, yerr= werr_data, ax = ratio_axis,  stack=False, histtype='errorbar', color='black', label = 'Ratio(Data_MET/Data_noMET)') 
    hep.histplot(wratio_mc, bins_mc, yerr= werr_mc, ax = ratio_axis,  stack=False, histtype='errorbar', color='orange', label = 'Ratio(MC_MET/MC_noMET)')
    hep.histplot(wratio, bins_mc, ax = ratio_axis,  stack=False, histtype='step', color='red', label = 'Ratio($((Data/MC)_{MET})/((Data/MC)_{noMET}))$')

    ratio_axis.legend()
    if file_name == 'Dijet_cut_Delta_R_jets_l_mu.root':
        ratio_axis.set_xlabel('$\\Delta R_{\\mu_{L} , jets}$')
    elif file_name == 'Dijet_cut_Delta_R_jets_s_mu.root':
        ratio_axis.set_xlabel('$\\Delta R_{\\mu_{S} , jets}$')
    elif file_name == 'Dijet_cut_pv_npvsGood.root':
        ratio_axis.set_xlabel('Number of good primary vertices')
    elif file_name == 'Dijet_cut_leading_muon_pt.root':
        ratio_axis.set_xlabel('Leading muon transverse momentum: $p_{T}$ [GeV]')   
    elif file_name == 'Dijet_cut_leading_muon_abseta.root':
        ratio_axis.set_xlabel('Leading muon $|\eta|$')
    elif file_name == 'Dijet_cut_Delta_R_l_mu_l_jet.root':
        ratio_axis.set_xlabel(' $\\Delta R_{\\mu_{L} , jet_{L}}$')
    elif file_name == 'Dijet_cut_Delta_R_l_mu_s_jet.root':
        ratio_axis.set_xlabel('$\\Delta R_{\\mu_{L} , jet_{S}}$')
    elif file_name == 'Dijet_cut_Delta_R_l_jet.root':
        ratio_axis.set_xlabel(' $\\Delta R_{j_{L} , \\mu \\mu}$')
    elif file_name == 'Dijet_cut_Delta_R_s_jet.root':
        ratio_axis.set_xlabel('$\\Delta R_{j_{S} , \\mu \\mu}$')
    elif file_name == 'Dijet_cut_Delta_phi_l_mu_l_jet.root':
        ratio_axis.set_xlabel(' $\\Delta \\phi_{\\mu_{L}, jet_{L}}$')
    elif file_name == 'Dijet_cut_Delta_phi_l_mu_s_jet.root':
        ratio_axis.set_xlabel('$\\Delta \\phi_{\\mu_{L}, jet_{S}}$')
    elif file_name == 'Dijet_cut_subleading_muon_pt.root':
        ratio_axis.set_xlabel('Subleading muon transverse momentum: $p_{T}$ [GeV]')
    elif file_name == 'Dijet_cut_subleading_muon_abseta.root':
        ratio_axis.set_xlabel('Subleading muon $|\eta|$')
    elif file_name == 'Dijet_cut_leading_jet_pt.root':
        ratio_axis.set_xlabel('Leading jet transverse momentum: $p_{T}$ [GeV]')
    elif file_name == 'Dijet_cut_Delta_R_s_mu_l_jet.root':
        ratio_axis.set_xlabel(' $\\Delta R_{\\mu_{S} , jet_{L}}$')
    elif file_name == 'Dijet_cut_Delta_R_s_mu_s_jet.root':
        ratio_axis.set_xlabel('$\\Delta R_{\\mu_{S} , jet_{S}}$')
    elif file_name == 'Dijet_cut_Delta_phi_s_mu_l_jet.root':
        ratio_axis.set_xlabel(' $\\Delta \\phi_{\\mu_{S}, jet_{L}}$')
    elif file_name == 'Dijet_cut_Delta_phi_s_mu_s_jet.root':
        ratio_axis.set_xlabel('$\\Delta \\phi_{\\mu_{S}, jet_{S}}$')
    elif file_name == 'Dijet_cut_leading_jet_abseta.root':
        ratio_axis.set_xlabel('Leading jet $|\eta|$')
    elif file_name == 'Dijet_cut_subleading_jet_pt.root':
        ratio_axis.set_xlabel('Subleading jet transverse momentum: $p_{T}$ [GeV]')
    elif file_name == 'Dijet_cut_subleading_jet_abseta.root':
        ratio_axis.set_xlabel('Subleading jet $|\eta|$')
    elif (file_name == 'Dijet_cut_mjj.root') or (file_name == 'Dijet_cut_Dijet_mass.root'):
        ratio_axis.set_xlabel('Dijet mass: $m_{jj}$ [GeV]', fontsize = 20)
    elif (file_name == 'Dijet_cut_pTjj.root') or (file_name == 'Dijet_cut_Dijet_pt.root'):
        ratio_axis.set_xlabel('$p_{Tjj}$ [GeV]', fontsize = 20)
    elif file_name == 'Dijet_cut_Dijet_phi.root':
        ratio_axis.set_xlabel(' $\\phi_{jj}$ ', fontsize = 20)
    elif file_name == 'Dijet_cut_Dijet_abseta.root':
        ratio_axis.set_xlabel(' $ | \\eta_{jj} | $ ', fontsize = 20)
    elif file_name == 'Dijet_cut_Zboson_mass.root':
        ratio_axis.set_xlabel('Z boson mass: $m_{l^{+}l^{-}}$ [GeV]', fontsize = 20)
    elif file_name == 'Dijet_cut_Zboson_pt.root':
        ratio_axis.set_xlabel('Z boson transverse momentum: $p_{T}$ [GeV]', fontsize = 20)
    elif file_name == 'Dijet_cut_Zboson_abseta.root':
        ratio_axis.set_xlabel('$|\\eta|_{l^{+}l^{-}}$ ', fontsize = 20)
    elif file_name == 'Dijet_cut_Zboson_phi.root':
        ratio_axis.set_xlabel('$|\\phi|_{l^{+}l^{-}}$ ', fontsize = 20)
    elif file_name == 'Dijet_cut_number_of_jets.root':
        ratio_axis.set_xlabel('Number of jets')
    elif file_name == 'Dijet_cut_number_of_low_pT_jets.root':
        ratio_axis.set_xlabel('Number of low pT jets')
    elif file_name == 'Dijet_cut_DphiZ_jets.root':
        ratio_axis.set_xlabel(' $\\Delta \\phi_{Z, jj}$ ', fontsize = 20)
    elif file_name == 'Dijet_cut_DphiZ_l_jet.root':
        ratio_axis.set_xlabel(' $\\Delta \\phi_{Z, j_{L}}$ ', fontsize = 20)
    elif file_name == 'Dijet_cut_DphiZ_s_jet.root':
        ratio_axis.set_xlabel(' $\\Delta \\phi_{Z, j_{S}}$ ', fontsize = 20)
    elif file_name == 'Dijet_cut_Delta_phi_l_jet_mus.root':
        ratio_axis.set_xlabel(' $\\Delta \\phi_{j_{L}, \\mu \\mu}$ ', fontsize = 20)
    elif file_name == 'Dijet_cut_Delta_phi_s_jet_mus.root':
        ratio_axis.set_xlabel(' $\\Delta \\phi_{j_{S}, \\mu \\mu}$ ', fontsize = 20)
    elif file_name == 'Dijet_cut_dphill.root':
        ratio_axis.set_xlabel(' $\\Delta \\phi_{\\mu \\mu}$ ', fontsize = 20)
    elif file_name == 'Dijet_cut_dphijj.root':
        ratio_axis.set_xlabel(' $\\Delta \\phi_{j_{L} j_{S}}$ ', fontsize = 20)
    elif file_name == 'Dijet_cut_detall.root':
        ratio_axis.set_xlabel(' $\\Delta \\eta_{\\mu \\mu}$ ', fontsize = 20)
    elif file_name == 'Dijet_cut_detajj.root':
        ratio_axis.set_xlabel(' $\\Delta \\eta_{j_{L} j_{S}}$ ',fontsize = 20)
    elif file_name == 'Dijet_cut_R_pT_hard.root':
        ratio_axis.set_xlabel('$R_{p_{T}}^{hard}$ ', fontsize = 20)
    elif file_name == 'Dijet_cut_z_star.root':
        ratio_axis.set_xlabel(' $z^*$ ')
    elif file_name == 'Dijet_cut_pv_npvs.root':
        ratio_axis.set_xlabel('Number of total reconstructed  primary vertices')
    elif file_name == 'Muon_SF_pv_npvs.root':
        ratio_axis.set_xlabel('Number of good primary vertices')
    elif file_name == 'Pileup reweighting_pv_npvs.root':
        ratio_axis.set_xlabel('Number of total reconstructed primary vertices')
    elif file_name == 'Dijet_cut_QGD.root':
        ratio_axis.set_xlabel('QGL Discriminant')
    elif file_name == 'Dijet_cut_Delta_R_jets_l_mu.root':
        ratio_axis.set_xlabel(' $R_{jets, \\mu_{L}}$ ')
    elif file_name == 'Dijet_cut_Delta_R_jets_s_mu.root':
        ratio_axis.set_xlabel(' $R_{jets, \\mu_{S}}$ ')
    elif file_name == 'Dijet_cut_leading_jet_QGL.root':
        ratio_axis.set_xlabel('Leading jet QGL', fontsize = 20)
    elif file_name == 'Dijet_cut_subleading_jet_QGL.root':
        ratio_axis.set_xlabel('Subleading jet QGL', fontsize = 20)
    scatter_ax.set_ylabel(r'Data/MC')
    ratio_axis.set_ylim(0.0, 2.4)
    
    #plt.savefig('/afs/desy.de/user/m/moralfk/www/EWZjj-test-figures/NEW_DetectorLevelPlots/Pepper_muon_7thmay_SR_'+file_name+'.pdf')
    #plt.savefig('/afs/desy.de/user/m/moralfk/www/EWZjj-test-figures/NEW_DetectorLevelPlots/Pepper_muon_8thjune_goodPV_'+file_name+'.png')    
    plt.savefig('/afs/desy.de/user/m/moralfk/www/EWZjj-test-figures/NEW_DetectorLevelPlots/Pepper_muon_7thjuly_MET_study__'+file_name+'.png')
    plt.show() 
