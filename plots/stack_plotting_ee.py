import matplotlib.pyplot as plt
import mplhep as hep
import uproot
import matplotlib.gridspec as gridspec
import numpy as np
import matplotlib.patches as patches
import matplotlib.path as path
import hist
from hist import Hist
#from pathlib import Path
#from pathlib import PathPatch
#DY.root  EWK_LLJJ_MLL.root  SingleMuon.root

def make_signif(hnum, hden):
  #hden = hden *SF
  wnum, bins = hnum.to_numpy()
  print(bins)
  wden, bins = hden.to_numpy()
  wratio = np.divide(wnum, np.sqrt(wden))
  percentage = np.divide(wnum, (wnum+wden))
  wratio = np.nan_to_num(wratio)
  percentage = np.nan_to_num(percentage)
  percentage = percentage*100
  print('EWK')
  print(wnum)
  print('DY')
  print(wden)
  bincenters = (bins[:-1] + bins[1:]) / 2
  return wratio, bins, percentage

def make_ratio(hnum, hden):
  wnum, bins = hnum.to_numpy()
  wden, bins = hden.to_numpy()

  numerr = np.sqrt(hnum.variances())
  
  #wratio = np.divide(wnum, wden) - 1
  wratio = np.divide(wnum, wden)
  print('data')
  print(wnum)
  print('mc')
  print(wden)
  ratioerr = np.divide(numerr, wden)
  bincenters = (bins[:-1] + bins[1:]) / 2
  return wratio, bins, ratioerr

def make_ratio_norm_SF(hnum, hden, SF):
  hnum = hnum * SF
  wnum, bins = hnum.to_numpy()
  wden, bins = hden.to_numpy()
  numerr = np.sqrt(hnum.variances())

  #wratio = np.divide(wnum, wden) - 1
  wratio = np.divide(wnum, wden)
  print('data')
  print(wnum)
  print('mc')
  print(wden)
  ratioerr = np.divide(numerr, wden)
  bincenters = (bins[:-1] + bins[1:]) / 2
  return wratio, bins, ratioerr

def systematics(mc_nom_file, sys_up_file, sys_down_file):
    sys_up_f = 0
    sys_down_f = 0
    for mc_nom, sys_up_value, sys_down_value in zip(mc_nom_file, sys_up_file, sys_down_file):
        mc_nom = hist.Hist(mc_nom)
        sys_up_value = hist.Hist(sys_up_value)
        sys_down_value = hist.Hist(sys_down_value)
        sys_up_temp = ((sys_up_value.values() - mc_nom.values())**2)
        sys_down_temp = ((sys_down_value.values() - mc_nom.values())**2)
        sys_up_f = sys_up_temp + sys_up_f
        sys_down_f = sys_down_temp + sys_down_f
    sys_up = np.sqrt(sys_up_f)
    sys_down = np.sqrt(sys_down_f)
    
    return sys_up, sys_down

def total_systematics(sys_up, sys_down):
    sys_up_f = 0
    sys_down_f = 0
    for sys_up_partial, sys_down_partial in zip(sys_up, sys_down):
        sys_up_temp = (sys_up_partial**2)
        sys_down_temp = (sys_down_partial**2)
        sys_up_f = sys_up_temp + sys_up_f
        sys_down_f = sys_down_temp + sys_down_f
    total_sys_up = np.sqrt(sys_up_f)
    total_sys_down = np.sqrt(sys_down_f)
    return total_sys_up, total_sys_down

def systematics_ratio(sys_up, sys_down, mc_nom_stacked):
    ratio_up_sys = 1 + sys_up/mc_nom_stacked  
    ratio_down_sys = 1 - sys_down/mc_nom_stacked
    return ratio_up_sys, ratio_down_sys

# Load histograms

# General results directory
#inputdir =  '/nfs/dust/cms/user/moralfk/EWZjj_Pepper/hists/'
inputdir =  '/nfs/dust/cms/user/moralfk/EWZjj_Pepper/ALL_22ndJanuary24_ee_ML_18_HLT32yesbtag2_sys/'
#inputdir =  '/nfs/dust/cms/user/moralfk/EWZjj_Pepper/'
#inputdir =  '/nfs/dust/cms/user/moralfk/EWZjj_Pepper/'
# New file names
if inputdir ==  '/nfs/dust/cms/user/moralfk/EWZjj_Pepper/ALL_22ndJanuary24_ee_ML_18_HLT32yesbtag2_sys/':
    sf =1/1.14400881615
elif inputdir ==  '/nfs/dust/cms/user/moralfk/EWZjj_Pepper/ALL_22ndJanuary24_ee_ML_17_HLT35yesbtag_sys/':
    sf = 1/1.19314188966
else:
    sf = 1

file_names = ['MET filters_pv_npvs.root', 'MET filters_leading_jet_QGL.root', 'MET filters_subleading_jet_QGL.root', 'MET filters_R_pT_hard.root', 'MET filters_z_star.root',
              'MET filters_leading_electron_pt.root', 'MET filters_leading_electron_abseta.root', 'MET filters_Delta_R_jets_l_e.root', 'MET filters_Delta_R_jets_s_e.root',
              'MET filters_subleading_electron_pt.root', 'MET filters_subleading_electron_abseta.root',
              'MET filters_leading_jet_pt.root', 'MET filters_leading_jet_abseta.root', 'MET filters_subleading_jet_pt.root', 'MET filters_subleading_jet_abseta.root',
              'MET filters_Dijet_pt.root', 'MET filters_Dijet_abseta.root', 'MET filters_Dijet_mass.root', 'MET filters_Dijet_phi.root',
              'MET filters_number_of_jets.root', 'MET filters_number_of_low_pT_jets.root', 'MET filters_dphijj.root', 'MET filters_detajj.root', 'MET filters_detall.root', 'MET filters_dphill.root',
              'MET filters_Zboson_pt.root', 'MET filters_Zboson_mass.root', 'MET filters_Zboson_abseta.root', 'MET filters_Zboson_phi.root',
              'MET filters_DphiZ_l_jet.root', 'MET filters_DphiZ_s_jet.root', 'MET filters_DphiZ_jets.root']

file_names_001 = ['MET filters_leading_jet_QGL.root', 'MET filters_subleading_jet_QGL.root']
file_names_01 = ['MET filters_z_star.root', 'MET filters_DphiZ_l_jet.root', 'MET filters_DphiZ_jets.root', 'MET filters_detajj.root','MET filters_detall.root', 'MET filters_leading_electron_abseta.root', 'MET filters_leading_muon_abseta.root']
file_names_05 = ['MET filters_Zboson_mass.root']
file_names_10 = ['MET filters_leading_jet_pt.root','MET filters_subleading_jet_pt.root', 'MET filters_Zboson_pt.root', 'MET filters_Dijet_pt.root']
file_names_50= ['MET filters_Dijet_mass.root']

files_PU_up_systematics = ['MET filters_pv_npvs_pileup_up.root', 'MET filters_leading_jet_QGL_pileup_up.root', 'MET filters_subleading_jet_QGL_pileup_up.root', 'MET filters_R_pT_hard_pileup_up.root', 'MET filters_z_star_pileup_up.root',
              'MET filters_leading_electron_pt_pileup_up.root', 'MET filters_leading_electron_abseta_pileup_up.root', 'MET filters_Delta_R_jets_l_e_pileup_up.root', 'MET filters_Delta_R_jets_s_e_pileup_up.root',
              'MET filters_subleading_electron_pt_pileup_up.root', 'MET filters_subleading_electron_abseta_pileup_up.root',
              'MET filters_leading_jet_pt_pileup_up.root', 'MET filters_leading_jet_abseta_pileup_up.root', 'MET filters_subleading_jet_pt_pileup_up.root', 'MET filters_subleading_jet_abseta_pileup_up.root',
              'MET filters_Dijet_pt_pileup_up.root', 'MET filters_Dijet_abseta_pileup_up.root', 'MET filters_Dijet_mass_pileup_up.root', 'MET filters_Dijet_phi_pileup_up.root',
              'MET filters_number_of_jets_pileup_up.root', 'MET filters_number_of_low_pT_jets_pileup_up.root', 'MET filters_dphijj_pileup_up.root', 'MET filters_detajj_pileup_up.root', 'MET filters_detall_pileup_up.root', 'MET filters_dphill_pileup_up.root',
              'MET filters_Zboson_pt_pileup_up.root', 'MET filters_Zboson_mass_pileup_up.root', 'MET filters_Zboson_abseta_pileup_up.root', 'MET filters_Zboson_phi_pileup_up.root',
              'MET filters_DphiZ_l_jet_pileup_up.root', 'MET filters_DphiZ_s_jet_pileup_up.root', 'MET filters_DphiZ_jets_pileup_up.root']

files_PU_down_systematics = ['MET filters_pv_npvs_pileup_down.root', 'MET filters_leading_jet_QGL_pileup_down.root', 'MET filters_subleading_jet_QGL_pileup_down.root', 'MET filters_R_pT_hard_pileup_down.root', 'MET filters_z_star_pileup_down.root',
              'MET filters_leading_electron_pt_pileup_down.root', 'MET filters_leading_electron_abseta_pileup_down.root', 'MET filters_Delta_R_jets_l_e_pileup_down.root', 'MET filters_Delta_R_jets_s_e_pileup_down.root',
              'MET filters_subleading_electron_pt_pileup_down.root', 'MET filters_subleading_electron_abseta_pileup_down.root',
              'MET filters_leading_jet_pt_pileup_down.root', 'MET filters_leading_jet_abseta_pileup_down.root', 'MET filters_subleading_jet_pt_pileup_down.root', 'MET filters_subleading_jet_abseta_pileup_down.root',
              'MET filters_Dijet_pt_pileup_down.root', 'MET filters_Dijet_abseta_pileup_down.root', 'MET filters_Dijet_mass_pileup_down.root', 'MET filters_Dijet_phi_pileup_down.root',
              'MET filters_number_of_jets_pileup_down.root', 'MET filters_number_of_low_pT_jets_pileup_down.root', 'MET filters_dphijj_pileup_down.root', 'MET filters_detajj_pileup_down.root', 'MET filters_detall_pileup_down.root', 'MET filters_dphill_pileup_down.root',
              'MET filters_Zboson_pt_pileup_down.root', 'MET filters_Zboson_mass_pileup_down.root', 'MET filters_Zboson_abseta_pileup_down.root', 'MET filters_Zboson_phi_pileup_down.root',
              'MET filters_DphiZ_l_jet_pileup_down.root', 'MET filters_DphiZ_s_jet_pileup_down.root', 'MET filters_DphiZ_jets_pileup_down.root']

files_btag_up_systematics = ['MET filters_pv_npvs_btagsf0_up.root', 'MET filters_leading_jet_QGL_btagsf0_up.root', 'MET filters_subleading_jet_QGL_btagsf0_up.root', 'MET filters_R_pT_hard_btagsf0_up.root', 'MET filters_z_star_btagsf0_up.root',
              'MET filters_leading_electron_pt_btagsf0_up.root', 'MET filters_leading_electron_abseta_btagsf0_up.root', 'MET filters_Delta_R_jets_l_e_btagsf0_up.root', 'MET filters_Delta_R_jets_s_e_btagsf0_up.root',
              'MET filters_subleading_electron_pt_btagsf0_up.root', 'MET filters_subleading_electron_abseta_btagsf0_up.root',
              'MET filters_leading_jet_pt_btagsf0_up.root', 'MET filters_leading_jet_abseta_btagsf0_up.root', 'MET filters_subleading_jet_pt_btagsf0_up.root', 'MET filters_subleading_jet_abseta_btagsf0_up.root',
              'MET filters_Dijet_pt_btagsf0_up.root', 'MET filters_Dijet_abseta_btagsf0_up.root', 'MET filters_Dijet_mass_btagsf0_up.root', 'MET filters_Dijet_phi_btagsf0_up.root',
              'MET filters_number_of_jets_btagsf0_up.root', 'MET filters_number_of_low_pT_jets_btagsf0_up.root', 'MET filters_dphijj_btagsf0_up.root', 'MET filters_detajj_btagsf0_up.root', 'MET filters_detall_btagsf0_up.root', 'MET filters_dphill_btagsf0_up.root',
              'MET filters_Zboson_pt_btagsf0_up.root', 'MET filters_Zboson_mass_btagsf0_up.root', 'MET filters_Zboson_abseta_btagsf0_up.root', 'MET filters_Zboson_phi_btagsf0_up.root',
              'MET filters_DphiZ_l_jet_btagsf0_up.root', 'MET filters_DphiZ_s_jet_btagsf0_up.root', 'MET filters_DphiZ_jets_btagsf0_up.root']

files_btag_down_systematics = ['MET filters_pv_npvs_btagsf0_down.root', 'MET filters_leading_jet_QGL_btagsf0_down.root', 'MET filters_subleading_jet_QGL_btagsf0_down.root', 'MET filters_R_pT_hard_btagsf0_down.root', 'MET filters_z_star_btagsf0_down.root',
              'MET filters_leading_electron_pt_btagsf0_down.root', 'MET filters_leading_electron_abseta_btagsf0_down.root', 'MET filters_Delta_R_jets_l_e_btagsf0_down.root', 'MET filters_Delta_R_jets_s_e_btagsf0_down.root',
              'MET filters_subleading_electron_pt_btagsf0_down.root', 'MET filters_subleading_electron_abseta_btagsf0_down.root',
              'MET filters_leading_jet_pt_btagsf0_down.root', 'MET filters_leading_jet_abseta_btagsf0_down.root', 'MET filters_subleading_jet_pt_btagsf0_down.root', 'MET filters_subleading_jet_abseta_btagsf0_down.root',
              'MET filters_Dijet_pt_btagsf0_down.root', 'MET filters_Dijet_abseta_btagsf0_down.root', 'MET filters_Dijet_mass_btagsf0_down.root', 'MET filters_Dijet_phi_btagsf0_down.root',
              'MET filters_number_of_jets_btagsf0_down.root', 'MET filters_number_of_low_pT_jets_btagsf0_down.root', 'MET filters_dphijj_btagsf0_down.root', 'MET filters_detajj_btagsf0_down.root', 'MET filters_detall_btagsf0_down.root', 'MET filters_dphill_btagsf0_down.root',
              'MET filters_Zboson_pt_btagsf0_down.root', 'MET filters_Zboson_mass_btagsf0_down.root', 'MET filters_Zboson_abseta_btagsf0_down.root', 'MET filters_Zboson_phi_btagsf0_down.root',
              'MET filters_DphiZ_l_jet_btagsf0_down.root', 'MET filters_DphiZ_s_jet_btagsf0_down.root', 'MET filters_DphiZ_jets_btagsf0_down.root']

files_JER_up_systematics = ['MET filters_pv_npvs_Jer_up.root', 'MET filters_leading_jet_QGL_Jer_up.root', 'MET filters_subleading_jet_QGL_Jer_up.root', 'MET filters_R_pT_hard_Jer_up.root', 'MET filters_z_star_Jer_up.root',
              'MET filters_leading_electron_pt_Jer_up.root', 'MET filters_leading_electron_abseta_Jer_up.root', 'MET filters_Delta_R_jets_l_e_Jer_up.root', 'MET filters_Delta_R_jets_s_e_Jer_up.root',
              'MET filters_subleading_electron_pt_Jer_up.root', 'MET filters_subleading_electron_abseta_Jer_up.root',
              'MET filters_leading_jet_pt_Jer_up.root', 'MET filters_leading_jet_abseta_Jer_up.root', 'MET filters_subleading_jet_pt_Jer_up.root', 'MET filters_subleading_jet_abseta_Jer_up.root',
              'MET filters_Dijet_pt_Jer_up.root', 'MET filters_Dijet_abseta_Jer_up.root', 'MET filters_Dijet_mass_Jer_up.root', 'MET filters_Dijet_phi_Jer_up.root',
              'MET filters_number_of_jets_Jer_up.root', 'MET filters_number_of_low_pT_jets_Jer_up.root', 'MET filters_dphijj_Jer_up.root', 'MET filters_detajj_Jer_up.root', 'MET filters_detall_Jer_up.root', 'MET filters_dphill_Jer_up.root',
              'MET filters_Zboson_pt_Jer_up.root', 'MET filters_Zboson_mass_Jer_up.root', 'MET filters_Zboson_abseta_Jer_up.root', 'MET filters_Zboson_phi_Jer_up.root',
              'MET filters_DphiZ_l_jet_Jer_up.root', 'MET filters_DphiZ_s_jet_Jer_up.root', 'MET filters_DphiZ_jets_Jer_up.root']

files_JER_down_systematics = ['MET filters_pv_npvs_Jer_down.root', 'MET filters_leading_jet_QGL_Jer_down.root', 'MET filters_subleading_jet_QGL_Jer_down.root', 'MET filters_R_pT_hard_Jer_down.root', 'MET filters_z_star_Jer_down.root',
              'MET filters_leading_electron_pt_Jer_down.root', 'MET filters_leading_electron_abseta_Jer_down.root', 'MET filters_Delta_R_jets_l_e_Jer_down.root', 'MET filters_Delta_R_jets_s_e_Jer_down.root',
              'MET filters_subleading_electron_pt_Jer_down.root', 'MET filters_subleading_electron_abseta_Jer_down.root',
              'MET filters_leading_jet_pt_Jer_down.root', 'MET filters_leading_jet_abseta_Jer_down.root', 'MET filters_subleading_jet_pt_Jer_down.root', 'MET filters_subleading_jet_abseta_Jer_down.root',
              'MET filters_Dijet_pt_Jer_down.root', 'MET filters_Dijet_abseta_Jer_down.root', 'MET filters_Dijet_mass_Jer_down.root', 'MET filters_Dijet_phi_Jer_down.root',
              'MET filters_number_of_jets_Jer_down.root', 'MET filters_number_of_low_pT_jets_Jer_down.root', 'MET filters_dphijj_Jer_down.root', 'MET filters_detajj_Jer_down.root', 'MET filters_detall_Jer_down.root', 'MET filters_dphill_Jer_down.root',
              'MET filters_Zboson_pt_Jer_down.root', 'MET filters_Zboson_mass_Jer_down.root', 'MET filters_Zboson_abseta_Jer_down.root', 'MET filters_Zboson_phi_Jer_down.root',
              'MET filters_DphiZ_l_jet_Jer_down.root', 'MET filters_DphiZ_s_jet_Jer_down.root', 'MET filters_DphiZ_jets_Jer_down.root']

files_junc_up_systematics = ['MET filters_pv_npvs_Junc_up.root', 'MET filters_leading_jet_QGL_Junc_up.root', 'MET filters_subleading_jet_QGL_Junc_up.root', 'MET filters_R_pT_hard_Junc_up.root', 'MET filters_z_star_Junc_up.root',
              'MET filters_leading_electron_pt_Junc_up.root', 'MET filters_leading_electron_abseta_Junc_up.root', 'MET filters_Delta_R_jets_l_e_Junc_up.root', 'MET filters_Delta_R_jets_s_e_Junc_up.root',
              'MET filters_subleading_electron_pt_Junc_up.root', 'MET filters_subleading_electron_abseta_Junc_up.root',
              'MET filters_leading_jet_pt_Junc_up.root', 'MET filters_leading_jet_abseta_Junc_up.root', 'MET filters_subleading_jet_pt_Junc_up.root', 'MET filters_subleading_jet_abseta_Junc_up.root',
              'MET filters_Dijet_pt_Junc_up.root', 'MET filters_Dijet_abseta_Junc_up.root', 'MET filters_Dijet_mass_Junc_up.root', 'MET filters_Dijet_phi_Junc_up.root',
              'MET filters_number_of_jets_Junc_up.root', 'MET filters_number_of_low_pT_jets_Junc_up.root', 'MET filters_dphijj_Junc_up.root', 'MET filters_detajj_Junc_up.root', 'MET filters_detall_Junc_up.root', 'MET filters_dphill_Junc_up.root',
              'MET filters_Zboson_pt_Junc_up.root', 'MET filters_Zboson_mass_Junc_up.root', 'MET filters_Zboson_abseta_Junc_up.root', 'MET filters_Zboson_phi_Junc_up.root',
              'MET filters_DphiZ_l_jet_Junc_up.root', 'MET filters_DphiZ_s_jet_Junc_up.root', 'MET filters_DphiZ_jets_Junc_up.root']

files_junc_down_systematics = ['MET filters_pv_npvs_Junc_down.root', 'MET filters_leading_jet_QGL_Junc_down.root', 'MET filters_subleading_jet_QGL_Junc_down.root', 'MET filters_R_pT_hard_Junc_down.root', 'MET filters_z_star_Junc_down.root',
              'MET filters_leading_electron_pt_Junc_down.root', 'MET filters_leading_electron_abseta_Junc_down.root', 'MET filters_Delta_R_jets_l_e_Junc_down.root', 'MET filters_Delta_R_jets_s_e_Junc_down.root',
              'MET filters_subleading_electron_pt_Junc_down.root', 'MET filters_subleading_electron_abseta_Junc_down.root',
              'MET filters_leading_jet_pt_Junc_down.root', 'MET filters_leading_jet_abseta_Junc_down.root', 'MET filters_subleading_jet_pt_Junc_down.root', 'MET filters_subleading_jet_abseta_Junc_down.root',
              'MET filters_Dijet_pt_Junc_down.root', 'MET filters_Dijet_abseta_Junc_down.root', 'MET filters_Dijet_mass_Junc_down.root', 'MET filters_Dijet_phi_Junc_down.root',
              'MET filters_number_of_jets_Junc_down.root', 'MET filters_number_of_low_pT_jets_Junc_down.root', 'MET filters_dphijj_Junc_down.root', 'MET filters_detajj_Junc_down.root', 'MET filters_detall_Junc_down.root', 'MET filters_dphill_Junc_down.root',
              'MET filters_Zboson_pt_Junc_down.root', 'MET filters_Zboson_mass_Junc_down.root', 'MET filters_Zboson_abseta_Junc_down.root', 'MET filters_Zboson_phi_Junc_down.root',
              'MET filters_DphiZ_l_jet_Junc_down.root', 'MET filters_DphiZ_s_jet_Junc_down.root', 'MET filters_DphiZ_jets_Junc_down.root']

files_btagsf0light_up_systematics = ['MET filters_pv_npvs_btagsf0light_up.root', 'MET filters_leading_jet_QGL_btagsf0light_up.root', 'MET filters_subleading_jet_QGL_btagsf0light_up.root', 'MET filters_R_pT_hard_btagsf0light_up.root', 'MET filters_z_star_btagsf0light_up.root',
              'MET filters_leading_electron_pt_btagsf0light_up.root', 'MET filters_leading_electron_abseta_btagsf0light_up.root', 'MET filters_Delta_R_jets_l_e_btagsf0light_up.root', 'MET filters_Delta_R_jets_s_e_btagsf0light_up.root',
              'MET filters_subleading_electron_pt_btagsf0light_up.root', 'MET filters_subleading_electron_abseta_btagsf0light_up.root',
              'MET filters_leading_jet_pt_btagsf0light_up.root', 'MET filters_leading_jet_abseta_btagsf0light_up.root', 'MET filters_subleading_jet_pt_btagsf0light_up.root', 'MET filters_subleading_jet_abseta_btagsf0light_up.root',
              'MET filters_Dijet_pt_btagsf0light_up.root', 'MET filters_Dijet_abseta_btagsf0light_up.root', 'MET filters_Dijet_mass_btagsf0light_up.root', 'MET filters_Dijet_phi_btagsf0light_up.root',
              'MET filters_number_of_jets_btagsf0light_up.root', 'MET filters_number_of_low_pT_jets_btagsf0light_up.root', 'MET filters_dphijj_btagsf0light_up.root', 'MET filters_detajj_btagsf0light_up.root', 'MET filters_detall_btagsf0light_up.root', 'MET filters_dphill_btagsf0light_up.root',
              'MET filters_Zboson_pt_btagsf0light_up.root', 'MET filters_Zboson_mass_btagsf0light_up.root', 'MET filters_Zboson_abseta_btagsf0light_up.root', 'MET filters_Zboson_phi_btagsf0light_up.root',
              'MET filters_DphiZ_l_jet_btagsf0light_up.root', 'MET filters_DphiZ_s_jet_btagsf0light_up.root', 'MET filters_DphiZ_jets_btagsf0light_up.root']
 
files_btagsf0light_down_systematics = ['MET filters_pv_npvs_btagsf0light_down.root', 'MET filters_leading_jet_QGL_btagsf0light_down.root', 'MET filters_subleading_jet_QGL_btagsf0light_down.root', 'MET filters_R_pT_hard_btagsf0light_down.root', 'MET filters_z_star_btagsf0light_down.root',
              'MET filters_leading_electron_pt_btagsf0light_down.root', 'MET filters_leading_electron_abseta_btagsf0light_down.root', 'MET filters_Delta_R_jets_l_e_btagsf0light_down.root', 'MET filters_Delta_R_jets_s_e_btagsf0light_down.root',
              'MET filters_subleading_electron_pt_btagsf0light_down.root', 'MET filters_subleading_electron_abseta_btagsf0light_down.root',
              'MET filters_leading_jet_pt_btagsf0light_down.root', 'MET filters_leading_jet_abseta_btagsf0light_down.root', 'MET filters_subleading_jet_pt_btagsf0light_down.root', 'MET filters_subleading_jet_abseta_btagsf0light_down.root',
              'MET filters_Dijet_pt_btagsf0light_down.root', 'MET filters_Dijet_abseta_btagsf0light_down.root', 'MET filters_Dijet_mass_btagsf0light_down.root', 'MET filters_Dijet_phi_btagsf0light_down.root',
              'MET filters_number_of_jets_btagsf0light_down.root', 'MET filters_number_of_low_pT_jets_btagsf0light_down.root', 'MET filters_dphijj_btagsf0light_down.root', 'MET filters_detajj_btagsf0light_down.root', 'MET filters_detall_btagsf0light_down.root', 'MET filters_dphill_btagsf0light_down.root',
              'MET filters_Zboson_pt_btagsf0light_down.root', 'MET filters_Zboson_mass_btagsf0light_down.root', 'MET filters_Zboson_abseta_btagsf0light_down.root', 'MET filters_Zboson_phi_btagsf0light_down.root',
              'MET filters_DphiZ_l_jet_btagsf0light_down.root', 'MET filters_DphiZ_s_jet_btagsf0light_down.root', 'MET filters_DphiZ_jets_btagsf0light_down.root']

files_electronsf0_up_systematics = ['MET filters_pv_npvs_electronsf0_up.root', 'MET filters_leading_jet_QGL_electronsf0_up.root', 'MET filters_subleading_jet_QGL_electronsf0_up.root', 'MET filters_R_pT_hard_electronsf0_up.root', 'MET filters_z_star_electronsf0_up.root',
              'MET filters_leading_electron_pt_electronsf0_up.root', 'MET filters_leading_electron_abseta_electronsf0_up.root', 'MET filters_Delta_R_jets_l_e_electronsf0_up.root', 'MET filters_Delta_R_jets_s_e_electronsf0_up.root',
              'MET filters_subleading_electron_pt_electronsf0_up.root', 'MET filters_subleading_electron_abseta_electronsf0_up.root',
              'MET filters_leading_jet_pt_electronsf0_up.root', 'MET filters_leading_jet_abseta_electronsf0_up.root', 'MET filters_subleading_jet_pt_electronsf0_up.root', 'MET filters_subleading_jet_abseta_electronsf0_up.root',
              'MET filters_Dijet_pt_electronsf0_up.root', 'MET filters_Dijet_abseta_electronsf0_up.root', 'MET filters_Dijet_mass_electronsf0_up.root', 'MET filters_Dijet_phi_electronsf0_up.root',
              'MET filters_number_of_jets_electronsf0_up.root', 'MET filters_number_of_low_pT_jets_electronsf0_up.root', 'MET filters_dphijj_electronsf0_up.root', 'MET filters_detajj_electronsf0_up.root', 'MET filters_detall_electronsf0_up.root', 'MET filters_dphill_electronsf0_up.root',
              'MET filters_Zboson_pt_electronsf0_up.root', 'MET filters_Zboson_mass_electronsf0_up.root', 'MET filters_Zboson_abseta_electronsf0_up.root', 'MET filters_Zboson_phi_electronsf0_up.root',
              'MET filters_DphiZ_l_jet_electronsf0_up.root', 'MET filters_DphiZ_s_jet_electronsf0_up.root', 'MET filters_DphiZ_jets_electronsf0_up.root'] 

files_electronsf0_down_systematics = ['MET filters_pv_npvs_electronsf0_down.root', 'MET filters_leading_jet_QGL_electronsf0_down.root', 'MET filters_subleading_jet_QGL_electronsf0_down.root', 'MET filters_R_pT_hard_electronsf0_down.root', 'MET filters_z_star_electronsf0_down.root',
              'MET filters_leading_electron_pt_electronsf0_down.root', 'MET filters_leading_electron_abseta_electronsf0_down.root', 'MET filters_Delta_R_jets_l_e_electronsf0_down.root', 'MET filters_Delta_R_jets_s_e_electronsf0_down.root',
              'MET filters_subleading_electron_pt_electronsf0_down.root', 'MET filters_subleading_electron_abseta_electronsf0_down.root',
              'MET filters_leading_jet_pt_electronsf0_down.root', 'MET filters_leading_jet_abseta_electronsf0_down.root', 'MET filters_subleading_jet_pt_electronsf0_down.root', 'MET filters_subleading_jet_abseta_electronsf0_down.root',
              'MET filters_Dijet_pt_electronsf0_down.root', 'MET filters_Dijet_abseta_electronsf0_down.root', 'MET filters_Dijet_mass_electronsf0_down.root', 'MET filters_Dijet_phi_electronsf0_down.root',
              'MET filters_number_of_jets_electronsf0_down.root', 'MET filters_number_of_low_pT_jets_electronsf0_down.root', 'MET filters_dphijj_electronsf0_down.root', 'MET filters_detajj_electronsf0_down.root', 'MET filters_detall_electronsf0_down.root', 'MET filters_dphill_electronsf0_down.root',
              'MET filters_Zboson_pt_electronsf0_down.root', 'MET filters_Zboson_mass_electronsf0_down.root', 'MET filters_Zboson_abseta_electronsf0_down.root', 'MET filters_Zboson_phi_electronsf0_down.root',
              'MET filters_DphiZ_l_jet_electronsf0_down.root', 'MET filters_DphiZ_s_jet_electronsf0_down.root', 'MET filters_DphiZ_jets_electronsf0_down.root']

files_electronsf1_up_systematics = ['MET filters_pv_npvs_electronsf1_up.root', 'MET filters_leading_jet_QGL_electronsf1_up.root', 'MET filters_subleading_jet_QGL_electronsf1_up.root', 'MET filters_R_pT_hard_electronsf1_up.root', 'MET filters_z_star_electronsf1_up.root',
              'MET filters_leading_electron_pt_electronsf1_up.root', 'MET filters_leading_electron_abseta_electronsf1_up.root', 'MET filters_Delta_R_jets_l_e_electronsf1_up.root', 'MET filters_Delta_R_jets_s_e_electronsf1_up.root',
              'MET filters_subleading_electron_pt_electronsf1_up.root', 'MET filters_subleading_electron_abseta_electronsf1_up.root',
              'MET filters_leading_jet_pt_electronsf1_up.root', 'MET filters_leading_jet_abseta_electronsf1_up.root', 'MET filters_subleading_jet_pt_electronsf1_up.root', 'MET filters_subleading_jet_abseta_electronsf1_up.root',
              'MET filters_Dijet_pt_electronsf1_up.root', 'MET filters_Dijet_abseta_electronsf1_up.root', 'MET filters_Dijet_mass_electronsf1_up.root', 'MET filters_Dijet_phi_electronsf1_up.root',
              'MET filters_number_of_jets_electronsf1_up.root', 'MET filters_number_of_low_pT_jets_electronsf1_up.root', 'MET filters_dphijj_electronsf1_up.root', 'MET filters_detajj_electronsf1_up.root', 'MET filters_detall_electronsf1_up.root', 'MET filters_dphill_electronsf1_up.root',
              'MET filters_Zboson_pt_electronsf1_up.root', 'MET filters_Zboson_mass_electronsf1_up.root', 'MET filters_Zboson_abseta_electronsf1_up.root', 'MET filters_Zboson_phi_electronsf1_up.root',
              'MET filters_DphiZ_l_jet_electronsf1_up.root', 'MET filters_DphiZ_s_jet_electronsf1_up.root', 'MET filters_DphiZ_jets_electronsf1_up.root']

files_electronsf1_down_systematics = ['MET filters_pv_npvs_electronsf1_down.root', 'MET filters_leading_jet_QGL_electronsf1_down.root', 'MET filters_subleading_jet_QGL_electronsf1_down.root', 'MET filters_R_pT_hard_electronsf1_down.root', 'MET filters_z_star_electronsf1_down.root',
              'MET filters_leading_electron_pt_electronsf1_down.root', 'MET filters_leading_electron_abseta_electronsf1_down.root', 'MET filters_Delta_R_jets_l_e_electronsf1_down.root', 'MET filters_Delta_R_jets_s_e_electronsf1_down.root',
              'MET filters_subleading_electron_pt_electronsf1_down.root', 'MET filters_subleading_electron_abseta_electronsf1_down.root',
              'MET filters_leading_jet_pt_electronsf1_down.root', 'MET filters_leading_jet_abseta_electronsf1_down.root', 'MET filters_subleading_jet_pt_electronsf1_down.root', 'MET filters_subleading_jet_abseta_electronsf1_down.root',
              'MET filters_Dijet_pt_electronsf1_down.root', 'MET filters_Dijet_abseta_electronsf1_down.root', 'MET filters_Dijet_mass_electronsf1_down.root', 'MET filters_Dijet_phi_electronsf1_down.root',
              'MET filters_number_of_jets_electronsf1_down.root', 'MET filters_number_of_low_pT_jets_electronsf1_down.root', 'MET filters_dphijj_electronsf1_down.root', 'MET filters_detajj_electronsf1_down.root', 'MET filters_detall_electronsf1_down.root', 'MET filters_dphill_electronsf1_down.root',
              'MET filters_Zboson_pt_electronsf1_down.root', 'MET filters_Zboson_mass_electronsf1_down.root', 'MET filters_Zboson_abseta_electronsf1_down.root', 'MET filters_Zboson_phi_electronsf1_down.root',
              'MET filters_DphiZ_l_jet_electronsf1_down.root', 'MET filters_DphiZ_s_jet_electronsf1_down.root', 'MET filters_DphiZ_jets_electronsf1_down.root']

files_electronsf2_up_systematics = ['MET filters_pv_npvs_electronsf2_up.root', 'MET filters_leading_jet_QGL_electronsf2_up.root', 'MET filters_subleading_jet_QGL_electronsf2_up.root', 'MET filters_R_pT_hard_electronsf2_up.root', 'MET filters_z_star_electronsf2_up.root',
              'MET filters_leading_electron_pt_electronsf2_up.root', 'MET filters_leading_electron_abseta_electronsf2_up.root', 'MET filters_Delta_R_jets_l_e_electronsf2_up.root', 'MET filters_Delta_R_jets_s_e_electronsf2_up.root',
              'MET filters_subleading_electron_pt_electronsf2_up.root', 'MET filters_subleading_electron_abseta_electronsf2_up.root',
              'MET filters_leading_jet_pt_electronsf2_up.root', 'MET filters_leading_jet_abseta_electronsf2_up.root', 'MET filters_subleading_jet_pt_electronsf2_up.root', 'MET filters_subleading_jet_abseta_electronsf2_up.root',
              'MET filters_Dijet_pt_electronsf2_up.root', 'MET filters_Dijet_abseta_electronsf2_up.root', 'MET filters_Dijet_mass_electronsf2_up.root', 'MET filters_Dijet_phi_electronsf2_up.root',
              'MET filters_number_of_jets_electronsf2_up.root', 'MET filters_number_of_low_pT_jets_electronsf2_up.root', 'MET filters_dphijj_electronsf2_up.root', 'MET filters_detajj_electronsf2_up.root', 'MET filters_detall_electronsf2_up.root', 'MET filters_dphill_electronsf2_up.root',
              'MET filters_Zboson_pt_electronsf2_up.root', 'MET filters_Zboson_mass_electronsf2_up.root', 'MET filters_Zboson_abseta_electronsf2_up.root', 'MET filters_Zboson_phi_electronsf2_up.root',
              'MET filters_DphiZ_l_jet_electronsf2_up.root', 'MET filters_DphiZ_s_jet_electronsf2_up.root', 'MET filters_DphiZ_jets_electronsf2_up.root']

files_electronsf2_down_systematics = ['MET filters_pv_npvs_electronsf2_down.root', 'MET filters_leading_jet_QGL_electronsf2_down.root', 'MET filters_subleading_jet_QGL_electronsf2_down.root', 'MET filters_R_pT_hard_electronsf2_down.root', 'MET filters_z_star_electronsf2_down.root',
              'MET filters_leading_electron_pt_electronsf2_down.root', 'MET filters_leading_electron_abseta_electronsf2_down.root', 'MET filters_Delta_R_jets_l_e_electronsf2_down.root', 'MET filters_Delta_R_jets_s_e_electronsf2_down.root',
              'MET filters_subleading_electron_pt_electronsf2_down.root', 'MET filters_subleading_electron_abseta_electronsf2_down.root',
              'MET filters_leading_jet_pt_electronsf2_down.root', 'MET filters_leading_jet_abseta_electronsf2_down.root', 'MET filters_subleading_jet_pt_electronsf2_down.root', 'MET filters_subleading_jet_abseta_electronsf2_down.root',
              'MET filters_Dijet_pt_electronsf2_down.root', 'MET filters_Dijet_abseta_electronsf2_down.root', 'MET filters_Dijet_mass_electronsf2_down.root', 'MET filters_Dijet_phi_electronsf2_down.root',
              'MET filters_number_of_jets_electronsf2_down.root', 'MET filters_number_of_low_pT_jets_electronsf2_down.root', 'MET filters_dphijj_electronsf2_down.root', 'MET filters_detajj_electronsf2_down.root', 'MET filters_detall_electronsf2_down.root', 'MET filters_dphill_electronsf2_down.root',
              'MET filters_Zboson_pt_electronsf2_down.root', 'MET filters_Zboson_mass_electronsf2_down.root', 'MET filters_Zboson_abseta_electronsf2_down.root', 'MET filters_Zboson_phi_electronsf2_down.root',
              'MET filters_DphiZ_l_jet_electronsf2_down.root', 'MET filters_DphiZ_s_jet_electronsf2_down.root', 'MET filters_DphiZ_jets_electronsf2_down.root']


#file_names = ['MET filters_Zboson_mass.root']
#file_names = ['Dijet_cut_dphijj.root', 'Dijet_cut_dphill.root', 'Dijet_cut_DphiZ_l_jet.root', 'Dijet_cut_DphiZ_s_jet.root', 'Dijet_cut_DphiZ_jets.root']
#file_names = ['Dijet_cut_leading_jet_QGL.root', 'Dijet_cut_subleading_jet_QGL.root']
#file_names = ['Dijet_cut_Delta_R_jets_l_mu.root', 'Dijet_cut_Delta_R_jets_s_mu.root', 'Dijet_cut_Zboson_mass.root', 'Dijet_cut_pv_npvs.root', 'PV_pv_npvs.root', 'Dijet_cut_Dijet_pt.root']
"""
samples = ['EWK_LLJJ_MLL-50_MJJ-120_TuneCP5_13TeV-madgraph-pythia8_dipole', 'WW_TuneCP5_13TeV-pythia8', 'WZ_TuneCP5_13TeV-pythia8', 'ZZ_TuneCP5_13TeV-pythia8',
           'WWTo2L2Nu_TuneCP5_13TeV-powheg-pythia8', 'WGToLNuG_TuneCP5_13TeV-madgraphMLM-pythia8', 'ZGToLLG_01J_5f_TuneCP5_13TeV-amcatnloFXFX-pythia8', 'ZZZ_TuneCP5_13TeV-amcatnlo-pythia8',
           'WZZ_TuneCP5_13TeV-amcatnlo-pythia8', 'WWZ_4F_TuneCP5_13TeV-amcatnlo-pythia8', 
           'ST_tW_top_5f_inclusiveDecays_TuneCP5_13TeV-powheg-pythia8', 'ST_t-channel_top_4f_InclusiveDecays_TuneCP5_13TeV-powheg-madspin-pythia8',
           'ST_tW_antitop_5f_inclusiveDecays_TuneCP5_13TeV-powheg-pythia8','ST_t-channel_antitop_4f_InclusiveDecays_TuneCP5_13TeV-powheg-madspin-pythia8',
           'ST_s-channel_4f_leptonDecays_TuneCP5_13TeV-amcatnlo-pythia8', 'TTTo2L2Nu_TuneCP5_13TeV-powheg-pythia8','TTToSemiLeptonic_TuneCP5_13TeV-powheg-pythia8', 
           'DYJetsToLL_M-50_TuneCP5_13TeV-amcatnloFXFX-pythia8', 'SingleMuon' ]

samples = ['EWK_LLJJ_MLL-50_MJJ-120_TuneCP5_13TeV-madgraph-pythia8_dipole', 'WW_TuneCP5_13TeV-pythia8', 'WZ_TuneCP5_13TeV-pythia8', 'ZZ_TuneCP5_13TeV-pythia8',
           'ST_tW_top_5f_inclusiveDecays_TuneCP5_13TeV-powheg-pythia8', 'ST_t-channel_top_4f_InclusiveDecays_TuneCP5_13TeV-powheg-madspin-pythia8',
           'ST_tW_antitop_5f_inclusiveDecays_TuneCP5_13TeV-powheg-pythia8','ST_t-channel_antitop_4f_InclusiveDecays_TuneCP5_13TeV-powheg-madspin-pythia8',
           'ST_s-channel_4f_leptonDecays_TuneCP5_13TeV-amcatnlo-pythia8', 'TTTo2L2Nu_TuneCP5_13TeV-powheg-pythia8','TTToSemiLeptonic_TuneCP5_13TeV-powheg-pythia8', 
           'DYJetsToLL_M-50_TuneCP5_13TeV-amcatnloFXFX-pythia8', 'SingleMuon' ]
"""
samples = ['EWK_LLJJ_MLL-50_MJJ-120_TuneCP5_13TeV-madgraph-pythia8_dipole', 'WW_TuneCP5_13TeV-pythia8', 'WZ_TuneCP5_13TeV-pythia8', 'ZZ_TuneCP5_13TeV-pythia8',
           'ST_tW_top_5f_inclusiveDecays_TuneCP5_13TeV-powheg-pythia8', 'ST_t-channel_top_4f_InclusiveDecays_TuneCP5_13TeV-powheg-madspin-pythia8',
           'ST_tW_antitop_5f_inclusiveDecays_TuneCP5_13TeV-powheg-pythia8','ST_t-channel_antitop_4f_InclusiveDecays_TuneCP5_13TeV-powheg-madspin-pythia8',
           'ST_s-channel_4f_leptonDecays_TuneCP5_13TeV-amcatnlo-pythia8', 'TTTo2L2Nu_TuneCP5_13TeV-powheg-pythia8','TTToSemiLeptonic_TuneCP5_13TeV-powheg-pythia8',
           'DYJetsToLL_M-50_TuneCP5_13TeV-amcatnloFXFX-pythia8', 'EGamma' ]

for(file_name, file_PU_up, file_PU_down, file_btag_up, file_btag_down, file_JER_up, file_JER_down, file_JUNC_up, file_JUNC_down, file_btagsf0light_up, file_btagsf0light_down, file_electronsf0_up, file_electronsf0_down, file_electronsf1_up, file_electronsf1_down, file_electronsf2_up, file_electronsf2_down) in zip(file_names, 
    files_PU_up_systematics, files_PU_down_systematics, files_btag_up_systematics, files_btag_down_systematics, 
    files_JER_up_systematics, files_JER_down_systematics, files_junc_up_systematics, 
    files_junc_down_systematics, files_btagsf0light_up_systematics, files_btagsf0light_down_systematics, files_electronsf0_up_systematics, files_electronsf0_down_systematics, 
    files_electronsf1_up_systematics, files_electronsf1_down_systematics, files_electronsf2_up_systematics, files_electronsf2_down_systematics): 

    mc_histograms = [None]*(len(samples)-1)
    PU_up_hist = [None]*(len(samples)-1)
    PU_down_hist = [None]*(len(samples)-1)
    btag_up_hist = [None]*(len(samples)-1)
    btag_down_hist = [None]*(len(samples)-1)
    JER_up_hist = [None]*(len(samples)-1)
    JER_down_hist = [None]*(len(samples)-1)
    JUNC_up_hist = [None]*(len(samples)-1)
    JUNC_down_hist = [None]*(len(samples)-1)
    btagsf0light_up_hist = [None]*(len(samples)-1)
    btagsf0light_down_hist = [None]*(len(samples)-1)
    electronsf0_up_hist = [None]*(len(samples)-1)
    electronsf0_down_hist = [None]*(len(samples)-1)
    electronsf1_up_hist = [None]*(len(samples)-1)
    electronsf1_down_hist = [None]*(len(samples)-1)
    electronsf2_up_hist = [None]*(len(samples)-1)
    electronsf2_down_hist = [None]*(len(samples)-1)

    for i in range(len(mc_histograms)):
        open_file = uproot.open(inputdir + file_name)
        open_PU_up_file = uproot.open(inputdir + file_PU_up)
        open_PU_down_file = uproot.open(inputdir + file_PU_down)
        open_btag_up_file = uproot.open(inputdir + file_btag_up)
        open_btag_down_file = uproot.open(inputdir + file_btag_down)
        open_JER_up_file = uproot.open(inputdir + file_JER_up)
        open_JER_down_file = uproot.open(inputdir + file_JER_down)
        open_JUNC_up_file = uproot.open(inputdir + file_JUNC_up)
        open_JUNC_down_file = uproot.open(inputdir + file_JUNC_down)
        open_btagsf0light_up_file = uproot.open(inputdir + file_btagsf0light_up)
        open_btagsf0light_down_file = uproot.open(inputdir + file_btagsf0light_down)
        open_electronsf0_up_file = uproot.open(inputdir + file_electronsf0_up)
        open_electronsf0_down_file = uproot.open(inputdir + file_electronsf0_down)
        open_electronsf1_up_file = uproot.open(inputdir + file_electronsf1_up)
        open_electronsf1_down_file = uproot.open(inputdir + file_electronsf1_down)
        open_electronsf2_up_file = uproot.open(inputdir + file_electronsf2_up)
        open_electronsf2_down_file = uproot.open(inputdir + file_electronsf2_down)
        data_histogram = open_file['EGamma'].to_hist()

        mc_histograms[i] = open_file[samples[i]].to_hist()*sf
        DY_hist = open_file['DYJetsToLL_M-50_TuneCP5_13TeV-amcatnloFXFX-pythia8'].to_hist()*sf
        EWK_hist = open_file['EWK_LLJJ_MLL-50_MJJ-120_TuneCP5_13TeV-madgraph-pythia8_dipole'].to_hist()*sf
        #print(EWK_hist.variances())
 
        PU_up_hist[i] = open_PU_up_file[samples[i]].to_hist()*sf
        PU_down_hist[i] = open_PU_down_file[samples[i]].to_hist()*sf
        btag_up_hist[i] = open_btag_up_file[samples[i]].to_hist()*sf
        btag_down_hist[i] = open_btag_down_file[samples[i]].to_hist()*sf
        JER_up_hist[i] = open_JER_up_file[samples[i]].to_hist()*sf
        JER_down_hist[i] = open_JER_down_file[samples[i]].to_hist()*sf
        JUNC_up_hist[i] = open_JUNC_up_file[samples[i]].to_hist()*sf
        JUNC_down_hist[i] = open_JUNC_down_file[samples[i]].to_hist()*sf
        btagsf0light_up_hist[i] = open_btagsf0light_up_file[samples[i]].to_hist()*sf
        btagsf0light_down_hist[i] = open_btagsf0light_down_file[samples[i]].to_hist()*sf
        electronsf0_up_hist[i] = open_electronsf0_up_file[samples[i]].to_hist()*sf
        electronsf0_down_hist[i] = open_electronsf0_down_file[samples[i]].to_hist()*sf
        electronsf1_up_hist[i] = open_electronsf1_up_file[samples[i]].to_hist()*sf
        electronsf1_down_hist[i] = open_electronsf1_down_file[samples[i]].to_hist()*sf
        electronsf2_up_hist[i] = open_electronsf2_up_file[samples[i]].to_hist()*sf
        electronsf2_down_hist[i] = open_electronsf2_down_file[samples[i]].to_hist()*sf
  
        scaled_mc_signal= open_file['EWK_LLJJ_MLL-50_MJJ-120_TuneCP5_13TeV-madgraph-pythia8_dipole'].to_hist()*sf*100   
    
    PU_up, PU_down = systematics(mc_histograms, PU_up_hist, PU_down_hist)
    btag_up, btag_down = systematics(mc_histograms, btag_up_hist, btag_down_hist)
    JER_up, JER_down = systematics(mc_histograms, JER_up_hist, JER_down_hist)
    JUNC_up, JUNC_down = systematics(mc_histograms, JUNC_up_hist, JUNC_down_hist)
    btagsf0light_up, btagsf0light_down = systematics(mc_histograms, btagsf0light_up_hist, btagsf0light_down_hist)
    btag_total_up, btag_total_down = total_systematics([btag_up, btagsf0light_up], [btag_down, btagsf0light_down])
    electronsf0_up, electronsf0_down = systematics(mc_histograms, electronsf0_up_hist, electronsf0_down_hist)
    electronsf0_up, electronsf0_down = systematics(mc_histograms, electronsf0_up_hist, electronsf0_down_hist)
    electronsf1_up, electronsf1_down = systematics(mc_histograms, electronsf1_up_hist, electronsf1_down_hist)
    electronsf1_up, electronsf1_down = systematics(mc_histograms, electronsf1_up_hist, electronsf1_down_hist)
    electronsf2_up, electronsf2_down = systematics(mc_histograms, electronsf2_up_hist, electronsf2_down_hist)
    electronsf2_up, electronsf2_down = systematics(mc_histograms, electronsf2_up_hist, electronsf2_down_hist)
    electron_total_up, electron_total_down = total_systematics([electronsf0_up, electronsf1_up, electronsf2_up], [electronsf0_down, electronsf1_down, electronsf2_down])

    total_up_sys, total_down_sys = total_systematics([PU_up, btag_up, JER_up, JUNC_up, btagsf0light_up, electronsf0_up, electronsf1_up, electronsf2_up],
    [PU_down, btag_down, JER_down, JUNC_down, btagsf0light_down, electronsf0_down, electronsf1_down, electronsf2_down])
    yrr = [total_up_sys, total_down_sys]

    # Set style
    plt.style.use(hep.style.ROOT)
    plt.rcParams.update({'font.size': 14})
    plt.rcParams.update({'axes.titlesize':20})
    plt.rcParams.update({'axes.labelsize':20})
   
    # Set stack and ratio plot
    # Set stack and ratio plot
    fig, ax = plt.subplots(2, 1, gridspec_kw={'height_ratios': [2, 1], 'hspace':0}, sharex=True)

    # Make stack plot in upper axis
    stack_axis = ax[0]
    color = ['purple', 'thistle', 'thistle', 'thistle',
             'darkslateblue', 'darkslateblue', 'darkslateblue', 'darkslateblue', 'darkslateblue', 'mediumvioletred', 'mediumvioletred', 'plum']
    legend_names = lenged_names = [r'EWK $Z\rightarrow e^{+} e^{-}$', 'VV', '_nolegend_', '_nolegend_',
                                   'Single top','_nolegend_', '_nolegend_', '_nolegend_', '_nolegend_',
                                   r'TT', '_nolegend_', r'DY $Z\rightarrow e^{+}e^{-}$']
    # Blinding
    wratio_sig, bins_sig, percentage = make_signif(EWK_hist, DY_hist)
    print(EWK_hist.values())
    #print(scaled_mc_signal)
    
    #wratio, bins, werr = make_ratio(data_histogram, sum_mc_histos)
    #bin_array = (bins[:-1] + bins[1:]) / 2
    ones = np.ones(len(scaled_mc_signal.values()))
    ####################### MASKED PLOTTING ##################
    
    for w in wratio_sig:
        if w >2.2 :
            masked_val_pos = (np.where(wratio_sig == w)[0])
            print("masked_val_pos: ", masked_val_pos)
            print(type(masked_val_pos))
            masked_val_pos = masked_val_pos.item(0)
            
            yrr[0][masked_val_pos] = 0
            yrr[1][masked_val_pos] = 0
            for i in range(len(mc_histograms)):
               mc_histograms[i].values()[masked_val_pos] = 0
               mc_histograms[i].variances()[masked_val_pos] = 0
               #print(mc_histograms[i])
               # np.delete(mc_histograms[i].values(), mc_histograms[i].values()[masked_val_pos])
            scaled_mc_signal.values()[masked_val_pos] = 0
            scaled_mc_signal.variances()[masked_val_pos] = 0
            # np.delete(scaled_mc_signal.values(), scaled_mc_signal.values()[masked_val_pos])
            data_histogram.values()[masked_val_pos] = 0
            data_histogram.variances()[masked_val_pos] = 0
            #np.delete(data_histogram.values(), data_histogram.values()[masked_val_pos])
  
            ones[masked_val_pos] = 0
    
            if file_name in  file_names_001:
                step = 0.01
            elif file_name in  file_names_01:
                step = 0.1
            elif file_name in  file_names_05:
                step = 0.5
            elif file_name in  file_names_10:
                step = 10
            elif file_name in  file_names_50:
                step = 50
            else:
                step = 0.02
            stack_axis.vlines(np.arange(bins_sig[masked_val_pos], bins_sig[masked_val_pos+1], step), 0, 1e4)     
    #################################################################

    hep.histplot(mc_histograms, color = color, ax=stack_axis, binwnorm=1., stack=True, histtype='fill', label = legend_names)
    hep.histplot(scaled_mc_signal, color = 'slategray', ax=stack_axis, binwnorm=1., stack=False, histtype='step', label = r'EWK $Z\rightarrow e^{+}e^{-} * 10^2$')
    hep.histplot(data_histogram, ax=stack_axis, binwnorm=1., stack=False, histtype='errorbar', color='black', label='Data')
    stack_axis.legend()
    stack_axis.set_yscale('log')
    stack_axis.set_ylabel(r'Events/bin')
    
    # Ratio axis
    ratio_axis = ax[1]
    ratio_axis.axhline(y=1, color='black')
    # Sum stack
    sum_mc_histos = sum(mc_histograms)
    print('total mc value events (integral)', sum(sum_mc_histos.values()))
    #print(sum_mc_histos.values())

    # Calculate ratio Data/MC
    wratio, bins, werr = make_ratio(data_histogram, sum_mc_histos)
    #ones = np.ones(len(wratio))
    
    print('ratio')
    print(wratio)
    print('old total ratio before  SF: ', sum(np.nan_to_num(wratio))/sf)

    #wratio, bins, werr = make_ratio_norm_SF(data_histogram, sum_mc_histos, sf)
    print('ratio')
    print(wratio)
    print('new total ratio after SF: ', sum(np.nan_to_num(wratio)))

    wratioMC, binsMC, werrMC = make_ratio(sum_mc_histos, sum_mc_histos)
    total_sys_up, total_sys_down = systematics_ratio(total_up_sys, total_down_sys, sum_mc_histos.values())
    hep.histplot(np.nan_to_num(wratio), bins, yerr = (werr), stack=False, histtype='errorbar', color='black', ax = ratio_axis, label = 'Statistical data uncertainty')
    hep.histplot(ones, bins, yerr = werrMC, stack=False, histtype='errorbar', marker = 'None', color='black', ax = ratio_axis, label = 'Statistical MC uncertainty')
    ratio_axis.fill_between((bins[:-1] + bins[1:]) / 2, total_sys_up, total_sys_down, color='paleturquoise', label='Syst. Uncertainty')
    ratio_axis.legend()

    if file_name == 'MET filters_Delta_R_jets_l_e.root':
        ratio_axis.set_xlabel('$\\Delta R_{e_{L} , jets}$')
    if file_name == 'MET filters_Delta_R_jets_s_e.root':
        ratio_axis.set_xlabel('$\\Delta R_{e_{S} , jets}$')
    if file_name == 'MET filters_pv_npvsGood.root':
        ratio_axis.set_xlabel('Number of good primary vertices')
    if file_name == 'MET filters_leading_electron_pt.root':
        ratio_axis.set_xlabel('Leading electron transverse momentum: $p_{T}$ [GeV]')   
    elif file_name == 'MET filters_leading_electron_abseta.root':
        ratio_axis.set_xlabel('Leading electron $|\eta|$')
    elif file_name == 'MET filters_Delta_R_l_e_l_jet.root':
        ratio_axis.set_xlabel(' $\\Delta R_{e_{L} , jet_{L}}$')
    elif file_name == 'MET filters_Delta_R_l_e_s_jet.root':
        ratio_axis.set_xlabel('$\\Delta R_{e_{L} , jet_{S}}$')
    elif file_name == 'MET filters_Delta_R_l_jet.root':
        ratio_axis.set_xlabel(' $\\Delta R_{j_{L} , e e}$')
    elif file_name == 'MET filters_Delta_R_s_jet.root':
        ratio_axis.set_xlabel('$\\Delta R_{j_{S} , e e}$')
    elif file_name == 'MET filters_Delta_phi_l_e_l_jet.root':
        ratio_axis.set_xlabel(' $\\Delta \\phi_{e_{L}, jet_{L}}$')
    elif file_name == 'MET filters_Delta_phi_l_e_s_jet.root':
        ratio_axis.set_xlabel('$\\Delta \\phi_{e_{L}, jet_{S}}$')
    elif file_name == 'MET filters_subleading_electron_pt.root':
        ratio_axis.set_xlabel('Subleading electron transverse momentum: $p_{T}$ [GeV]')
    elif file_name == 'MET filters_subleading_electron_abseta.root':
        ratio_axis.set_xlabel('Subleading electron $|\eta|$')
    elif file_name == 'MET filters_leading_jet_pt.root':
        ratio_axis.set_xlabel('Leading jet transverse momentum: $p_{T}$ [GeV]')
    elif file_name == 'MET filters_Delta_R_s_e_l_jet.root':
        ratio_axis.set_xlabel(' $\\Delta R_{e_{S} , jet_{L}}$')
    elif file_name == 'MET filters_Delta_R_s_e_s_jet.root':
        ratio_axis.set_xlabel('$\\Delta R_{e_{S} , jet_{S}}$')
    elif file_name == 'MET filters_Delta_phi_s_e_l_jet.root':
        ratio_axis.set_xlabel(' $\\Delta \\phi_{e_{S}, jet_{L}}$')
    elif file_name == 'MET filters_Delta_phi_s_e_s_jet.root':
        ratio_axis.set_xlabel('$\\Delta \\phi_{e_{S}, jet_{S}}$')
    elif file_name == 'MET filters_leading_jet_abseta.root':
        ratio_axis.set_xlabel('Leading jet $|\eta|$')
    elif file_name == 'MET filters_subleading_jet_pt.root':
        ratio_axis.set_xlabel('Subleading jet transverse momentum: $p_{T}$ [GeV]')
    elif file_name == 'MET filters_subleading_jet_abseta.root':
        ratio_axis.set_xlabel('Subleading jet $|\eta|$')
    elif (file_name == 'MET filters_mjj.root') or (file_name == 'MET filters_Dijet_mass.root'):
        ratio_axis.set_xlabel('Dijet mass: $m_{jj}$ [GeV]', fontsize = 20)
    elif (file_name == 'MET filters_pTjj.root') or (file_name == 'MET filters_Dijet_pt.root'):
        ratio_axis.set_xlabel('$p_{Tjj}$ [GeV]', fontsize = 20)
    elif file_name == 'MET filters_Dijet_phi.root':
        ratio_axis.set_xlabel(' $\\phi_{jj}$ ', fontsize = 20)
    elif file_name == 'MET filters_Dijet_abseta.root':
        ratio_axis.set_xlabel(' $ | \\eta_{jj} | $ ', fontsize = 20)
    elif file_name == 'MET filters_Zboson_mass.root':
        ratio_axis.set_xlabel('Z boson mass: $m_{l^{+}l^{-}}$ [GeV]', fontsize = 20)
    elif file_name == 'MET filters_Zboson_pt.root':
        ratio_axis.set_xlabel('Z boson transverse momentum: $p_{T}$ [GeV]', fontsize = 20)
    elif file_name == 'MET filters_Zboson_abseta.root':
        ratio_axis.set_xlabel('$|\\eta|_{l^{+}l^{-}}$ ', fontsize = 20)
    elif file_name == 'MET filters_Zboson_phi.root':
        ratio_axis.set_xlabel('$|\\phi|_{l^{+}l^{-}}$ ', fontsize = 20)
    elif file_name == 'MET filters_number_of_jets.root':
        ratio_axis.set_xlabel('Number of jets')
    elif file_name == 'MET filters_number_of_low_pT_jets.root':
        ratio_axis.set_xlabel('Number of low pT jets')
    elif file_name == 'MET filters_DphiZ_jets.root':
        ratio_axis.set_xlabel(' $\\Delta \\phi_{Z, jj}$ ', fontsize = 20)
    elif file_name == 'MET filters_DphiZ_l_jet.root':
        ratio_axis.set_xlabel(' $\\Delta \\phi_{Z, j_{L}}$ ', fontsize = 20)
    elif file_name == 'MET filters_DphiZ_s_jet.root':
        ratio_axis.set_xlabel(' $\\Delta \\phi_{Z, j_{S}}$ ', fontsize = 20)
    elif file_name == 'MET filters_Delta_phi_l_jet_ee.root':
        ratio_axis.set_xlabel(' $\\Delta \\phi_{j_{L}, e e}$ ', fontsize = 20)
    elif file_name == 'MET filters_Delta_phi_s_jet_ee.root':
        ratio_axis.set_xlabel(' $\\Delta \\phi_{j_{S}, e e}$ ', fontsize = 20)
    elif file_name == 'MET filters_dphill.root':
        ratio_axis.set_xlabel(' $\\Delta \\phi_{e e}$ ', fontsize = 20)
    elif file_name == 'MET filters_dphijj.root':
        ratio_axis.set_xlabel(' $\\Delta \\phi_{j_{L} j_{S}}$ ', fontsize = 20)
    elif file_name == 'MET filters_detall.root':
        ratio_axis.set_xlabel(' $\\Delta \\eta_{e e}$ ', fontsize = 20)
    elif file_name == 'MET filters_detajj.root':
        ratio_axis.set_xlabel(' $| \\Delta \\eta_{j_{L} j_{S}} |$ ',fontsize = 20)
    elif file_name == 'MET filters_R_pT_hard.root':
        ratio_axis.set_xlabel('$R_{p_{T}}^{hard}$ ', fontsize = 20)
    elif file_name == 'MET filters_z_star.root':
        ratio_axis.set_xlabel(' $z^*$ ')
    elif file_name == 'MET filters_pv_npvs.root':
        ratio_axis.set_xlabel('Number of total reconstructed  primary vertices')
    elif file_name == 'Electron_SF_pv_npvs.root':
        ratio_axis.set_xlabel('Number of good primary vertices')
    elif file_name == 'Pileup reweighting_pv_npvs.root':
        ratio_axis.set_xlabel('Number of total reconstructed primary vertices')
    elif file_name == 'MET filters_QGD.root':
        ratio_axis.set_xlabel('QGL Discriminant')
    elif file_name == 'MET filters_Delta_R_jets_l_e.root':
        ratio_axis.set_xlabel(' $R_{jets, e_{L}}$ ')
    elif file_name == 'MET filters_Delta_R_jets_s_e.root':
        ratio_axis.set_xlabel(' $R_{jets, e_{S}}$ ')
    elif file_name == 'MET filters_leading_jet_QGL.root':
        ratio_axis.set_xlabel('Leading jet QGL', fontsize = 20)
    elif file_name == 'MET filters_subleading_jet_QGL.root':
        ratio_axis.set_xlabel('Subleading jet QGL', fontsize = 20)
    ratio_axis.set_ylabel(r'Data/MC')
    ratio_axis.set_ylim(0.5, 1.5)
    hep.cms.label(loc=0, data=True, ax=stack_axis,  llabel= 'Work in progress', lumi=59.83, year=2018)
    #plt.savefig('/afs/desy.de/user/m/moralfk/www/EWZjj-test-figures/NEW_DetectorLevelPlots/BDT_studies/preBDT/Detector_level_plots/DL_plots_blinded/2018/ee_channel/Pepper_ML_ee_2018_HLT32_Leade40_postnormSF_'+file_name+'.pdf')
    #plt.savefig('/afs/desy.de/user/m/moralfk/www/EWZjj-test-figures/NEW_DetectorLevelPlots/Pepper_muon_8thjune_goodPV_'+file_name+'.png')    
    #plt.savefig('/afs/desy.de/user/m/moralfk/www/EWZjj-test-figures/NEW_DetectorLevelPlots/Pepper_muon_ALL_23rdjune_sys_'+file_name+'.png')
    #plt.savefig('/afs/desy.de/user/m/moralfk/www/EWZjj-test-figures/NEW_DetectorLevelPlots/Pepper_electron_ee_nobtag_yes_sf_yes_trigger32_WPL_DR03_btag_'+file_name+'.png')
    #plt.savefig('/afs/desy.de/user/m/moralfk/www/EWZjj-test-figures/NEW_DetectorLevelPlots/Pepper_electron_ee_nobtagSF_90ID_convVeto_R03__yes_triggerSF_'+file_name+'.png')
    #plt.savefig('/afs/desy.de/user/m/moralfk/www/EWZjj-test-figures/NEW_DetectorLevelPlots/BDT_studies/preBDT/Detector_level_plots/DL_plots_non_blinded/2018/ee_channel/Pepper_ML_ee_2018_HLT32_Leade40_postnormSF__'+file_name+'.png')
    #plt.show()
