import matplotlib.pyplot as plt
import mplhep as hep
import uproot
import matplotlib.gridspec as gridspec
import numpy as np
import matplotlib.patches as patches
import matplotlib.path as path
import hist
from hist import Hist
#from pathlib import Path
#from pathlib import PathPatch
#DY.root  EWK_LLJJ_MLL.root  SingleMuon.root

def make_ratio(hnum, hden):
  wnum, bins = hnum.to_numpy()
  wden, bins = hden.to_numpy()

  numerr = np.sqrt(hnum.variances())
  
  #wratio = np.divide(wnum, wden) - 1
  wratio = np.divide(wnum, wden)
  print('data')
  print(wnum)
  print('mc')
  print(wden)
  ratioerr = np.divide(numerr, wden)
  bincenters = (bins[:-1] + bins[1:]) / 2
  return wratio, bins, ratioerr

def systematics(mc_nom_file, sys_up_file, sys_down_file):
    sys_up_f = 0
    sys_down_f = 0
    for mc_nom, sys_up_value, sys_down_value in zip(mc_nom_file, sys_up_file, sys_down_file):
        mc_nom = hist.Hist(mc_nom)
        sys_up_value = hist.Hist(sys_up_value)
        sys_down_value = hist.Hist(sys_down_value)
        sys_up_temp = ((sys_up_value.values() - mc_nom.values())**2)
        sys_down_temp = ((sys_down_value.values() - mc_nom.values())**2)
        sys_up_f = sys_up_temp + sys_up_f
        sys_down_f = sys_down_temp + sys_down_f
    sys_up = np.sqrt(sys_up_f)
    sys_down = np.sqrt(sys_down_f)
    
    return sys_up, sys_down

def total_systematics(sys_up, sys_down):
    sys_up_f = 0
    sys_down_f = 0
    for sys_up_partial, sys_down_partial in zip(sys_up, sys_down):
        sys_up_temp = (sys_up_partial**2)
        sys_down_temp = (sys_down_partial**2)
        sys_up_f = sys_up_temp + sys_up_f
        sys_down_f = sys_down_temp + sys_down_f
    total_sys_up = np.sqrt(sys_up_f)
    total_sys_down = np.sqrt(sys_down_f)
    return total_sys_up, total_sys_down

def systematics_ratio(sys_up, sys_down, mc_nom_stacked):
    ratio_up_sys = 1 + sys_up/mc_nom_stacked  
    ratio_down_sys = 1 - sys_down/mc_nom_stacked
    return ratio_up_sys, ratio_down_sys

# Load histograms

# General results directory
#inputdir =  '/nfs/dust/cms/user/moralfk/EWZjj_Pepper/ee_yes_btagSF_90ID_convVeto_R03_yes_triggerSF_sys/'
inputdir =  '/nfs/dust/cms/user/moralfk/EWZjj_Pepper/hists/'
#inputdir = '/nfs/dust/cms/user/moralfk/EWZjj_Pepper/ee_nobtag_no_sf_yes_trigger32_WPL/'
#inputdir = '/nfs/dust/cms/user/moralfk/EWZjj_Pepper/ee_nobtag_no_sf_yes_trigger32_WP90/'
#inputdir = '/nfs/dust/cms/user/moralfk/EWZjj_Pepper/ee_nobtag_yes_sf_yes_trigger32_WPL_DR03_btag/'

# New file names

file_names = ['MET filters_pv_npvs.root', 'MET filters_leading_jet_QGL.root', 'MET filters_subleading_jet_QGL.root', 'MET filters_R_pT_hard.root', 'MET filters_z_star.root',
              'MET filters_leading_electron_pt.root', 'MET filters_leading_electron_abseta.root', 'MET filters_Delta_R_jets_l_e.root', 'MET filters_Delta_R_jets_s_e.root',
              'MET filters_subleading_electron_pt.root', 'MET filters_subleading_electron_abseta.root',
              'MET filters_leading_jet_pt.root', 'MET filters_leading_jet_abseta.root', 'MET filters_subleading_jet_pt.root', 'MET filters_subleading_jet_abseta.root',
              'MET filters_Dijet_pt.root', 'MET filters_Dijet_abseta.root', 'MET filters_Dijet_mass.root', 'MET filters_Dijet_phi.root',
              'MET filters_number_of_jets.root', 'MET filters_number_of_low_pT_jets.root', 'MET filters_dphijj.root', 'MET filters_detajj.root', 'MET filters_detall.root', 'MET filters_dphill.root',
              'MET filters_Zboson_pt.root', 'MET filters_Zboson_mass.root', 'MET filters_Zboson_abseta.root', 'MET filters_Zboson_phi.root',
              'MET filters_DphiZ_l_jet.root', 'MET filters_DphiZ_s_jet.root', 'MET filters_DphiZ_jets.root']

files_PU_up_systematics = ['MET filters_pv_npvs_pileup_up.root', 'MET filters_leading_jet_QGL_pileup_up.root', 'MET filters_subleading_jet_QGL_pileup_up.root', 'MET filters_R_pT_hard_pileup_up.root', 'MET filters_z_star_pileup_up.root',
              'MET filters_leading_electron_pt_pileup_up.root', 'MET filters_leading_electron_abseta_pileup_up.root', 'MET filters_Delta_R_jets_l_e_pileup_up.root', 'MET filters_Delta_R_jets_s_e_pileup_up.root',
              'MET filters_subleading_electron_pt_pileup_up.root', 'MET filters_subleading_electron_abseta_pileup_up.root',
              'MET filters_leading_jet_pt_pileup_up.root', 'MET filters_leading_jet_abseta_pileup_up.root', 'MET filters_subleading_jet_pt_pileup_up.root', 'MET filters_subleading_jet_abseta_pileup_up.root',
              'MET filters_Dijet_pt_pileup_up.root', 'MET filters_Dijet_abseta_pileup_up.root', 'MET filters_Dijet_mass_pileup_up.root', 'MET filters_Dijet_phi_pileup_up.root',
              'MET filters_number_of_jets_pileup_up.root', 'MET filters_number_of_low_pT_jets_pileup_up.root', 'MET filters_dphijj_pileup_up.root', 'MET filters_detajj_pileup_up.root', 'MET filters_detall_pileup_up.root', 'MET filters_dphill_pileup_up.root',
              'MET filters_Zboson_pt_pileup_up.root', 'MET filters_Zboson_mass_pileup_up.root', 'MET filters_Zboson_abseta_pileup_up.root', 'MET filters_Zboson_phi_pileup_up.root',
              'MET filters_DphiZ_l_jet_pileup_up.root', 'MET filters_DphiZ_s_jet_pileup_up.root', 'MET filters_DphiZ_jets_pileup_up.root']

files_PU_down_systematics = ['MET filters_pv_npvs_pileup_down.root', 'MET filters_leading_jet_QGL_pileup_down.root', 'MET filters_subleading_jet_QGL_pileup_down.root', 'MET filters_R_pT_hard_pileup_down.root', 'MET filters_z_star_pileup_down.root',
              'MET filters_leading_electron_pt_pileup_down.root', 'MET filters_leading_electron_abseta_pileup_down.root', 'MET filters_Delta_R_jets_l_e_pileup_down.root', 'MET filters_Delta_R_jets_s_e_pileup_down.root',
              'MET filters_subleading_electron_pt_pileup_down.root', 'MET filters_subleading_electron_abseta_pileup_down.root',
              'MET filters_leading_jet_pt_pileup_down.root', 'MET filters_leading_jet_abseta_pileup_down.root', 'MET filters_subleading_jet_pt_pileup_down.root', 'MET filters_subleading_jet_abseta_pileup_down.root',
              'MET filters_Dijet_pt_pileup_down.root', 'MET filters_Dijet_abseta_pileup_down.root', 'MET filters_Dijet_mass_pileup_down.root', 'MET filters_Dijet_phi_pileup_down.root',
              'MET filters_number_of_jets_pileup_down.root', 'MET filters_number_of_low_pT_jets_pileup_down.root', 'MET filters_dphijj_pileup_down.root', 'MET filters_detajj_pileup_down.root', 'MET filters_detall_pileup_down.root', 'MET filters_dphill_pileup_down.root',
              'MET filters_Zboson_pt_pileup_down.root', 'MET filters_Zboson_mass_pileup_down.root', 'MET filters_Zboson_abseta_pileup_down.root', 'MET filters_Zboson_phi_pileup_down.root',
              'MET filters_DphiZ_l_jet_pileup_down.root', 'MET filters_DphiZ_s_jet_pileup_down.root', 'MET filters_DphiZ_jets_pileup_down.root']

files_btag_up_systematics = ['MET filters_pv_npvs_btagsf0_up.root', 'MET filters_leading_jet_QGL_btagsf0_up.root', 'MET filters_subleading_jet_QGL_btagsf0_up.root', 'MET filters_R_pT_hard_btagsf0_up.root', 'MET filters_z_star_btagsf0_up.root',
              'MET filters_leading_electron_pt_btagsf0_up.root', 'MET filters_leading_electron_abseta_btagsf0_up.root', 'MET filters_Delta_R_jets_l_e_btagsf0_up.root', 'MET filters_Delta_R_jets_s_e_btagsf0_up.root',
              'MET filters_subleading_electron_pt_btagsf0_up.root', 'MET filters_subleading_electron_abseta_btagsf0_up.root',
              'MET filters_leading_jet_pt_btagsf0_up.root', 'MET filters_leading_jet_abseta_btagsf0_up.root', 'MET filters_subleading_jet_pt_btagsf0_up.root', 'MET filters_subleading_jet_abseta_btagsf0_up.root',
              'MET filters_Dijet_pt_btagsf0_up.root', 'MET filters_Dijet_abseta_btagsf0_up.root', 'MET filters_Dijet_mass_btagsf0_up.root', 'MET filters_Dijet_phi_btagsf0_up.root',
              'MET filters_number_of_jets_btagsf0_up.root', 'MET filters_number_of_low_pT_jets_btagsf0_up.root', 'MET filters_dphijj_btagsf0_up.root', 'MET filters_detajj_btagsf0_up.root', 'MET filters_detall_btagsf0_up.root', 'MET filters_dphill_btagsf0_up.root',
              'MET filters_Zboson_pt_btagsf0_up.root', 'MET filters_Zboson_mass_btagsf0_up.root', 'MET filters_Zboson_abseta_btagsf0_up.root', 'MET filters_Zboson_phi_btagsf0_up.root',
              'MET filters_DphiZ_l_jet_btagsf0_up.root', 'MET filters_DphiZ_s_jet_btagsf0_up.root', 'MET filters_DphiZ_jets_btagsf0_up.root']

files_btag_down_systematics = ['MET filters_pv_npvs_btagsf0_down.root', 'MET filters_leading_jet_QGL_btagsf0_down.root', 'MET filters_subleading_jet_QGL_btagsf0_down.root', 'MET filters_R_pT_hard_btagsf0_down.root', 'MET filters_z_star_btagsf0_down.root',
              'MET filters_leading_electron_pt_btagsf0_down.root', 'MET filters_leading_electron_abseta_btagsf0_down.root', 'MET filters_Delta_R_jets_l_e_btagsf0_down.root', 'MET filters_Delta_R_jets_s_e_btagsf0_down.root',
              'MET filters_subleading_electron_pt_btagsf0_down.root', 'MET filters_subleading_electron_abseta_btagsf0_down.root',
              'MET filters_leading_jet_pt_btagsf0_down.root', 'MET filters_leading_jet_abseta_btagsf0_down.root', 'MET filters_subleading_jet_pt_btagsf0_down.root', 'MET filters_subleading_jet_abseta_btagsf0_down.root',
              'MET filters_Dijet_pt_btagsf0_down.root', 'MET filters_Dijet_abseta_btagsf0_down.root', 'MET filters_Dijet_mass_btagsf0_down.root', 'MET filters_Dijet_phi_btagsf0_down.root',
              'MET filters_number_of_jets_btagsf0_down.root', 'MET filters_number_of_low_pT_jets_btagsf0_down.root', 'MET filters_dphijj_btagsf0_down.root', 'MET filters_detajj_btagsf0_down.root', 'MET filters_detall_btagsf0_down.root', 'MET filters_dphill_btagsf0_down.root',
              'MET filters_Zboson_pt_btagsf0_down.root', 'MET filters_Zboson_mass_btagsf0_down.root', 'MET filters_Zboson_abseta_btagsf0_down.root', 'MET filters_Zboson_phi_btagsf0_down.root',
              'MET filters_DphiZ_l_jet_btagsf0_down.root', 'MET filters_DphiZ_s_jet_btagsf0_down.root', 'MET filters_DphiZ_jets_btagsf0_down.root']

files_JER_up_systematics = ['MET filters_pv_npvs_Jer_up.root', 'MET filters_leading_jet_QGL_Jer_up.root', 'MET filters_subleading_jet_QGL_Jer_up.root', 'MET filters_R_pT_hard_Jer_up.root', 'MET filters_z_star_Jer_up.root',
              'MET filters_leading_electron_pt_Jer_up.root', 'MET filters_leading_electron_abseta_Jer_up.root', 'MET filters_Delta_R_jets_l_e_Jer_up.root', 'MET filters_Delta_R_jets_s_e_Jer_up.root',
              'MET filters_subleading_electron_pt_Jer_up.root', 'MET filters_subleading_electron_abseta_Jer_up.root',
              'MET filters_leading_jet_pt_Jer_up.root', 'MET filters_leading_jet_abseta_Jer_up.root', 'MET filters_subleading_jet_pt_Jer_up.root', 'MET filters_subleading_jet_abseta_Jer_up.root',
              'MET filters_Dijet_pt_Jer_up.root', 'MET filters_Dijet_abseta_Jer_up.root', 'MET filters_Dijet_mass_Jer_up.root', 'MET filters_Dijet_phi_Jer_up.root',
              'MET filters_number_of_jets_Jer_up.root', 'MET filters_number_of_low_pT_jets_Jer_up.root', 'MET filters_dphijj_Jer_up.root', 'MET filters_detajj_Jer_up.root', 'MET filters_detall_Jer_up.root', 'MET filters_dphill_Jer_up.root',
              'MET filters_Zboson_pt_Jer_up.root', 'MET filters_Zboson_mass_Jer_up.root', 'MET filters_Zboson_abseta_Jer_up.root', 'MET filters_Zboson_phi_Jer_up.root',
              'MET filters_DphiZ_l_jet_Jer_up.root', 'MET filters_DphiZ_s_jet_Jer_up.root', 'MET filters_DphiZ_jets_Jer_up.root']

files_JER_down_systematics = ['MET filters_pv_npvs_Jer_down.root', 'MET filters_leading_jet_QGL_Jer_down.root', 'MET filters_subleading_jet_QGL_Jer_down.root', 'MET filters_R_pT_hard_Jer_down.root', 'MET filters_z_star_Jer_down.root',
              'MET filters_leading_electron_pt_Jer_down.root', 'MET filters_leading_electron_abseta_Jer_down.root', 'MET filters_Delta_R_jets_l_e_Jer_down.root', 'MET filters_Delta_R_jets_s_e_Jer_down.root',
              'MET filters_subleading_electron_pt_Jer_down.root', 'MET filters_subleading_electron_abseta_Jer_down.root',
              'MET filters_leading_jet_pt_Jer_down.root', 'MET filters_leading_jet_abseta_Jer_down.root', 'MET filters_subleading_jet_pt_Jer_down.root', 'MET filters_subleading_jet_abseta_Jer_down.root',
              'MET filters_Dijet_pt_Jer_down.root', 'MET filters_Dijet_abseta_Jer_down.root', 'MET filters_Dijet_mass_Jer_down.root', 'MET filters_Dijet_phi_Jer_down.root',
              'MET filters_number_of_jets_Jer_down.root', 'MET filters_number_of_low_pT_jets_Jer_down.root', 'MET filters_dphijj_Jer_down.root', 'MET filters_detajj_Jer_down.root', 'MET filters_detall_Jer_down.root', 'MET filters_dphill_Jer_down.root',
              'MET filters_Zboson_pt_Jer_down.root', 'MET filters_Zboson_mass_Jer_down.root', 'MET filters_Zboson_abseta_Jer_down.root', 'MET filters_Zboson_phi_Jer_down.root',
              'MET filters_DphiZ_l_jet_Jer_down.root', 'MET filters_DphiZ_s_jet_Jer_down.root', 'MET filters_DphiZ_jets_Jer_down.root']

files_junc_up_systematics = ['MET filters_pv_npvs_Junc_up.root', 'MET filters_leading_jet_QGL_Junc_up.root', 'MET filters_subleading_jet_QGL_Junc_up.root', 'MET filters_R_pT_hard_Junc_up.root', 'MET filters_z_star_Junc_up.root',
              'MET filters_leading_electron_pt_Junc_up.root', 'MET filters_leading_electron_abseta_Junc_up.root', 'MET filters_Delta_R_jets_l_e_Junc_up.root', 'MET filters_Delta_R_jets_s_e_Junc_up.root',
              'MET filters_subleading_electron_pt_Junc_up.root', 'MET filters_subleading_electron_abseta_Junc_up.root',
              'MET filters_leading_jet_pt_Junc_up.root', 'MET filters_leading_jet_abseta_Junc_up.root', 'MET filters_subleading_jet_pt_Junc_up.root', 'MET filters_subleading_jet_abseta_Junc_up.root',
              'MET filters_Dijet_pt_Junc_up.root', 'MET filters_Dijet_abseta_Junc_up.root', 'MET filters_Dijet_mass_Junc_up.root', 'MET filters_Dijet_phi_Junc_up.root',
              'MET filters_number_of_jets_Junc_up.root', 'MET filters_number_of_low_pT_jets_Junc_up.root', 'MET filters_dphijj_Junc_up.root', 'MET filters_detajj_Junc_up.root', 'MET filters_detall_Junc_up.root', 'MET filters_dphill_Junc_up.root',
              'MET filters_Zboson_pt_Junc_up.root', 'MET filters_Zboson_mass_Junc_up.root', 'MET filters_Zboson_abseta_Junc_up.root', 'MET filters_Zboson_phi_Junc_up.root',
              'MET filters_DphiZ_l_jet_Junc_up.root', 'MET filters_DphiZ_s_jet_Junc_up.root', 'MET filters_DphiZ_jets_Junc_up.root']

files_junc_down_systematics = ['MET filters_pv_npvs_Junc_down.root', 'MET filters_leading_jet_QGL_Junc_down.root', 'MET filters_subleading_jet_QGL_Junc_down.root', 'MET filters_R_pT_hard_Junc_down.root', 'MET filters_z_star_Junc_down.root',
              'MET filters_leading_electron_pt_Junc_down.root', 'MET filters_leading_electron_abseta_Junc_down.root', 'MET filters_Delta_R_jets_l_e_Junc_down.root', 'MET filters_Delta_R_jets_s_e_Junc_down.root',
              'MET filters_subleading_electron_pt_Junc_down.root', 'MET filters_subleading_electron_abseta_Junc_down.root',
              'MET filters_leading_jet_pt_Junc_down.root', 'MET filters_leading_jet_abseta_Junc_down.root', 'MET filters_subleading_jet_pt_Junc_down.root', 'MET filters_subleading_jet_abseta_Junc_down.root',
              'MET filters_Dijet_pt_Junc_down.root', 'MET filters_Dijet_abseta_Junc_down.root', 'MET filters_Dijet_mass_Junc_down.root', 'MET filters_Dijet_phi_Junc_down.root',
              'MET filters_number_of_jets_Junc_down.root', 'MET filters_number_of_low_pT_jets_Junc_down.root', 'MET filters_dphijj_Junc_down.root', 'MET filters_detajj_Junc_down.root', 'MET filters_detall_Junc_down.root', 'MET filters_dphill_Junc_down.root',
              'MET filters_Zboson_pt_Junc_down.root', 'MET filters_Zboson_mass_Junc_down.root', 'MET filters_Zboson_abseta_Junc_down.root', 'MET filters_Zboson_phi_Junc_down.root',
              'MET filters_DphiZ_l_jet_Junc_down.root', 'MET filters_DphiZ_s_jet_Junc_down.root', 'MET filters_DphiZ_jets_Junc_down.root']

files_btagsf0light_up_systematics = ['MET filters_pv_npvs_btagsf0light_up.root', 'MET filters_leading_jet_QGL_btagsf0light_up.root', 'MET filters_subleading_jet_QGL_btagsf0light_up.root', 'MET filters_R_pT_hard_btagsf0light_up.root', 'MET filters_z_star_btagsf0light_up.root',
              'MET filters_leading_electron_pt_btagsf0light_up.root', 'MET filters_leading_electron_abseta_btagsf0light_up.root', 'MET filters_Delta_R_jets_l_e_btagsf0light_up.root', 'MET filters_Delta_R_jets_s_e_btagsf0light_up.root',
              'MET filters_subleading_electron_pt_btagsf0light_up.root', 'MET filters_subleading_electron_abseta_btagsf0light_up.root',
              'MET filters_leading_jet_pt_btagsf0light_up.root', 'MET filters_leading_jet_abseta_btagsf0light_up.root', 'MET filters_subleading_jet_pt_btagsf0light_up.root', 'MET filters_subleading_jet_abseta_btagsf0light_up.root',
              'MET filters_Dijet_pt_btagsf0light_up.root', 'MET filters_Dijet_abseta_btagsf0light_up.root', 'MET filters_Dijet_mass_btagsf0light_up.root', 'MET filters_Dijet_phi_btagsf0light_up.root',
              'MET filters_number_of_jets_btagsf0light_up.root', 'MET filters_number_of_low_pT_jets_btagsf0light_up.root', 'MET filters_dphijj_btagsf0light_up.root', 'MET filters_detajj_btagsf0light_up.root', 'MET filters_detall_btagsf0light_up.root', 'MET filters_dphill_btagsf0light_up.root',
              'MET filters_Zboson_pt_btagsf0light_up.root', 'MET filters_Zboson_mass_btagsf0light_up.root', 'MET filters_Zboson_abseta_btagsf0light_up.root', 'MET filters_Zboson_phi_btagsf0light_up.root',
              'MET filters_DphiZ_l_jet_btagsf0light_up.root', 'MET filters_DphiZ_s_jet_btagsf0light_up.root', 'MET filters_DphiZ_jets_btagsf0light_up.root']
 
files_btagsf0light_down_systematics = ['MET filters_pv_npvs_btagsf0light_down.root', 'MET filters_leading_jet_QGL_btagsf0light_down.root', 'MET filters_subleading_jet_QGL_btagsf0light_down.root', 'MET filters_R_pT_hard_btagsf0light_down.root', 'MET filters_z_star_btagsf0light_down.root',
              'MET filters_leading_electron_pt_btagsf0light_down.root', 'MET filters_leading_electron_abseta_btagsf0light_down.root', 'MET filters_Delta_R_jets_l_e_btagsf0light_down.root', 'MET filters_Delta_R_jets_s_e_btagsf0light_down.root',
              'MET filters_subleading_electron_pt_btagsf0light_down.root', 'MET filters_subleading_electron_abseta_btagsf0light_down.root',
              'MET filters_leading_jet_pt_btagsf0light_down.root', 'MET filters_leading_jet_abseta_btagsf0light_down.root', 'MET filters_subleading_jet_pt_btagsf0light_down.root', 'MET filters_subleading_jet_abseta_btagsf0light_down.root',
              'MET filters_Dijet_pt_btagsf0light_down.root', 'MET filters_Dijet_abseta_btagsf0light_down.root', 'MET filters_Dijet_mass_btagsf0light_down.root', 'MET filters_Dijet_phi_btagsf0light_down.root',
              'MET filters_number_of_jets_btagsf0light_down.root', 'MET filters_number_of_low_pT_jets_btagsf0light_down.root', 'MET filters_dphijj_btagsf0light_down.root', 'MET filters_detajj_btagsf0light_down.root', 'MET filters_detall_btagsf0light_down.root', 'MET filters_dphill_btagsf0light_down.root',
              'MET filters_Zboson_pt_btagsf0light_down.root', 'MET filters_Zboson_mass_btagsf0light_down.root', 'MET filters_Zboson_abseta_btagsf0light_down.root', 'MET filters_Zboson_phi_btagsf0light_down.root',
              'MET filters_DphiZ_l_jet_btagsf0light_down.root', 'MET filters_DphiZ_s_jet_btagsf0light_down.root', 'MET filters_DphiZ_jets_btagsf0light_down.root']

files_electronsf0_up_systematics = ['MET filters_pv_npvs_electronsf0_up.root', 'MET filters_leading_jet_QGL_electronsf0_up.root', 'MET filters_subleading_jet_QGL_electronsf0_up.root', 'MET filters_R_pT_hard_electronsf0_up.root', 'MET filters_z_star_electronsf0_up.root',
              'MET filters_leading_electron_pt_electronsf0_up.root', 'MET filters_leading_electron_abseta_electronsf0_up.root', 'MET filters_Delta_R_jets_l_e_electronsf0_up.root', 'MET filters_Delta_R_jets_s_e_electronsf0_up.root',
              'MET filters_subleading_electron_pt_electronsf0_up.root', 'MET filters_subleading_electron_abseta_electronsf0_up.root',
              'MET filters_leading_jet_pt_electronsf0_up.root', 'MET filters_leading_jet_abseta_electronsf0_up.root', 'MET filters_subleading_jet_pt_electronsf0_up.root', 'MET filters_subleading_jet_abseta_electronsf0_up.root',
              'MET filters_Dijet_pt_electronsf0_up.root', 'MET filters_Dijet_abseta_electronsf0_up.root', 'MET filters_Dijet_mass_electronsf0_up.root', 'MET filters_Dijet_phi_electronsf0_up.root',
              'MET filters_number_of_jets_electronsf0_up.root', 'MET filters_number_of_low_pT_jets_electronsf0_up.root', 'MET filters_dphijj_electronsf0_up.root', 'MET filters_detajj_electronsf0_up.root', 'MET filters_detall_electronsf0_up.root', 'MET filters_dphill_electronsf0_up.root',
              'MET filters_Zboson_pt_electronsf0_up.root', 'MET filters_Zboson_mass_electronsf0_up.root', 'MET filters_Zboson_abseta_electronsf0_up.root', 'MET filters_Zboson_phi_electronsf0_up.root',
              'MET filters_DphiZ_l_jet_electronsf0_up.root', 'MET filters_DphiZ_s_jet_electronsf0_up.root', 'MET filters_DphiZ_jets_electronsf0_up.root'] 

files_electronsf0_down_systematics = ['MET filters_pv_npvs_electronsf0_down.root', 'MET filters_leading_jet_QGL_electronsf0_down.root', 'MET filters_subleading_jet_QGL_electronsf0_down.root', 'MET filters_R_pT_hard_electronsf0_down.root', 'MET filters_z_star_electronsf0_down.root',
              'MET filters_leading_electron_pt_electronsf0_down.root', 'MET filters_leading_electron_abseta_electronsf0_down.root', 'MET filters_Delta_R_jets_l_e_electronsf0_down.root', 'MET filters_Delta_R_jets_s_e_electronsf0_down.root',
              'MET filters_subleading_electron_pt_electronsf0_down.root', 'MET filters_subleading_electron_abseta_electronsf0_down.root',
              'MET filters_leading_jet_pt_electronsf0_down.root', 'MET filters_leading_jet_abseta_electronsf0_down.root', 'MET filters_subleading_jet_pt_electronsf0_down.root', 'MET filters_subleading_jet_abseta_electronsf0_down.root',
              'MET filters_Dijet_pt_electronsf0_down.root', 'MET filters_Dijet_abseta_electronsf0_down.root', 'MET filters_Dijet_mass_electronsf0_down.root', 'MET filters_Dijet_phi_electronsf0_down.root',
              'MET filters_number_of_jets_electronsf0_down.root', 'MET filters_number_of_low_pT_jets_electronsf0_down.root', 'MET filters_dphijj_electronsf0_down.root', 'MET filters_detajj_electronsf0_down.root', 'MET filters_detall_electronsf0_down.root', 'MET filters_dphill_electronsf0_down.root',
              'MET filters_Zboson_pt_electronsf0_down.root', 'MET filters_Zboson_mass_electronsf0_down.root', 'MET filters_Zboson_abseta_electronsf0_down.root', 'MET filters_Zboson_phi_electronsf0_down.root',
              'MET filters_DphiZ_l_jet_electronsf0_down.root', 'MET filters_DphiZ_s_jet_electronsf0_down.root', 'MET filters_DphiZ_jets_electronsf0_down.root']

files_electronsf1_up_systematics = ['MET filters_pv_npvs_electronsf1_up.root', 'MET filters_leading_jet_QGL_electronsf1_up.root', 'MET filters_subleading_jet_QGL_electronsf1_up.root', 'MET filters_R_pT_hard_electronsf1_up.root', 'MET filters_z_star_electronsf1_up.root',
              'MET filters_leading_electron_pt_electronsf1_up.root', 'MET filters_leading_electron_abseta_electronsf1_up.root', 'MET filters_Delta_R_jets_l_e_electronsf1_up.root', 'MET filters_Delta_R_jets_s_e_electronsf1_up.root',
              'MET filters_subleading_electron_pt_electronsf1_up.root', 'MET filters_subleading_electron_abseta_electronsf1_up.root',
              'MET filters_leading_jet_pt_electronsf1_up.root', 'MET filters_leading_jet_abseta_electronsf1_up.root', 'MET filters_subleading_jet_pt_electronsf1_up.root', 'MET filters_subleading_jet_abseta_electronsf1_up.root',
              'MET filters_Dijet_pt_electronsf1_up.root', 'MET filters_Dijet_abseta_electronsf1_up.root', 'MET filters_Dijet_mass_electronsf1_up.root', 'MET filters_Dijet_phi_electronsf1_up.root',
              'MET filters_number_of_jets_electronsf1_up.root', 'MET filters_number_of_low_pT_jets_electronsf1_up.root', 'MET filters_dphijj_electronsf1_up.root', 'MET filters_detajj_electronsf1_up.root', 'MET filters_detall_electronsf1_up.root', 'MET filters_dphill_electronsf1_up.root',
              'MET filters_Zboson_pt_electronsf1_up.root', 'MET filters_Zboson_mass_electronsf1_up.root', 'MET filters_Zboson_abseta_electronsf1_up.root', 'MET filters_Zboson_phi_electronsf1_up.root',
              'MET filters_DphiZ_l_jet_electronsf1_up.root', 'MET filters_DphiZ_s_jet_electronsf1_up.root', 'MET filters_DphiZ_jets_electronsf1_up.root']

files_electronsf1_down_systematics = ['MET filters_pv_npvs_electronsf1_down.root', 'MET filters_leading_jet_QGL_electronsf1_down.root', 'MET filters_subleading_jet_QGL_electronsf1_down.root', 'MET filters_R_pT_hard_electronsf1_down.root', 'MET filters_z_star_electronsf1_down.root',
              'MET filters_leading_electron_pt_electronsf1_down.root', 'MET filters_leading_electron_abseta_electronsf1_down.root', 'MET filters_Delta_R_jets_l_e_electronsf1_down.root', 'MET filters_Delta_R_jets_s_e_electronsf1_down.root',
              'MET filters_subleading_electron_pt_electronsf1_down.root', 'MET filters_subleading_electron_abseta_electronsf1_down.root',
              'MET filters_leading_jet_pt_electronsf1_down.root', 'MET filters_leading_jet_abseta_electronsf1_down.root', 'MET filters_subleading_jet_pt_electronsf1_down.root', 'MET filters_subleading_jet_abseta_electronsf1_down.root',
              'MET filters_Dijet_pt_electronsf1_down.root', 'MET filters_Dijet_abseta_electronsf1_down.root', 'MET filters_Dijet_mass_electronsf1_down.root', 'MET filters_Dijet_phi_electronsf1_down.root',
              'MET filters_number_of_jets_electronsf1_down.root', 'MET filters_number_of_low_pT_jets_electronsf1_down.root', 'MET filters_dphijj_electronsf1_down.root', 'MET filters_detajj_electronsf1_down.root', 'MET filters_detall_electronsf1_down.root', 'MET filters_dphill_electronsf1_down.root',
              'MET filters_Zboson_pt_electronsf1_down.root', 'MET filters_Zboson_mass_electronsf1_down.root', 'MET filters_Zboson_abseta_electronsf1_down.root', 'MET filters_Zboson_phi_electronsf1_down.root',
              'MET filters_DphiZ_l_jet_electronsf1_down.root', 'MET filters_DphiZ_s_jet_electronsf1_down.root', 'MET filters_DphiZ_jets_electronsf1_down.root']

files_electronsf2_up_systematics = ['MET filters_pv_npvs_electronsf2_up.root', 'MET filters_leading_jet_QGL_electronsf2_up.root', 'MET filters_subleading_jet_QGL_electronsf2_up.root', 'MET filters_R_pT_hard_electronsf2_up.root', 'MET filters_z_star_electronsf2_up.root',
              'MET filters_leading_electron_pt_electronsf2_up.root', 'MET filters_leading_electron_abseta_electronsf2_up.root', 'MET filters_Delta_R_jets_l_e_electronsf2_up.root', 'MET filters_Delta_R_jets_s_e_electronsf2_up.root',
              'MET filters_subleading_electron_pt_electronsf2_up.root', 'MET filters_subleading_electron_abseta_electronsf2_up.root',
              'MET filters_leading_jet_pt_electronsf2_up.root', 'MET filters_leading_jet_abseta_electronsf2_up.root', 'MET filters_subleading_jet_pt_electronsf2_up.root', 'MET filters_subleading_jet_abseta_electronsf2_up.root',
              'MET filters_Dijet_pt_electronsf2_up.root', 'MET filters_Dijet_abseta_electronsf2_up.root', 'MET filters_Dijet_mass_electronsf2_up.root', 'MET filters_Dijet_phi_electronsf2_up.root',
              'MET filters_number_of_jets_electronsf2_up.root', 'MET filters_number_of_low_pT_jets_electronsf2_up.root', 'MET filters_dphijj_electronsf2_up.root', 'MET filters_detajj_electronsf2_up.root', 'MET filters_detall_electronsf2_up.root', 'MET filters_dphill_electronsf2_up.root',
              'MET filters_Zboson_pt_electronsf2_up.root', 'MET filters_Zboson_mass_electronsf2_up.root', 'MET filters_Zboson_abseta_electronsf2_up.root', 'MET filters_Zboson_phi_electronsf2_up.root',
              'MET filters_DphiZ_l_jet_electronsf2_up.root', 'MET filters_DphiZ_s_jet_electronsf2_up.root', 'MET filters_DphiZ_jets_electronsf2_up.root']

files_electronsf2_down_systematics = ['MET filters_pv_npvs_electronsf2_down.root', 'MET filters_leading_jet_QGL_electronsf2_down.root', 'MET filters_subleading_jet_QGL_electronsf2_down.root', 'MET filters_R_pT_hard_electronsf2_down.root', 'MET filters_z_star_electronsf2_down.root',
              'MET filters_leading_electron_pt_electronsf2_down.root', 'MET filters_leading_electron_abseta_electronsf2_down.root', 'MET filters_Delta_R_jets_l_e_electronsf2_down.root', 'MET filters_Delta_R_jets_s_e_electronsf2_down.root',
              'MET filters_subleading_electron_pt_electronsf2_down.root', 'MET filters_subleading_electron_abseta_electronsf2_down.root',
              'MET filters_leading_jet_pt_electronsf2_down.root', 'MET filters_leading_jet_abseta_electronsf2_down.root', 'MET filters_subleading_jet_pt_electronsf2_down.root', 'MET filters_subleading_jet_abseta_electronsf2_down.root',
              'MET filters_Dijet_pt_electronsf2_down.root', 'MET filters_Dijet_abseta_electronsf2_down.root', 'MET filters_Dijet_mass_electronsf2_down.root', 'MET filters_Dijet_phi_electronsf2_down.root',
              'MET filters_number_of_jets_electronsf2_down.root', 'MET filters_number_of_low_pT_jets_electronsf2_down.root', 'MET filters_dphijj_electronsf2_down.root', 'MET filters_detajj_electronsf2_down.root', 'MET filters_detall_electronsf2_down.root', 'MET filters_dphill_electronsf2_down.root',
              'MET filters_Zboson_pt_electronsf2_down.root', 'MET filters_Zboson_mass_electronsf2_down.root', 'MET filters_Zboson_abseta_electronsf2_down.root', 'MET filters_Zboson_phi_electronsf2_down.root',
              'MET filters_DphiZ_l_jet_electronsf2_down.root', 'MET filters_DphiZ_s_jet_electronsf2_down.root', 'MET filters_DphiZ_jets_electronsf2_down.root']


#file_names = ['MET filters_Zboson_mass.root']
#file_names = ['Dijet_cut_dphijj.root', 'Dijet_cut_dphill.root', 'Dijet_cut_DphiZ_l_jet.root', 'Dijet_cut_DphiZ_s_jet.root', 'Dijet_cut_DphiZ_jets.root']
#file_names = ['Dijet_cut_leading_jet_QGL.root', 'Dijet_cut_subleading_jet_QGL.root']
#file_names = ['Dijet_cut_Delta_R_jets_l_mu.root', 'Dijet_cut_Delta_R_jets_s_mu.root', 'Dijet_cut_Zboson_mass.root', 'Dijet_cut_pv_npvs.root', 'PV_pv_npvs.root', 'Dijet_cut_Dijet_pt.root']
"""
samples = ['EWK_LLJJ_MLL-50_MJJ-120_TuneCP5_13TeV-madgraph-pythia8_dipole', 'WW_TuneCP5_13TeV-pythia8', 'WZ_TuneCP5_13TeV-pythia8', 'ZZ_TuneCP5_13TeV-pythia8',
           'WWTo2L2Nu_TuneCP5_13TeV-powheg-pythia8', 'WGToLNuG_TuneCP5_13TeV-madgraphMLM-pythia8', 'ZGToLLG_01J_5f_TuneCP5_13TeV-amcatnloFXFX-pythia8', 'ZZZ_TuneCP5_13TeV-amcatnlo-pythia8',
           'WZZ_TuneCP5_13TeV-amcatnlo-pythia8', 'WWZ_4F_TuneCP5_13TeV-amcatnlo-pythia8', 
           'ST_tW_top_5f_inclusiveDecays_TuneCP5_13TeV-powheg-pythia8', 'ST_t-channel_top_4f_InclusiveDecays_TuneCP5_13TeV-powheg-madspin-pythia8',
           'ST_tW_antitop_5f_inclusiveDecays_TuneCP5_13TeV-powheg-pythia8','ST_t-channel_antitop_4f_InclusiveDecays_TuneCP5_13TeV-powheg-madspin-pythia8',
           'ST_s-channel_4f_leptonDecays_TuneCP5_13TeV-amcatnlo-pythia8', 'TTTo2L2Nu_TuneCP5_13TeV-powheg-pythia8','TTToSemiLeptonic_TuneCP5_13TeV-powheg-pythia8', 
           'DYJetsToLL_M-50_TuneCP5_13TeV-amcatnloFXFX-pythia8', 'SingleMuon' ]

"""
samples = ['EWK_LLJJ_MLL-50_MJJ-120_TuneCP5_13TeV-madgraph-pythia8_dipole', 'WW_TuneCP5_13TeV-pythia8', 'WZ_TuneCP5_13TeV-pythia8', 'ZZ_TuneCP5_13TeV-pythia8',
           'ST_tW_top_5f_inclusiveDecays_TuneCP5_13TeV-powheg-pythia8', 'ST_t-channel_top_4f_InclusiveDecays_TuneCP5_13TeV-powheg-madspin-pythia8',
           'ST_tW_antitop_5f_inclusiveDecays_TuneCP5_13TeV-powheg-pythia8','ST_t-channel_antitop_4f_InclusiveDecays_TuneCP5_13TeV-powheg-madspin-pythia8',
           'ST_s-channel_4f_leptonDecays_TuneCP5_13TeV-amcatnlo-pythia8', 'TTTo2L2Nu_TuneCP5_13TeV-powheg-pythia8','TTToSemiLeptonic_TuneCP5_13TeV-powheg-pythia8',
           'DYJetsToLL_M-50_TuneCP5_13TeV-amcatnloFXFX-pythia8', 'EGamma' ]

for(file_name, file_PU_up, file_PU_down, file_btag_up, file_btag_down, file_JER_up, file_JER_down, file_JUNC_up, file_JUNC_down, file_btagsf0light_up, file_btagsf0light_down, file_electronsf0_up,
    file_electronsf0_down, file_electronsf1_up, file_electronsf1_down, file_electronsf2_up, file_electronsf2_down) in zip(file_names, 
    files_PU_up_systematics, files_PU_down_systematics, files_btag_up_systematics, files_btag_down_systematics, files_JER_up_systematics, files_JER_down_systematics, files_junc_up_systematics, 
    files_junc_down_systematics, files_btagsf0light_up_systematics, files_btagsf0light_down_systematics, files_electronsf0_up_systematics, files_electronsf0_down_systematics, 
    files_electronsf1_up_systematics, files_electronsf1_down_systematics, files_electronsf2_up_systematics, files_electronsf2_down_systematics): 

    mc_histograms = [None]*(len(samples)-1)
    PU_up_hist = [None]*(len(samples)-1)
    PU_down_hist = [None]*(len(samples)-1)
    btag_up_hist = [None]*(len(samples)-1)
    btag_down_hist = [None]*(len(samples)-1)
    JER_up_hist = [None]*(len(samples)-1)
    JER_down_hist = [None]*(len(samples)-1)
    JUNC_up_hist = [None]*(len(samples)-1)
    JUNC_down_hist = [None]*(len(samples)-1)
    btagsf0light_up_hist = [None]*(len(samples)-1)
    btagsf0light_down_hist = [None]*(len(samples)-1)
    electronsf0_up_hist = [None]*(len(samples)-1)
    electronsf0_down_hist = [None]*(len(samples)-1)
    electronsf1_up_hist = [None]*(len(samples)-1)
    electronsf1_down_hist = [None]*(len(samples)-1)
    electronsf2_up_hist = [None]*(len(samples)-1)
    electronsf2_down_hist = [None]*(len(samples)-1)

    for i in range(len(mc_histograms)):
        open_file = uproot.open(inputdir + file_name)
        open_PU_up_file = uproot.open(inputdir + file_PU_up)
        open_PU_down_file = uproot.open(inputdir + file_PU_down)
        open_btag_up_file = uproot.open(inputdir + file_btag_up)
        open_btag_down_file = uproot.open(inputdir + file_btag_down)
        open_JER_up_file = uproot.open(inputdir + file_JER_up)
        open_JER_down_file = uproot.open(inputdir + file_JER_down)
        open_JUNC_up_file = uproot.open(inputdir + file_JUNC_up)
        open_JUNC_down_file = uproot.open(inputdir + file_JUNC_down)
        open_btagsf0light_up_file = uproot.open(inputdir + file_btagsf0light_up)
        open_btagsf0light_down_file = uproot.open(inputdir + file_btagsf0light_down)
        open_electronsf0_up_file = uproot.open(inputdir + file_electronsf0_up)
        open_electronsf0_down_file = uproot.open(inputdir + file_electronsf0_down)
        open_electronsf1_up_file = uproot.open(inputdir + file_electronsf1_up)
        open_electronsf1_down_file = uproot.open(inputdir + file_electronsf1_down)
        open_electronsf2_up_file = uproot.open(inputdir + file_electronsf2_up)
        open_electronsf2_down_file = uproot.open(inputdir + file_electronsf2_down)

        mc_histograms[i] = open_file[samples[i]].to_hist()
        data_histogram = open_file['EGamma'].to_hist()
        
        PU_up_hist[i] = open_PU_up_file[samples[i]].to_hist()
        PU_down_hist[i] = open_PU_down_file[samples[i]].to_hist()
        btag_up_hist[i] = open_btag_up_file[samples[i]].to_hist()
        btag_down_hist[i] = open_btag_down_file[samples[i]].to_hist()
        JER_up_hist[i] = open_JER_up_file[samples[i]].to_hist()
        JER_down_hist[i] = open_JER_down_file[samples[i]].to_hist()
        JUNC_up_hist[i] = open_JUNC_up_file[samples[i]].to_hist()
        JUNC_down_hist[i] = open_JUNC_down_file[samples[i]].to_hist()
        btagsf0light_up_hist[i] = open_btagsf0light_up_file[samples[i]].to_hist()
        btagsf0light_down_hist[i] = open_btagsf0light_down_file[samples[i]].to_hist()
        electronsf0_up_hist[i] = open_electronsf0_up_file[samples[i]].to_hist()
        electronsf0_down_hist[i] = open_electronsf0_down_file[samples[i]].to_hist()
        electronsf1_up_hist[i] = open_electronsf1_up_file[samples[i]].to_hist()
        electronsf1_down_hist[i] = open_electronsf1_down_file[samples[i]].to_hist()
        electronsf2_up_hist[i] = open_electronsf2_up_file[samples[i]].to_hist()
        electronsf2_down_hist[i] = open_electronsf2_down_file[samples[i]].to_hist()
  
        scaled_mc_signal= open_file['EWK_LLJJ_MLL-50_MJJ-120_TuneCP5_13TeV-madgraph-pythia8_dipole'].to_hist()*100    

    PU_up, PU_down = systematics(mc_histograms, PU_up_hist, PU_down_hist)
    btag_up, btag_down = systematics(mc_histograms, btag_up_hist, btag_down_hist)
    JER_up, JER_down = systematics(mc_histograms, JER_up_hist, JER_down_hist)
    JUNC_up, JUNC_down = systematics(mc_histograms, JUNC_up_hist, JUNC_down_hist)
    btagsf0light_up, btagsf0light_down = systematics(mc_histograms, btagsf0light_up_hist, btagsf0light_down_hist)
    btag_total_up, btag_total_down = total_systematics([btag_up, btagsf0light_up], [btag_down, btagsf0light_down])
    electronsf0_up, electronsf0_down = systematics(mc_histograms, electronsf0_up_hist, electronsf0_down_hist)
    electronsf0_up, electronsf0_down = systematics(mc_histograms, electronsf0_up_hist, electronsf0_down_hist)
    electronsf1_up, electronsf1_down = systematics(mc_histograms, electronsf1_up_hist, electronsf1_down_hist)
    electronsf1_up, electronsf1_down = systematics(mc_histograms, electronsf1_up_hist, electronsf1_down_hist)
    electronsf2_up, electronsf2_down = systematics(mc_histograms, electronsf2_up_hist, electronsf2_down_hist)
    electronsf2_up, electronsf2_down = systematics(mc_histograms, electronsf2_up_hist, electronsf2_down_hist)
    electron_total_up, electron_total_down = total_systematics([electronsf0_up, electronsf1_up, electronsf2_up], [electronsf0_down, electronsf1_down, electronsf2_down])

    total_up_sys, total_down_sys = total_systematics([PU_up, btag_up, JER_up, JUNC_up, btagsf0light_up, electronsf0_up, electronsf1_up, electronsf2_up],
    [PU_down, btag_down, JER_down, JUNC_down, btagsf0light_down, electronsf0_down, electronsf1_down, electronsf2_down])
    yrr = [total_up_sys, total_down_sys]

    # Set style
    plt.style.use(hep.style.ROOT)
    plt.rcParams.update({'font.size': 14})
    plt.rcParams.update({'axes.titlesize':20})
    plt.rcParams.update({'axes.labelsize':20})

    # Set stack and ratio plot
    fig, ax = plt.subplots(5, 1, sharex=True)

    # Make stack plot in upper axis
    sum_mc_histos = sum(mc_histograms)
    wratio, bins, werr = make_ratio(data_histogram, sum_mc_histos)
    PU_axis = ax[0]
    PU_axis.axhline(y=1, color='black')
    PU_axis.fill_between((bins[:-1] + bins[1:]) / 2, 1 + PU_up/sum_mc_histos.values(), 1- PU_down/sum_mc_histos.values(), color='lavender', label='PU syst. Uncertainty')
    PU_axis.legend()
    PU_axis.set_ylim(0.75, 1.25)

    btag_axis = ax[1]
    btag_axis.axhline(y=1, color='black')
    btag_axis.fill_between((bins[:-1] + bins[1:]) / 2, 1+btag_total_up/sum_mc_histos.values(), 1-btag_total_down/sum_mc_histos.values(), color='slategrey', label='btag syst. Uncertainty')
    btag_axis.legend()
    btag_axis.set_ylim(0.75, 1.25)

    JER_axis = ax[2]
    JER_axis.axhline(y=1, color='black')
    JER_axis.fill_between((bins[:-1] + bins[1:]) / 2, 1+JER_up/sum_mc_histos.values(), 1-JER_down/sum_mc_histos.values(), color='darkseagreen', label='JER syst. Uncertainty')
    JER_axis.legend()
    JER_axis.set_ylim(0.75, 1.25)

    JUNC_axis = ax[3]
    JUNC_axis.axhline(y=1, color='black')
    JUNC_axis.fill_between((bins[:-1] + bins[1:]) / 2, 1+JUNC_up/sum_mc_histos.values(), 1-JUNC_down/sum_mc_histos.values(), color='lightsteelblue', label='JEC syst. Uncertainty')
    JUNC_axis.legend()
    JUNC_axis.set_ylim(0.75, 1.25)

    electron_axis = ax[4]
    electron_axis.axhline(y=1, color='black')
    electron_axis.fill_between((bins[:-1] + bins[1:]) / 2, 1+electron_total_up/sum_mc_histos.values(), 1-electron_total_down/sum_mc_histos.values(), color='darkslateblue', label='electron syst. Uncertainty')
    electron_axis.legend()
    electron_axis.set_ylim(0.95, 1.05)

    if file_name == 'MET filters_Delta_R_jets_l_e.root':
        electron_axis.set_xlabel('$\\Delta R_{e_{L} , jets}$')
    if file_name == 'MET filters_Delta_R_jets_s_e.root':
        electron_axis.set_xlabel('$\\Delta R_{e_{S} , jets}$')
    if file_name == 'MET filters_pv_npvsGood.root':
        electron_axis.set_xlabel('Number of good primary vertices')
    if file_name == 'MET filters_leading_electron_pt.root':
        electron_axis.set_xlabel('Leading electron transverse momentum: $p_{T}$ [GeV]')   
    elif file_name == 'MET filters_leading_electron_abseta.root':
        electron_axis.set_xlabel('Leading electron $|\eta|$')
    elif file_name == 'MET filters_Delta_R_l_e_l_jet.root':
        electron_axis.set_xlabel(' $\\Delta R_{e_{L} , jet_{L}}$')
    elif file_name == 'MET filters_Delta_R_l_e_s_jet.root':
        electron_axis.set_xlabel('$\\Delta R_{e_{L} , jet_{S}}$')
    elif file_name == 'MET filters_Delta_R_l_jet.root':
        electron_axis.set_xlabel(' $\\Delta R_{j_{L} , e e}$')
    elif file_name == 'MET filters_Delta_R_s_jet.root':
        electron_axis.set_xlabel('$\\Delta R_{j_{S} , e e}$')
    elif file_name == 'MET filters_Delta_phi_l_e_l_jet.root':
        electron_axis.set_xlabel(' $\\Delta \\phi_{e_{L}, jet_{L}}$')
    elif file_name == 'MET filters_Delta_phi_l_e_s_jet.root':
        electron_axis.set_xlabel('$\\Delta \\phi_{e_{L}, jet_{S}}$')
    elif file_name == 'MET filters_subleading_electron_pt.root':
        electron_axis.set_xlabel('Subleading electron transverse momentum: $p_{T}$ [GeV]')
    elif file_name == 'MET filters_subleading_electron_abseta.root':
        electron_axis.set_xlabel('Subleading electron $|\eta|$')
    elif file_name == 'MET filters_leading_jet_pt.root':
        electron_axis.set_xlabel('Leading jet transverse momentum: $p_{T}$ [GeV]')
    elif file_name == 'MET filters_Delta_R_s_e_l_jet.root':
        electron_axis.set_xlabel(' $\\Delta R_{e_{S} , jet_{L}}$')
    elif file_name == 'MET filters_Delta_R_s_e_s_jet.root':
        electron_axis.set_xlabel('$\\Delta R_{e_{S} , jet_{S}}$')
    elif file_name == 'MET filters_Delta_phi_s_e_l_jet.root':
        electron_axis.set_xlabel(' $\\Delta \\phi_{e_{S}, jet_{L}}$')
    elif file_name == 'MET filters_Delta_phi_s_e_s_jet.root':
        electron_axis.set_xlabel('$\\Delta \\phi_{e_{S}, jet_{S}}$')
    elif file_name == 'MET filters_leading_jet_abseta.root':
        electron_axis.set_xlabel('Leading jet $|\eta|$')
    elif file_name == 'MET filters_subleading_jet_pt.root':
        electron_axis.set_xlabel('Subleading jet transverse momentum: $p_{T}$ [GeV]')
    elif file_name == 'MET filters_subleading_jet_abseta.root':
        electron_axis.set_xlabel('Subleading jet $|\eta|$')
    elif (file_name == 'MET filters_mjj.root') or (file_name == 'MET filters_Dijet_mass.root'):
        electron_axis.set_xlabel('Dijet mass: $m_{jj}$ [GeV]', fontsize = 20)
    elif (file_name == 'MET filters_pTjj.root') or (file_name == 'MET filters_Dijet_pt.root'):
        electron_axis.set_xlabel('$p_{Tjj}$ [GeV]', fontsize = 20)
    elif file_name == 'MET filters_Dijet_phi.root':
        electron_axis.set_xlabel(' $\\phi_{jj}$ ', fontsize = 20)
    elif file_name == 'MET filters_Dijet_abseta.root':
        electron_axis.set_xlabel(' $ | \\eta_{jj} | $ ', fontsize = 20)
    elif file_name == 'MET filters_Zboson_mass.root':
        electron_axis.set_xlabel('Z boson mass: $m_{l^{+}l^{-}}$ [GeV]', fontsize = 20)
    elif file_name == 'MET filters_Zboson_pt.root':
        electron_axis.set_xlabel('Z boson transverse momentum: $p_{T}$ [GeV]', fontsize = 20)
    elif file_name == 'MET filters_Zboson_abseta.root':
        electron_axis.set_xlabel('$|\\eta|_{l^{+}l^{-}}$ ', fontsize = 20)
    elif file_name == 'MET filters_Zboson_phi.root':
        electron_axis.set_xlabel('$|\\phi|_{l^{+}l^{-}}$ ', fontsize = 20)
    elif file_name == 'MET filters_number_of_jets.root':
        electron_axis.set_xlabel('Number of jets')
    elif file_name == 'MET filters_number_of_low_pT_jets.root':
        electron_axis.set_xlabel('Number of low pT jets')
    elif file_name == 'MET filters_DphiZ_jets.root':
        electron_axis.set_xlabel(' $\\Delta \\phi_{Z, jj}$ ', fontsize = 20)
    elif file_name == 'MET filters_DphiZ_l_jet.root':
        electron_axis.set_xlabel(' $\\Delta \\phi_{Z, j_{L}}$ ', fontsize = 20)
    elif file_name == 'MET filters_DphiZ_s_jet.root':
        electron_axis.set_xlabel(' $\\Delta \\phi_{Z, j_{S}}$ ', fontsize = 20)
    elif file_name == 'MET filters_Delta_phi_l_jet_ee.root':
        electron_axis.set_xlabel(' $\\Delta \\phi_{j_{L}, e e}$ ', fontsize = 20)
    elif file_name == 'MET filters_Delta_phi_s_jet_ee.root':
        electron_axis.set_xlabel(' $\\Delta \\phi_{j_{S}, e e}$ ', fontsize = 20)
    elif file_name == 'MET filters_dphill.root':
        electron_axis.set_xlabel(' $\\Delta \\phi_{e e}$ ', fontsize = 20)
    elif file_name == 'MET filters_dphijj.root':
        electron_axis.set_xlabel(' $\\Delta \\phi_{j_{L} j_{S}}$ ', fontsize = 20)
    elif file_name == 'MET filters_detall.root':
        electron_axis.set_xlabel(' $\\Delta \\eta_{e e}$ ', fontsize = 20)
    elif file_name == 'MET filters_detajj.root':
        electron_axis.set_xlabel(' $\\Delta \\eta_{j_{L} j_{S}}$ ',fontsize = 20)
    elif file_name == 'MET filters_R_pT_hard.root':
        electron_axis.set_xlabel('$R_{p_{T}}^{hard}$ ', fontsize = 20)
    elif file_name == 'MET filters_z_star.root':
        electron_axis.set_xlabel(' $z^*$ ')
    elif file_name == 'MET filters_pv_npvs.root':
        electron_axis.set_xlabel('Number of total reconstructed  primary vertices')
    elif file_name == 'Electron_SF_pv_npvs.root':
        electron_axis.set_xlabel('Number of good primary vertices')
    elif file_name == 'Pileup reweighting_pv_npvs.root':
        electron_axis.set_xlabel('Number of total reconstructed primary vertices')
    elif file_name == 'MET filters_QGD.root':
        electron_axis.set_xlabel('QGL Discriminant')
    elif file_name == 'MET filters_Delta_R_jets_l_e.root':
        electron_axis.set_xlabel(' $R_{jets, e_{L}}$ ')
    elif file_name == 'MET filters_Delta_R_jets_s_e.root':
        electron_axis.set_xlabel(' $R_{jets, e_{S}}$ ')
    elif file_name == 'MET filters_leading_jet_QGL.root':
        electron_axis.set_xlabel('Leading jet QGL', fontsize = 20)
    elif file_name == 'MET filters_subleading_jet_QGL.root':
        electron_axis.set_xlabel('Subleading jet QGL', fontsize = 20)
    #electron_axis.set_xlabel(file_name+'[GeV]')
    #electron_axis.set_ylabel(r'Data/MC')
    # electron y_axis (0.8, 1.2)
    electron_axis.set_ylim(0.5, 1.5)
    #electron_axis.set_ylim(-0.5, 0.5)
    #hep.cms.label(loc=0, data=True, ax=PU_axis, lumi=59.83, year=2018)
    
    #plt.savefig('/afs/desy.de/user/m/moralfk/www/EWZjj-test-figures/NEW_DetectorLevelPlots/Pepper_muon_7thmay_SR_'+file_name+'.pdf')
    #plt.savefig('/afs/desy.de/user/m/moralfk/www/EWZjj-test-figures/NEW_DetectorLevelPlots/Pepper_muon_8thjune_goodPV_'+file_name+'.png')    
    #plt.savefig('/afs/desy.de/user/m/moralfk/www/EWZjj-test-figures/NEW_DetectorLevelPlots/Pepper_muon_ALL_23rdjune_sys_'+file_name+'.png')
    #plt.savefig('/afs/desy.de/user/m/moralfk/www/EWZjj-test-figures/NEW_DetectorLevelPlots/Pepper_electron_ee_nobtag_yes_sf_yes_trigger32_WPL_DR03_btag_'+file_name+'.png')
    #plt.savefig('/afs/desy.de/user/m/moralfk/www/EWZjj-test-figures/NEW_DetectorLevelPlots/Pepper_electron_ee_nobtagSF_90ID_convVeto_R03__yes_triggerSF_'+file_name+'.png')
    #plt.savefig('/afs/desy.de/user/m/moralfk/www/EWZjj-test-figures/NEW_DetectorLevelPlots/Pepper_electron_ee_nobtagSF_90ID_convVeto_R03_'+file_name+'.png')
    #plt.savefig('/afs/desy.de/user/m/moralfk/www/EWZjj-test-figures/NEW_DetectorLevelPlots/Pepper_ee_ML_2018_sys_uncert_'+file_name+'.png')
    plt.show()
