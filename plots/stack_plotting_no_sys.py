import matplotlib.pyplot as plt
import mplhep as hep
import uproot
import matplotlib.gridspec as gridspec
import numpy as np
import matplotlib.patches as patches
import matplotlib.path as path
import hist
from hist import Hist
#from pathlib import Path
#from pathlib import PathPatch
#DY.root  EWK_LLJJ_MLL.root  SingleMuon.root

def make_ratio(hnum, hden):
  wnum, bins = hnum.to_numpy()
  wden, bins = hden.to_numpy()

  numerr = np.sqrt(hnum.variances())
  
  #wratio = np.divide(wnum, wden) - 1
  wratio = np.divide(wnum, wden)
  print('data')
  print(wnum)
  print('mc')
  print(wden)
  ratioerr = np.divide(numerr, wden)
  bincenters = (bins[:-1] + bins[1:]) / 2
  return wratio, bins, ratioerr

def systematics(mc_nom_file, sys_up_file, sys_down_file):
    sys_up_f = 0
    sys_down_f = 0
    for mc_nom, sys_up_value, sys_down_value in zip(mc_nom_file, sys_up_file, sys_down_file):
        mc_nom = hist.Hist(mc_nom)
        sys_up_value = hist.Hist(sys_up_value)
        sys_down_value = hist.Hist(sys_down_value)
        sys_up_temp = ((sys_up_value.values() - mc_nom.values())**2)
        sys_down_temp = ((sys_down_value.values() - mc_nom.values())**2)
        sys_up_f = sys_up_temp + sys_up_f
        sys_down_f = sys_down_temp + sys_down_f
    sys_up = np.sqrt(sys_up_f)
    sys_down = np.sqrt(sys_down_f)
    
    return sys_up, sys_down

def total_systematics(sys_up, sys_down):
    sys_up_f = 0
    sys_down_f = 0
    for sys_up_partial, sys_down_partial in zip(sys_up, sys_down):
        sys_up_temp = (sys_up_partial**2)
        sys_down_temp = (sys_down_partial**2)
        sys_up_f = sys_up_temp + sys_up_f
        sys_down_f = sys_down_temp + sys_down_f
    total_sys_up = np.sqrt(sys_up_f)
    total_sys_down = np.sqrt(sys_down_f)
    return total_sys_up, total_sys_down

def systematics_ratio(sys_up, sys_down, mc_nom_stacked):
    ratio_up_sys = 1 + sys_up/mc_nom_stacked  
    ratio_down_sys = 1 - sys_down/mc_nom_stacked
    return ratio_up_sys, ratio_down_sys

# Load histograms

# General results directory
#inputdir = '/nfs/dust/cms/user/moralfk/EWZjj_Pepper/ALL_8thjune_newDeltaR_QGL_good_b/'
#inputdir = '/nfs/dust/cms/user/moralfk/EWZjj_Pepper/ALL_9thjune_yes_JER_good_Delta_phi_better_binning/'
###inputdir =  '/nfs/dust/cms/user/moralfk/EWZjj_Pepper/ALL_14thjune_sys/'
#inputdir =  '/nfs/dust/cms/user/moralfk/EWZjj_Pepper/ALL_16thjune_sys_47abseta_jets/'
#inputdir =  '/nfs/dust/cms/user/moralfk/EWZjj_Pepper/ALL_22ndjune_no_jet_veto/'
#inputdir =  '/nfs/dust/cms/user/moralfk/EWZjj_Pepper/ALL_6thjuly_MET_nosys/'
inputdir =  '/nfs/dust/cms/user/moralfk/EWZjj_Pepper/hists/'
#inputdir= '/nfs/dust/cms/user/moralfk/EWZjj_Pepper/ALL_8thOctober_Mu_detajj15_mjj400_dphijj1_dphill28_pTll30_etastarcond/'
#inputdir =  '/nfs/dust/cms/user/moralfk/EWZjj_Pepper/ALL_8thOctober_Mu_detajj15_mjj400_dphijj1_dphill28_pTl30/'
#inputdir =  '/nfs/dust/cms/user/moralfk/EWZjj_Pepper/ALL_18thseptember_multiboson/'
#inputdir =  '/nfs/dust/cms/user/moralfk/EWZjj_Pepper/ALL_15thjune_sys_newbins_for_num_jets/'
#inputdir =  '/nfs/dust/cms/user/moralfk/EWZjj_Pepper/ALL_12thjune_all_systematics/'
#inputdir = '/nfs/dust/cms/user/moralfk/EWZjj_Pepper/ALL_7thjune_inclusive_lumimask_trigger_xsecsf_pu_reweight_muon_SF_dijet_yes_btag_yes_DeltaR_yes_JER_jetabseta_24/'
#inputdir = '/nfs/dust/cms/user/moralfk/EWZjj_Pepper/ALL_7thjune_inclusive_lumimask_trigger_xsecsf_pu_reweight_muon_SF_dijet_yes_btag_yes_DeltaR_yes_JER_jetabseta_24_yes_PV/'

# New file names

file_names = ['MET filters_pv_npvs.root', 'MET filters_leading_jet_QGL.root', 'MET filters_subleading_jet_QGL.root', 'MET filters_R_pT_hard.root', 'MET filters_z_star.root',
              'MET filters_leading_muon_pt.root', 'MET filters_leading_muon_abseta.root', 'MET filters_Delta_R_jets_l_mu.root', 'MET filters_Delta_R_jets_s_mu.root',
              'MET filters_subleading_muon_pt.root', 'MET filters_subleading_muon_abseta.root',
              'MET filters_leading_jet_pt.root', 'MET filters_leading_jet_abseta.root', 'MET filters_subleading_jet_pt.root', 'MET filters_subleading_jet_abseta.root', 
              'MET filters_Dijet_pt.root', 'MET filters_Dijet_abseta.root', 'MET filters_Dijet_mass.root', 'MET filters_Dijet_phi.root', 
              'MET filters_number_of_jets.root', 'MET filters_number_of_low_pT_jets.root', 'MET filters_dphijj.root', 'MET filters_detajj.root', 'MET filters_detall.root', 'MET filters_dphill.root',
              'MET filters_Zboson_pt.root', 'MET filters_Zboson_mass.root', 'MET filters_Zboson_abseta.root', 'MET filters_Zboson_phi.root', 
              'MET filters_DphiZ_l_jet.root', 'MET filters_DphiZ_s_jet.root', 'MET filters_DphiZ_jets.root']
"""
file_names = ['MET filters_number_of_jets_30.root', 'MET filters_number_of_jets_50.root', 'MET filters_number_of_jets_70.root', 'MET filters_number_of_jets_90.root',
              'MET filters_number_of_jets_150.root', 'MET filters_number_of_jets_200.root', 'MET filters_number_of_jets_250.root', 'MET filters_Zboson_absrapidity.root',
              'MET filters_Zboson_rapidity.root']
"""
#file_names = ['MET filters_leading_muon_eta.root', 'MET filters_subleading_muon_eta.root', 'MET filters_leading_jet_eta.root', 'MET filters_subleading_jet_eta.root']
file_names = ['MET filters_Zboson_pt.root', 'MET filters_Zboson_mass.root']
#file_names = ['Dimuon_cut_Zboson_mass.root', 'b-tag_veto_Zboson_mass.root', 'Number_of_jets_Zboson_mass.root', 'Dijet_cut_Zboson_mass.root', 'MET filters_Zboson_mass.root', 'MET filters_leading_jet_abseta.root', 'MET filters_subleading_jet_abseta.root', 'MET filters_Dijet_mass.root',  'MET filters_Zboson_pt.root']
#file_names = ['Dimuon_cut_Zboson_pt.root', 'Dimuon_cut_leading_jet_abseta.root', 'Dimuon_cut_subleading_jet_abseta.root', 'Dimuon_cut_leading_jet_pt.root', 'Dimuon_cut_subleading_jet_pt.root', 'Dimuon_cut_leading_muon_pt.root', 'Dimuon_cut_subleading_muon_pt.root']
#file_names = ['Dijet_cut_dphijj.root', 'Dijet_cut_dphill.root', 'Dijet_cut_DphiZ_l_jet.root', 'Dijet_cut_DphiZ_s_jet.root', 'Dijet_cut_DphiZ_jets.root']
#file_names = ['Number_of_jets_Zboson_mass.root', 'Number_of_jets_Zboson_pt.root', 'Number_of_jets_leading_jet_pt.root']
#file_names = ['Dijet_cut_Delta_R_jets_l_mu.root', 'Dijet_cut_Delta_R_jets_s_mu.root', 'Dijet_cut_Zboson_mass.root', 'Dijet_cut_pv_npvs.root', 'PV_pv_npvs.root', 'Dijet_cut_Dijet_pt.root']
"""
samples = ['EWK_LLJJ_MLL-50_MJJ-120_TuneCP5_13TeV-madgraph-pythia8_dipole', 'WW_TuneCP5_13TeV-pythia8', 'WZ_TuneCP5_13TeV-pythia8', 'ZZ_TuneCP5_13TeV-pythia8',
           'WWTo2L2Nu_TuneCP5_13TeV-powheg-pythia8', 'WGToLNuG_TuneCP5_13TeV-madgraphMLM-pythia8', 'ZGToLLG_01J_5f_TuneCP5_13TeV-amcatnloFXFX-pythia8', 'ZZZ_TuneCP5_13TeV-amcatnlo-pythia8',
           'WZZ_TuneCP5_13TeV-amcatnlo-pythia8', 'WWZ_4F_TuneCP5_13TeV-amcatnlo-pythia8', 
           'ST_tW_top_5f_inclusiveDecays_TuneCP5_13TeV-powheg-pythia8', 'ST_t-channel_top_4f_InclusiveDecays_TuneCP5_13TeV-powheg-madspin-pythia8',
           'ST_tW_antitop_5f_inclusiveDecays_TuneCP5_13TeV-powheg-pythia8','ST_t-channel_antitop_4f_InclusiveDecays_TuneCP5_13TeV-powheg-madspin-pythia8',
           'ST_s-channel_4f_leptonDecays_TuneCP5_13TeV-amcatnlo-pythia8', 'TTTo2L2Nu_TuneCP5_13TeV-powheg-pythia8','TTToSemiLeptonic_TuneCP5_13TeV-powheg-pythia8', 
           'DYJetsToLL_M-50_TuneCP5_13TeV-amcatnloFXFX-pythia8', 'SingleMuon' ]
"""
samples = ['EWK_LLJJ_MLL-50_MJJ-120_TuneCP5_13TeV-madgraph-pythia8_dipole', 'WW_TuneCP5_13TeV-pythia8', 'WZ_TuneCP5_13TeV-pythia8', 'ZZ_TuneCP5_13TeV-pythia8',
           'ST_tW_top_5f_inclusiveDecays_TuneCP5_13TeV-powheg-pythia8', 'ST_t-channel_top_4f_InclusiveDecays_TuneCP5_13TeV-powheg-madspin-pythia8',
           'ST_tW_antitop_5f_inclusiveDecays_TuneCP5_13TeV-powheg-pythia8','ST_t-channel_antitop_4f_InclusiveDecays_TuneCP5_13TeV-powheg-madspin-pythia8',
           'ST_s-channel_4f_leptonDecays_TuneCP5_13TeV-amcatnlo-pythia8', 'TTTo2L2Nu_TuneCP5_13TeV-powheg-pythia8','TTToSemiLeptonic_TuneCP5_13TeV-powheg-pythia8', 
           'DYJetsToLL_M-50_TuneCP5_13TeV-amcatnloFXFX-pythia8', 'SingleMuon' ]

for file_name in file_names: 
    mc_histograms = [None]*(len(samples)-1)
    
    for i in range(len(mc_histograms)):
        open_file = uproot.open(inputdir + file_name)
        mc_histograms[i] = open_file[samples[i]].to_hist()
        data_histogram = open_file['SingleMuon'].to_hist()
        scaled_mc_signal= open_file['EWK_LLJJ_MLL-50_MJJ-120_TuneCP5_13TeV-madgraph-pythia8_dipole'].to_hist()*100    

    # Set style
    plt.style.use(hep.style.ROOT)
    plt.rcParams.update({'font.size': 14})   
    plt.rcParams.update({'axes.titlesize':20})
    plt.rcParams.update({'axes.labelsize':20})

    # Set stack and ratio plot
    fig, ax = plt.subplots(2, 1, gridspec_kw={'height_ratios': [2, 1], 'hspace':0}, sharex=True)

    # Make stack plot in upper axis
    stack_axis = ax[0]
    color = ['firebrick', 'lightcoral', 'lightcoral', 'lightcoral', 
             'tomato', 'tomato', 'tomato', 'tomato', 'tomato', 'chocolate', 'chocolate', 'darksalmon'] 
    legend_names = lenged_names = [r'EWK $Z\rightarrow\mu^{+}\mu^{-}$', 'VV', '_nolegend_', '_nolegend_', 
                                   'Single top','_nolegend_', '_nolegend_', '_nolegend_', '_nolegend_',
                                   r'TT', '_nolegend_', r'DY $Z\rightarrow \mu^{+}\mu^{-}$']
  
    hep.histplot(mc_histograms, color = color, ax=stack_axis, binwnorm=1., stack=True, histtype='fill', label = legend_names)
    hep.histplot(scaled_mc_signal, color = 'slategray', ax=stack_axis, binwnorm=1., stack=False, histtype='step', label = r'EWK $Z\rightarrow\mu^{+}\mu^{-} * 10^2$')
    hep.histplot(data_histogram, ax=stack_axis, binwnorm=1., stack=False, histtype='errorbar', color='black', label='Data')
    stack_axis.legend()
    stack_axis.set_yscale('log')
    stack_axis.set_ylabel(r'Events/bin')
    
    # Ratio axis
    ratio_axis = ax[1]
    ratio_axis.axhline(y=1, color='black')
    # Sum stack
    sum_mc_histos = sum(mc_histograms)
    print('total mc value events (integral)', sum(sum_mc_histos.values()))
    #print(sum_mc_histos.values())
    
    # Calculate ratio Data/MC
    wratio, bins, werr = make_ratio(data_histogram, sum_mc_histos)
    print('ratio') 
    print(wratio)
    wratioMC, binsMC, werrMC = make_ratio(sum_mc_histos, sum_mc_histos)
    hep.histplot(wratio, bins, yerr = (werr), stack=False, histtype='errorbar', color='black', ax = ratio_axis, label = 'Statistical data uncertainty')
    hep.histplot(np.ones(len(wratio)), bins, yerr = werrMC, stack=False, histtype='errorbar', marker = 'None', color='black', ax = ratio_axis, label = 'Statistical MC uncertainty')
    ratio_axis.legend() 

    if file_name == 'MET filters_Delta_R_jets_l_mu.root':
        ratio_axis.set_xlabel('$\\Delta R_{\\mu_{L} , jets}$')
    if file_name == 'MET filters_Delta_R_jets_s_mu.root':
        ratio_axis.set_xlabel('$\\Delta R_{\\mu_{S} , jets}$')
    if file_name == 'MET filters_pv_npvsGood.root':
        ratio_axis.set_xlabel('Number of good primary vertices')
    if file_name == 'MET filters_leading_muon_pt.root':
        ratio_axis.set_xlabel('Leading muon transverse momentum: $p_{T}$ [GeV]')   
    elif file_name == 'MET filters_leading_muon_abseta.root':
        ratio_axis.set_xlabel('Leading muon $|\eta|$')
    elif file_name == 'MET filters_Delta_R_l_mu_l_jet.root':
        ratio_axis.set_xlabel(' $\\Delta R_{\\mu_{L} , jet_{L}}$')
    elif file_name == 'MET filters_Delta_R_l_mu_s_jet.root':
        ratio_axis.set_xlabel('$\\Delta R_{\\mu_{L} , jet_{S}}$')
    elif file_name == 'MET filters_Delta_R_l_jet.root':
        ratio_axis.set_xlabel(' $\\Delta R_{j_{L} , \\mu \\mu}$')
    elif file_name == 'MET filters_Delta_R_s_jet.root':
        ratio_axis.set_xlabel('$\\Delta R_{j_{S} , \\mu \\mu}$')
    elif file_name == 'MET filters_Delta_phi_l_mu_l_jet.root':
        ratio_axis.set_xlabel(' $\\Delta \\phi_{\\mu_{L}, jet_{L}}$')
    elif file_name == 'MET filters_Delta_phi_l_mu_s_jet.root':
        ratio_axis.set_xlabel('$\\Delta \\phi_{\\mu_{L}, jet_{S}}$')
    elif file_name == 'MET filters_subleading_muon_pt.root':
        ratio_axis.set_xlabel('Subleading muon transverse momentum: $p_{T}$ [GeV]')
    elif file_name == 'MET filters_subleading_muon_abseta.root':
        ratio_axis.set_xlabel('Subleading muon $|\eta|$')
    elif file_name == 'MET filters_leading_jet_pt.root':
        ratio_axis.set_xlabel('Leading jet transverse momentum: $p_{T}$ [GeV]')
    elif file_name == 'MET filters_Delta_R_s_mu_l_jet.root':
        ratio_axis.set_xlabel(' $\\Delta R_{\\mu_{S} , jet_{L}}$')
    elif file_name == 'MET filters_Delta_R_s_mu_s_jet.root':
        ratio_axis.set_xlabel('$\\Delta R_{\\mu_{S} , jet_{S}}$')
    elif file_name == 'MET filters_Delta_phi_s_mu_l_jet.root':
        ratio_axis.set_xlabel(' $\\Delta \\phi_{\\mu_{S}, jet_{L}}$')
    elif file_name == 'MET filters_Delta_phi_s_mu_s_jet.root':
        ratio_axis.set_xlabel('$\\Delta \\phi_{\\mu_{S}, jet_{S}}$')
    elif file_name == 'MET filters_leading_jet_abseta.root':
        ratio_axis.set_xlabel('Leading jet $|\eta|$')
    elif file_name == 'MET filters_subleading_jet_pt.root':
        ratio_axis.set_xlabel('Subleading jet transverse momentum: $p_{T}$ [GeV]')
    elif file_name == 'MET filters_subleading_jet_abseta.root':
        ratio_axis.set_xlabel('Subleading jet $|\eta|$')
    elif (file_name == 'MET filters_mjj.root') or (file_name == 'MET filters_Dijet_mass.root'):
        ratio_axis.set_xlabel('Dijet mass: $m_{jj}$ [GeV]', fontsize = 20)
    elif (file_name == 'MET filters_pTjj.root') or (file_name == 'MET filters_Dijet_pt.root'):
        ratio_axis.set_xlabel('$p_{Tjj}$ [GeV]', fontsize = 20)
    elif file_name == 'MET filters_Dijet_phi.root':
        ratio_axis.set_xlabel(' $\\phi_{jj}$ ', fontsize = 20)
    elif file_name == 'MET filters_Dijet_abseta.root':
        ratio_axis.set_xlabel(' $ | \\eta_{jj} | $ ', fontsize = 20)
    elif file_name == 'MET filters_Zboson_mass.root':
        ratio_axis.set_xlabel('Z boson mass: $m_{l^{+}l^{-}}$ [GeV]', fontsize = 20)
    elif file_name == 'MET filters_Zboson_pt.root':
        ratio_axis.set_xlabel('Z boson transverse momentum: $p_{T}$ [GeV]', fontsize = 20)
    elif file_name == 'MET filters_Zboson_abseta.root':
        ratio_axis.set_xlabel('$|\\eta|_{l^{+}l^{-}}$ ', fontsize = 20)
    elif file_name == 'MET filters_Zboson_phi.root':
        ratio_axis.set_xlabel('$|\\phi|_{l^{+}l^{-}}$ ', fontsize = 20)
    elif file_name == 'MET filters_number_of_jets.root':
        ratio_axis.set_xlabel('Number of jets')
    elif file_name == 'MET filters_number_of_low_pT_jets.root':
        ratio_axis.set_xlabel('Number of low pT jets')
    elif file_name == 'MET filters_DphiZ_jets.root':
        ratio_axis.set_xlabel(' $\\Delta \\phi_{Z, jj}$ ', fontsize = 20)
    elif file_name == 'MET filters_DphiZ_l_jet.root':
        ratio_axis.set_xlabel(' $\\Delta \\phi_{Z, j_{L}}$ ', fontsize = 20)
    elif file_name == 'MET filters_DphiZ_s_jet.root':
        ratio_axis.set_xlabel(' $\\Delta \\phi_{Z, j_{S}}$ ', fontsize = 20)
    elif file_name == 'MET filters_Delta_phi_l_jet_mus.root':
        ratio_axis.set_xlabel(' $\\Delta \\phi_{j_{L}, \\mu \\mu}$ ', fontsize = 20)
    elif file_name == 'MET filters_Delta_phi_s_jet_mus.root':
        ratio_axis.set_xlabel(' $\\Delta \\phi_{j_{S}, \\mu \\mu}$ ', fontsize = 20)
    elif file_name == 'MET filters_dphill.root':
        ratio_axis.set_xlabel(' $\\Delta \\phi_{\\mu \\mu}$ ', fontsize = 20)
    elif file_name == 'MET filters_dphijj.root':
        ratio_axis.set_xlabel(' $\\Delta \\phi_{j_{L} j_{S}}$ ', fontsize = 20)
    elif file_name == 'MET filters_detall.root':
        ratio_axis.set_xlabel(' $\\Delta \\eta_{\\mu \\mu}$ ', fontsize = 20)
    elif file_name == 'MET filters_detajj.root':
        ratio_axis.set_xlabel(' $\\Delta \\eta_{j_{L} j_{S}}$ ',fontsize = 20)
    elif file_name == 'MET filters_R_pT_hard.root':
        ratio_axis.set_xlabel('$R_{p_{T}}^{hard}$ ', fontsize = 20)
    elif file_name == 'MET filters_z_star.root':
        ratio_axis.set_xlabel(' $z^*$ ')
    elif file_name == 'MET filters_pv_npvs.root':
        ratio_axis.set_xlabel('Number of total reconstructed  primary vertices')
    elif file_name == 'Muon_SF_pv_npvs.root':
        ratio_axis.set_xlabel('Number of good primary vertices')
    elif file_name == 'Pileup reweighting_pv_npvs.root':
        ratio_axis.set_xlabel('Number of total reconstructed primary vertices')
    elif file_name == 'MET filters_QGD.root':
        ratio_axis.set_xlabel('QGL Discriminant')
    elif file_name == 'MET filters_Delta_R_jets_l_mu.root':
        ratio_axis.set_xlabel(' $R_{jets, \\mu_{L}}$ ')
    elif file_name == 'MET filters_Delta_R_jets_s_mu.root':
        ratio_axis.set_xlabel(' $R_{jets, \\mu_{S}}$ ')
    elif file_name == 'MET filters_leading_jet_QGL.root':
        ratio_axis.set_xlabel('Leading jet QGL', fontsize = 20)
    elif file_name == 'MET filters_subleading_jet_QGL.root':
        ratio_axis.set_xlabel('Subleading jet QGL', fontsize = 20)
    #ratio_axis.set_xlabel(file_name+'[GeV]')
    ratio_axis.set_ylabel(r'Data/MC')
    # ratio y_axis (0.8, 1.2)
    ratio_axis.set_ylim(0.5, 1.5)
    #ratio_axis.set_ylim(-0.5, 0.5)
    #hep.cms.label(loc=0, data=True, ax=stack_axis, lumi=59.83, year=2018)
    hep.cms.label(loc=0, data=True, ax=stack_axis, lumi=41.48, year=2017)
    
    #plt.savefig('/afs/desy.de/user/m/moralfk/www/EWZjj-test-figures/NEW_DetectorLevelPlots/Pepper_muon_ALL_18thDecember_nominal_MuMu_2017_test_nojetveto_nobtagSF_noPUid_'+file_name+'.png')
    #plt.savefig('/afs/desy.de/user/m/moralfk/www/EWZjj-test-figures/NEW_DetectorLevelPlots/Pepper_muon_8thjune_goodPV_'+file_name+'.png')    
    #plt.savefig('/afs/desy.de/user/m/moralfk/www/EWZjj-test-figures/NEW_DetectorLevelPlots/Pepper_muon_ALL_23rdjune_sys_'+file_name+'.png')
    #plt.savefig('/afs/desy.de/user/m/moralfk/www/EWZjj-test-figures/NEW_DetectorLevelPlots/Pepper_muon_ALL_8thOctober_Mu_detajj15_mjj400_dphijj1_dphill28_pTll30_etastarcond_'+file_name+'.png')
    #plt.savefig('/afs/desy.de/user/m/moralfk/www/EWZjj-test-figures/NEW_DetectorLevelPlots/Pepper_muon_ALL_19thOctober_Mu_detajj4_mjj400_dphill28_mu_eta_cond_nobtagSF_'+file_name+'.png')i
    #plt.savefig('/afs/desy.de/user/m/moralfk/www/EWZjj-test-figures/NEW_DetectorLevelPlots/Pepper_muon_ALL_5thNovember_detajj4_mjj_dphill_pt_ordered_jets_pTj50_mjj200_Detajj15_Zetacuts_nobtagSF_'+file_name+'.png')
    plt.show() 
