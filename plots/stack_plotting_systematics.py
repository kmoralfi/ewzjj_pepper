import matplotlib.pyplot as plt
import mplhep as hep
import uproot
import matplotlib.gridspec as gridspec
import numpy as np
import matplotlib.patches as patches
import matplotlib.path as path
import hist
from hist import Hist
#from pathlib import Path
#from pathlib import PathPatch
#DY.root  EWK_LLJJ_MLL.root  SingleMuon.root

def make_ratio(hnum, hden):
  wnum, bins = hnum.to_numpy()
  wden, bins = hden.to_numpy()

  numerr = np.sqrt(hnum.variances())
  
  #wratio = np.divide(wnum, wden) - 1
  wratio = np.divide(wnum, wden)
  print('data')
  print(wnum)
  print('mc')
  print(wden)
  ratioerr = np.divide(numerr, wden)
  bincenters = (bins[:-1] + bins[1:]) / 2
  return wratio, bins, ratioerr

def systematics(mc_nom_file, sys_up_file, sys_down_file):
    sys_up_f = 0
    sys_down_f = 0
    for mc_nom, sys_up_value, sys_down_value in zip(mc_nom_file, sys_up_file, sys_down_file):
        mc_nom = hist.Hist(mc_nom)
        sys_up_value = hist.Hist(sys_up_value)
        sys_down_value = hist.Hist(sys_down_value)
        sys_up_temp = ((sys_up_value.values() - mc_nom.values())**2)
        sys_down_temp = ((sys_down_value.values() - mc_nom.values())**2)
        sys_up_f = sys_up_temp + sys_up_f
        sys_down_f = sys_down_temp + sys_down_f
    sys_up = np.sqrt(sys_up_f)
    sys_down = np.sqrt(sys_down_f)
    
    return sys_up, sys_down

def total_systematics(sys_up, sys_down):
    sys_up_f = 0
    sys_down_f = 0
    for sys_up_partial, sys_down_partial in zip(sys_up, sys_down):
        sys_up_temp = (sys_up_partial**2)
        sys_down_temp = (sys_down_partial**2)
        sys_up_f = sys_up_temp + sys_up_f
        sys_down_f = sys_down_temp + sys_down_f
    total_sys_up = np.sqrt(sys_up_f)
    total_sys_down = np.sqrt(sys_down_f)
    return total_sys_up, total_sys_down

def systematics_ratio(sys_up, sys_down, mc_nom_stacked):
    ratio_up_sys = 1 + sys_up/mc_nom_stacked  
    ratio_down_sys = 1 - sys_down/mc_nom_stacked
    return ratio_up_sys, ratio_down_sys

# Load histograms

# General results directory
#inputdir = '/nfs/dust/cms/user/moralfk/EWZjj_Pepper/ALL_8thjune_newDeltaR_QGL_good_b/'
#inputdir = '/nfs/dust/cms/user/moralfk/EWZjj_Pepper/ALL_9thjune_yes_JER_good_Delta_phi_better_binning/'
#########inputdir =  '/nfs/dust/cms/user/moralfk/EWZjj_Pepper/ALL_24thseptember_check_sys/'
inputdir =  '/nfs/dust/cms/user/moralfk/EWZjj_Pepper/hists/'
#inputdir =  '/nfs/dust/cms/user/moralfk/EWZjj_Pepper/ALL_12thjune_all_systematics/'
#inputdir = '/nfs/dust/cms/user/moralfk/EWZjj_Pepper/ALL_7thjune_inclusive_lumimask_trigger_xsecsf_pu_reweight_muon_SF_dijet_yes_btag_yes_DeltaR_yes_JER_jetabseta_24/'
#inputdir = '/nfs/dust/cms/user/moralfk/EWZjj_Pepper/ALL_7thjune_inclusive_lumimask_trigger_xsecsf_pu_reweight_muon_SF_dijet_yes_btag_yes_DeltaR_yes_JER_jetabseta_24_yes_PV/'

# New file names
file_names = ['MET filters_pv_npvs.root', 'MET filters_pv_npvsGood.root', 'MET filters_leading_jet_QGL.root', 'MET filters_subleading_jet_QGL.root', 'MET filters_R_pT_hard.root', 'MET filters_z_star.root',
              'MET filters_leading_muon_pt.root', 'MET filters_leading_muon_abseta.root', 'MET filters_Delta_R_jets_l_mu.root', 'MET filters_Delta_R_jets_s_mu.root', 
              'MET filters_subleading_muon_pt.root', 'MET filters_subleading_muon_abseta.root',
              'MET filters_leading_jet_pt.root', 'MET filters_leading_jet_abseta.root', 'MET filters_subleading_jet_pt.root', 'MET filters_subleading_jet_abseta.root', 
              'MET filters_Dijet_pt.root', 'MET filters_Dijet_abseta.root', 'MET filters_Dijet_mass.root', 'MET filters_Dijet_phi.root', 
              'MET filters_number_of_jets.root', 'MET filters_number_of_low_pT_jets.root', 'MET filters_dphijj.root', 'MET filters_detajj.root', 'MET filters_detall.root', 'MET filters_dphill.root',
              'MET filters_Zboson_pt.root', 'MET filters_Zboson_mass.root', 'MET filters_Zboson_abseta.root', 'MET filters_Zboson_phi.root', 
              'MET filters_DphiZ_l_jet.root', 'MET filters_DphiZ_s_jet.root', 'MET filters_DphiZ_jets.root']

files_PU_up_systematics = ['MET filters_pv_npvs_pileup_up.root', 'MET filters_pv_npvsGood_pileup_up.root', 'MET filters_leading_jet_QGL_pileup_up.root', 'MET filters_subleading_jet_QGL_pileup_up.root', 
              'MET filters_R_pT_hard_pileup_up.root', 'MET filters_z_star_pileup_up.root',
              'MET filters_leading_muon_pt_pileup_up.root', 'MET filters_leading_muon_abseta_pileup_up.root', 'MET filters_Delta_R_jets_l_mu_pileup_up.root', 'MET filters_Delta_R_jets_s_mu_pileup_up.root',
              'MET filters_subleading_muon_pt_pileup_up.root', 'MET filters_subleading_muon_abseta_pileup_up.root',
              'MET filters_leading_jet_pt_pileup_up.root', 'MET filters_leading_jet_abseta_pileup_up.root', 'MET filters_subleading_jet_pt_pileup_up.root', 'MET filters_subleading_jet_abseta_pileup_up.root',
              'MET filters_Dijet_pt_pileup_up.root', 'MET filters_Dijet_abseta_pileup_up.root', 'MET filters_Dijet_mass_pileup_up.root', 'MET filters_Dijet_phi_pileup_up.root',
              'MET filters_number_of_jets_pileup_up.root', 'MET filters_number_of_low_pT_jets_pileup_up.root', 'MET filters_dphijj_pileup_up.root', 'MET filters_detajj_pileup_up.root', 
              'MET filters_detall_pileup_up.root', 'MET filters_dphill_pileup_up.root',
              'MET filters_Zboson_pt_pileup_up.root', 'MET filters_Zboson_mass_pileup_up.root', 'MET filters_Zboson_abseta_pileup_up.root', 'MET filters_Zboson_phi_pileup_up.root',
              'MET filters_DphiZ_l_jet_pileup_up.root', 'MET filters_DphiZ_s_jet_pileup_up.root', 'MET filters_DphiZ_jets_pileup_up.root'] 

files_PU_down_systematics = ['MET filters_pv_npvs_pileup_down.root', 'MET filters_pv_npvsGood_pileup_down.root', 'MET filters_leading_jet_QGL_pileup_down.root', 'MET filters_subleading_jet_QGL_pileup_down.root', 
              'MET filters_R_pT_hard_pileup_down.root', 'MET filters_z_star_pileup_down.root',
              'MET filters_leading_muon_pt_pileup_down.root', 'MET filters_leading_muon_abseta_pileup_down.root', 'MET filters_Delta_R_jets_l_mu_pileup_down.root', 'MET filters_Delta_R_jets_s_mu_pileup_down.root',
              'MET filters_subleading_muon_pt_pileup_down.root', 'MET filters_subleading_muon_abseta_pileup_down.root',
              'MET filters_leading_jet_pt_pileup_down.root', 'MET filters_leading_jet_abseta_pileup_down.root', 'MET filters_subleading_jet_pt_pileup_down.root', 
              'MET filters_subleading_jet_abseta_pileup_down.root',
              'MET filters_Dijet_pt_pileup_down.root', 'MET filters_Dijet_abseta_pileup_down.root', 'MET filters_Dijet_mass_pileup_down.root', 'MET filters_Dijet_phi_pileup_down.root',
              'MET filters_number_of_jets_pileup_down.root', 'MET filters_number_of_low_pT_jets_pileup_down.root', 'MET filters_dphijj_pileup_down.root', 'MET filters_detajj_pileup_down.root', 
              'MET filters_detall_pileup_down.root', 'MET filters_dphill_pileup_down.root',
              'MET filters_Zboson_pt_pileup_down.root', 'MET filters_Zboson_mass_pileup_down.root', 'MET filters_Zboson_abseta_pileup_down.root', 'MET filters_Zboson_phi_pileup_down.root',
              'MET filters_DphiZ_l_jet_pileup_down.root', 'MET filters_DphiZ_s_jet_pileup_down.root', 'MET filters_DphiZ_jets_pileup_down.root']

files_btag_up_systematics = ['MET filters_pv_npvs_btagsf0_up.root', 'MET filters_pv_npvsGood_btagsf0_up.root', 'MET filters_leading_jet_QGL_btagsf0_up.root', 'MET filters_subleading_jet_QGL_btagsf0_up.root', 
              'MET filters_R_pT_hard_btagsf0_up.root', 'MET filters_z_star_btagsf0_up.root',
              'MET filters_leading_muon_pt_btagsf0_up.root', 'MET filters_leading_muon_abseta_btagsf0_up.root', 'MET filters_Delta_R_jets_l_mu_btagsf0_up.root', 'MET filters_Delta_R_jets_s_mu_btagsf0_up.root',
              'MET filters_subleading_muon_pt_btagsf0_up.root', 'MET filters_subleading_muon_abseta_btagsf0_up.root',
              'MET filters_leading_jet_pt_btagsf0_up.root', 'MET filters_leading_jet_abseta_btagsf0_up.root', 'MET filters_subleading_jet_pt_btagsf0_up.root', 
              'MET filters_subleading_jet_abseta_btagsf0_up.root',
              'MET filters_Dijet_pt_btagsf0_up.root', 'MET filters_Dijet_abseta_btagsf0_up.root', 'MET filters_Dijet_mass_btagsf0_up.root', 'MET filters_Dijet_phi_btagsf0_up.root',
              'MET filters_number_of_jets_btagsf0_up.root', 'MET filters_number_of_low_pT_jets_btagsf0_up.root', 'MET filters_dphijj_btagsf0_up.root', 'MET filters_detajj_btagsf0_up.root', 
              'MET filters_detall_btagsf0_up.root', 'MET filters_dphill_btagsf0_up.root',
              'MET filters_Zboson_pt_btagsf0_up.root', 'MET filters_Zboson_mass_btagsf0_up.root', 'MET filters_Zboson_abseta_btagsf0_up.root', 'MET filters_Zboson_phi_btagsf0_up.root',
              'MET filters_DphiZ_l_jet_btagsf0_up.root', 'MET filters_DphiZ_s_jet_btagsf0_up.root', 'MET filters_DphiZ_jets_btagsf0_up.root']

files_btag_down_systematics = ['MET filters_pv_npvs_btagsf0_down.root', 'MET filters_pv_npvsGood_btagsf0_down.root', 'MET filters_leading_jet_QGL_btagsf0_down.root', 'MET filters_subleading_jet_QGL_btagsf0_down.root', 
              'MET filters_R_pT_hard_btagsf0_down.root', 'MET filters_z_star_btagsf0_down.root',
              'MET filters_leading_muon_pt_btagsf0_down.root', 'MET filters_leading_muon_abseta_btagsf0_down.root', 'MET filters_Delta_R_jets_l_mu_btagsf0_down.root', 'MET filters_Delta_R_jets_s_mu_btagsf0_down.root',
              'MET filters_subleading_muon_pt_btagsf0_down.root', 'MET filters_subleading_muon_abseta_btagsf0_down.root',
              'MET filters_leading_jet_pt_btagsf0_down.root', 'MET filters_leading_jet_abseta_btagsf0_down.root', 'MET filters_subleading_jet_pt_btagsf0_down.root', 
              'MET filters_subleading_jet_abseta_btagsf0_down.root',
              'MET filters_Dijet_pt_btagsf0_down.root', 'MET filters_Dijet_abseta_btagsf0_down.root', 'MET filters_Dijet_mass_btagsf0_down.root', 'MET filters_Dijet_phi_btagsf0_down.root',
              'MET filters_number_of_jets_btagsf0_down.root', 'MET filters_number_of_low_pT_jets_btagsf0_down.root', 'MET filters_dphijj_btagsf0_down.root', 'MET filters_detajj_btagsf0_down.root', 
              'MET filters_detall_btagsf0_down.root', 'MET filters_dphill_btagsf0_down.root',
              'MET filters_Zboson_pt_btagsf0_down.root', 'MET filters_Zboson_mass_btagsf0_down.root', 'MET filters_Zboson_abseta_btagsf0_down.root', 'MET filters_Zboson_phi_btagsf0_down.root',
              'MET filters_DphiZ_l_jet_btagsf0_down.root', 'MET filters_DphiZ_s_jet_btagsf0_down.root', 'MET filters_DphiZ_jets_btagsf0_down.root']

files_JER_up_systematics = ['MET filters_pv_npvs_Jer_up.root', 'MET filters_pv_npvsGood_Jer_up.root', 'MET filters_leading_jet_QGL_Jer_up.root', 'MET filters_subleading_jet_QGL_Jer_up.root', 
              'MET filters_R_pT_hard_Jer_up.root', 'MET filters_z_star_Jer_up.root',
              'MET filters_leading_muon_pt_Jer_up.root', 'MET filters_leading_muon_abseta_Jer_up.root', 'MET filters_Delta_R_jets_l_mu_Jer_up.root', 'MET filters_Delta_R_jets_s_mu_Jer_up.root',
              'MET filters_subleading_muon_pt_Jer_up.root', 'MET filters_subleading_muon_abseta_Jer_up.root',
              'MET filters_leading_jet_pt_Jer_up.root', 'MET filters_leading_jet_abseta_Jer_up.root', 'MET filters_subleading_jet_pt_Jer_up.root', 'MET filters_subleading_jet_abseta_Jer_up.root',
              'MET filters_Dijet_pt_Jer_up.root', 'MET filters_Dijet_abseta_Jer_up.root', 'MET filters_Dijet_mass_Jer_up.root', 'MET filters_Dijet_phi_Jer_up.root',
              'MET filters_number_of_jets_Jer_up.root', 'MET filters_number_of_low_pT_jets_Jer_up.root', 'MET filters_dphijj_Jer_up.root', 'MET filters_detajj_Jer_up.root', 
              'MET filters_detall_Jer_up.root', 'MET filters_dphill_Jer_up.root',
              'MET filters_Zboson_pt_Jer_up.root', 'MET filters_Zboson_mass_Jer_up.root', 'MET filters_Zboson_abseta_Jer_up.root', 'MET filters_Zboson_phi_Jer_up.root',
              'MET filters_DphiZ_l_jet_Jer_up.root', 'MET filters_DphiZ_s_jet_Jer_up.root', 'MET filters_DphiZ_jets_Jer_up.root']

files_JER_down_systematics = ['MET filters_pv_npvs_Jer_down.root', 'MET filters_pv_npvsGood_Jer_down.root', 'MET filters_leading_jet_QGL_Jer_down.root', 'MET filters_subleading_jet_QGL_Jer_down.root', 
              'MET filters_R_pT_hard_Jer_down.root', 'MET filters_z_star_Jer_down.root',
              'MET filters_leading_muon_pt_Jer_down.root', 'MET filters_leading_muon_abseta_Jer_down.root', 'MET filters_Delta_R_jets_l_mu_Jer_down.root', 'MET filters_Delta_R_jets_s_mu_Jer_down.root',
              'MET filters_subleading_muon_pt_Jer_down.root', 'MET filters_subleading_muon_abseta_Jer_down.root',
              'MET filters_leading_jet_pt_Jer_down.root', 'MET filters_leading_jet_abseta_Jer_down.root', 'MET filters_subleading_jet_pt_Jer_down.root', 'MET filters_subleading_jet_abseta_Jer_down.root',
              'MET filters_Dijet_pt_Jer_down.root', 'MET filters_Dijet_abseta_Jer_down.root', 'MET filters_Dijet_mass_Jer_down.root', 'MET filters_Dijet_phi_Jer_down.root',
              'MET filters_number_of_jets_Jer_down.root', 'MET filters_number_of_low_pT_jets_Jer_down.root', 'MET filters_dphijj_Jer_down.root', 'MET filters_detajj_Jer_down.root', 
              'MET filters_detall_Jer_down.root', 'MET filters_dphill_Jer_down.root',
              'MET filters_Zboson_pt_Jer_down.root', 'MET filters_Zboson_mass_Jer_down.root', 'MET filters_Zboson_abseta_Jer_down.root', 'MET filters_Zboson_phi_Jer_down.root',
              'MET filters_DphiZ_l_jet_Jer_down.root', 'MET filters_DphiZ_s_jet_Jer_down.root', 'MET filters_DphiZ_jets_Jer_down.root']

files_junc_up_systematics = ['MET filters_pv_npvs_Junc_up.root', 'MET filters_pv_npvsGood_Junc_up.root', 'MET filters_leading_jet_QGL_Junc_up.root', 'MET filters_subleading_jet_QGL_Junc_up.root', 
              'MET filters_R_pT_hard_Junc_up.root', 'MET filters_z_star_Junc_up.root',
              'MET filters_leading_muon_pt_Junc_up.root', 'MET filters_leading_muon_abseta_Junc_up.root', 'MET filters_Delta_R_jets_l_mu_Junc_up.root', 'MET filters_Delta_R_jets_s_mu_Junc_up.root',
              'MET filters_subleading_muon_pt_Junc_up.root', 'MET filters_subleading_muon_abseta_Junc_up.root',
              'MET filters_leading_jet_pt_Junc_up.root', 'MET filters_leading_jet_abseta_Junc_up.root', 'MET filters_subleading_jet_pt_Junc_up.root', 'MET filters_subleading_jet_abseta_Junc_up.root',
              'MET filters_Dijet_pt_Junc_up.root', 'MET filters_Dijet_abseta_Junc_up.root', 'MET filters_Dijet_mass_Junc_up.root', 'MET filters_Dijet_phi_Junc_up.root',
              'MET filters_number_of_jets_Junc_up.root', 'MET filters_number_of_low_pT_jets_Junc_up.root', 'MET filters_dphijj_Junc_up.root', 'MET filters_detajj_Junc_up.root', 
              'MET filters_detall_Junc_up.root', 'MET filters_dphill_Junc_up.root',
              'MET filters_Zboson_pt_Junc_up.root', 'MET filters_Zboson_mass_Junc_up.root', 'MET filters_Zboson_abseta_Junc_up.root', 'MET filters_Zboson_phi_Junc_up.root',
              'MET filters_DphiZ_l_jet_Junc_up.root', 'MET filters_DphiZ_s_jet_Junc_up.root', 'MET filters_DphiZ_jets_Junc_up.root']

files_junc_down_systematics = ['MET filters_pv_npvs_Junc_down.root', 'MET filters_pv_npvsGood_Junc_down.root', 'MET filters_leading_jet_QGL_Junc_down.root', 'MET filters_subleading_jet_QGL_Junc_down.root', 
              'MET filters_R_pT_hard_Junc_down.root', 'MET filters_z_star_Junc_down.root',
              'MET filters_leading_muon_pt_Junc_down.root', 'MET filters_leading_muon_abseta_Junc_down.root', 'MET filters_Delta_R_jets_l_mu_Junc_down.root', 'MET filters_Delta_R_jets_s_mu_Junc_down.root',
              'MET filters_subleading_muon_pt_Junc_down.root', 'MET filters_subleading_muon_abseta_Junc_down.root',
              'MET filters_leading_jet_pt_Junc_down.root', 'MET filters_leading_jet_abseta_Junc_down.root', 'MET filters_subleading_jet_pt_Junc_down.root', 'MET filters_subleading_jet_abseta_Junc_down.root',
              'MET filters_Dijet_pt_Junc_down.root', 'MET filters_Dijet_abseta_Junc_down.root', 'MET filters_Dijet_mass_Junc_down.root', 'MET filters_Dijet_phi_Junc_down.root',
              'MET filters_number_of_jets_Junc_down.root', 'MET filters_number_of_low_pT_jets_Junc_down.root', 'MET filters_dphijj_Junc_down.root', 'MET filters_detajj_Junc_down.root', 
              'MET filters_detall_Junc_down.root', 'MET filters_dphill_Junc_down.root',
              'MET filters_Zboson_pt_Junc_down.root', 'MET filters_Zboson_mass_Junc_down.root', 'MET filters_Zboson_abseta_Junc_down.root', 'MET filters_Zboson_phi_Junc_down.root',
              'MET filters_DphiZ_l_jet_Junc_down.root', 'MET filters_DphiZ_s_jet_Junc_down.root', 'MET filters_DphiZ_jets_Junc_down.root']

files_btagsf0light_up_systematics = ['MET filters_pv_npvs_btagsf0light_up.root', 'MET filters_pv_npvsGood_btagsf0light_up.root', 'MET filters_leading_jet_QGL_btagsf0light_up.root',
              'MET filters_subleading_jet_QGL_btagsf0light_up.root',
              'MET filters_R_pT_hard_btagsf0light_up.root', 'MET filters_z_star_btagsf0light_up.root',
              'MET filters_leading_muon_pt_btagsf0light_up.root', 'MET filters_leading_muon_abseta_btagsf0light_up.root', 'MET filters_Delta_R_jets_l_mu_btagsf0light_up.root',
              'MET filters_Delta_R_jets_s_mu_btagsf0light_up.root',
              'MET filters_subleading_muon_pt_btagsf0light_up.root', 'MET filters_subleading_muon_abseta_btagsf0light_up.root',
              'MET filters_leading_jet_pt_btagsf0light_up.root', 'MET filters_leading_jet_abseta_btagsf0light_up.root', 'MET filters_subleading_jet_pt_btagsf0light_up.root',
              'MET filters_subleading_jet_abseta_btagsf0light_up.root',
              'MET filters_Dijet_pt_btagsf0light_up.root', 'MET filters_Dijet_abseta_btagsf0light_up.root', 'MET filters_Dijet_mass_btagsf0light_up.root', 'MET filters_Dijet_phi_btagsf0light_up.root',
              'MET filters_number_of_jets_btagsf0light_up.root', 'MET filters_number_of_low_pT_jets_btagsf0light_up.root', 'MET filters_dphijj_btagsf0light_up.root',
              'MET filters_detajj_btagsf0light_up.root',
              'MET filters_detall_btagsf0light_up.root', 'MET filters_dphill_btagsf0light_up.root',
              'MET filters_Zboson_pt_btagsf0light_up.root', 'MET filters_Zboson_mass_btagsf0light_up.root', 'MET filters_Zboson_abseta_btagsf0light_up.root', 'MET filters_Zboson_phi_btagsf0light_up.root',
              'MET filters_DphiZ_l_jet_btagsf0light_up.root', 'MET filters_DphiZ_s_jet_btagsf0light_up.root', 'MET filters_DphiZ_jets_btagsf0light_up.root']

files_btagsf0light_down_systematics = ['MET filters_pv_npvs_btagsf0light_down.root', 'MET filters_pv_npvsGood_btagsf0light_down.root', 'MET filters_leading_jet_QGL_btagsf0light_down.root',
              'MET filters_subleading_jet_QGL_btagsf0light_down.root',
              'MET filters_R_pT_hard_btagsf0light_down.root', 'MET filters_z_star_btagsf0light_down.root',
              'MET filters_leading_muon_pt_btagsf0light_down.root', 'MET filters_leading_muon_abseta_btagsf0light_down.root', 'MET filters_Delta_R_jets_l_mu_btagsf0light_down.root',
              'MET filters_Delta_R_jets_s_mu_btagsf0light_down.root',
              'MET filters_subleading_muon_pt_btagsf0light_down.root', 'MET filters_subleading_muon_abseta_btagsf0light_down.root',
              'MET filters_leading_jet_pt_btagsf0light_down.root', 'MET filters_leading_jet_abseta_btagsf0light_down.root', 'MET filters_subleading_jet_pt_btagsf0light_down.root',
              'MET filters_subleading_jet_abseta_btagsf0light_down.root',
              'MET filters_Dijet_pt_btagsf0light_down.root', 'MET filters_Dijet_abseta_btagsf0light_down.root', 'MET filters_Dijet_mass_btagsf0light_down.root', 'MET filters_Dijet_phi_btagsf0light_down.root',
              'MET filters_number_of_jets_btagsf0light_down.root', 'MET filters_number_of_low_pT_jets_btagsf0light_down.root', 'MET filters_dphijj_btagsf0light_down.root',
              'MET filters_detajj_btagsf0light_down.root',
              'MET filters_detall_btagsf0light_down.root', 'MET filters_dphill_btagsf0light_down.root',
              'MET filters_Zboson_pt_btagsf0light_down.root', 'MET filters_Zboson_mass_btagsf0light_down.root', 'MET filters_Zboson_abseta_btagsf0light_down.root',
              'MET filters_Zboson_phi_btagsf0light_down.root',
              'MET filters_DphiZ_l_jet_btagsf0light_down.root', 'MET filters_DphiZ_s_jet_btagsf0light_down.root', 'MET filters_DphiZ_jets_btagsf0light_down.root']

files_muonsf0_up_systematics = ['MET filters_pv_npvs_muonsf0_up.root', 'MET filters_pv_npvsGood_muonsf0_up.root', 'MET filters_leading_jet_QGL_muonsf0_up.root', 'MET filters_subleading_jet_QGL_muonsf0_up.root',
              'MET filters_R_pT_hard_muonsf0_up.root', 'MET filters_z_star_muonsf0_up.root',
              'MET filters_leading_muon_pt_muonsf0_up.root', 'MET filters_leading_muon_abseta_muonsf0_up.root', 'MET filters_Delta_R_jets_l_mu_muonsf0_up.root', 'MET filters_Delta_R_jets_s_mu_muonsf0_up.root',
              'MET filters_subleading_muon_pt_muonsf0_up.root', 'MET filters_subleading_muon_abseta_muonsf0_up.root',
              'MET filters_leading_jet_pt_muonsf0_up.root', 'MET filters_leading_jet_abseta_muonsf0_up.root', 'MET filters_subleading_jet_pt_muonsf0_up.root', 'MET filters_subleading_jet_abseta_muonsf0_up.root',
              'MET filters_Dijet_pt_muonsf0_up.root', 'MET filters_Dijet_abseta_muonsf0_up.root', 'MET filters_Dijet_mass_muonsf0_up.root', 'MET filters_Dijet_phi_muonsf0_up.root',
              'MET filters_number_of_jets_muonsf0_up.root', 'MET filters_number_of_low_pT_jets_muonsf0_up.root', 'MET filters_dphijj_muonsf0_up.root', 'MET filters_detajj_muonsf0_up.root',
              'MET filters_detall_muonsf0_up.root', 'MET filters_dphill_muonsf0_up.root',
              'MET filters_Zboson_pt_muonsf0_up.root', 'MET filters_Zboson_mass_muonsf0_up.root', 'MET filters_Zboson_abseta_muonsf0_up.root', 'MET filters_Zboson_phi_muonsf0_up.root',
              'MET filters_DphiZ_l_jet_muonsf0_up.root', 'MET filters_DphiZ_s_jet_muonsf0_up.root', 'MET filters_DphiZ_jets_muonsf0_up.root']

files_muonsf0_down_systematics = ['MET filters_pv_npvs_muonsf0_down.root', 'MET filters_pv_npvsGood_muonsf0_down.root', 'MET filters_leading_jet_QGL_muonsf0_down.root',
              'MET filters_subleading_jet_QGL_muonsf0_down.root',
              'MET filters_R_pT_hard_muonsf0_down.root', 'MET filters_z_star_muonsf0_down.root',
              'MET filters_leading_muon_pt_muonsf0_down.root', 'MET filters_leading_muon_abseta_muonsf0_down.root', 'MET filters_Delta_R_jets_l_mu_muonsf0_down.root',
              'MET filters_Delta_R_jets_s_mu_muonsf0_down.root',
              'MET filters_subleading_muon_pt_muonsf0_down.root', 'MET filters_subleading_muon_abseta_muonsf0_down.root',
              'MET filters_leading_jet_pt_muonsf0_down.root', 'MET filters_leading_jet_abseta_muonsf0_down.root', 'MET filters_subleading_jet_pt_muonsf0_down.root',
              'MET filters_subleading_jet_abseta_muonsf0_down.root',
              'MET filters_Dijet_pt_muonsf0_down.root', 'MET filters_Dijet_abseta_muonsf0_down.root', 'MET filters_Dijet_mass_muonsf0_down.root', 'MET filters_Dijet_phi_muonsf0_down.root',
              'MET filters_number_of_jets_muonsf0_down.root', 'MET filters_number_of_low_pT_jets_muonsf0_down.root', 'MET filters_dphijj_muonsf0_down.root', 'MET filters_detajj_muonsf0_down.root',
              'MET filters_detall_muonsf0_down.root', 'MET filters_dphill_muonsf0_down.root',
              'MET filters_Zboson_pt_muonsf0_down.root', 'MET filters_Zboson_mass_muonsf0_down.root', 'MET filters_Zboson_abseta_muonsf0_down.root', 'MET filters_Zboson_phi_muonsf0_down.root',
              'MET filters_DphiZ_l_jet_muonsf0_down.root', 'MET filters_DphiZ_s_jet_muonsf0_down.root', 'MET filters_DphiZ_jets_muonsf0_down.root']

files_muonsf1_up_systematics = ['MET filters_pv_npvs_muonsf1_up.root', 'MET filters_pv_npvsGood_muonsf1_up.root', 'MET filters_leading_jet_QGL_muonsf1_up.root', 'MET filters_subleading_jet_QGL_muonsf1_up.root',
              'MET filters_R_pT_hard_muonsf1_up.root', 'MET filters_z_star_muonsf1_up.root',
              'MET filters_leading_muon_pt_muonsf1_up.root', 'MET filters_leading_muon_abseta_muonsf1_up.root', 'MET filters_Delta_R_jets_l_mu_muonsf1_up.root', 'MET filters_Delta_R_jets_s_mu_muonsf1_up.root',
              'MET filters_subleading_muon_pt_muonsf1_up.root', 'MET filters_subleading_muon_abseta_muonsf1_up.root',
              'MET filters_leading_jet_pt_muonsf1_up.root', 'MET filters_leading_jet_abseta_muonsf1_up.root', 'MET filters_subleading_jet_pt_muonsf1_up.root', 'MET filters_subleading_jet_abseta_muonsf1_up.root',
              'MET filters_Dijet_pt_muonsf1_up.root', 'MET filters_Dijet_abseta_muonsf1_up.root', 'MET filters_Dijet_mass_muonsf1_up.root', 'MET filters_Dijet_phi_muonsf1_up.root',
              'MET filters_number_of_jets_muonsf1_up.root', 'MET filters_number_of_low_pT_jets_muonsf1_up.root', 'MET filters_dphijj_muonsf1_up.root', 'MET filters_detajj_muonsf1_up.root',
              'MET filters_detall_muonsf1_up.root', 'MET filters_dphill_muonsf1_up.root',
              'MET filters_Zboson_pt_muonsf1_up.root', 'MET filters_Zboson_mass_muonsf1_up.root', 'MET filters_Zboson_abseta_muonsf1_up.root', 'MET filters_Zboson_phi_muonsf1_up.root',
              'MET filters_DphiZ_l_jet_muonsf1_up.root', 'MET filters_DphiZ_s_jet_muonsf1_up.root', 'MET filters_DphiZ_jets_muonsf1_up.root']

files_muonsf1_down_systematics = ['MET filters_pv_npvs_muonsf1_down.root', 'MET filters_pv_npvsGood_muonsf1_down.root', 'MET filters_leading_jet_QGL_muonsf1_down.root',
              'MET filters_subleading_jet_QGL_muonsf1_down.root',
              'MET filters_R_pT_hard_muonsf1_down.root', 'MET filters_z_star_muonsf1_down.root',
              'MET filters_leading_muon_pt_muonsf1_down.root', 'MET filters_leading_muon_abseta_muonsf1_down.root', 'MET filters_Delta_R_jets_l_mu_muonsf1_down.root',
              'MET filters_Delta_R_jets_s_mu_muonsf1_down.root',
              'MET filters_subleading_muon_pt_muonsf1_down.root', 'MET filters_subleading_muon_abseta_muonsf1_down.root',
              'MET filters_leading_jet_pt_muonsf1_down.root', 'MET filters_leading_jet_abseta_muonsf1_down.root', 'MET filters_subleading_jet_pt_muonsf1_down.root',
              'MET filters_subleading_jet_abseta_muonsf1_down.root',
              'MET filters_Dijet_pt_muonsf1_down.root', 'MET filters_Dijet_abseta_muonsf1_down.root', 'MET filters_Dijet_mass_muonsf1_down.root', 'MET filters_Dijet_phi_muonsf1_down.root',
              'MET filters_number_of_jets_muonsf1_down.root', 'MET filters_number_of_low_pT_jets_muonsf1_down.root', 'MET filters_dphijj_muonsf1_down.root', 'MET filters_detajj_muonsf1_down.root',
              'MET filters_detall_muonsf1_down.root', 'MET filters_dphill_muonsf1_down.root',
              'MET filters_Zboson_pt_muonsf1_down.root', 'MET filters_Zboson_mass_muonsf1_down.root', 'MET filters_Zboson_abseta_muonsf1_down.root', 'MET filters_Zboson_phi_muonsf1_down.root',
              'MET filters_DphiZ_l_jet_muonsf1_down.root', 'MET filters_DphiZ_s_jet_muonsf1_down.root', 'MET filters_DphiZ_jets_muonsf1_down.root']
files_muonsf2_up_systematics = ['MET filters_pv_npvs_muonsf2_up.root', 'MET filters_pv_npvsGood_muonsf2_up.root', 'MET filters_leading_jet_QGL_muonsf2_up.root', 'MET filters_subleading_jet_QGL_muonsf2_up.root',
              'MET filters_R_pT_hard_muonsf2_up.root', 'MET filters_z_star_muonsf2_up.root',
              'MET filters_leading_muon_pt_muonsf2_up.root', 'MET filters_leading_muon_abseta_muonsf2_up.root', 'MET filters_Delta_R_jets_l_mu_muonsf2_up.root', 'MET filters_Delta_R_jets_s_mu_muonsf2_up.root',
              'MET filters_subleading_muon_pt_muonsf2_up.root', 'MET filters_subleading_muon_abseta_muonsf2_up.root',
              'MET filters_leading_jet_pt_muonsf2_up.root', 'MET filters_leading_jet_abseta_muonsf2_up.root', 'MET filters_subleading_jet_pt_muonsf2_up.root', 'MET filters_subleading_jet_abseta_muonsf2_up.root',
              'MET filters_Dijet_pt_muonsf2_up.root', 'MET filters_Dijet_abseta_muonsf2_up.root', 'MET filters_Dijet_mass_muonsf2_up.root', 'MET filters_Dijet_phi_muonsf2_up.root',
              'MET filters_number_of_jets_muonsf2_up.root', 'MET filters_number_of_low_pT_jets_muonsf2_up.root', 'MET filters_dphijj_muonsf2_up.root', 'MET filters_detajj_muonsf2_up.root',
              'MET filters_detall_muonsf2_up.root', 'MET filters_dphill_muonsf2_up.root',
              'MET filters_Zboson_pt_muonsf2_up.root', 'MET filters_Zboson_mass_muonsf2_up.root', 'MET filters_Zboson_abseta_muonsf2_up.root', 'MET filters_Zboson_phi_muonsf2_up.root',
              'MET filters_DphiZ_l_jet_muonsf2_up.root', 'MET filters_DphiZ_s_jet_muonsf2_up.root', 'MET filters_DphiZ_jets_muonsf2_up.root']

files_muonsf2_down_systematics = ['MET filters_pv_npvs_muonsf2_down.root', 'MET filters_pv_npvsGood_muonsf2_down.root', 'MET filters_leading_jet_QGL_muonsf2_down.root',
              'MET filters_subleading_jet_QGL_muonsf2_down.root',
              'MET filters_R_pT_hard_muonsf2_down.root', 'MET filters_z_star_muonsf2_down.root',
              'MET filters_leading_muon_pt_muonsf2_down.root', 'MET filters_leading_muon_abseta_muonsf2_down.root', 'MET filters_Delta_R_jets_l_mu_muonsf2_down.root',
              'MET filters_Delta_R_jets_s_mu_muonsf2_down.root',
              'MET filters_subleading_muon_pt_muonsf2_down.root', 'MET filters_subleading_muon_abseta_muonsf2_down.root',
              'MET filters_leading_jet_pt_muonsf2_down.root', 'MET filters_leading_jet_abseta_muonsf2_down.root', 'MET filters_subleading_jet_pt_muonsf2_down.root',
              'MET filters_subleading_jet_abseta_muonsf2_down.root',
              'MET filters_Dijet_pt_muonsf2_down.root', 'MET filters_Dijet_abseta_muonsf2_down.root', 'MET filters_Dijet_mass_muonsf2_down.root', 'MET filters_Dijet_phi_muonsf2_down.root',
              'MET filters_number_of_jets_muonsf2_down.root', 'MET filters_number_of_low_pT_jets_muonsf2_down.root', 'MET filters_dphijj_muonsf2_down.root', 'MET filters_detajj_muonsf2_down.root',
              'MET filters_detall_muonsf2_down.root', 'MET filters_dphill_muonsf2_down.root',
              'MET filters_Zboson_pt_muonsf2_down.root', 'MET filters_Zboson_mass_muonsf2_down.root', 'MET filters_Zboson_abseta_muonsf2_down.root', 'MET filters_Zboson_phi_muonsf2_down.root',
              'MET filters_DphiZ_l_jet_muonsf2_down.root', 'MET filters_DphiZ_s_jet_muonsf2_down.root', 'MET filters_DphiZ_jets_muonsf2_down.root']

files_muonsf3_up_systematics = ['MET filters_pv_npvs_muonsf3_up.root', 'MET filters_pv_npvsGood_muonsf3_up.root', 'MET filters_leading_jet_QGL_muonsf3_up.root', 'MET filters_subleading_jet_QGL_muonsf3_up.root',
              'MET filters_R_pT_hard_muonsf3_up.root', 'MET filters_z_star_muonsf3_up.root',
              'MET filters_leading_muon_pt_muonsf3_up.root', 'MET filters_leading_muon_abseta_muonsf3_up.root', 'MET filters_Delta_R_jets_l_mu_muonsf3_up.root', 'MET filters_Delta_R_jets_s_mu_muonsf3_up.root',
              'MET filters_subleading_muon_pt_muonsf3_up.root', 'MET filters_subleading_muon_abseta_muonsf3_up.root',
              'MET filters_leading_jet_pt_muonsf3_up.root', 'MET filters_leading_jet_abseta_muonsf3_up.root', 'MET filters_subleading_jet_pt_muonsf3_up.root', 'MET filters_subleading_jet_abseta_muonsf3_up.root',
              'MET filters_Dijet_pt_muonsf3_up.root', 'MET filters_Dijet_abseta_muonsf3_up.root', 'MET filters_Dijet_mass_muonsf3_up.root', 'MET filters_Dijet_phi_muonsf3_up.root',
              'MET filters_number_of_jets_muonsf3_up.root', 'MET filters_number_of_low_pT_jets_muonsf3_up.root', 'MET filters_dphijj_muonsf3_up.root', 'MET filters_detajj_muonsf3_up.root',
              'MET filters_detall_muonsf3_up.root', 'MET filters_dphill_muonsf3_up.root',
              'MET filters_Zboson_pt_muonsf3_up.root', 'MET filters_Zboson_mass_muonsf3_up.root', 'MET filters_Zboson_abseta_muonsf3_up.root', 'MET filters_Zboson_phi_muonsf3_up.root',
              'MET filters_DphiZ_l_jet_muonsf3_up.root', 'MET filters_DphiZ_s_jet_muonsf3_up.root', 'MET filters_DphiZ_jets_muonsf3_up.root']

files_muonsf3_down_systematics = ['MET filters_pv_npvs_muonsf3_down.root', 'MET filters_pv_npvsGood_muonsf3_down.root', 'MET filters_leading_jet_QGL_muonsf3_down.root',
              'MET filters_subleading_jet_QGL_muonsf3_down.root',
              'MET filters_R_pT_hard_muonsf3_down.root', 'MET filters_z_star_muonsf3_down.root',
              'MET filters_leading_muon_pt_muonsf3_down.root', 'MET filters_leading_muon_abseta_muonsf3_down.root', 'MET filters_Delta_R_jets_l_mu_muonsf3_down.root',
              'MET filters_Delta_R_jets_s_mu_muonsf3_down.root',
              'MET filters_subleading_muon_pt_muonsf3_down.root', 'MET filters_subleading_muon_abseta_muonsf3_down.root',
              'MET filters_leading_jet_pt_muonsf3_down.root', 'MET filters_leading_jet_abseta_muonsf3_down.root', 'MET filters_subleading_jet_pt_muonsf3_down.root',
              'MET filters_subleading_jet_abseta_muonsf3_down.root',
              'MET filters_Dijet_pt_muonsf3_down.root', 'MET filters_Dijet_abseta_muonsf3_down.root', 'MET filters_Dijet_mass_muonsf3_down.root', 'MET filters_Dijet_phi_muonsf3_down.root',
              'MET filters_number_of_jets_muonsf3_down.root', 'MET filters_number_of_low_pT_jets_muonsf3_down.root', 'MET filters_dphijj_muonsf3_down.root', 'MET filters_detajj_muonsf3_down.root',
              'MET filters_detall_muonsf3_down.root', 'MET filters_dphill_muonsf3_down.root',
              'MET filters_Zboson_pt_muonsf3_down.root', 'MET filters_Zboson_mass_muonsf3_down.root', 'MET filters_Zboson_abseta_muonsf3_down.root', 'MET filters_Zboson_phi_muonsf3_down.root',
              'MET filters_DphiZ_l_jet_muonsf3_down.root', 'MET filters_DphiZ_s_jet_muonsf3_down.root', 'MET filters_DphiZ_jets_muonsf3_down.root']

#file_names = ['MET filters_number_of_jets.root']
#file_names = ['MET filters_dphijj.root', 'MET filters_dphill.root', 'MET filters_DphiZ_l_jet.root', 'MET filters_DphiZ_s_jet.root', 'MET filters_DphiZ_jets.root']
#file_names = ['MET filters_leading_jet_QGL.root', 'MET filters_subleading_jet_QGL.root']
#file_names = ['MET filters_Delta_R_jets_l_mu.root', 'MET filters_Delta_R_jets_s_mu.root', 'MET filters_Zboson_mass.root', 'MET filters_pv_npvs.root', 'PV_pv_npvs.root', 'MET filters_Dijet_pt.root']

samples = ['EWK_LLJJ_MLL-50_MJJ-120_TuneCP5_13TeV-madgraph-pythia8_dipole', 'WW_TuneCP5_13TeV-pythia8', 'WZ_TuneCP5_13TeV-pythia8', 'ZZ_TuneCP5_13TeV-pythia8',
           'ST_tW_top_5f_inclusiveDecays_TuneCP5_13TeV-powheg-pythia8', 'ST_t-channel_top_4f_InclusiveDecays_TuneCP5_13TeV-powheg-madspin-pythia8',
           'ST_tW_antitop_5f_inclusiveDecays_TuneCP5_13TeV-powheg-pythia8','ST_t-channel_antitop_4f_InclusiveDecays_TuneCP5_13TeV-powheg-madspin-pythia8',
           'ST_s-channel_4f_leptonDecays_TuneCP5_13TeV-amcatnlo-pythia8', 'TTTo2L2Nu_TuneCP5_13TeV-powheg-pythia8','TTToSemiLeptonic_TuneCP5_13TeV-powheg-pythia8', 
           'DYJetsToLL_M-50_TuneCP5_13TeV-amcatnloFXFX-pythia8', 'SingleMuon' ]

for (file_name, file_PU_up, file_PU_down, file_btag_up, file_btag_down, file_JER_up, file_JER_down, file_JUNC_up, file_JUNC_down, file_btagsf0light_up, file_btagsf0light_down, file_muonsf0_up,
    file_muonsf0_down, file_muonsf1_up, file_muonsf1_down, file_muonsf2_up, file_muonsf2_down, file_muonsf3_up, file_muonsf3_down) in zip(file_names, files_PU_up_systematics, files_PU_down_systematics,
    files_btag_up_systematics, files_btag_down_systematics, files_JER_up_systematics, files_JER_down_systematics, files_junc_up_systematics, files_junc_down_systematics,
    files_btagsf0light_up_systematics, files_btagsf0light_down_systematics, files_muonsf0_up_systematics, files_muonsf0_down_systematics, files_muonsf1_up_systematics, files_muonsf1_down_systematics,
    files_muonsf2_up_systematics, files_muonsf2_down_systematics, files_muonsf3_up_systematics, files_muonsf3_down_systematics):

    mc_histograms = [None]*(len(samples)-1)
    PU_up_hist = [None]*(len(samples)-1)
    PU_down_hist = [None]*(len(samples)-1)
    btag_up_hist = [None]*(len(samples)-1)
    btag_down_hist = [None]*(len(samples)-1)
    JER_up_hist = [None]*(len(samples)-1)
    JER_down_hist = [None]*(len(samples)-1)
    JUNC_up_hist = [None]*(len(samples)-1)
    JUNC_down_hist = [None]*(len(samples)-1)
    btagsf0light_up_hist = [None]*(len(samples)-1)
    btagsf0light_down_hist = [None]*(len(samples)-1)
    muonsf0_up_hist = [None]*(len(samples)-1)
    muonsf0_down_hist = [None]*(len(samples)-1)
    muonsf1_up_hist = [None]*(len(samples)-1)
    muonsf1_down_hist = [None]*(len(samples)-1)
    muonsf2_up_hist = [None]*(len(samples)-1)
    muonsf2_down_hist = [None]*(len(samples)-1)
    muonsf3_up_hist = [None]*(len(samples)-1)
    muonsf3_down_hist = [None]*(len(samples)-1)

    for i in range(len(mc_histograms)):
        open_file = uproot.open(inputdir + file_name)
        open_PU_up_file = uproot.open(inputdir + file_PU_up)
        open_PU_down_file = uproot.open(inputdir + file_PU_down)
        open_btag_up_file = uproot.open(inputdir + file_btag_up)
        open_btag_down_file = uproot.open(inputdir + file_btag_down)
        open_JER_up_file = uproot.open(inputdir + file_JER_up)
        open_JER_down_file = uproot.open(inputdir + file_JER_down)
        open_JUNC_up_file = uproot.open(inputdir + file_JUNC_up)
        open_JUNC_down_file = uproot.open(inputdir + file_JUNC_down)
        open_btagsf0light_up_file = uproot.open(inputdir + file_btagsf0light_up)
        open_btagsf0light_down_file = uproot.open(inputdir + file_btagsf0light_down)
        open_muonsf0_up_file = uproot.open(inputdir + file_muonsf0_up)
        open_muonsf0_down_file = uproot.open(inputdir + file_muonsf0_down)
        open_muonsf1_up_file = uproot.open(inputdir + file_muonsf1_up)
        open_muonsf1_down_file = uproot.open(inputdir + file_muonsf1_down)
        open_muonsf2_up_file = uproot.open(inputdir + file_muonsf2_up)
        open_muonsf2_down_file = uproot.open(inputdir + file_muonsf2_down)
        open_muonsf3_up_file = uproot.open(inputdir + file_muonsf3_up)
        open_muonsf3_down_file = uproot.open(inputdir + file_muonsf3_down)

        mc_histograms[i] = open_file[samples[i]].to_hist()
        data_histogram = open_file['SingleMuon'].to_hist()

        PU_up_hist[i] = open_PU_up_file[samples[i]].to_hist()
        PU_down_hist[i] = open_PU_down_file[samples[i]].to_hist()
        btag_up_hist[i] = open_btag_up_file[samples[i]].to_hist()
        btag_down_hist[i] = open_btag_down_file[samples[i]].to_hist()
        JER_up_hist[i] = open_JER_up_file[samples[i]].to_hist()
        JER_down_hist[i] = open_JER_down_file[samples[i]].to_hist()
        JUNC_up_hist[i] = open_JUNC_up_file[samples[i]].to_hist()
        JUNC_down_hist[i] = open_JUNC_down_file[samples[i]].to_hist()
        btagsf0light_up_hist[i] = open_btagsf0light_up_file[samples[i]].to_hist()
        btagsf0light_down_hist[i] = open_btagsf0light_down_file[samples[i]].to_hist()
        muonsf0_up_hist[i] = open_muonsf0_up_file[samples[i]].to_hist()
        muonsf0_down_hist[i] = open_muonsf0_down_file[samples[i]].to_hist()
        muonsf1_up_hist[i] = open_muonsf1_up_file[samples[i]].to_hist()
        muonsf1_down_hist[i] = open_muonsf1_down_file[samples[i]].to_hist()
        muonsf2_up_hist[i] = open_muonsf2_up_file[samples[i]].to_hist()
        muonsf2_down_hist[i] = open_muonsf2_down_file[samples[i]].to_hist()
        muonsf3_up_hist[i] = open_muonsf3_up_file[samples[i]].to_hist()
        muonsf3_down_hist[i] = open_muonsf3_down_file[samples[i]].to_hist()

        scaled_mc_signal= open_file['EWK_LLJJ_MLL-50_MJJ-120_TuneCP5_13TeV-madgraph-pythia8_dipole'].to_hist()*100

    PU_up, PU_down = systematics(mc_histograms, PU_up_hist, PU_down_hist)
    btag_up, btag_down = systematics(mc_histograms, btag_up_hist, btag_down_hist)
    JER_up, JER_down = systematics(mc_histograms, JER_up_hist, JER_down_hist)
    JUNC_up, JUNC_down = systematics(mc_histograms, JUNC_up_hist, JUNC_down_hist)
    btagsf0light_up, btagsf0light_down = systematics(mc_histograms, btagsf0light_up_hist, btagsf0light_down_hist)
    btag_total_up, btag_total_down = total_systematics([btag_up, btagsf0light_up], [btag_down, btagsf0light_down])
    muonsf0_up, muonsf0_down = systematics(mc_histograms, muonsf0_up_hist, muonsf0_down_hist)
    muonsf0_up, muonsf0_down = systematics(mc_histograms, muonsf0_up_hist, muonsf0_down_hist)
    muonsf1_up, muonsf1_down = systematics(mc_histograms, muonsf1_up_hist, muonsf1_down_hist)
    muonsf1_up, muonsf1_down = systematics(mc_histograms, muonsf1_up_hist, muonsf1_down_hist)
    muonsf2_up, muonsf2_down = systematics(mc_histograms, muonsf2_up_hist, muonsf2_down_hist)
    muonsf2_up, muonsf2_down = systematics(mc_histograms, muonsf2_up_hist, muonsf2_down_hist)
    muonsf3_up, muonsf3_down = systematics(mc_histograms, muonsf3_up_hist, muonsf3_down_hist)
    muonsf3_up, muonsf3_down = systematics(mc_histograms, muonsf3_up_hist, muonsf3_down_hist)
    muon_total_up, muon_total_down = total_systematics([muonsf0_up, muonsf1_up, muonsf2_up, muonsf3_up], [muonsf0_down, muonsf1_down, muonsf2_down, muonsf3_down])

    total_up_sys, total_down_sys = total_systematics([PU_up, btag_up, JER_up, JUNC_up, btagsf0light_up, muonsf0_up, muonsf1_up, muonsf2_up, muonsf3_up],
    [PU_down, btag_down, JER_down, JUNC_down, btagsf0light_down, muonsf0_down, muonsf1_down, muonsf2_down, muonsf3_down])
    yrr = [total_up_sys, total_down_sys]

    # Set style
    plt.style.use(hep.style.ROOT)
    plt.rcParams.update({'font.size': 14})   
    plt.rcParams.update({'axes.titlesize':20})
    plt.rcParams.update({'axes.labelsize':20})

    # Set stack and ratio plot
    fig, ax = plt.subplots(5, 1, sharex=True)

    # Make stack plot in upper axis
    sum_mc_histos = sum(mc_histograms)
    wratio, bins, werr = make_ratio(data_histogram, sum_mc_histos)
    PU_axis = ax[0]
    PU_axis.axhline(y=1, color='black')
    PU_axis.fill_between((bins[:-1] + bins[1:]) / 2, 1 + PU_up/sum_mc_histos.values(), 1- PU_down/sum_mc_histos.values(), color='paleturquoise', label='PU syst. Uncertainty')
    PU_axis.legend()
    PU_axis.set_ylim(0.75, 1.25)

    btag_axis = ax[1]
    btag_axis.axhline(y=1, color='black')
    btag_axis.fill_between((bins[:-1] + bins[1:]) / 2, 1+btag_total_up/sum_mc_histos.values(), 1-btag_total_down/sum_mc_histos.values(), color='slateblue', label='btag syst. Uncertainty')
    btag_axis.legend()
    btag_axis.set_ylim(0.75, 1.25)

    JER_axis = ax[2]
    JER_axis.axhline(y=1, color='black')
    JER_axis.fill_between((bins[:-1] + bins[1:]) / 2, 1+JER_up/sum_mc_histos.values(), 1-JER_down/sum_mc_histos.values(), color='cadetblue', label='JER syst. Uncertainty')
    JER_axis.legend()
    JER_axis.set_ylim(0.75, 1.25)

    JUNC_axis = ax[3]
    JUNC_axis.axhline(y=1, color='black')
    JUNC_axis.fill_between((bins[:-1] + bins[1:]) / 2, 1+JUNC_up/sum_mc_histos.values(), 1-JUNC_down/sum_mc_histos.values(), color='aquamarine', label='JEC syst. Uncertainty')
    JUNC_axis.legend()
    JUNC_axis.set_ylim(0.75, 1.25)

    muon_axis = ax[4]
    muon_axis.axhline(y=1, color='black')
    muon_axis.fill_between((bins[:-1] + bins[1:]) / 2, 1+muon_total_up/sum_mc_histos.values(), 1-muon_total_down/sum_mc_histos.values(), color='plum', label='muon syst. Uncertainty')
    muon_axis.legend()
    muon_axis.set_ylim(0.95, 1.05)


    if file_name == 'MET filters_Delta_R_jets_l_mu.root':
        muon_axis.set_xlabel('$\\Delta R_{\\mu_{L} , jets}$')
    if file_name == 'MET filters_Delta_R_jets_s_mu.root':
        muon_axis.set_xlabel('$\\Delta R_{\\mu_{S} , jets}$')
    if file_name == 'MET filters_pv_npvsGood.root':
        muon_axis.set_xlabel('Number of good primary vertices')
    if file_name == 'MET filters_leading_muon_pt.root':
        muon_axis.set_xlabel('Leading muon transverse momentum: $p_{T}$ [GeV]')   
    elif file_name == 'MET filters_leading_muon_abseta.root':
        muon_axis.set_xlabel('Leading muon $|\eta|$')
    elif file_name == 'MET filters_Delta_R_l_mu_l_jet.root':
        muon_axis.set_xlabel(' $\\Delta R_{\\mu_{L} , jet_{L}}$')
    elif file_name == 'MET filters_Delta_R_l_mu_s_jet.root':
        muon_axis.set_xlabel('$\\Delta R_{\\mu_{L} , jet_{S}}$')
    elif file_name == 'MET filters_Delta_R_l_jet.root':
        muon_axis.set_xlabel(' $\\Delta R_{j_{L} , \\mu \\mu}$')
    elif file_name == 'MET filters_Delta_R_s_jet.root':
        muon_axis.set_xlabel('$\\Delta R_{j_{S} , \\mu \\mu}$')
    elif file_name == 'MET filters_Delta_phi_l_mu_l_jet.root':
        muon_axis.set_xlabel(' $\\Delta \\phi_{\\mu_{L}, jet_{L}}$')
    elif file_name == 'MET filters_Delta_phi_l_mu_s_jet.root':
        muon_axis.set_xlabel('$\\Delta \\phi_{\\mu_{L}, jet_{S}}$')
    elif file_name == 'MET filters_subleading_muon_pt.root':
        muon_axis.set_xlabel('Subleading muon transverse momentum: $p_{T}$ [GeV]')
    elif file_name == 'MET filters_subleading_muon_abseta.root':
        muon_axis.set_xlabel('Subleading muon $|\eta|$')
    elif file_name == 'MET filters_leading_jet_pt.root':
        muon_axis.set_xlabel('Leading jet transverse momentum: $p_{T}$ [GeV]')
    elif file_name == 'MET filters_Delta_R_s_mu_l_jet.root':
        muon_axis.set_xlabel(' $\\Delta R_{\\mu_{S} , jet_{L}}$')
    elif file_name == 'MET filters_Delta_R_s_mu_s_jet.root':
        muon_axis.set_xlabel('$\\Delta R_{\\mu_{S} , jet_{S}}$')
    elif file_name == 'MET filters_Delta_phi_s_mu_l_jet.root':
        muon_axis.set_xlabel(' $\\Delta \\phi_{\\mu_{S}, jet_{L}}$')
    elif file_name == 'MET filters_Delta_phi_s_mu_s_jet.root':
        muon_axis.set_xlabel('$\\Delta \\phi_{\\mu_{S}, jet_{S}}$')
    elif file_name == 'MET filters_leading_jet_abseta.root':
        muon_axis.set_xlabel('Leading jet $|\eta|$')
    elif file_name == 'MET filters_subleading_jet_pt.root':
        muon_axis.set_xlabel('Subleading jet transverse momentum: $p_{T}$ [GeV]')
    elif file_name == 'MET filters_subleading_jet_abseta.root':
        muon_axis.set_xlabel('Subleading jet $|\eta|$')
    elif (file_name == 'MET filters_mjj.root') or (file_name == 'MET filters_Dijet_mass.root'):
        muon_axis.set_xlabel('Dijet mass: $m_{jj}$ [GeV]', fontsize = 20)
    elif (file_name == 'MET filters_pTjj.root') or (file_name == 'MET filters_Dijet_pt.root'):
        muon_axis.set_xlabel('$p_{Tjj}$ [GeV]', fontsize = 20)
    elif file_name == 'MET filters_Dijet_phi.root':
        muon_axis.set_xlabel(' $\\phi_{jj}$ ', fontsize = 20)
    elif file_name == 'MET filters_Dijet_abseta.root':
        muon_axis.set_xlabel(' $ | \\eta_{jj} | $ ', fontsize = 20)
    elif file_name == 'MET filters_Zboson_mass.root':
        muon_axis.set_xlabel('Z boson mass: $m_{l^{+}l^{-}}$ [GeV]', fontsize = 20)
    elif file_name == 'MET filters_Zboson_pt.root':
        muon_axis.set_xlabel('Z boson transverse momentum: $p_{T}$ [GeV]', fontsize = 20)
    elif file_name == 'MET filters_Zboson_abseta.root':
        muon_axis.set_xlabel('$|\\eta|_{l^{+}l^{-}}$ ', fontsize = 20)
    elif file_name == 'MET filters_Zboson_phi.root':
        muon_axis.set_xlabel('$|\\phi|_{l^{+}l^{-}}$ ', fontsize = 20)
    elif file_name == 'MET filters_number_of_jets.root':
        muon_axis.set_xlabel('Number of jets')
    elif file_name == 'MET filters_number_of_low_pT_jets.root':
        muon_axis.set_xlabel('Number of low pT jets')
    elif file_name == 'MET filters_DphiZ_jets.root':
        muon_axis.set_xlabel(' $\\Delta \\phi_{Z, jj}$ ', fontsize = 20)
    elif file_name == 'MET filters_DphiZ_l_jet.root':
        muon_axis.set_xlabel(' $\\Delta \\phi_{Z, j_{L}}$ ', fontsize = 20)
    elif file_name == 'MET filters_DphiZ_s_jet.root':
        muon_axis.set_xlabel(' $\\Delta \\phi_{Z, j_{S}}$ ', fontsize = 20)
    elif file_name == 'MET filters_Delta_phi_l_jet_mus.root':
        muon_axis.set_xlabel(' $\\Delta \\phi_{j_{L}, \\mu \\mu}$ ', fontsize = 20)
    elif file_name == 'MET filters_Delta_phi_s_jet_mus.root':
        muon_axis.set_xlabel(' $\\Delta \\phi_{j_{S}, \\mu \\mu}$ ', fontsize = 20)
    elif file_name == 'MET filters_dphill.root':
        muon_axis.set_xlabel(' $\\Delta \\phi_{\\mu \\mu}$ ', fontsize = 20)
    elif file_name == 'MET filters_dphijj.root':
        muon_axis.set_xlabel(' $\\Delta \\phi_{j_{L} j_{S}}$ ', fontsize = 20)
    elif file_name == 'MET filters_detall.root':
        muon_axis.set_xlabel(' $\\Delta \\eta_{\\mu \\mu}$ ', fontsize = 20)
    elif file_name == 'MET filters_detajj.root':
        muon_axis.set_xlabel(' $\\Delta \\eta_{j_{L} j_{S}}$ ',fontsize = 20)
    elif file_name == 'MET filters_R_pT_hard.root':
        muon_axis.set_xlabel('$R_{p_{T}}^{hard}$ ', fontsize = 20)
    elif file_name == 'MET filters_z_star.root':
        muon_axis.set_xlabel(' $z^*$ ')
    elif file_name == 'MET filters_pv_npvs.root':
        muon_axis.set_xlabel('Number of total reconstructed primary vertices')
    elif file_name == 'Muon_SF_pv_npvs.root':
        muon_axis.set_xlabel('Number of good primary vertices')
    elif file_name == 'Pileup reweighting_pv_npvs.root':
        muon_axis.set_xlabel('Number of good primary vertices')
    elif file_name == 'MET filters_QGD.root':
        muon_axis.set_xlabel('QGL Discriminant')
    elif file_name == 'MET filters_Delta_R_jets_l_mu.root':
        muon_axis.set_xlabel(' $R_{jets, \\mu_{L}}$ ')
    elif file_name == 'MET filters_Delta_R_jets_s_mu.root':
        muon_axis.set_xlabel(' $R_{jets, \\mu_{S}}$ ')
    elif file_name == 'MET filters_leading_jet_QGL.root':
        muon_axis.set_xlabel('Leading jet QGL', fontsize = 20)
    elif file_name == 'MET filters_subleading_jet_QGL.root':
        muon_axis.set_xlabel('Subleading jet QGL', fontsize = 20)
    
    plt.savefig('/afs/desy.de/user/m/moralfk/www/EWZjj-test-figures/NEW_DetectorLevelPlots/Pepper_MuMu_signal_2018_sys_uncert_'+file_name+'.png')
    #plt.show() 
