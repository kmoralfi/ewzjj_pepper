import matplotlib.pyplot as plt
import mplhep as hep
import uproot
import matplotlib.gridspec as gridspec
import numpy as np
import matplotlib.patches as patches
import matplotlib.path as path
import hist
from hist import Hist
from numpy.random import seed
from numpy.random import randn
from numpy.random import lognormal
from scipy.stats import ks_2samp
#from pathlib import Path
#from pathlib import PathPatch
#DY.root  EWK_LLJJ_MLL.root  SingleMuon.root

def make_signif(hnum, hden):
  #hden = hden *SF
  wnum, bins = hnum.to_numpy()
  print(bins)
  wden, bins = hden.to_numpy()
  wratio = np.divide(wnum, np.sqrt(wden))
  percentage = np.divide(wnum, (wnum+wden))
  wratio = np.nan_to_num(wratio)
  percentage = np.nan_to_num(percentage)
  percentage = percentage*100
  print('EWK')
  print(wnum)
  print('DY')
  print(wden)
  bincenters = (bins[:-1] + bins[1:]) / 2
  return wratio, bins, percentage

def make_ratio_norm_SF(hnum, hden, SF):
  hden = hden * SF
  wnum, bins = hnum.to_numpy()
  wden, bins = hden.to_numpy()
  numerr = np.sqrt(hnum.variances())

  #wratio = np.divide(wnum, wden) - 1
  wratio = np.divide(wnum, wden)
  print('data')
  print(wnum)
  print('mc')
  print(wden)
  ratioerr = np.divide(numerr, wden)
  bincenters = (bins[:-1] + bins[1:]) / 2
  return wratio, bins, ratioerr

def make_ratio(hnum, hden):
  wnum, bins = hnum.to_numpy()
  wden, bins = hden.to_numpy()
  numerr = np.sqrt(hnum.variances())
  
  wratio = np.divide(wnum, wden)
  print('data')
  print(wnum)
  print('mc')
  print(wden)
  ratioerr = np.divide(numerr, wden)
  bincenters = (bins[:-1] + bins[1:]) / 2
  return wratio, bins, ratioerr


# General results directory
#inputdir = '/nfs/dust/cms/user/moralfk/EWZjj_Pepper/ALL_24thJanuary_MuMu_ML_18_LeadMu40_yesbtag_sys/'
inputdir = '/nfs/dust/cms/user/moralfk/EWZjj_Pepper/ALL_22ndJanuary24_ee_ML_17_HLT35yesbtag_sys/'
#inputdir= '/nfs/dust/cms/user/moralfk/EWZjj_Pepper/ALL_24thJanuary_MuMu_ML_17_LeadMu40_yesbtag_sys/'
#inputdir= '/nfs/dust/cms/user/moralfk/EWZjj_Pepper/ALL_22ndJanuary24_ee_ML_18_HLT32yesbtag2_sys/'

# New file names
file_names_001 = ['MET filters_leading_jet_QGL.root', 'MET filters_subleading_jet_QGL.root']
file_names_01 = ['MET filters_z_star.root', 'MET filters_DphiZ_l_jet.root', 'MET filters_DphiZ_jets.root', 'MET filters_detajj.root','MET filters_detall.root', 'MET filters_leading_electron_abseta.root', 'MET filters_leading_muon_abseta.root']
file_names_10 = ['MET filters_leading_jet_pt.root','MET filters_subleading_jet_pt.root', 'MET filters_Zboson_pt.root', 'MET filters_Dijet_pt.root']
file_names_50= ['MET filters_Dijet_mass.root']

# electron channel

file_names = ['MET filters_leading_jet_QGL.root', 'MET filters_subleading_jet_QGL.root', 'MET filters_z_star.root', 'MET filters_DphiZ_l_jet.root', 'MET filters_DphiZ_jets.root', 
              'MET filters_detajj.root','MET filters_detall.root', 'MET filters_leading_electron_abseta.root', 'MET filters_leading_jet_pt.root','MET filters_subleading_jet_pt.root', 
              'MET filters_Zboson_pt.root', 'MET filters_Dijet_pt.root', 'MET filters_Dijet_mass.root']
"""
# muon channel
file_names = ['MET filters_leading_jet_QGL.root', 'MET filters_subleading_jet_QGL.root', 'MET filters_z_star.root', 'MET filters_DphiZ_l_jet.root', 'MET filters_DphiZ_jets.root', 
              'MET filters_detajj.root','MET filters_detall.root', 'MET filters_leading_muon_abseta.root', 'MET filters_leading_jet_pt.root','MET filters_subleading_jet_pt.root', 
              'MET filters_Zboson_pt.root', 'MET filters_Dijet_pt.root', 'MET filters_Dijet_mass.root']
"""
#file_names = ['MET filters_leading_jet_pt.root']
samples = ['EWK_LLJJ_MLL-50_MJJ-120_TuneCP5_13TeV-madgraph-pythia8_dipole', 'WW_TuneCP5_13TeV-pythia8', 'WZ_TuneCP5_13TeV-pythia8', 'ZZ_TuneCP5_13TeV-pythia8',
           'ST_tW_top_5f_inclusiveDecays_TuneCP5_13TeV-powheg-pythia8', 'ST_t-channel_top_4f_InclusiveDecays_TuneCP5_13TeV-powheg-madspin-pythia8',
           'ST_tW_antitop_5f_inclusiveDecays_TuneCP5_13TeV-powheg-pythia8','ST_t-channel_antitop_4f_InclusiveDecays_TuneCP5_13TeV-powheg-madspin-pythia8',
           'ST_s-channel_4f_leptonDecays_TuneCP5_13TeV-amcatnlo-pythia8', 'TTTo2L2Nu_TuneCP5_13TeV-powheg-pythia8','TTToSemiLeptonic_TuneCP5_13TeV-powheg-pythia8',
           'DYJetsToLL_M-50_TuneCP5_13TeV-amcatnloFXFX-pythia8', 'EGamma' ]

if inputdir ==  '/nfs/dust/cms/user/moralfk/EWZjj_Pepper/ALL_22ndJanuary24_ee_ML_18_HLT32yesbtag2_sys/':
    sf =1/1.14400881615
    legend = r'EWK $Z\rightarrow e^{+}e^{-}$ significance in background region'
    y_label = '$\\quad EWK \\quad Z \\rightarrow e^{+}e^{-} \\quad / \\sqrt{DY \\quad Z}$'
elif inputdir ==  '/nfs/dust/cms/user/moralfk/EWZjj_Pepper/ALL_22ndJanuary24_ee_ML_17_HLT35yesbtag_sys/':
    sf = 1/1.19314188966
    legend = r'EWK $Z\rightarrow e^{+}e^{-}$ significance in background region'
    y_label = '$\\quad EWK \\quad Z \\rightarrow e^{+}e^{-} \\quad / \\sqrt{DY \\quad Z}$'
elif inputdir == '/nfs/dust/cms/user/moralfk/EWZjj_Pepper/ALL_24thJanuary_MuMu_ML_18_LeadMu40_yesbtag_sys/':
    sf = 1/1.1401104195
    legend = r'EWK $Z\rightarrow \mu^{+}\mu^{-}$ significance in background region'
    y_label = '$\\quad EWK \\quad Z \\rightarrow \\mu^{+}\\mu^{-} \\quad / \\sqrt{DY \\quad Z}$'
elif inputdir == '/nfs/dust/cms/user/moralfk/EWZjj_Pepper/ALL_24thJanuary_MuMu_ML_17_LeadMu40_yesbtag_sys/':
    sf = 1/1.07160584896
    legend = r'EWK $Z\rightarrow \mu^{+}\mu^{-}$ significance in background region'
    y_label = '$\\quad EWK \\quad Z \\rightarrow \\mu^{+}\\mu^{-} \\quad / \\sqrt{DY \\quad Z}$'

for file_name in file_names: 
    mc_histograms = [None]*(len(samples)-1)
    mc_histograms_old = [None]*(len(samples)-1)
    for i in range(len(mc_histograms)):
        open_file = uproot.open(inputdir + file_name)
        mc_histograms[i] = open_file[samples[i]].to_hist()*sf
        mc_histograms_old[i] = open_file[samples[i]].to_hist()
        #data_histogram = open_file['SingleMuon'].to_hist()
        data_histogram = open_file['EGamma'].to_hist()
        DY_hist = open_file['DYJetsToLL_M-50_TuneCP5_13TeV-amcatnloFXFX-pythia8'].to_hist()*sf
        EWK_hist = open_file['EWK_LLJJ_MLL-50_MJJ-120_TuneCP5_13TeV-madgraph-pythia8_dipole'].to_hist()*sf
        scaled_mc_signal= open_file['EWK_LLJJ_MLL-50_MJJ-120_TuneCP5_13TeV-madgraph-pythia8_dipole'].to_hist()*sf*100

    # Set style
    plt.style.use(hep.style.ROOT)
    plt.rcParams.update({'font.size': 14})   
    plt.rcParams.update({'axes.titlesize':20})
    plt.rcParams.update({'axes.labelsize':20})

    # Set stack and ratio plot
    fig, ax = plt.subplots(2, 1, gridspec_kw={'height_ratios': [2, 1], 'hspace':0}, sharex=True)

    # Make stack plot in upper axis
    stack_axis = ax[0]
    print(samples[len(samples)-2])
 
    wratio_sig, bins_sig, percentage = make_signif(EWK_hist, DY_hist)
    wratio, bins, werr = make_ratio(data_histogram, sum(mc_histograms))
    wratio_background = wratio.copy()
    wratio_signal = wratio.copy()
    wratio_old, bins_old, werr_old = make_ratio(data_histogram, sum(mc_histograms_old))
    wratio_old_background = wratio_old.copy()
    wratio_old_signal = wratio_old.copy()
    print('significance: ',wratio_sig)
    print('percentage EWK/(DY+EWK)*100', percentage)
    wratio_sig[wratio_sig>2.2] = 0
    if file_name == 'MET filters_leading_electron_abseta.root':
        wratio_sig[len(wratio_sig)-1] = 0.000001
    colour = 'indianred'
    #colour = 'cadetblue'
    masked_vals = np.where(wratio_sig == 0)[0]
    for i in range(len(bins_sig)-1):
        if (wratio_sig[i] == 0.):
            print(bins_sig[i])
            
            wratio_old_background[i] = 0
            wratio_background[i] = 0
            # Vertical lines step: 0.01 for QGL, 0.1 for z* and DeltaphiZ-leadingjet/jets, 0.5 for lepton abseta, 10 for the rest
            if file_name in  file_names_001:
                step = 0.01
            elif file_name in  file_names_01:
                step = 0.1
            elif file_name in  file_names_10:
                step = 10
            elif file_name in  file_names_50:
                step = 50
            stack_axis.vlines(np.arange(bins_sig[i], bins_sig[i+1], step), 0,3)
            for s in range(len(mc_histograms)):
                mc_histograms[s].values()[i] = 0
            data_histogram.values()[i] = 0
        elif (wratio_sig[i] != 0.):
            wratio_old_signal[i] = 0
            wratio_signal[i] = 0
    print(wratio_sig)
    # Add labels for each bin

    hep.histplot(wratio_sig, bins_sig, color = 'indianred', ax=stack_axis,stack=False, histtype='step', label = legend)
    stack_axis.legend()
    #stack_axis.set_yscale('log')
    #stack_axis.set_xticks(bin_centers, np.array2string(percentage, formatter={'float_kind':lambda percentage: "%.2f" % percentage}))
    #stack_axis.set_ylabel('$\\quad EWK \\quad Z \\rightarrow e^{+}e^{-} \\quad / \\sqrt{DY \\quad Z}$')
    stack_axis.set_ylabel(y_label)
    stack_axis.set_ylim(0, 6) 
    # Ratio axis
    ratio_axis = ax[1]
    ratio_axis.axhline(y=1, color='black')
    # Sum stack
    sum_mc_histos = sum(mc_histograms)
    print('total mc value events (integral)', sum(sum_mc_histos.values()))
    #print(sum_mc_histos.values())
    
    # Calculate ratio Data/MC
    #wratio, bins, werr = make_ratio(data_histogram, sum_mc_histos)
    #wratio_old, bins_old, werr_old = make_ratio(data_histogram, sum(mc_histograms_old))
    print(file_name)
    wratio_old = np.nan_to_num(wratio_old)
    wratio= np.nan_to_num(wratio)
    print('old ratio array: ', wratio_old)
    print('old ratio scalar: ', sum(wratio_old)/len(np.where(wratio_old !=0)[0]))
    #print(len(np.where(wratio_old_background !=0)[0]))
    #print(len(np.where(wratio_old_signal !=0)[0]))
    #print(sum(wratio_old_signal))
    #print(wratio_old_signal)
    wso =  np.nan_to_num(wratio_old_signal[np.where(wratio_old_signal !=0)[0]])
    wbo = np.nan_to_num(wratio_old_background[np.where(wratio_old_background !=0)[0]])
    wso = np.delete(wso, np.where(wso==0))
    wbo = np.delete(wbo, np.where(wbo==0))
    print('old ratio background region: ', sum(wbo)/len(np.where(wbo !=0)[0]))
    #if (file_name !='MET filters_detajj.root') and (file_name !='MET filters_leading_electron_abseta.root') and (file_name !='MET filters_DphiZ_l_jet.root'):
    print('old ratio signal region: ', sum(wso)/len(np.where(wso !=0)[0]))
    #print('old ratio background region: ', sum(wbo)/len(np.where(wbo !=0)[0]))

    #print('old ratio signal region: ', sum(wratio_old_signal)/len(np.where(wratio_old_signal !=0)[0]))
    #print('old ratio background region: ',sum(wratio_old_background)/len(np.where(wratio_old_background !=0)[0]))
   

    print('new ratio array :', wratio) 
    print('new ratio scalar : ', sum(wratio)/len(np.where(wratio !=0)[0]))
    #print('new ratio signal region: ', sum(wratio_signal)/len(np.where(wratio_signal !=0)[0]))
    #print('new ratio background region: ', sum(wratio_background)/len(np.where(wratio_background !=0)[0]))
    ws =  np.nan_to_num(wratio_signal[np.where(wratio_signal !=0)[0]])
    wb = np.nan_to_num(wratio_background[np.where(wratio_background !=0)[0]])
    ws = np.delete(ws, np.where(ws==0))
    wb = np.delete(wb, np.where(wb==0))
    print('wratio_signal : ', ws)
    print('wratio_background : ', wb)
    #print('new ratio signal region: ', sum(ws)/len(np.where(ws !=0)[0]))
    #print('new ratio background region: ', sum(ws)/len(np.where(ws !=0)[0]))
    print('new ratio background region: ', sum(wb)/len(np.where(wb !=0)[0]))
    #2018 ee channel
    #if (file_name !='MET filters_detajj.root') and (file_name !='MET filters_leading_electron_abseta.root') and (file_name !='MET filters_DphiZ_l_jet.root'):
    print('new ratio signal region: ', sum(ws)/len(np.where(ws !=0)[0]))
    #print('new ratio background region: ', sum(wb)/len(np.where(wb !=0)[0]))
    print('Kolmogorov-Smirnov test :', ks_2samp(wb, ws))
 
 
    wratioMC, binsMC, werrMC = make_ratio(sum_mc_histos, sum_mc_histos)
    ones = np.ones(len(wratio))
    #(wratio[i] = 0) for i in masked_vals
    #(wratioMC[i] = 0) for i in masked_vals
    masked_vals = np.where(wratio_sig == 0)[0]
    for j in masked_vals:
        #print(i)
        wratio[j] = 0
        werr[j] = 0
        werrMC[j] = 0
        ones[j] = 0
    hep.histplot(wratio, bins, yerr = (werr), stack=False, histtype='errorbar', color='black', ax = ratio_axis, label = 'Statistical data uncertainty')
    hep.histplot(ones, binsMC, yerr = werrMC, stack=False, histtype='errorbar', marker = 'None', color='black', ax = ratio_axis, label = 'Statistical MC uncertainty')

    ratio_axis.legend() 
    if file_name == 'MET filters_Delta_R_jets_l_e.root':
        ratio_axis.set_xlabel('$\\Delta R_{e_{L} , jets}$')
    if file_name == 'MET filters_Delta_R_jets_s_e.root':
        ratio_axis.set_xlabel('$\\Delta R_{e_{S} , jets}$')
    if file_name == 'MET filters_pv_npvsGood.root':
        ratio_axis.set_xlabel('Number of good primary vertices')
    if file_name == 'MET filters_leading_electron_pt.root':
        ratio_axis.set_xlabel('Leading electron transverse momentum: $p_{T}$ [GeV]')   
    elif file_name == 'MET filters_leading_electron_abseta.root':
        ratio_axis.set_xlabel('Leading electron $|\eta|$')
    elif file_name == 'MET filters_Delta_R_l_e_l_jet.root':
        ratio_axis.set_xlabel(' $\\Delta R_{e_{L} , jet_{L}}$')
    elif file_name == 'MET filters_Delta_R_l_e_s_jet.root':
        ratio_axis.set_xlabel('$\\Delta R_{e_{L} , jet_{S}}$')
    elif file_name == 'MET filters_Delta_R_l_jet.root':
        ratio_axis.set_xlabel(' $\\Delta R_{j_{L} , e e}$')
    elif file_name == 'MET filters_Delta_R_s_jet.root':
        ratio_axis.set_xlabel('$\\Delta R_{j_{S} , e e}$')
    elif file_name == 'MET filters_Delta_phi_l_e_l_jet.root':
        ratio_axis.set_xlabel(' $\\Delta \\phi_{e_{L}, jet_{L}}$')
    elif file_name == 'MET filters_Delta_phi_l_e_s_jet.root':
        ratio_axis.set_xlabel('$\\Delta \\phi_{e_{L}, jet_{S}}$')
    elif file_name == 'MET filters_subleading_electron_pt.root':
        ratio_axis.set_xlabel('Subleading electron transverse momentum: $p_{T}$ [GeV]')
    elif file_name == 'MET filters_subleading_electron_abseta.root':
        ratio_axis.set_xlabel('Subleading electron $|\eta|$')
    elif file_name == 'MET filters_leading_jet_pt.root':
        ratio_axis.set_xlabel('Leading jet transverse momentum: $p_{T}$ [GeV]')
    elif file_name == 'MET filters_Delta_R_s_e_l_jet.root':
        ratio_axis.set_xlabel(' $\\Delta R_{e_{S} , jet_{L}}$')
    elif file_name == 'MET filters_Delta_R_s_e_s_jet.root':
        ratio_axis.set_xlabel('$\\Delta R_{e_{S} , jet_{S}}$')
    elif file_name == 'MET filters_Delta_phi_s_e_l_jet.root':
        ratio_axis.set_xlabel(' $\\Delta \\phi_{e_{S}, jet_{L}}$')
    elif file_name == 'MET filters_Delta_phi_s_e_s_jet.root':
        ratio_axis.set_xlabel('$\\Delta \\phi_{e_{S}, jet_{S}}$')
    elif file_name == 'MET filters_leading_jet_abseta.root':
        ratio_axis.set_xlabel('Leading jet $|\eta|$')
    elif file_name == 'MET filters_subleading_jet_pt.root':
        ratio_axis.set_xlabel('Subleading jet transverse momentum: $p_{T}$ [GeV]')
    elif file_name == 'MET filters_subleading_jet_abseta.root':
        ratio_axis.set_xlabel('Subleading jet $|\eta|$')
    elif file_name == 'MET filters_leading_muon_abseta.root':
        ratio_axis.set_xlabel('Leading muon $|\eta|$')
    elif (file_name == 'MET filters_mjj.root') or (file_name == 'MET filters_Dijet_mass.root'):
        ratio_axis.set_xlabel('Dijet mass: $m_{jj}$ [GeV]', fontsize = 20)
    elif (file_name == 'MET filters_pTjj.root') or (file_name == 'MET filters_Dijet_pt.root'):
        ratio_axis.set_xlabel('$p_{Tjj}$ [GeV]', fontsize = 20)
    elif file_name == 'MET filters_Dijet_phi.root':
        ratio_axis.set_xlabel(' $\\phi_{jj}$ ', fontsize = 20)
    elif file_name == 'MET filters_Dijet_abseta.root':
        ratio_axis.set_xlabel(' $ | \\eta_{jj} | $ ', fontsize = 20)
    elif file_name == 'MET filters_Zboson_mass.root':
        ratio_axis.set_xlabel('Z boson mass: $m_{l^{+}l^{-}}$ [GeV]', fontsize = 20)
    elif file_name == 'MET filters_Zboson_pt.root':
        ratio_axis.set_xlabel('Z boson transverse momentum: $p_{T}$ [GeV]', fontsize = 20)
    elif file_name == 'MET filters_Zboson_abseta.root':
        ratio_axis.set_xlabel('$|\\eta|_{l^{+}l^{-}}$ ', fontsize = 20)
    elif file_name == 'MET filters_Zboson_phi.root':
        ratio_axis.set_xlabel('$|\\phi|_{l^{+}l^{-}}$ ', fontsize = 20)
    elif file_name == 'MET filters_number_of_jets.root':
        ratio_axis.set_xlabel('Number of jets')
    elif file_name == 'MET filters_number_of_low_pT_jets.root':
        ratio_axis.set_xlabel('Number of low pT jets')
    elif file_name == 'MET filters_DphiZ_jets.root':
        ratio_axis.set_xlabel(' $\\Delta \\phi_{Z, jj}$ ', fontsize = 20)
    elif file_name == 'MET filters_DphiZ_l_jet.root':
        ratio_axis.set_xlabel(' $\\Delta \\phi_{Z, j_{L}}$ ', fontsize = 20)
    elif file_name == 'MET filters_DphiZ_s_jet.root':
        ratio_axis.set_xlabel(' $\\Delta \\phi_{Z, j_{S}}$ ', fontsize = 20)
    elif file_name == 'MET filters_Delta_phi_l_jet_ee.root':
        ratio_axis.set_xlabel(' $\\Delta \\phi_{j_{L}, e e}$ ', fontsize = 20)
    elif file_name == 'MET filters_Delta_phi_s_jet_ee.root':
        ratio_axis.set_xlabel(' $\\Delta \\phi_{j_{S}, e e}$ ', fontsize = 20)
    elif file_name == 'MET filters_dphill.root':
        ratio_axis.set_xlabel(' $\\Delta \\phi_{e e}$ ', fontsize = 20)
    elif file_name == 'MET filters_dphijj.root':
        ratio_axis.set_xlabel(' $\\Delta \\phi_{j_{L} j_{S}}$ ', fontsize = 20)
    elif file_name == 'MET filters_detall.root':
        ratio_axis.set_xlabel(' $\\Delta \\eta_{e e}$ ', fontsize = 20)
    elif file_name == 'MET filters_detajj.root':
        ratio_axis.set_xlabel('$ | \\Delta \\eta_{j_{L} j_{S}} | $ ',fontsize = 20)
    elif file_name == 'MET filters_R_pT_hard.root':
        ratio_axis.set_xlabel('$R_{p_{T}}^{hard}$ ', fontsize = 20)
    elif file_name == 'MET filters_z_star.root':
        ratio_axis.set_xlabel(' $z^*$ ')
    elif file_name == 'MET filters_pv_npvs.root':
        ratio_axis.set_xlabel('Number of total reconstructed  primary vertices')
    elif file_name == 'Electron_SF_pv_npvs.root':
        ratio_axis.set_xlabel('Number of good primary vertices')
    elif file_name == 'Pileup reweighting_pv_npvs.root':
        ratio_axis.set_xlabel('Number of total reconstructed primary vertices')
    elif file_name == 'MET filters_QGD.root':
        ratio_axis.set_xlabel('QGL Discriminant')
    elif file_name == 'MET filters_Delta_R_jets_l_e.root':
        ratio_axis.set_xlabel(' $R_{jets, e_{L}}$ ')
    elif file_name == 'MET filters_Delta_R_jets_s_e.root':
        ratio_axis.set_xlabel(' $R_{jets, e_{S}}$ ')
    elif file_name == 'MET filters_leading_jet_QGL.root':
        ratio_axis.set_xlabel('Leading jet QGL', fontsize = 20)
    elif file_name == 'MET filters_subleading_jet_QGL.root':
        ratio_axis.set_xlabel('Subleading jet QGL', fontsize = 20)
    #ratio_axis.set_xlabel(file_name+'[GeV]')
    ratio_axis.set_ylabel(r'Data/MC')
    # ratio y_axis (0.8, 1.2)
    ratio_axis.set_ylim(0.5, 1.5)
    #ratio_axis.set_ylim(-0.5, 0.5)
    #hep.cms.label(loc=0, data=True, llabel='Work in Progress' ,ax=stack_axis, lumi=59.83, year=2018)
    #hep.cms.label(loc=0, data=True, llabel='Work in Progress', ax=stack_axis, lumi=41.48, year=2017)
    #plt.savefig('/afs/desy.de/user/m/moralfk/www/EWZjj-test-figures/NEW_DetectorLevelPlots/BDT_studies/preBDT/Significance_studies_blinding/2017/mumu_channel/mumu_ML_17_signal_blinded_2.2sigma_'+file_name+'.png')
    #plt.savefig('/afs/desy.de/user/m/moralfk/www/EWZjj-test-figures/NEW_DetectorLevelPlots/BDT_studies/preBDT/Significance_studies_blinding/2018/mumu_channel/mumu_ML_18_signal_blinded_3sigma_'+file_name+'.png')
    #plt.savefig('/afs/desy.de/user/m/moralfk/www/EWZjj-test-figures/NEW_DetectorLevelPlots/BDT_studies/preBDT/Significance_studies_blinding/2017/ee_channel/ee_ML_17_signal_blinded_3sigma_'+file_name+'.png')
    #plt.savefig('/afs/desy.de/user/m/moralfk/www/EWZjj-test-figures/NEW_DetectorLevelPlots/BDT_studies/preBDT/Significance_studies_blinding/2018/ee_channel/ee_ML_18_signal_blinded_3sigma_'+file_name+'.png')
    #plt.show()
