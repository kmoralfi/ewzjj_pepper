import matplotlib.pyplot as plt
import mplhep as hep
import uproot
import matplotlib.gridspec as gridspec
import numpy as np
import matplotlib.patches as patches
import matplotlib.path as path
import hist
from hist import Hist
#from pathlib import Path
#from pathlib import PathPatch
#DY.root  EWK_LLJJ_MLL.root  SingleMuon.root

def make_ratio(hnum, hden):
  wnum, bins = hnum.to_numpy()
  wden, bins = hden.to_numpy()

  numerr = np.sqrt(hnum.variances())
  
  #wratio = np.divide(wnum, wden) - 1
  wratio = np.divide(wnum, wden)
  print('data')
  print(wnum)
  print('mc')
  print(wden)
  ratioerr = np.divide(numerr, wden)
  bincenters = (bins[:-1] + bins[1:]) / 2
  return wratio, bins, ratioerr


# Load histograms

# General results directory
inputdir =  '/nfs/dust/cms/user/moralfk/EWZjj_Pepper/hists/'

# New file names
file_names = ['MET filters_leading_jet_pt.root', 'MET filters_Zboson_mass.root']

samples = ['EWK_LLJJ_MLL-50_MJJ-120_TuneCP5_13TeV-madgraph-pythia8_dipole']


for file_name in file_names:
   open_file = uproot.open(inputdir + file_name) 
   mc_histograms = open_file[samples[0]].to_hist()

   # Set style
   plt.style.use(hep.style.ROOT)
   plt.rcParams.update({'font.size': 14})   
   plt.rcParams.update({'axes.titlesize':20})
   plt.rcParams.update({'axes.labelsize':20})

   # Set stack and ratio plot
   fig, ax = plt.subplots(2, 1, gridspec_kw={'height_ratios': [2, 1], 'hspace':0}, sharex=True)

   # Make stack plot in upper axis
   stack_axis = ax[0]
   color = ['firebrick'] 
   legend_names = lenged_names = [r'EWK $Z\rightarrow\mu^{+}\mu^{-}$'] 
  
   hep.histplot(mc_histograms, color = color, ax=stack_axis, binwnorm=1., stack=True, histtype='fill', label = legend_names)
   stack_axis.legend()
   stack_axis.set_yscale('log')
   stack_axis.set_ylabel(r'Events/bin')
   
   # Ratio axis
   ratio_axis = ax[1]
   ratio_axis.axhline(y=1, color='black')
   # Sum stack
   sum_mc_histos = mc_histograms
   print('total mc value events (integral)', sum(sum_mc_histos.values()))
   #print(sum_mc_histos.values())
   
   # Calculate ratio Data/MC
   wratio, bins, werr = make_ratio(sum_mc_histos, sum_mc_histos)
   print('ratio') 
   #wratio = np.ones(len(sum_mc_histos.values()))
   #print(wratio)
   wratioMC, binsMC, werrMC = make_ratio(sum_mc_histos, sum_mc_histos)
   hep.histplot(wratio, bins, yerr = (werr), stack=False, histtype='errorbar', color='black', ax = ratio_axis, label = 'Statistical data uncertainty')
   hep.histplot(np.ones(len(wratio)), bins, yerr = werrMC, stack=False, histtype='errorbar', marker = 'None', color='black', ax = ratio_axis, label = 'Statistical MC uncertainty')
   ratio_axis.legend() 

   if file_name == 'Dijet_cut_Delta_R_jets_l_mu.root':
       ratio_axis.set_xlabel('$\\Delta R_{\\mu_{L} , jets}$')
   if file_name == 'Dijet_cut_Delta_R_jets_s_mu.root':
       ratio_axis.set_xlabel('$\\Delta R_{\\mu_{S} , jets}$')
   if file_name == 'Dijet_cut_pv_npvsGood.root':
       ratio_axis.set_xlabel('Number of good primary vertices')
   if file_name == 'Dijet_cut_leading_muon_pt.root':
       ratio_axis.set_xlabel('Leading muon transverse momentum: $p_{T}$ [GeV]')   
   elif file_name == 'Dijet_cut_leading_muon_abseta.root':
       ratio_axis.set_xlabel('Leading muon $|\eta|$')
   elif file_name == 'Dijet_cut_Delta_R_l_mu_l_jet.root':
       ratio_axis.set_xlabel(' $\\Delta R_{\\mu_{L} , jet_{L}}$')
   elif file_name == 'Dijet_cut_Delta_R_l_mu_s_jet.root':
       ratio_axis.set_xlabel('$\\Delta R_{\\mu_{L} , jet_{S}}$')
   elif file_name == 'Dijet_cut_Delta_R_l_jet.root':
       ratio_axis.set_xlabel(' $\\Delta R_{j_{L} , \\mu \\mu}$')
   elif file_name == 'Dijet_cut_Delta_R_s_jet.root':
       ratio_axis.set_xlabel('$\\Delta R_{j_{S} , \\mu \\mu}$')
   elif file_name == 'Dijet_cut_Delta_phi_l_mu_l_jet.root':
       ratio_axis.set_xlabel(' $\\Delta \\phi_{\\mu_{L}, jet_{L}}$')
   elif file_name == 'Dijet_cut_Delta_phi_l_mu_s_jet.root':
       ratio_axis.set_xlabel('$\\Delta \\phi_{\\mu_{L}, jet_{S}}$')
   elif file_name == 'Dijet_cut_subleading_muon_pt.root':
       ratio_axis.set_xlabel('Subleading muon transverse momentum: $p_{T}$ [GeV]')
   elif file_name == 'Dijet_cut_subleading_muon_abseta.root':
       ratio_axis.set_xlabel('Subleading muon $|\eta|$')
   elif file_name == 'Dijet_cut_leading_jet_pt.root':
       ratio_axis.set_xlabel('Leading jet transverse momentum: $p_{T}$ [GeV]')
   elif file_name == 'Dijet_cut_Delta_R_s_mu_l_jet.root':
       ratio_axis.set_xlabel(' $\\Delta R_{\\mu_{S} , jet_{L}}$')
   elif file_name == 'Dijet_cut_Delta_R_s_mu_s_jet.root':
       ratio_axis.set_xlabel('$\\Delta R_{\\mu_{S} , jet_{S}}$')
   elif file_name == 'Dijet_cut_Delta_phi_s_mu_l_jet.root':
       ratio_axis.set_xlabel(' $\\Delta \\phi_{\\mu_{S}, jet_{L}}$')
   elif file_name == 'Dijet_cut_Delta_phi_s_mu_s_jet.root':
       ratio_axis.set_xlabel('$\\Delta \\phi_{\\mu_{S}, jet_{S}}$')
   elif file_name == 'Dijet_cut_leading_jet_abseta.root':
       ratio_axis.set_xlabel('Leading jet $|\eta|$')
   elif file_name == 'Dijet_cut_subleading_jet_pt.root':
       ratio_axis.set_xlabel('Subleading jet transverse momentum: $p_{T}$ [GeV]')
   elif file_name == 'Dijet_cut_subleading_jet_abseta.root':
       ratio_axis.set_xlabel('Subleading jet $|\eta|$')
   elif (file_name == 'Dijet_cut_mjj.root') or (file_name == 'Dijet_cut_Dijet_mass.root'):
       ratio_axis.set_xlabel('Dijet mass: $m_{jj}$ [GeV]', fontsize = 20)
   elif (file_name == 'Dijet_cut_pTjj.root') or (file_name == 'Dijet_cut_Dijet_pt.root'):
       ratio_axis.set_xlabel('$p_{Tjj}$ [GeV]', fontsize = 20)
   elif file_name == 'Dijet_cut_Dijet_phi.root':
       ratio_axis.set_xlabel(' $\\phi_{jj}$ ', fontsize = 20)
   elif file_name == 'Dijet_cut_Dijet_abseta.root':
       ratio_axis.set_xlabel(' $ | \\eta_{jj} | $ ', fontsize = 20)
   elif file_name == 'Dijet_cut_Zboson_mass.root':
       ratio_axis.set_xlabel('Z boson mass: $m_{l^{+}l^{-}}$ [GeV]', fontsize = 20)
   elif file_name == 'Dijet_cut_Zboson_pt.root':
       ratio_axis.set_xlabel('Z boson transverse momentum: $p_{T}$ [GeV]', fontsize = 20)
   elif file_name == 'Dijet_cut_Zboson_abseta.root':
       ratio_axis.set_xlabel('$|\\eta|_{l^{+}l^{-}}$ ', fontsize = 20)
   elif file_name == 'Dijet_cut_Zboson_phi.root':
       ratio_axis.set_xlabel('$|\\phi|_{l^{+}l^{-}}$ ', fontsize = 20)
   elif file_name == 'Dijet_cut_number_of_jets.root':
       ratio_axis.set_xlabel('Number of jets')
   elif file_name == 'Dijet_cut_number_of_low_pT_jets.root':
       ratio_axis.set_xlabel('Number of low pT jets')
   elif file_name == 'Dijet_cut_DphiZ_jets.root':
       ratio_axis.set_xlabel(' $\\Delta \\phi_{Z, jj}$ ', fontsize = 20)
   elif file_name == 'Dijet_cut_DphiZ_l_jet.root':
       ratio_axis.set_xlabel(' $\\Delta \\phi_{Z, j_{L}}$ ', fontsize = 20)
   elif file_name == 'Dijet_cut_DphiZ_s_jet.root':
       ratio_axis.set_xlabel(' $\\Delta \\phi_{Z, j_{S}}$ ', fontsize = 20)
   elif file_name == 'Dijet_cut_Delta_phi_l_jet_mus.root':
       ratio_axis.set_xlabel(' $\\Delta \\phi_{j_{L}, \\mu \\mu}$ ', fontsize = 20)
   elif file_name == 'Dijet_cut_Delta_phi_s_jet_mus.root':
       ratio_axis.set_xlabel(' $\\Delta \\phi_{j_{S}, \\mu \\mu}$ ', fontsize = 20)
   elif file_name == 'Dijet_cut_dphill.root':
       ratio_axis.set_xlabel(' $\\Delta \\phi_{\\mu \\mu}$ ', fontsize = 20)
   elif file_name == 'Dijet_cut_dphijj.root':
       ratio_axis.set_xlabel(' $\\Delta \\phi_{j_{L} j_{S}}$ ', fontsize = 20)
   elif file_name == 'Dijet_cut_detall.root':
       ratio_axis.set_xlabel(' $\\Delta \\eta_{\\mu \\mu}$ ', fontsize = 20)
   elif file_name == 'Dijet_cut_detajj.root':
       ratio_axis.set_xlabel(' $\\Delta \\eta_{j_{L} j_{S}}$ ',fontsize = 20)
   elif file_name == 'Dijet_cut_R_pT_hard.root':
       ratio_axis.set_xlabel('$R_{p_{T}}^{hard}$ ', fontsize = 20)
   elif file_name == 'Dijet_cut_z_star.root':
       ratio_axis.set_xlabel(' $z^*$ ')
   elif file_name == 'Dijet_cut_pv_npvs.root':
       ratio_axis.set_xlabel('Number of total reconstructed  primary vertices')
   elif file_name == 'Muon_SF_pv_npvs.root':
       ratio_axis.set_xlabel('Number of good primary vertices')
   elif file_name == 'Pileup reweighting_pv_npvs.root':
       ratio_axis.set_xlabel('Number of total reconstructed primary vertices')
   elif file_name == 'Dijet_cut_QGD.root':
       ratio_axis.set_xlabel('QGL Discriminant')
   elif file_name == 'Dijet_cut_Delta_R_jets_l_mu.root':
       ratio_axis.set_xlabel(' $R_{jets, \\mu_{L}}$ ')
   elif file_name == 'Dijet_cut_Delta_R_jets_s_mu.root':
       ratio_axis.set_xlabel(' $R_{jets, \\mu_{S}}$ ')
   elif file_name == 'Dijet_cut_leading_jet_QGL.root':
       ratio_axis.set_xlabel('Leading jet QGL', fontsize = 20)
   elif file_name == 'Dijet_cut_subleading_jet_QGL.root':
       ratio_axis.set_xlabel('Subleading jet QGL', fontsize = 20)
   #ratio_axis.set_xlabel(file_name+'[GeV]')
   ratio_axis.set_ylabel(r'Data/MC')
   # ratio y_axis (0.8, 1.2)
   ratio_axis.set_ylim(0.5, 1.5)
   #ratio_axis.set_ylim(-0.5, 0.5)
   hep.cms.label(loc=0, data=True, ax=stack_axis, lumi=59.83, year=2018)
   
   #plt.savefig('/afs/desy.de/user/m/moralfk/www/EWZjj-test-figures/NEW_DetectorLevelPlots/Pepper_muon_7thmay_SR_'+file_name+'.pdf')
   #plt.savefig('/afs/desy.de/user/m/moralfk/www/EWZjj-test-figures/NEW_DetectorLevelPlots/Pepper_muon_8thjune_goodPV_'+file_name+'.png')    
   #plt.savefig('/afs/desy.de/user/m/moralfk/www/EWZjj-test-figures/NEW_DetectorLevelPlots/Pepper_muon_ALL_23rdjune_sys_'+file_name+'.png')
   plt.show() 
