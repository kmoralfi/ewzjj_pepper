import matplotlib.pyplot as plt
import mplhep as hep
import uproot
import matplotlib.gridspec as gridspec
import numpy as np
import matplotlib.patches as patches
import matplotlib.path as path
import hist
from hist import Hist
#from pathlib import Path
#from pathlib import PathPatch
#DY.root  EWK_LLJJ_MLL.root  SingleMuon.root

def make_ratio(hnum, hden):
  wnum, bins = hnum.to_numpy()
  wden, bins = hden.to_numpy()

  numerr = np.sqrt(hnum.variances())
  
  #wratio = np.divide(wnum, wden) - 1
  wratio = np.divide(wnum, wden)
  print('data')
  print(wnum)
  print('mc')
  print(wden)
  ratioerr = np.divide(numerr, wden)
  bincenters = (bins[:-1] + bins[1:]) / 2
  return wratio, bins, ratioerr

def make_ratio_array(hnum, hden):
  wnum = hnum
  wden = hden
  

  #wratio = np.divide(wnum, wden) - 1
  wratio = np.divide(wnum, wden)
  print('data')
  print(wnum)
  print('mc')
  print(wden)
  
  return wratio

def systematics(mc_nom_file, sys_up_file, sys_down_file):
    sys_up_f = 0
    sys_down_f = 0
    for mc_nom, sys_up_value, sys_down_value in zip(mc_nom_file, sys_up_file, sys_down_file):
        mc_nom = hist.Hist(mc_nom)
        sys_up_value = hist.Hist(sys_up_value)
        sys_down_value = hist.Hist(sys_down_value)
        sys_up_temp = ((sys_up_value.values() - mc_nom.values())**2)
        sys_down_temp = ((sys_down_value.values() - mc_nom.values())**2)
        sys_up_f = sys_up_temp + sys_up_f
        sys_down_f = sys_down_temp + sys_down_f
    sys_up = np.sqrt(sys_up_f)
    sys_down = np.sqrt(sys_down_f)
    
    return sys_up, sys_down

def total_systematics(sys_up, sys_down):
    sys_up_f = 0
    sys_down_f = 0
    for sys_up_partial, sys_down_partial in zip(sys_up, sys_down):
        sys_up_temp = (sys_up_partial**2)
        sys_down_temp = (sys_down_partial**2)
        sys_up_f = sys_up_temp + sys_up_f
        sys_down_f = sys_down_temp + sys_down_f
    total_sys_up = np.sqrt(sys_up_f)
    total_sys_down = np.sqrt(sys_down_f)
    return total_sys_up, total_sys_down

def systematics_ratio(sys_up, sys_down, mc_nom_stacked):
    ratio_up_sys = 1 + sys_up/mc_nom_stacked  
    ratio_down_sys = 1 - sys_down/mc_nom_stacked
    return ratio_up_sys, ratio_down_sys

# Load histograms

# General results directory
inputdir =  '/nfs/dust/cms/user/moralfk/EWZjj_Pepper/ALL_24thseptember_check/'
inputdir_onlyDphijj = '/nfs/dust/cms/user/moralfk/EWZjj_Pepper/ALL_24thseptember_only_dphijj_fullbtag/'
inputdir_DRmumu_DpTbalance = '/nfs/dust/cms/user/moralfk/EWZjj_Pepper/ALL_22ndseptember_additional_cuts_pTbalance_and_DRll_fullbtag/'
inputdir_QGL_cut = '/nfs/dust/cms/user/moralfk/EWZjj_Pepper/ALL_24thseptember_QGL_cut/'
# New file names

file_names = ['MET filters_pv_npvs.root', 'MET filters_leading_jet_QGL.root', 'MET filters_subleading_jet_QGL.root', 'MET filters_R_pT_hard.root', 'MET filters_z_star.root',
              'MET filters_leading_muon_pt.root', 'MET filters_leading_muon_abseta.root', 'MET filters_Delta_R_jets_l_mu.root', 'MET filters_Delta_R_jets_s_mu.root',
              'MET filters_subleading_muon_pt.root', 'MET filters_subleading_muon_abseta.root',
              'MET filters_leading_jet_pt.root', 'MET filters_leading_jet_abseta.root', 'MET filters_subleading_jet_pt.root', 'MET filters_subleading_jet_abseta.root',
              'MET filters_Dijet_pt.root', 'MET filters_Dijet_abseta.root', 'MET filters_Dijet_mass.root', 'MET filters_Dijet_phi.root',
              'MET filters_number_of_jets.root', 'MET filters_number_of_low_pT_jets.root', 'MET filters_dphijj.root', 'MET filters_detajj.root', 'MET filters_detall.root', 'MET filters_dphill.root',
              'MET filters_Zboson_pt.root', 'MET filters_Zboson_mass.root', 'MET filters_Zboson_abseta.root', 'MET filters_Zboson_phi.root',
              'MET filters_DphiZ_l_jet.root', 'MET filters_DphiZ_s_jet.root', 'MET filters_DphiZ_jets.root']

samples = ['EWK_LLJJ_MLL-50_MJJ-120_TuneCP5_13TeV-madgraph-pythia8_dipole', 'WW_TuneCP5_13TeV-pythia8', 'WZ_TuneCP5_13TeV-pythia8', 'ZZ_TuneCP5_13TeV-pythia8',
           'ST_tW_top_5f_inclusiveDecays_TuneCP5_13TeV-powheg-pythia8', 'ST_t-channel_top_4f_InclusiveDecays_TuneCP5_13TeV-powheg-madspin-pythia8',
           'ST_tW_antitop_5f_inclusiveDecays_TuneCP5_13TeV-powheg-pythia8','ST_t-channel_antitop_4f_InclusiveDecays_TuneCP5_13TeV-powheg-madspin-pythia8',
           'ST_s-channel_4f_leptonDecays_TuneCP5_13TeV-amcatnlo-pythia8', 'TTTo2L2Nu_TuneCP5_13TeV-powheg-pythia8','TTToSemiLeptonic_TuneCP5_13TeV-powheg-pythia8',
           'DYJetsToLL_M-50_TuneCP5_13TeV-amcatnloFXFX-pythia8', 'SingleMuon' ]

for file_name in file_names:

    mc_histograms = [None]*(len(samples)-1)
    mc_histograms_Dphijj = [None]*(len(samples)-1)
    mc_histograms_DRmumu_DpTbalance = [None]*(len(samples)-1)
    mc_histograms_QGL_cut = [None]*(len(samples)-1)

    for i in range(len(mc_histograms)):
        open_file = uproot.open(inputdir + file_name)
        mc_histograms[i] = open_file[samples[i]].to_hist()
        data_histogram = open_file['SingleMuon'].to_hist()
        
        open_file_Dphijj = uproot.open(inputdir_onlyDphijj + file_name)
        mc_histograms_Dphijj[i] = open_file_Dphijj[samples[i]].to_hist()
        data_histogram_Dphijj = open_file_Dphijj['SingleMuon'].to_hist()

        open_file_DRmumu_DpTbalance = uproot.open(inputdir_DRmumu_DpTbalance + file_name)
        mc_histograms_DRmumu_DpTbalance[i] = open_file_DRmumu_DpTbalance[samples[i]].to_hist()
        data_histogram_DRmumu_DpTbalance = open_file_DRmumu_DpTbalance['SingleMuon'].to_hist()

        open_file_QGL_cut = uproot.open(inputdir_QGL_cut+ file_name)
        mc_histograms_QGL_cut[i] = open_file_QGL_cut[samples[i]].to_hist()
        data_histogram_QGL_cut = open_file_QGL_cut['SingleMuon'].to_hist()

    # Set style
    plt.style.use(hep.style.ROOT)
    plt.rcParams.update({'font.size': 14})   
    plt.rcParams.update({'axes.titlesize':20})
    plt.rcParams.update({'axes.labelsize':20})

    # Set stack and ratio plot
    fig, ax = plt.subplots(2, 1, gridspec_kw={'height_ratios': [2, 1], 'hspace':0}, sharex=True)
    scatter_ax = ax[0]
    scatter_ax.axhline(y=1, color='black')
    scatter_ax.set_ylim(0.0, 1.5)
    # Calculate ratio Data/MC
    sum_mc_histos = sum(mc_histograms)
    wratio, bins, werr = make_ratio(data_histogram, sum_mc_histos)

    sum_mc_histos_Dphijj = sum(mc_histograms_Dphijj)
    wratio_Dphijj, bins_Dphijj, werr_Dphijj = make_ratio(data_histogram_Dphijj, sum_mc_histos_Dphijj)

    sum_mc_histos_DRmumu_DpTbalance = sum(mc_histograms_DRmumu_DpTbalance)
    wratio_DRmumu_DpTbalance, bins_DRmumu_DpTbalance, werr_DRmumu_DpTbalance = make_ratio(data_histogram_DRmumu_DpTbalance, sum_mc_histos_DRmumu_DpTbalance)
    
    sum_mc_histos_QGL_cut = sum(mc_histograms_QGL_cut)
    wratio_QGL_cut, bins_QGL_cut, werr_QGL_cut = make_ratio(data_histogram_QGL_cut, sum_mc_histos_QGL_cut)

    hep.histplot(wratio, bins, yerr = werr, ax = scatter_ax,  stack=False, histtype='errorbar', color='red', label = 'No Dphijj, No DRmumu, No pTBalance')
    hep.histplot(wratio_Dphijj, bins_Dphijj, yerr = (werr_Dphijj), ax= scatter_ax, stack=False, histtype='errorbar', color='blue', label = 'Only Dphijj cut')
    hep.histplot(wratio_DRmumu_DpTbalance, bins_DRmumu_DpTbalance, yerr = (werr_DRmumu_DpTbalance), ax= scatter_ax, stack=False, histtype='errorbar', 
    color='green', label = 'No Dphijj, yes DRmumu cut, yes DpTbalance ')
    hep.histplot(wratio_QGL_cut, bins_QGL_cut, yerr = werr_QGL_cut, ax = scatter_ax,  stack=False, histtype='errorbar', color='magenta', label = 'QGL cut')
    scatter_ax.legend()

    ratio_axis = ax[1]
    ratio_axis.axhline(y=1, color='black')
    wratio_data_Dphijj, bins_data_Dphijj, werr_data_Dphijj = make_ratio(data_histogram_Dphijj, data_histogram)
    wratio_mc_Dphijj, bins_mc_Dphijj, werr_mc_Dphijj = make_ratio(sum_mc_histos_Dphijj, sum_mc_histos)
    wratio_total_Dphijj = make_ratio_array(wratio_Dphijj, wratio)

    wratio_data_DRmumu_DpTbalance, bins_data_DRmumu_DpTbalance, werr_data_DRmumu_DpTbalance = make_ratio(data_histogram_DRmumu_DpTbalance, data_histogram)
    wratio_mc_DRmumu_DpTbalance, bins_mc_DRmumu_DpTbalance, werr_mc_DRmumu_DpTbalance = make_ratio(sum_mc_histos_DRmumu_DpTbalance, sum_mc_histos)
    wratio_total_DRmumu_DpTbalance = make_ratio_array(wratio_DRmumu_DpTbalance, wratio)

    wratio_data_QGL_cut, bins_data_QGL_cut, werr_data_QGL_cut = make_ratio(data_histogram_QGL_cut, data_histogram)
    wratio_mc_QGL_cut, bins_mc_QGL_cut, werr_mc_QGL_cut = make_ratio(sum_mc_histos_QGL_cut, sum_mc_histos)
    wratio_total_QGL_cut = make_ratio_array(wratio_QGL_cut, wratio)

    hep.histplot(wratio_data_Dphijj, bins_data_Dphijj, yerr= werr_data_Dphijj, ax = ratio_axis,  stack=False, histtype='errorbar', color='black', 
    label = 'Ratio(Data_Dphijj/Data_no_Dphijj_DRmumu_DpTbalance)') 
    hep.histplot(wratio_data_DRmumu_DpTbalance, bins_data_DRmumu_DpTbalance, yerr= werr_data_DRmumu_DpTbalance, ax = ratio_axis,  stack=False, 
    histtype='errorbar', color='darkviolet', label = 'Ratio(Data_DRmumu_DpTbalance/Data_no_Dphijj_DRmumu_DpTbalance)')
    hep.histplot(wratio_data_QGL_cut, bins_data_QGL_cut, yerr= werr_data_QGL_cut, ax = ratio_axis,  stack=False, histtype='errorbar', color='deeppink',
    label = 'Ratio(Data_QGL_cut/Data_no_Dphijj_DRmumu_DpTbalance)')

    hep.histplot(wratio_mc_Dphijj, bins_mc_Dphijj, yerr= werr_mc_Dphijj, ax = ratio_axis,  stack=False, histtype='errorbar', 
    color='teal', label = 'Ratio(MC_Dphijj/MC_no_Dphijj_DRmumu_DpTbalance)')
    hep.histplot(wratio_mc_DRmumu_DpTbalance, bins_mc_DRmumu_DpTbalance, yerr= werr_mc_DRmumu_DpTbalance, ax = ratio_axis,  stack=False, histtype='errorbar', 
    color='skyblue', label = 'Ratio(MC_DRmumu_DpTbalance/MC_no_Dphijj_DRmumu_DpTbalance)')
    hep.histplot(wratio_mc_Dphijj, bins_mc_Dphijj, yerr= werr_mc_Dphijj, ax = ratio_axis,  stack=False, histtype='errorbar',
    color='lightpink', label = 'Ratio(MC_QGL_cut/MC_no_Dphijj_DRmumu_DpTbalance)')
    
    #hep.histplot(wratio, bins_mc, ax = ratio_axis,  stack=False, histtype='step', color='red', label = 'Ratio($((Data/MC)_{MET})/((Data/MC)_{noMET}))$')

    ratio_axis.legend()
    if file_name == 'MET filters_Delta_R_jets_l_mu.root':
        ratio_axis.set_xlabel('$\\Delta R_{\\mu_{L} , jets}$')
    elif file_name == 'MET filters_Delta_R_jets_s_mu.root':
        ratio_axis.set_xlabel('$\\Delta R_{\\mu_{S} , jets}$')
    elif file_name == 'MET filters_pv_npvsGood.root':
        ratio_axis.set_xlabel('Number of good primary vertices')
    elif file_name == 'MET filters_leading_muon_pt.root':
        ratio_axis.set_xlabel('Leading muon transverse momentum: $p_{T}$ [GeV]')   
    elif file_name == 'MET filters_leading_muon_abseta.root':
        ratio_axis.set_xlabel('Leading muon $|\eta|$')
    elif file_name == 'MET filters_Delta_R_l_mu_l_jet.root':
        ratio_axis.set_xlabel(' $\\Delta R_{\\mu_{L} , jet_{L}}$')
    elif file_name == 'MET filters_Delta_R_l_mu_s_jet.root':
        ratio_axis.set_xlabel('$\\Delta R_{\\mu_{L} , jet_{S}}$')
    elif file_name == 'MET filters_Delta_R_l_jet.root':
        ratio_axis.set_xlabel(' $\\Delta R_{j_{L} , \\mu \\mu}$')
    elif file_name == 'MET filters_Delta_R_s_jet.root':
        ratio_axis.set_xlabel('$\\Delta R_{j_{S} , \\mu \\mu}$')
    elif file_name == 'MET filters_Delta_phi_l_mu_l_jet.root':
        ratio_axis.set_xlabel(' $\\Delta \\phi_{\\mu_{L}, jet_{L}}$')
    elif file_name == 'MET filters_Delta_phi_l_mu_s_jet.root':
        ratio_axis.set_xlabel('$\\Delta \\phi_{\\mu_{L}, jet_{S}}$')
    elif file_name == 'MET filters_subleading_muon_pt.root':
        ratio_axis.set_xlabel('Subleading muon transverse momentum: $p_{T}$ [GeV]')
    elif file_name == 'MET filters_subleading_muon_abseta.root':
        ratio_axis.set_xlabel('Subleading muon $|\eta|$')
    elif file_name == 'MET filters_leading_jet_pt.root':
        ratio_axis.set_xlabel('Leading jet transverse momentum: $p_{T}$ [GeV]')
    elif file_name == 'MET filters_Delta_R_s_mu_l_jet.root':
        ratio_axis.set_xlabel(' $\\Delta R_{\\mu_{S} , jet_{L}}$')
    elif file_name == 'MET filters_Delta_R_s_mu_s_jet.root':
        ratio_axis.set_xlabel('$\\Delta R_{\\mu_{S} , jet_{S}}$')
    elif file_name == 'MET filters_Delta_phi_s_mu_l_jet.root':
        ratio_axis.set_xlabel(' $\\Delta \\phi_{\\mu_{S}, jet_{L}}$')
    elif file_name == 'MET filters_Delta_phi_s_mu_s_jet.root':
        ratio_axis.set_xlabel('$\\Delta \\phi_{\\mu_{S}, jet_{S}}$')
    elif file_name == 'MET filters_leading_jet_abseta.root':
        ratio_axis.set_xlabel('Leading jet $|\eta|$')
    elif file_name == 'MET filters_subleading_jet_pt.root':
        ratio_axis.set_xlabel('Subleading jet transverse momentum: $p_{T}$ [GeV]')
    elif file_name == 'MET filters_subleading_jet_abseta.root':
        ratio_axis.set_xlabel('Subleading jet $|\eta|$')
    elif (file_name == 'MET filters_mjj.root') or (file_name == 'MET filters_Dijet_mass.root'):
        ratio_axis.set_xlabel('Dijet mass: $m_{jj}$ [GeV]', fontsize = 20)
    elif (file_name == 'MET filters_pTjj.root') or (file_name == 'MET filters_Dijet_pt.root'):
        ratio_axis.set_xlabel('$p_{Tjj}$ [GeV]', fontsize = 20)
    elif file_name == 'MET filters_Dijet_phi.root':
        ratio_axis.set_xlabel(' $\\phi_{jj}$ ', fontsize = 20)
    elif file_name == 'MET filters_Dijet_abseta.root':
        ratio_axis.set_xlabel(' $ | \\eta_{jj} | $ ', fontsize = 20)
    elif file_name == 'MET filters_Zboson_mass.root':
        ratio_axis.set_xlabel('Z boson mass: $m_{l^{+}l^{-}}$ [GeV]', fontsize = 20)
    elif file_name == 'MET filters_Zboson_pt.root':
        ratio_axis.set_xlabel('Z boson transverse momentum: $p_{T}$ [GeV]', fontsize = 20)
    elif file_name == 'MET filters_Zboson_abseta.root':
        ratio_axis.set_xlabel('$|\\eta|_{l^{+}l^{-}}$ ', fontsize = 20)
    elif file_name == 'MET filters_Zboson_phi.root':
        ratio_axis.set_xlabel('$|\\phi|_{l^{+}l^{-}}$ ', fontsize = 20)
    elif file_name == 'MET filters_number_of_jets.root':
        ratio_axis.set_xlabel('Number of jets')
    elif file_name == 'MET filters_number_of_low_pT_jets.root':
        ratio_axis.set_xlabel('Number of low pT jets')
    elif file_name == 'MET filters_DphiZ_jets.root':
        ratio_axis.set_xlabel(' $\\Delta \\phi_{Z, jj}$ ', fontsize = 20)
    elif file_name == 'MET filters_DphiZ_l_jet.root':
        ratio_axis.set_xlabel(' $\\Delta \\phi_{Z, j_{L}}$ ', fontsize = 20)
    elif file_name == 'MET filters_DphiZ_s_jet.root':
        ratio_axis.set_xlabel(' $\\Delta \\phi_{Z, j_{S}}$ ', fontsize = 20)
    elif file_name == 'MET filters_Delta_phi_l_jet_mus.root':
        ratio_axis.set_xlabel(' $\\Delta \\phi_{j_{L}, \\mu \\mu}$ ', fontsize = 20)
    elif file_name == 'MET filters_Delta_phi_s_jet_mus.root':
        ratio_axis.set_xlabel(' $\\Delta \\phi_{j_{S}, \\mu \\mu}$ ', fontsize = 20)
    elif file_name == 'MET filters_dphill.root':
        ratio_axis.set_xlabel(' $\\Delta \\phi_{\\mu \\mu}$ ', fontsize = 20)
    elif file_name == 'MET filters_dphijj.root':
        ratio_axis.set_xlabel(' $\\Delta \\phi_{j_{L} j_{S}}$ ', fontsize = 20)
    elif file_name == 'MET filters_detall.root':
        ratio_axis.set_xlabel(' $\\Delta \\eta_{\\mu \\mu}$ ', fontsize = 20)
    elif file_name == 'MET filters_detajj.root':
        ratio_axis.set_xlabel(' $\\Delta \\eta_{j_{L} j_{S}}$ ',fontsize = 20)
    elif file_name == 'MET filters_R_pT_hard.root':
        ratio_axis.set_xlabel('$R_{p_{T}}^{hard}$ ', fontsize = 20)
    elif file_name == 'MET filters_z_star.root':
        ratio_axis.set_xlabel(' $z^*$ ')
    elif file_name == 'MET filters_pv_npvs.root':
        ratio_axis.set_xlabel('Number of total reconstructed  primary vertices')
    elif file_name == 'Muon_SF_pv_npvs.root':
        ratio_axis.set_xlabel('Number of good primary vertices')
    elif file_name == 'Pileup reweighting_pv_npvs.root':
        ratio_axis.set_xlabel('Number of total reconstructed primary vertices')
    elif file_name == 'MET filters_QGD.root':
        ratio_axis.set_xlabel('QGL Discriminant')
    elif file_name == 'MET filters_Delta_R_jets_l_mu.root':
        ratio_axis.set_xlabel(' $R_{jets, \\mu_{L}}$ ')
    elif file_name == 'MET filters_Delta_R_jets_s_mu.root':
        ratio_axis.set_xlabel(' $R_{jets, \\mu_{S}}$ ')
    elif file_name == 'MET filters_leading_jet_QGL.root':
        ratio_axis.set_xlabel('Leading jet QGL', fontsize = 20)
    elif file_name == 'MET filters_subleading_jet_QGL.root':
        ratio_axis.set_xlabel('Subleading jet QGL', fontsize = 20)
    scatter_ax.set_ylabel(r'Data/MC')
    ratio_axis.set_ylim(0.0, 2.4)
    
    #plt.savefig('/afs/desy.de/user/m/moralfk/www/EWZjj-test-figures/NEW_DetectorLevelPlots/Pepper_muon_7thmay_SR_'+file_name+'.pdf')
    #plt.savefig('/afs/desy.de/user/m/moralfk/www/EWZjj-test-figures/NEW_DetectorLevelPlots/Pepper_muon_8thjune_goodPV_'+file_name+'.png')    
    plt.savefig('/afs/desy.de/user/m/moralfk/www/EWZjj-test-figures/NEW_DetectorLevelPlots/Pepper_muon_24thseptember_new_cuts_study__'+file_name+'.png')
    #plt.show() 
