import matplotlib.pyplot as plt
import mplhep as hep
import uproot
import matplotlib.gridspec as gridspec
import numpy as np
#DY.root  EWK_LLJJ_MLL.root  SingleMuon.root

def make_ratio(hnum, hden):
  wnum, bins = hnum.to_numpy()
  wden, bins = hden.to_numpy()

  numerr = np.sqrt(hnum.variances())
  
  #wratio = np.divide(wnum, wden) - 1
  wratio = np.divide(wnum, wden)
  print('data')
  print(wnum)
  print('mc')
  print(wden)
  ratioerr = np.divide(numerr, wden)
  bincenters = (bins[:-1] + bins[1:]) / 2
  return wratio, bins, ratioerr





# Load histograms

# General results directory
inputdir = '/nfs/dust/cms/user/moralfk/EWZjj_Pepper/hists/'
"""
file_names = ['Dimuon_cut_leading_muon_pt.root', 'Dimuon_cut_leading_muon_abseta.root', 'Dimuon_cut_subleading_muon_pt.root', 'Dimuon_cut_subleading_muon_abseta.root', 
              'Dimuon_cut_leading_jet_pt.root', 'Dimuon_cut_leading_jet_abseta.root', 'Dimuon_cut_subleading_jet_pt.root', 'Dimuon_cut_subleading_jet_abseta.root', 
              'Dimuon_cut_mjj.root', 'Dimuon_cut_Zboson_mass.root', 'Dimuon_cut_Zboson_pt.root', 'Dimuon_cut_number_of_jets.root']
"""
file_names = ['SR_Dimuon_cut_Zboson_mass.root']
samples = ['EWK_LLJJ_MLL-50_MJJ-120_TuneCP5_13TeV-madgraph-pythia8_dipole_SR', 'WW_TuneCP5_13TeV-pythia8_SR', 'WZ_TuneCP5_13TeV-pythia8_SR', 'ZZ_TuneCP5_13TeV-pythia8_SR',
           'ST_tW_top_5f_inclusiveDecays_TuneCP5_13TeV-powheg-pythia8_SR', 'ST_t-channel_top_4f_InclusiveDecays_TuneCP5_13TeV-powheg-madspin-pythia8_SR',
           'ST_tW_antitop_5f_inclusiveDecays_TuneCP5_13TeV-powheg-pythia8_SR','ST_t-channel_top_4f_InclusiveDecays_TuneCP5_13TeV-powheg-madspin-pythia8_SR',
           'ST_s-channel_4f_leptonDecays_TuneCP5_13TeV-amcatnlo-pythia8_SR', 'TTTo2L2Nu_TuneCP5_13TeV-powheg-pythia8_SR','DYJetsToLL_M-50_TuneCP5_13TeV-amcatnloFXFX-pythia8_SR','SingleMuon_SR' ]

for file_name in file_names: 
    mc_histograms = [None]*(len(samples)-1)

    for i in range(len(mc_histograms)):
        open_file = uproot.open(inputdir + file_name)
        mc_histograms[i] = open_file[samples[i]].to_hist() 
        data_histogram = open_file['SingleMuon_SR'].to_hist()

    # Set style
    plt.style.use(hep.style.ROOT)
    plt.rcParams.update({'font.size': 14})   
    plt.rcParams.update({'axes.titlesize':10})
    plt.rcParams.update({'axes.titlesize':10})

    # Set stack and ratio plot
    fig, ax = plt.subplots(2, 1, gridspec_kw={'height_ratios': [2, 1], 'hspace':0}, sharex=True)

    # Make stack plot in upper axis
    stack_axis = ax[0]
    """
    color = ['firebrick', 'lightcoral', 'lightcoral', 'lightcoral', 'coral', 'coral', 'coral', 'coral', 'coral', 'chocolate', 'darksalmon', 'darksalmon', 'darksalmon', 'darksalmon', 'darksalmon']
    #color = ['firebrick', 'lightcoral', 'coral', 'chocolate', 'darksalmon']
    legend_names = lenged_names = [r'EWK $Z\rightarrow\mu^{+}\mu^{-}$', 'VV', '_nolegend_', '_nolegend_', 'Single top','_nolegend_', '_nolegend_', '_nolegend_', '_nolegend_',
                                   r'TT $\rightarrow\l^{+}\l^{-}$', r'DY $Z\rightarrow \mu^{+}\mu^{-}$', '_nolegend_', '_nolegend_', '_nolegend_', '_nolegend_']  
    """
    color = ['firebrick', 'lightcoral', 'lightcoral', 'lightcoral', 'tomato', 'tomato', 'tomato', 'tomato', 'tomato', 'chocolate', 'darksalmon']
    legend_names = lenged_names = [r'EWK $Z\rightarrow\mu^{+}\mu^{-}$', 'VV', '_nolegend_', '_nolegend_', 'Single top','_nolegend_', '_nolegend_', '_nolegend_', '_nolegend_',
                                   r'TT $\rightarrow\l^{+}\l^{-}$', r'DY $Z\rightarrow \mu^{+}\mu^{-}$']
  
    hep.histplot(mc_histograms, color = color, ax=stack_axis, binwnorm=1., stack=True, histtype='fill', label = legend_names)
    hep.histplot(data_histogram, ax=stack_axis, binwnorm=1., stack=False, histtype='errorbar', color='black', label='Data')
    stack_axis.legend()
    stack_axis.set_yscale('log')
    stack_axis.set_ylabel(r'Events/bin')
    
    # Ratio axis
    ratio_axis = ax[1]
    ratio_axis.axhline(y=1, color='black')
    # Sum stack
    
    sum_mc_histos = sum(mc_histograms)
    # Calculate ratio Data/MC
    wratio, bins, werr = make_ratio(data_histogram, sum_mc_histos)
    print('ratio') 
    print(wratio)
    wratioMC, binsMC, werrMC = make_ratio(sum_mc_histos, sum_mc_histos)
    hep.histplot(wratio, bins, yerr = (werr), stack=False, histtype='errorbar', color='black', ax = ratio_axis)
    hep.histplot(np.ones(len(wratio)), bins, yerr = (werrMC), stack=False, histtype='errorbar', color='black', ax = ratio_axis, marker = "") 
    if file_name == 'Dimuon_cut_leading_muon_pt.root':
        ratio_axis.set_xlabel('Leading muon transverse momentum: $p_{T}$ [GeV]')   
    elif file_name == 'Dimuon_cut_leading_muon_abseta.root':
        ratio_axis.set_xlabel('Leading muon $|\eta|$')
    elif file_name == 'Dimuon_cut_subleading_muon_pt.root':
        ratio_axis.set_xlabel('Subleading muon transverse momentum: $p_{T}$ [GeV]')
    elif file_name == 'Dimuon_cut_subleading_muon_abseta.root':
        ratio_axis.set_xlabel('Subleading muon $|\eta|$')
    elif file_name == 'Dimuon_cut_leading_jet_pt.root':
        ratio_axis.set_xlabel('Leading jet transverse momentum: $p_{T}$ [GeV]')
    elif file_name == 'Dimuon_cut_leading_jet_abseta.root':
        ratio_axis.set_xlabel('Leading jet $|\eta|$')
    elif file_name == 'Dimuon_cut_subleading_jet_pt.root':
        ratio_axis.set_xlabel('Subleading jet transverse momentum: $p_{T}$ [GeV]')
    elif file_name == 'Dimuon_cut_subleading_jet_abseta.root':
        ratio_axis.set_xlabel('Subleading jet $|\eta|$')
    elif file_name == 'Dimuon_cut_mjj.root':
        ratio_axis.set_xlabel('Dijet mass: $m_{jj}$ [GeV]')
    elif file_name == 'SR_Dimuon_cut_Zboson_mass.root':
        ratio_axis.set_xlabel('Z boson mass: $m_{l^{+}l^{-}}$ [GeV]')
    elif file_name == 'Dimuon_cut_Zboson_pt.root':
        ratio_axis.set_xlabel('Z boson transverse momentum: $p_{T}$ [GeV]')
    elif file_name == 'Dimuon_cut_number_of_jets.root':
        ratio_axis.set_xlabel('Number of jets')

    #ratio_axis.set_xlabel(file_name+'[GeV]')
    ratio_axis.set_ylabel(r'Data/MC')
    # ratio y_axis (0.8, 1.2)
    ratio_axis.set_ylim(0.5, 1.5)
    #ratio_axis.set_ylim(-0.5, 0.5)
    hep.cms.label(loc=0, data=True, ax=stack_axis, lumi=59.83, year=2018)

    #plt.savefig('/afs/desy.de/user/m/moralfk/www/EWZjj-test-figures/NEW_DetectorLevelPlots/Pepper_muon_21stapril_'+file_name+'.pdf')
    #plt.savefig('/afs/desy.de/user/m/moralfk/www/EWZjj-test-figures/NEW_DetectorLevelPlots/Pepper_muon_27thapril_'+file_name+'.png')    
    plt.show() 
