import matplotlib.pyplot as plt
import mplhep as hep
import uproot
import matplotlib.gridspec as gridspec
import numpy as np
import matplotlib.patches as patches
import matplotlib.path as path
import hist
from hist import Hist
#from pathlib import Path
#from pathlib import PathPatch
#DY.root  EWK_LLJJ_MLL.root  SingleMuon.root

def make_ratio(hnum, hden):
  wnum, bins = hnum.to_numpy()
  wden, bins = hden.to_numpy()

  numerr = np.sqrt(hnum.variances())
  
  #wratio = np.divide(wnum, wden) - 1
  wratio = np.divide(wnum, np.sqrt(wden))
  print('data')
  print(wnum)
  print('mc')
  print(wden)
  ratioerr = np.divide(numerr, wden)
  bincenters = (bins[:-1] + bins[1:]) / 2
  return wratio, bins, ratioerr

def systematics(mc_nom_file, sys_up_file, sys_down_file):
    sys_up_f = 0
    sys_down_f = 0
    for mc_nom, sys_up_value, sys_down_value in zip(mc_nom_file, sys_up_file, sys_down_file):
        mc_nom = hist.Hist(mc_nom)
        sys_up_value = hist.Hist(sys_up_value)
        sys_down_value = hist.Hist(sys_down_value)
        sys_up_temp = ((sys_up_value.values() - mc_nom.values())**2)
        sys_down_temp = ((sys_down_value.values() - mc_nom.values())**2)
        sys_up_f = sys_up_temp + sys_up_f
        sys_down_f = sys_down_temp + sys_down_f
    sys_up = np.sqrt(sys_up_f)
    sys_down = np.sqrt(sys_down_f)
    
    return sys_up, sys_down

def total_systematics(sys_up, sys_down):
    sys_up_f = 0
    sys_down_f = 0
    for sys_up_partial, sys_down_partial in zip(sys_up, sys_down):
        sys_up_temp = (sys_up_partial**2)
        sys_down_temp = (sys_down_partial**2)
        sys_up_f = sys_up_temp + sys_up_f
        sys_down_f = sys_down_temp + sys_down_f
    total_sys_up = np.sqrt(sys_up_f)
    total_sys_down = np.sqrt(sys_down_f)
    return total_sys_up, total_sys_down

def systematics_ratio(sys_up, sys_down, mc_nom_stacked):
    ratio_up_sys = 1 + sys_up/mc_nom_stacked  
    ratio_down_sys = 1 - sys_down/mc_nom_stacked
    return ratio_up_sys, ratio_down_sys

# Load histograms

# General results directory
inputdir =  '/nfs/dust/cms/user/moralfk/EWZjj_Pepper/ALL_24thseptember_check_sys/'
inputdir_etajj =  '/nfs/dust/cms/user/moralfk/EWZjj_Pepper/ALL_8thOctober_Mu_detajj15/'
inputdir_etajj_mjj =  '/nfs/dust/cms/user/moralfk/EWZjj_Pepper/ALL_8thOctober_Mu_detajj15_mjj400/'
inputdir_etajj_mjj_dphijj = '/nfs/dust/cms/user/moralfk/EWZjj_Pepper/ALL_8thOctober_Mu_detajj15_mjj400_dphijj1/'
inputdir_etajj_mjj_dphill =  '/nfs/dust/cms/user/moralfk/EWZjj_Pepper/ALL_8thOctober_Mu_detajj15_mjj400_dphill28/'
inputdir_etajj_mjj_dphijj_dphill =  '/nfs/dust/cms/user/moralfk/EWZjj_Pepper/ALL_8thOctober_Mu_detajj15_mjj400_dphijj1i_dphill28/'
inputdir_etajj_mjj_pTll = '/nfs/dust/cms/user/moralfk/EWZjj_Pepper/ALL_8thOctober_Mu_detajj15_mjj400_pTll30/'
inputdir_etajj_mjj_dphijj_dphill_pTll = '/nfs/dust/cms/user/moralfk/EWZjj_Pepper/ALL_8thOctober_Mu_detajj15_mjj400_dphijj1_dphill28_pTl30/'
inputdir_etajj_mjj_etastar =  '/nfs/dust/cms/user/moralfk/EWZjj_Pepper/ALL_8thOctober_Mu_detajj15_mjj400_etastar_cond/'
inputdir_etajj_mjj_dphijj_dphill_etastar = '/nfs/dust/cms/user/moralfk/EWZjj_Pepper/ALL_8thOctober_Mu_detajj15_mjj400_dphijj1_dphill28_etastarcond/'
inputdir_etajj_mjj_dphijj_dphill_pTll_etastar = '/nfs/dust/cms/user/moralfk/EWZjj_Pepper/ALL_8thOctober_Mu_detajj15_mjj400_dphijj1_dphill28_pTll30_etastarcond/'
inputdir_etajj4_mjj_dphill_etacuts = '/nfs/dust/cms/user/moralfk/EWZjj_Pepper/ALL_19thOctober_detajj4_etacuts3_sys/'
inputdir_jets_pTj50_mjj200_Detajj15_Zetacuts =  '/nfs/dust/cms/user/moralfk/EWZjj_Pepper/ALL_5thNovember_pt_ordered_jets_pTj50_mjj200_Detajj15_Zetacuts_sys/'
# New file names
file_names = ['MET filters_pv_npvs.root', 'MET filters_leading_jet_QGL.root', 'MET filters_subleading_jet_QGL.root', 'MET filters_R_pT_hard.root', 'MET filters_z_star.root',
              'MET filters_leading_muon_pt.root', 'MET filters_leading_muon_abseta.root', 'MET filters_Delta_R_jets_l_mu.root', 'MET filters_Delta_R_jets_s_mu.root',
              'MET filters_subleading_muon_pt.root', 'MET filters_subleading_muon_abseta.root',
              'MET filters_leading_jet_pt.root', 'MET filters_leading_jet_abseta.root', 'MET filters_subleading_jet_pt.root', 'MET filters_subleading_jet_abseta.root',
              'MET filters_Dijet_pt.root', 'MET filters_Dijet_abseta.root', 'MET filters_Dijet_mass.root', 'MET filters_Dijet_phi.root',
              'MET filters_number_of_jets.root', 'MET filters_number_of_low_pT_jets.root', 'MET filters_dphijj.root', 'MET filters_detajj.root', 'MET filters_detall.root', 'MET filters_dphill.root',
              'MET filters_Zboson_pt.root', 'MET filters_Zboson_mass.root', 'MET filters_Zboson_abseta.root', 'MET filters_Zboson_phi.root',
              'MET filters_DphiZ_l_jet.root', 'MET filters_DphiZ_s_jet.root', 'MET filters_DphiZ_jets.root']

#file_names = ['MET filters_detajj.root']
samples = ['EWK_LLJJ_MLL-50_MJJ-120_TuneCP5_13TeV-madgraph-pythia8_dipole',
           'DYJetsToLL_M-50_TuneCP5_13TeV-amcatnloFXFX-pythia8', 'SingleMuon']

for file_name in file_names: 

    mc_histograms = [None]*(len(samples)-1)

    for i in range(len(mc_histograms)):
        open_file = uproot.open(inputdir + file_name)
        open_file_etajj = uproot.open(inputdir_etajj + file_name)
        open_file_etajj_mjj = uproot.open(inputdir_etajj_mjj + file_name)
        open_file_etajj_mjj_dphijj = uproot.open(inputdir_etajj_mjj_dphijj + file_name)
        open_file_etajj_mjj_dphill = uproot.open(inputdir_etajj_mjj_dphill + file_name)
        open_file_etajj_mjj_dphijj_dphill = uproot.open(inputdir_etajj_mjj_dphijj_dphill + file_name)
        open_file_etajj_mjj_pTll = uproot.open(inputdir_etajj_mjj_pTll + file_name)
        open_file_etajj_mjj_dphijj_dphill_pTll = uproot.open(inputdir_etajj_mjj_dphijj_dphill_pTll + file_name)
        open_file_etajj_mjj_etastar = uproot.open(inputdir_etajj_mjj_etastar + file_name)
        open_file_etajj_mjj_dphijj_dphill_etastar = uproot.open(inputdir_etajj_mjj_dphijj_dphill_etastar + file_name)
        open_file_etajj_mjj_dphijj_dphill_pTll_etastar = uproot.open(inputdir_etajj_mjj_dphijj_dphill_pTll_etastar + file_name)
        open_file_etajj4_mjj_dphill_etacuts = uproot.open(inputdir_etajj4_mjj_dphill_etacuts + file_name)
        open_file_jets_pTj50_mjj200_Detajj15_Zetacuts = uproot.open(inputdir_jets_pTj50_mjj200_Detajj15_Zetacuts + file_name)

        DY_histograms = open_file['DYJetsToLL_M-50_TuneCP5_13TeV-amcatnloFXFX-pythia8'].to_hist()
        EWK_histograms = open_file['EWK_LLJJ_MLL-50_MJJ-120_TuneCP5_13TeV-madgraph-pythia8_dipole'].to_hist()
        
        DY_histograms_etajj = open_file_etajj['DYJetsToLL_M-50_TuneCP5_13TeV-amcatnloFXFX-pythia8'].to_hist()
        EWK_histograms_etajj = open_file_etajj['EWK_LLJJ_MLL-50_MJJ-120_TuneCP5_13TeV-madgraph-pythia8_dipole'].to_hist()

        DY_histograms_etajj_mjj = open_file_etajj_mjj['DYJetsToLL_M-50_TuneCP5_13TeV-amcatnloFXFX-pythia8'].to_hist()
        EWK_histograms_etajj_mjj = open_file_etajj_mjj['EWK_LLJJ_MLL-50_MJJ-120_TuneCP5_13TeV-madgraph-pythia8_dipole'].to_hist()

        DY_histograms_etajj_mjj_dphijj = open_file_etajj_mjj_dphijj['DYJetsToLL_M-50_TuneCP5_13TeV-amcatnloFXFX-pythia8'].to_hist()
        EWK_histograms_etajj_mjj_dphijj = open_file_etajj_mjj_dphijj['EWK_LLJJ_MLL-50_MJJ-120_TuneCP5_13TeV-madgraph-pythia8_dipole'].to_hist()

        DY_histograms_etajj_mjj_dphill = open_file_etajj_mjj_dphill['DYJetsToLL_M-50_TuneCP5_13TeV-amcatnloFXFX-pythia8'].to_hist()
        EWK_histograms_etajj_mjj_dphill = open_file_etajj_mjj_dphill['EWK_LLJJ_MLL-50_MJJ-120_TuneCP5_13TeV-madgraph-pythia8_dipole'].to_hist()

        DY_histograms_etajj_mjj_dphijj_dphill = open_file_etajj_mjj_dphijj_dphill['DYJetsToLL_M-50_TuneCP5_13TeV-amcatnloFXFX-pythia8'].to_hist()
        EWK_histograms_etajj_mjj_dphijj_dphill = open_file_etajj_mjj_dphijj_dphill['EWK_LLJJ_MLL-50_MJJ-120_TuneCP5_13TeV-madgraph-pythia8_dipole'].to_hist()

        DY_histograms_etajj_mjj_pTll = open_file_etajj_mjj_pTll['DYJetsToLL_M-50_TuneCP5_13TeV-amcatnloFXFX-pythia8'].to_hist()
        EWK_histograms_etajj_mjj_pTll = open_file_etajj_mjj_pTll['EWK_LLJJ_MLL-50_MJJ-120_TuneCP5_13TeV-madgraph-pythia8_dipole'].to_hist()
        
        DY_histograms_etajj_mjj_dphijj_dphill_pTll = open_file_etajj_mjj_dphijj_dphill_pTll['DYJetsToLL_M-50_TuneCP5_13TeV-amcatnloFXFX-pythia8'].to_hist()
        EWK_histograms_etajj_mjj_dphijj_dphill_pTll = open_file_etajj_mjj_dphijj_dphill_pTll['EWK_LLJJ_MLL-50_MJJ-120_TuneCP5_13TeV-madgraph-pythia8_dipole'].to_hist()

        DY_histograms_etajj_mjj_etastar = open_file_etajj_mjj_etastar['DYJetsToLL_M-50_TuneCP5_13TeV-amcatnloFXFX-pythia8'].to_hist()
        EWK_histograms_etajj_mjj_etastar = open_file_etajj_mjj_etastar['EWK_LLJJ_MLL-50_MJJ-120_TuneCP5_13TeV-madgraph-pythia8_dipole'].to_hist()

        DY_histograms_etajj_mjj_dphijj_dphill_etastar = open_file_etajj_mjj_dphijj_dphill_etastar['DYJetsToLL_M-50_TuneCP5_13TeV-amcatnloFXFX-pythia8'].to_hist()
        EWK_histograms_etajj_mjj_dphijj_dphill_etastar = open_file_etajj_mjj_dphijj_dphill_etastar['EWK_LLJJ_MLL-50_MJJ-120_TuneCP5_13TeV-madgraph-pythia8_dipole'].to_hist()

        DY_histograms_etajj_mjj_dphijj_dphill_pTll_etastar = open_file_etajj_mjj_dphijj_dphill_pTll_etastar['DYJetsToLL_M-50_TuneCP5_13TeV-amcatnloFXFX-pythia8'].to_hist()
        EWK_histograms_etajj_mjj_dphijj_dphill_pTll_etastar = open_file_etajj_mjj_dphijj_dphill_pTll_etastar['EWK_LLJJ_MLL-50_MJJ-120_TuneCP5_13TeV-madgraph-pythia8_dipole'].to_hist()
        
        DY_histograms_etajj4_mjj_dphill_etacuts = open_file_etajj4_mjj_dphill_etacuts['DYJetsToLL_M-50_TuneCP5_13TeV-amcatnloFXFX-pythia8'].to_hist()
        EWK_histograms_etajj4_mjj_dphill_etacuts = open_file_etajj4_mjj_dphill_etacuts['EWK_LLJJ_MLL-50_MJJ-120_TuneCP5_13TeV-madgraph-pythia8_dipole'].to_hist()

        DY_histograms_pTj50_mjj200_Detajj15_Zetacuts = open_file_jets_pTj50_mjj200_Detajj15_Zetacuts['DYJetsToLL_M-50_TuneCP5_13TeV-amcatnloFXFX-pythia8'].to_hist()
        EWK_histograms_pTj50_mjj200_Detajj15_Zetacuts = open_file_jets_pTj50_mjj200_Detajj15_Zetacuts['EWK_LLJJ_MLL-50_MJJ-120_TuneCP5_13TeV-madgraph-pythia8_dipole'].to_hist()
    # Set style
    plt.style.use(hep.style.ROOT)
    plt.rcParams.update({'font.size': 14})   
    plt.rcParams.update({'axes.titlesize':20})
    plt.rcParams.update({'axes.labelsize':20})

    # Set stack and ratio plot
    fig, ratio_axis = plt.subplots()
    # Calculate ratio Data/MC
    wratio, bins, werr = make_ratio(EWK_histograms, DY_histograms)
    wratio_etajj, bins_etajj, werr_etajj = make_ratio(EWK_histograms_etajj, DY_histograms_etajj)
    wratio_etajj_mjj, bins_etajj_mjj, werr_etajj_mjj = make_ratio(EWK_histograms_etajj_mjj, DY_histograms_etajj_mjj)
    wratio_etajj_mjj_dphijj, bins_etajj_mjj_dphijj, werr_etajj_mjj_dphijj = make_ratio(EWK_histograms_etajj_mjj_dphijj, DY_histograms_etajj_mjj_dphijj)
    wratio_etajj_mjj_dphill, bins_etajj_mjj_dphill, werr_etajj_mjj_dphill = make_ratio(EWK_histograms_etajj_mjj_dphill, DY_histograms_etajj_mjj_dphill)
    wratio_etajj_mjj_dphijj_dphill, bins_etajj_mjj_dphijj_dphill, werr_etajj_mjj_dphijj_dphill = make_ratio(EWK_histograms_etajj_mjj_dphijj_dphill, DY_histograms_etajj_mjj_dphijj_dphill)
    wratio_etajj_mjj_pTll, bins_etajj_mjj_pTll, werr_etajj_mjj_pTll = make_ratio(EWK_histograms_etajj_mjj_pTll, DY_histograms_etajj_mjj_pTll)
    wratio_etajj_mjj_dphijj_dphill_pTll, bins_etajj_mjj_dphijj_dphill_pTll, werr_etajj_mjj_dphijj_dphill_pTll = make_ratio(EWK_histograms_etajj_mjj_dphijj_dphill_pTll, 
      DY_histograms_etajj_mjj_dphijj_dphill_pTll)
    wratio_etajj_mjj_etastar, bins_etajj_mjj_etastar, werr_etajj_mjj_etastar = make_ratio(EWK_histograms_etajj_mjj_etastar, DY_histograms_etajj_mjj_etastar)
    wratio_etajj_mjj_dphijj_dphill_etastar, bins_etajj_mjj_dphijj_dphill_etastar, werr_etajj_mjj_dphijj_dphill_etastar = make_ratio(EWK_histograms_etajj_mjj_dphijj_dphill_etastar, 
      DY_histograms_etajj_mjj_dphijj_dphill_etastar)
    wratio_etajj_mjj_dphijj_dphill_pTll_etastar, bins_etajj_mjj_dphijj_dphill_pTll_etastar, werr_etajj_mjj_dphijj_dphill_pTll_etastar = make_ratio(EWK_histograms_etajj_mjj_dphijj_dphill_pTll_etastar, 
      DY_histograms_etajj_mjj_dphijj_dphill_pTll_etastar)
    wratio_etajj4_mjj_dphill_etacuts, bins_etajj4_mjj_dphill_etacuts, werr_etajj4_mjj_dphill_etacuts = make_ratio(EWK_histograms_etajj4_mjj_dphill_etacuts, DY_histograms_etajj4_mjj_dphill_etacuts)
    wratio_pTj50_mjj200_Detajj15_Zetacuts, bins_pTj50_mjj200_Detajj15_Zetacuts, werr_pTj50_mjj200_Detajj15_Zetacuts = make_ratio(EWK_histograms_pTj50_mjj200_Detajj15_Zetacuts, 
      DY_histograms_pTj50_mjj200_Detajj15_Zetacuts)
    # Plots
    hep.histplot(wratio, bins, yerr = (werr), stack=False, histtype='step', color='mediumturquoise', label = 'Nominal')
    #hep.histplot(np.sqrt(wratio_etajj), bins_etajj, yerr = (werr_etajj), stack=False, histtype='step', color='crimson', label = 'Detajj > 1.5')
    #hep.histplot(np.sqrt(wratio_etajj_mjj), bins_etajj_mjj, yerr = (werr_etajj_mjj), stack=False, histtype='step', color='slateblue', label = 'Detajj > 1.5, mjj > 400 GeV')
    
    #hep.histplot(np.sqrt(wratio_etajj_mjj_dphijj), bins_etajj_mjj_dphijj, yerr = (werr_etajj_mjj_dphijj), stack=False, histtype='step', color='orchid', label = 'Detajj > 1.5, mjj > 400 GeV, dphijj > 1')
    
    hep.histplot(wratio_etajj_mjj_dphill, bins_etajj_mjj_dphill, yerr = (werr_etajj_mjj_dphill), stack=False, histtype='step', color='green', 
                 label = 'Detajj > 1.5, mjj > 400 GeV, dphill < 2.8')
    """
    hep.histplot(np.sqrt(wratio_etajj_mjj_dphijj_dphill), bins_etajj_mjj_dphijj_dphill, yerr = (werr_etajj_mjj_dphijj_dphill), stack=False, histtype='step', color='darkviolet',
                 label = 'Detajj > 1.5, mjj > 400 GeV, dphijj > 1, dphill < 2.8')
    hep.histplot(np.sqrt(wratio_etajj_mjj_pTll), bins_etajj_mjj_pTll, yerr = (werr_etajj_mjj_pTll), stack=False, histtype='step', color='darkorange', 
                  label = 'Detajj > 1.5, mjj > 400 GeV, pTll > 30 GeV')
    hep.histplot(np.sqrt(wratio_etajj_mjj_dphijj_dphill_pTll), bins_etajj_mjj_dphijj_dphill_pTll, yerr = (werr_etajj_mjj_dphijj_dphill_pTll), stack=False, histtype='step', color='dodgerblue',
                 label = 'Detajj > 1.5, mjj > 400 GeV, dphijj > 1, dphill < 2.8, pTll > 30 GeV')
    hep.histplot(np.sqrt(wratio_etajj_mjj_etastar), bins_etajj_mjj_etastar, yerr = (werr_etajj_mjj_etastar), stack=False, histtype='step', color='chocolate', 
                 label = 'Detajj > 1.5, mjj > 400 GeV, eta* < 3')
    hep.histplot(np.sqrt(wratio_etajj_mjj_dphijj_dphill_etastar), bins_etajj_mjj_dphijj_dphill_etastar, yerr = (werr_etajj_mjj_dphijj_dphill_etastar), stack=False, histtype='step', color='grey',
                 label = 'Detajj > 1.5, mjj > 400 GeV, dphijj > 1, dphill < 2.8, eta* < 3')
    hep.histplot(np.sqrt(wratio_etajj_mjj_dphijj_dphill_pTll_etastar), bins_etajj_mjj_dphijj_dphill_pTll_etastar, yerr = (werr_etajj_mjj_dphijj_dphill_pTll_etastar), stack=False, 
                 histtype='step', color='black', label = 'Detajj > 1.5, mjj > 400 GeV, dphijj > 1, dphill < 2.8, pTll > 30 GeV, eta* < 3')
    """
    hep.histplot(wratio_etajj4_mjj_dphill_etacuts, bins_etajj4_mjj_dphill_etacuts, yerr = (werr_etajj4_mjj_dphill_etacuts), stack=False, histtype='step', color='mediumvioletred', 
                label = 'Detajj > 4, mjj > 400 GeV, dphill < 2.8, min_eta_jets < eta_muons < max_eta_jets')     
    hep.histplot(wratio_pTj50_mjj200_Detajj15_Zetacuts, bins_pTj50_mjj200_Detajj15_Zetacuts, yerr = (werr_pTj50_mjj200_Detajj15_Zetacuts), stack=False, histtype='step', color='orange', 
                label = 'pTj > 50 GeV, Detajj > 1.5, mjj > 200 GeV, dphill < 2.8, min_eta_jets < eta_Z < max_eta_jets')

    #comment
    """
    hep.histplot(wratio, bins, yerr = (werr), stack=False, histtype='step', color='mediumturquoise', label = 'No additional cuts')
    hep.histplot(wratio_pTbalance_and_DRll, bins_pTbalance_and_DRll, yerr = (werr_pTbalance_and_DRll), stack=False, histtype='step', color='crimson', label = 'pTbalance_and_DRll')
    hep.histplot(wratio_dphijj, bins_dphijj, yerr = (werr_dphijj), stack=False, histtype='step', color='blue', label = 'dphijj')
    hep.histplot(wratio_QGL, bins_QGL, yerr = (werr_QGL), stack=False, histtype='step', color='orchid', label = 'QGL')
    """
    plt.legend() 
    if file_name == 'MET filters_Delta_R_jets_l_mu.root':
        ratio_axis.set_xlabel('$\\Delta R_{\\mu_{L} , jets}$')
    elif file_name == 'MET filters_Delta_R_jets_s_mu.root':
        ratio_axis.set_xlabel('$\\Delta R_{\\mu_{S} , jets}$')
    elif file_name == 'MET filters_pv_npvsGood.root':
        ratio_axis.set_xlabel('Number of good primary vertices')
    elif file_name == 'MET filters_leading_muon_pt.root':
        ratio_axis.set_xlabel('Leading muon transverse momentum: $p_{T}$ [GeV]')   
    elif file_name == 'MET filters_leading_muon_abseta.root':
        ratio_axis.set_xlabel('Leading muon $|\eta|$')
    elif file_name == 'MET filters_Delta_R_l_mu_l_jet.root':
        ratio_axis.set_xlabel(' $\\Delta R_{\\mu_{L} , jet_{L}}$')
    elif file_name == 'MET filters_Delta_R_l_mu_s_jet.root':
        ratio_axis.set_xlabel('$\\Delta R_{\\mu_{L} , jet_{S}}$')
    elif file_name == 'MET filters_Delta_R_l_jet.root':
        ratio_axis.set_xlabel(' $\\Delta R_{j_{L} , \\mu \\mu}$')
    elif file_name == 'MET filters_Delta_R_s_jet.root':
        ratio_axis.set_xlabel('$\\Delta R_{j_{S} , \\mu \\mu}$')
    elif file_name == 'MET filters_Delta_phi_l_mu_l_jet.root':
        ratio_axis.set_xlabel(' $\\Delta \\phi_{\\mu_{L}, jet_{L}}$')
    elif file_name == 'MET filters_Delta_phi_l_mu_s_jet.root':
        ratio_axis.set_xlabel('$\\Delta \\phi_{\\mu_{L}, jet_{S}}$')
    elif file_name == 'MET filters_subleading_muon_pt.root':
        ratio_axis.set_xlabel('Subleading muon transverse momentum: $p_{T}$ [GeV]')
    elif file_name == 'MET filters_subleading_muon_abseta.root':
        ratio_axis.set_xlabel('Subleading muon $|\eta|$')
    elif file_name == 'MET filters_leading_jet_pt.root':
        ratio_axis.set_xlabel('Leading jet transverse momentum: $p_{T}$ [GeV]')
    elif file_name == 'MET filters_Delta_R_s_mu_l_jet.root':
        ratio_axis.set_xlabel(' $\\Delta R_{\\mu_{S} , jet_{L}}$')
    elif file_name == 'MET filters_Delta_R_s_mu_s_jet.root':
        ratio_axis.set_xlabel('$\\Delta R_{\\mu_{S} , jet_{S}}$')
    elif file_name == 'MET filters_Delta_phi_s_mu_l_jet.root':
        ratio_axis.set_xlabel(' $\\Delta \\phi_{\\mu_{S}, jet_{L}}$')
    elif file_name == 'MET filters_Delta_phi_s_mu_s_jet.root':
        ratio_axis.set_xlabel('$\\Delta \\phi_{\\mu_{S}, jet_{S}}$')
    elif file_name == 'MET filters_leading_jet_abseta.root':
        ratio_axis.set_xlabel('Leading jet $|\eta|$')
    elif file_name == 'MET filters_subleading_jet_pt.root':
        ratio_axis.set_xlabel('Subleading jet transverse momentum: $p_{T}$ [GeV]')
    elif file_name == 'MET filters_subleading_jet_abseta.root':
        ratio_axis.set_xlabel('Subleading jet $|\eta|$')
    elif (file_name == 'MET filters_mjj.root') or (file_name == 'MET filters_Dijet_mass.root'):
        ratio_axis.set_xlabel('Dijet mass: $m_{jj}$ [GeV]', fontsize = 20)
    elif (file_name == 'MET filters_pTjj.root') or (file_name == 'MET filters_Dijet_pt.root'):
        ratio_axis.set_xlabel('$p_{Tjj}$ [GeV]', fontsize = 20)
    elif file_name == 'MET filters_Dijet_phi.root':
        ratio_axis.set_xlabel(' $\\phi_{jj}$ ', fontsize = 20)
    elif file_name == 'MET filters_Dijet_abseta.root':
        ratio_axis.set_xlabel(' $ | \\eta_{jj} | $ ', fontsize = 20)
    elif file_name == 'MET filters_Zboson_mass.root':
        ratio_axis.set_xlabel('Z boson mass: $m_{l^{+}l^{-}}$ [GeV]', fontsize = 20)
    elif file_name == 'MET filters_Zboson_pt.root':
        ratio_axis.set_xlabel('Z boson transverse momentum: $p_{T}$ [GeV]', fontsize = 20)
    elif file_name == 'MET filters_Zboson_abseta.root':
        ratio_axis.set_xlabel('$|\\eta|_{l^{+}l^{-}}$ ', fontsize = 20)
    elif file_name == 'MET filters_Zboson_phi.root':
        ratio_axis.set_xlabel('$|\\phi|_{l^{+}l^{-}}$ ', fontsize = 20)
    elif file_name == 'MET filters_number_of_jets.root':
        ratio_axis.set_xlabel('Number of jets')
    elif file_name == 'MET filters_number_of_low_pT_jets.root':
        ratio_axis.set_xlabel('Number of low pT jets')
    elif file_name == 'MET filters_DphiZ_jets.root':
        ratio_axis.set_xlabel(' $\\Delta \\phi_{Z, jj}$ ', fontsize = 20)
    elif file_name == 'MET filters_DphiZ_l_jet.root':
        ratio_axis.set_xlabel(' $\\Delta \\phi_{Z, j_{L}}$ ', fontsize = 20)
    elif file_name == 'MET filters_DphiZ_s_jet.root':
        ratio_axis.set_xlabel(' $\\Delta \\phi_{Z, j_{S}}$ ', fontsize = 20)
    elif file_name == 'MET filters_Delta_phi_l_jet_mus.root':
        ratio_axis.set_xlabel(' $\\Delta \\phi_{j_{L}, \\mu \\mu}$ ', fontsize = 20)
    elif file_name == 'MET filters_Delta_phi_s_jet_mus.root':
        ratio_axis.set_xlabel(' $\\Delta \\phi_{j_{S}, \\mu \\mu}$ ', fontsize = 20)
    elif file_name == 'MET filters_dphill.root':
        ratio_axis.set_xlabel(' $\\Delta \\phi_{\\mu \\mu}$ ', fontsize = 20)
    elif file_name == 'MET filters_dphijj.root':
        ratio_axis.set_xlabel(' $\\Delta \\phi_{j_{L} j_{S}}$ ', fontsize = 20)
    elif file_name == 'MET filters_detall.root':
        ratio_axis.set_xlabel(' $\\Delta \\eta_{\\mu \\mu}$ ', fontsize = 20)
    elif file_name == 'MET filters_detajj.root':
        ratio_axis.set_xlabel(' $\\Delta \\eta_{j_{L} j_{S}}$ ',fontsize = 20)
    elif file_name == 'MET filters_R_pT_hard.root':
        ratio_axis.set_xlabel('$R_{p_{T}}^{hard}$ ', fontsize = 20)
    elif file_name == 'MET filters_z_star.root':
        ratio_axis.set_xlabel(' $z^*$ ')
    elif file_name == 'MET filters_pv_npvs.root':
        ratio_axis.set_xlabel('Number of total reconstructed  primary vertices')
    elif file_name == 'Muon_SF_pv_npvs.root':
        ratio_axis.set_xlabel('Number of good primary vertices')
    elif file_name == 'Pileup reweighting_pv_npvs.root':
        ratio_axis.set_xlabel('Number of total reconstructed primary vertices')
    elif file_name == 'MET filters_QGD.root':
        ratio_axis.set_xlabel('QGL Discriminant')
    elif file_name == 'MET filters_Delta_R_jets_l_mu.root':
        ratio_axis.set_xlabel(' $R_{jets, \\mu_{L}}$ ')
    elif file_name == 'MET filters_Delta_R_jets_s_mu.root':
        ratio_axis.set_xlabel(' $R_{jets, \\mu_{S}}$ ')
    elif file_name == 'MET filters_leading_jet_QGL.root':
        ratio_axis.set_xlabel('Leading jet QGL', fontsize = 20)
    elif file_name == 'MET filters_subleading_jet_QGL.root':
        ratio_axis.set_xlabel('Subleading jet QGL', fontsize = 20)
    #ratio_axis.set_xlabel(file_name+'[GeV]')
    ratio_axis.set_ylabel('$EWK \\quad Z / \\sqrt{(QCD \\quad Z + jets)}$')
    # ratio y_axis (0.8, 1.2)
    #ratio_axis.set_ylim(0.09, 0.5)
    
    #plt.savefig('/afs/desy.de/user/m/moralfk/www/EWZjj-test-figures/NEW_DetectorLevelPlots/Pepper_muon_7thmay_SR_'+file_name+'.pdf')
    #plt.savefig('/afs/desy.de/user/m/moralfk/www/EWZjj-test-figures/NEW_DetectorLevelPlots/Pepper_muon_8thjune_goodPV_'+file_name+'.png')    
    #plt.savefig('/afs/desy.de/user/m/moralfk/www/EWZjj-test-figures/NEW_DetectorLevelPlots/Pepper_muon_mjj_detajj_dphi_etastar_study_combined_'+file_name+'.png')
    #plt.savefig('/afs/desy.de/user/m/moralfk/www/EWZjj-test-figures/NEW_DetectorLevelPlots/Pepper_muon_mjj_study_etc_mjj_detajj_dphill_'+file_name+'.png') i
    plt.savefig('/afs/desy.de/user/m/moralfk/www/EWZjj-test-figures/NEW_DetectorLevelPlots/Pepper_muon_mjj_detajj_dphill_Zcuts_'+file_name+'.png') 
    #plt.show() 
