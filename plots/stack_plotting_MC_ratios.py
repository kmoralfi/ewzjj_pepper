import matplotlib.pyplot as plt
import mplhep as hep
import uproot
import matplotlib.gridspec as gridspec
import numpy as np
import matplotlib.patches as patches
import matplotlib.path as path
import hist
from hist import Hist
#from pathlib import Path
#from pathlib import PathPatch
#DY.root  EWK_LLJJ_MLL.root  SingleMuon.root

def make_ratio(hnum, hden):
  wnum, bins = hnum.to_numpy()
  wden, bins = hden.to_numpy()

  numerr = np.sqrt(hnum.variances())
  
  #wratio = np.divide(wnum, wden) - 1
  wratio = np.divide(wnum, wden)
  print('data')
  print(wnum)
  print('mc')
  print(wden)
  ratioerr = np.divide(numerr, wden)
  bincenters = (bins[:-1] + bins[1:]) / 2
  return wratio, bins, ratioerr

def systematics(mc_nom_file, sys_up_file, sys_down_file):
    sys_up_f = 0
    sys_down_f = 0
    for mc_nom, sys_up_value, sys_down_value in zip(mc_nom_file, sys_up_file, sys_down_file):
        mc_nom = hist.Hist(mc_nom)
        sys_up_value = hist.Hist(sys_up_value)
        sys_down_value = hist.Hist(sys_down_value)
        sys_up_temp = ((sys_up_value.values() - mc_nom.values())**2)
        sys_down_temp = ((sys_down_value.values() - mc_nom.values())**2)
        sys_up_f = sys_up_temp + sys_up_f
        sys_down_f = sys_down_temp + sys_down_f
    sys_up = np.sqrt(sys_up_f)
    sys_down = np.sqrt(sys_down_f)
    
    return sys_up, sys_down

def total_systematics(sys_up, sys_down):
    sys_up_f = 0
    sys_down_f = 0
    for sys_up_partial, sys_down_partial in zip(sys_up, sys_down):
        sys_up_temp = (sys_up_partial**2)
        sys_down_temp = (sys_down_partial**2)
        sys_up_f = sys_up_temp + sys_up_f
        sys_down_f = sys_down_temp + sys_down_f
    total_sys_up = np.sqrt(sys_up_f)
    total_sys_down = np.sqrt(sys_down_f)
    return total_sys_up, total_sys_down

def systematics_ratio(sys_up, sys_down, mc_nom_stacked):
    ratio_up_sys = 1 + sys_up/mc_nom_stacked  
    ratio_down_sys = 1 - sys_down/mc_nom_stacked
    return ratio_up_sys, ratio_down_sys

# Load histograms

# General results directory
#inputdir = '/nfs/dust/cms/user/moralfk/EWZjj_Pepper/ALL_8thjune_newDeltaR_QGL_good_b/'
#inputdir = '/nfs/dust/cms/user/moralfk/EWZjj_Pepper/ALL_9thjune_yes_JER_good_Delta_phi_better_binning/'
###inputdir =  '/nfs/dust/cms/user/moralfk/EWZjj_Pepper/ALL_14thjune_sys/'
inputdir =  '/nfs/dust/cms/user/moralfk/EWZjj_Pepper/ALL_16thjune_sys_47abseta_jets/'
#inputdir =  '/nfs/dust/cms/user/moralfk/EWZjj_Pepper/hists/'
#inputdir =  '/nfs/dust/cms/user/moralfk/EWZjj_Pepper/ALL_12thjune_all_systematics/'
#inputdir = '/nfs/dust/cms/user/moralfk/EWZjj_Pepper/ALL_7thjune_inclusive_lumimask_trigger_xsecsf_pu_reweight_muon_SF_dijet_yes_btag_yes_DeltaR_yes_JER_jetabseta_24/'
#inputdir = '/nfs/dust/cms/user/moralfk/EWZjj_Pepper/ALL_7thjune_inclusive_lumimask_trigger_xsecsf_pu_reweight_muon_SF_dijet_yes_btag_yes_DeltaR_yes_JER_jetabseta_24_yes_PV/'

# New file names
file_names = ['Dijet_cut_pv_npvs.root', 'Dijet_cut_pv_npvsGood.root', 'Dijet_cut_leading_jet_QGL.root', 'Dijet_cut_subleading_jet_QGL.root', 'Dijet_cut_R_pT_hard.root', 'Dijet_cut_z_star.root',
              'Dijet_cut_leading_muon_pt.root', 'Dijet_cut_leading_muon_abseta.root', 'Dijet_cut_Delta_R_jets_l_mu.root', 'Dijet_cut_Delta_R_jets_s_mu.root',
              'Dijet_cut_subleading_muon_pt.root', 'Dijet_cut_subleading_muon_abseta.root',
              'Dijet_cut_leading_jet_pt.root', 'Dijet_cut_leading_jet_abseta.root', 'Dijet_cut_subleading_jet_pt.root', 'Dijet_cut_subleading_jet_abseta.root', 
              'Dijet_cut_Dijet_pt.root', 'Dijet_cut_Dijet_abseta.root', 'Dijet_cut_Dijet_mass.root', 'Dijet_cut_Dijet_phi.root', 
              'Dijet_cut_number_of_jets.root', 'Dijet_cut_number_of_low_pT_jets.root', 'Dijet_cut_dphijj.root', 'Dijet_cut_detajj.root', 'Dijet_cut_detall.root', 'Dijet_cut_dphill.root',
              'Dijet_cut_Zboson_pt.root', 'Dijet_cut_Zboson_mass.root', 'Dijet_cut_Zboson_abseta.root', 'Dijet_cut_Zboson_phi.root', 
              'Dijet_cut_DphiZ_l_jet.root', 'Dijet_cut_DphiZ_s_jet.root', 'Dijet_cut_DphiZ_jets.root']

files_PU_up_systematics = ['Dijet_cut_pv_npvs_pileup_up.root', 'Dijet_cut_pv_npvsGood_pileup_up.root', 'Dijet_cut_leading_jet_QGL_pileup_up.root', 'Dijet_cut_subleading_jet_QGL_pileup_up.root', 
              'Dijet_cut_R_pT_hard_pileup_up.root', 'Dijet_cut_z_star_pileup_up.root',
              'Dijet_cut_leading_muon_pt_pileup_up.root', 'Dijet_cut_leading_muon_abseta_pileup_up.root', 'Dijet_cut_Delta_R_jets_l_mu_pileup_up.root', 'Dijet_cut_Delta_R_jets_s_mu_pileup_up.root',
              'Dijet_cut_subleading_muon_pt_pileup_up.root', 'Dijet_cut_subleading_muon_abseta_pileup_up.root',
              'Dijet_cut_leading_jet_pt_pileup_up.root', 'Dijet_cut_leading_jet_abseta_pileup_up.root', 'Dijet_cut_subleading_jet_pt_pileup_up.root', 'Dijet_cut_subleading_jet_abseta_pileup_up.root',
              'Dijet_cut_Dijet_pt_pileup_up.root', 'Dijet_cut_Dijet_abseta_pileup_up.root', 'Dijet_cut_Dijet_mass_pileup_up.root', 'Dijet_cut_Dijet_phi_pileup_up.root',
              'Dijet_cut_number_of_jets_pileup_up.root', 'Dijet_cut_number_of_low_pT_jets_pileup_up.root', 'Dijet_cut_dphijj_pileup_up.root', 'Dijet_cut_detajj_pileup_up.root', 
              'Dijet_cut_detall_pileup_up.root', 'Dijet_cut_dphill_pileup_up.root',
              'Dijet_cut_Zboson_pt_pileup_up.root', 'Dijet_cut_Zboson_mass_pileup_up.root', 'Dijet_cut_Zboson_abseta_pileup_up.root', 'Dijet_cut_Zboson_phi_pileup_up.root',
              'Dijet_cut_DphiZ_l_jet_pileup_up.root', 'Dijet_cut_DphiZ_s_jet_pileup_up.root', 'Dijet_cut_DphiZ_jets_pileup_up.root'] 

files_PU_down_systematics = ['Dijet_cut_pv_npvs_pileup_down.root', 'Dijet_cut_pv_npvsGood_pileup_down.root', 'Dijet_cut_leading_jet_QGL_pileup_down.root', 'Dijet_cut_subleading_jet_QGL_pileup_down.root', 
              'Dijet_cut_R_pT_hard_pileup_down.root', 'Dijet_cut_z_star_pileup_down.root',
              'Dijet_cut_leading_muon_pt_pileup_down.root', 'Dijet_cut_leading_muon_abseta_pileup_down.root', 'Dijet_cut_Delta_R_jets_l_mu_pileup_down.root', 'Dijet_cut_Delta_R_jets_s_mu_pileup_down.root',
              'Dijet_cut_subleading_muon_pt_pileup_down.root', 'Dijet_cut_subleading_muon_abseta_pileup_down.root',
              'Dijet_cut_leading_jet_pt_pileup_down.root', 'Dijet_cut_leading_jet_abseta_pileup_down.root', 'Dijet_cut_subleading_jet_pt_pileup_down.root', 
              'Dijet_cut_subleading_jet_abseta_pileup_down.root',
              'Dijet_cut_Dijet_pt_pileup_down.root', 'Dijet_cut_Dijet_abseta_pileup_down.root', 'Dijet_cut_Dijet_mass_pileup_down.root', 'Dijet_cut_Dijet_phi_pileup_down.root',
              'Dijet_cut_number_of_jets_pileup_down.root', 'Dijet_cut_number_of_low_pT_jets_pileup_down.root', 'Dijet_cut_dphijj_pileup_down.root', 'Dijet_cut_detajj_pileup_down.root', 
              'Dijet_cut_detall_pileup_down.root', 'Dijet_cut_dphill_pileup_down.root',
              'Dijet_cut_Zboson_pt_pileup_down.root', 'Dijet_cut_Zboson_mass_pileup_down.root', 'Dijet_cut_Zboson_abseta_pileup_down.root', 'Dijet_cut_Zboson_phi_pileup_down.root',
              'Dijet_cut_DphiZ_l_jet_pileup_down.root', 'Dijet_cut_DphiZ_s_jet_pileup_down.root', 'Dijet_cut_DphiZ_jets_pileup_down.root']

files_btag_up_systematics = ['Dijet_cut_pv_npvs_btagsf0_up.root', 'Dijet_cut_pv_npvsGood_btagsf0_up.root', 'Dijet_cut_leading_jet_QGL_btagsf0_up.root', 'Dijet_cut_subleading_jet_QGL_btagsf0_up.root', 
              'Dijet_cut_R_pT_hard_btagsf0_up.root', 'Dijet_cut_z_star_btagsf0_up.root',
              'Dijet_cut_leading_muon_pt_btagsf0_up.root', 'Dijet_cut_leading_muon_abseta_btagsf0_up.root', 'Dijet_cut_Delta_R_jets_l_mu_btagsf0_up.root', 'Dijet_cut_Delta_R_jets_s_mu_btagsf0_up.root',
              'Dijet_cut_subleading_muon_pt_btagsf0_up.root', 'Dijet_cut_subleading_muon_abseta_btagsf0_up.root',
              'Dijet_cut_leading_jet_pt_btagsf0_up.root', 'Dijet_cut_leading_jet_abseta_btagsf0_up.root', 'Dijet_cut_subleading_jet_pt_btagsf0_up.root', 
              'Dijet_cut_subleading_jet_abseta_btagsf0_up.root',
              'Dijet_cut_Dijet_pt_btagsf0_up.root', 'Dijet_cut_Dijet_abseta_btagsf0_up.root', 'Dijet_cut_Dijet_mass_btagsf0_up.root', 'Dijet_cut_Dijet_phi_btagsf0_up.root',
              'Dijet_cut_number_of_jets_btagsf0_up.root', 'Dijet_cut_number_of_low_pT_jets_btagsf0_up.root', 'Dijet_cut_dphijj_btagsf0_up.root', 'Dijet_cut_detajj_btagsf0_up.root', 
              'Dijet_cut_detall_btagsf0_up.root', 'Dijet_cut_dphill_btagsf0_up.root',
              'Dijet_cut_Zboson_pt_btagsf0_up.root', 'Dijet_cut_Zboson_mass_btagsf0_up.root', 'Dijet_cut_Zboson_abseta_btagsf0_up.root', 'Dijet_cut_Zboson_phi_btagsf0_up.root',
              'Dijet_cut_DphiZ_l_jet_btagsf0_up.root', 'Dijet_cut_DphiZ_s_jet_btagsf0_up.root', 'Dijet_cut_DphiZ_jets_btagsf0_up.root']

files_btag_down_systematics = ['Dijet_cut_pv_npvs_btagsf0_down.root','Dijet_cut_pv_npvsGood_btagsf0_down.root', 'Dijet_cut_leading_jet_QGL_btagsf0_down.root', 'Dijet_cut_subleading_jet_QGL_btagsf0_down.root', 
              'Dijet_cut_R_pT_hard_btagsf0_down.root', 'Dijet_cut_z_star_btagsf0_down.root',
              'Dijet_cut_leading_muon_pt_btagsf0_down.root', 'Dijet_cut_leading_muon_abseta_btagsf0_down.root', 'Dijet_cut_Delta_R_jets_l_mu_btagsf0_down.root', 'Dijet_cut_Delta_R_jets_s_mu_btagsf0_down.root',
              'Dijet_cut_subleading_muon_pt_btagsf0_down.root', 'Dijet_cut_subleading_muon_abseta_btagsf0_down.root',
              'Dijet_cut_leading_jet_pt_btagsf0_down.root', 'Dijet_cut_leading_jet_abseta_btagsf0_down.root', 'Dijet_cut_subleading_jet_pt_btagsf0_down.root', 
              'Dijet_cut_subleading_jet_abseta_btagsf0_down.root',
              'Dijet_cut_Dijet_pt_btagsf0_down.root', 'Dijet_cut_Dijet_abseta_btagsf0_down.root', 'Dijet_cut_Dijet_mass_btagsf0_down.root', 'Dijet_cut_Dijet_phi_btagsf0_down.root',
              'Dijet_cut_number_of_jets_btagsf0_down.root', 'Dijet_cut_number_of_low_pT_jets_btagsf0_down.root', 'Dijet_cut_dphijj_btagsf0_down.root', 'Dijet_cut_detajj_btagsf0_down.root', 
              'Dijet_cut_detall_btagsf0_down.root', 'Dijet_cut_dphill_btagsf0_down.root',
              'Dijet_cut_Zboson_pt_btagsf0_down.root', 'Dijet_cut_Zboson_mass_btagsf0_down.root', 'Dijet_cut_Zboson_abseta_btagsf0_down.root', 'Dijet_cut_Zboson_phi_btagsf0_down.root',
              'Dijet_cut_DphiZ_l_jet_btagsf0_down.root', 'Dijet_cut_DphiZ_s_jet_btagsf0_down.root', 'Dijet_cut_DphiZ_jets_btagsf0_down.root']

files_JER_up_systematics = ['Dijet_cut_pv_npvs_Jer_up.root', 'Dijet_cut_pv_npvsGood_Jer_up.root', 'Dijet_cut_leading_jet_QGL_Jer_up.root', 'Dijet_cut_subleading_jet_QGL_Jer_up.root', 
              'Dijet_cut_R_pT_hard_Jer_up.root', 'Dijet_cut_z_star_Jer_up.root',
              'Dijet_cut_leading_muon_pt_Jer_up.root', 'Dijet_cut_leading_muon_abseta_Jer_up.root', 'Dijet_cut_Delta_R_jets_l_mu_Jer_up.root', 'Dijet_cut_Delta_R_jets_s_mu_Jer_up.root',
              'Dijet_cut_subleading_muon_pt_Jer_up.root', 'Dijet_cut_subleading_muon_abseta_Jer_up.root',
              'Dijet_cut_leading_jet_pt_Jer_up.root', 'Dijet_cut_leading_jet_abseta_Jer_up.root', 'Dijet_cut_subleading_jet_pt_Jer_up.root', 'Dijet_cut_subleading_jet_abseta_Jer_up.root',
              'Dijet_cut_Dijet_pt_Jer_up.root', 'Dijet_cut_Dijet_abseta_Jer_up.root', 'Dijet_cut_Dijet_mass_Jer_up.root', 'Dijet_cut_Dijet_phi_Jer_up.root',
              'Dijet_cut_number_of_jets_Jer_up.root', 'Dijet_cut_number_of_low_pT_jets_Jer_up.root', 'Dijet_cut_dphijj_Jer_up.root', 'Dijet_cut_detajj_Jer_up.root', 
              'Dijet_cut_detall_Jer_up.root', 'Dijet_cut_dphill_Jer_up.root',
              'Dijet_cut_Zboson_pt_Jer_up.root', 'Dijet_cut_Zboson_mass_Jer_up.root', 'Dijet_cut_Zboson_abseta_Jer_up.root', 'Dijet_cut_Zboson_phi_Jer_up.root',
              'Dijet_cut_DphiZ_l_jet_Jer_up.root', 'Dijet_cut_DphiZ_s_jet_Jer_up.root', 'Dijet_cut_DphiZ_jets_Jer_up.root']

files_JER_down_systematics = ['Dijet_cut_pv_npvs_Jer_down.root', 'Dijet_cut_pv_npvsGood_Jer_down.root', 'Dijet_cut_leading_jet_QGL_Jer_down.root', 'Dijet_cut_subleading_jet_QGL_Jer_down.root', 
              'Dijet_cut_R_pT_hard_Jer_down.root', 'Dijet_cut_z_star_Jer_down.root',
              'Dijet_cut_leading_muon_pt_Jer_down.root', 'Dijet_cut_leading_muon_abseta_Jer_down.root', 'Dijet_cut_Delta_R_jets_l_mu_Jer_down.root', 'Dijet_cut_Delta_R_jets_s_mu_Jer_down.root',
              'Dijet_cut_subleading_muon_pt_Jer_down.root', 'Dijet_cut_subleading_muon_abseta_Jer_down.root',
              'Dijet_cut_leading_jet_pt_Jer_down.root', 'Dijet_cut_leading_jet_abseta_Jer_down.root', 'Dijet_cut_subleading_jet_pt_Jer_down.root', 'Dijet_cut_subleading_jet_abseta_Jer_down.root',
              'Dijet_cut_Dijet_pt_Jer_down.root', 'Dijet_cut_Dijet_abseta_Jer_down.root', 'Dijet_cut_Dijet_mass_Jer_down.root', 'Dijet_cut_Dijet_phi_Jer_down.root',
              'Dijet_cut_number_of_jets_Jer_down.root', 'Dijet_cut_number_of_low_pT_jets_Jer_down.root', 'Dijet_cut_dphijj_Jer_down.root', 'Dijet_cut_detajj_Jer_down.root', 
              'Dijet_cut_detall_Jer_down.root', 'Dijet_cut_dphill_Jer_down.root',
              'Dijet_cut_Zboson_pt_Jer_down.root', 'Dijet_cut_Zboson_mass_Jer_down.root', 'Dijet_cut_Zboson_abseta_Jer_down.root', 'Dijet_cut_Zboson_phi_Jer_down.root',
              'Dijet_cut_DphiZ_l_jet_Jer_down.root', 'Dijet_cut_DphiZ_s_jet_Jer_down.root', 'Dijet_cut_DphiZ_jets_Jer_down.root']

files_junc_up_systematics = ['Dijet_cut_pv_npvs_Junc_up.root', 'Dijet_cut_pv_npvsGood_Junc_up.root', 'Dijet_cut_leading_jet_QGL_Junc_up.root', 'Dijet_cut_subleading_jet_QGL_Junc_up.root', 
              'Dijet_cut_R_pT_hard_Junc_up.root', 'Dijet_cut_z_star_Junc_up.root',
              'Dijet_cut_leading_muon_pt_Junc_up.root', 'Dijet_cut_leading_muon_abseta_Junc_up.root', 'Dijet_cut_Delta_R_jets_l_mu_Junc_up.root', 'Dijet_cut_Delta_R_jets_s_mu_Junc_up.root',
              'Dijet_cut_subleading_muon_pt_Junc_up.root', 'Dijet_cut_subleading_muon_abseta_Junc_up.root',
              'Dijet_cut_leading_jet_pt_Junc_up.root', 'Dijet_cut_leading_jet_abseta_Junc_up.root', 'Dijet_cut_subleading_jet_pt_Junc_up.root', 'Dijet_cut_subleading_jet_abseta_Junc_up.root',
              'Dijet_cut_Dijet_pt_Junc_up.root', 'Dijet_cut_Dijet_abseta_Junc_up.root', 'Dijet_cut_Dijet_mass_Junc_up.root', 'Dijet_cut_Dijet_phi_Junc_up.root',
              'Dijet_cut_number_of_jets_Junc_up.root', 'Dijet_cut_number_of_low_pT_jets_Junc_up.root', 'Dijet_cut_dphijj_Junc_up.root', 'Dijet_cut_detajj_Junc_up.root', 
              'Dijet_cut_detall_Junc_up.root', 'Dijet_cut_dphill_Junc_up.root',
              'Dijet_cut_Zboson_pt_Junc_up.root', 'Dijet_cut_Zboson_mass_Junc_up.root', 'Dijet_cut_Zboson_abseta_Junc_up.root', 'Dijet_cut_Zboson_phi_Junc_up.root',
              'Dijet_cut_DphiZ_l_jet_Junc_up.root', 'Dijet_cut_DphiZ_s_jet_Junc_up.root', 'Dijet_cut_DphiZ_jets_Junc_up.root']

files_junc_down_systematics = ['Dijet_cut_pv_npvs_Junc_down.root', 'Dijet_cut_pv_npvsGood_Junc_down.root', 'Dijet_cut_leading_jet_QGL_Junc_down.root', 'Dijet_cut_subleading_jet_QGL_Junc_down.root', 
              'Dijet_cut_R_pT_hard_Junc_down.root', 'Dijet_cut_z_star_Junc_down.root',
              'Dijet_cut_leading_muon_pt_Junc_down.root', 'Dijet_cut_leading_muon_abseta_Junc_down.root', 'Dijet_cut_Delta_R_jets_l_mu_Junc_down.root', 'Dijet_cut_Delta_R_jets_s_mu_Junc_down.root',
              'Dijet_cut_subleading_muon_pt_Junc_down.root', 'Dijet_cut_subleading_muon_abseta_Junc_down.root',
              'Dijet_cut_leading_jet_pt_Junc_down.root', 'Dijet_cut_leading_jet_abseta_Junc_down.root', 'Dijet_cut_subleading_jet_pt_Junc_down.root', 'Dijet_cut_subleading_jet_abseta_Junc_down.root',
              'Dijet_cut_Dijet_pt_Junc_down.root', 'Dijet_cut_Dijet_abseta_Junc_down.root', 'Dijet_cut_Dijet_mass_Junc_down.root', 'Dijet_cut_Dijet_phi_Junc_down.root',
              'Dijet_cut_number_of_jets_Junc_down.root', 'Dijet_cut_number_of_low_pT_jets_Junc_down.root', 'Dijet_cut_dphijj_Junc_down.root', 'Dijet_cut_detajj_Junc_down.root', 
              'Dijet_cut_detall_Junc_down.root', 'Dijet_cut_dphill_Junc_down.root',
              'Dijet_cut_Zboson_pt_Junc_down.root', 'Dijet_cut_Zboson_mass_Junc_down.root', 'Dijet_cut_Zboson_abseta_Junc_down.root', 'Dijet_cut_Zboson_phi_Junc_down.root',
              'Dijet_cut_DphiZ_l_jet_Junc_down.root', 'Dijet_cut_DphiZ_s_jet_Junc_down.root', 'Dijet_cut_DphiZ_jets_Junc_down.root']


samples = ['EWK_LLJJ_MLL-50_MJJ-120_TuneCP5_13TeV-madgraph-pythia8_dipole',
           'DYJetsToLL_M-50_TuneCP5_13TeV-amcatnloFXFX-pythia8', 'SingleMuon']

for file_name, file_PU_up, file_PU_down, file_btag_up, file_btag_down, file_JER_up, file_JER_down, file_JUNC_up, file_JUNC_down in zip(file_names, files_PU_up_systematics, files_PU_down_systematics, 
    files_btag_up_systematics, files_btag_down_systematics, files_JER_up_systematics, files_JER_down_systematics, files_junc_up_systematics, files_junc_down_systematics):

    mc_histograms = [None]*(len(samples)-1)
    PU_up_hist = [None]*(len(samples)-1)
    PU_down_hist = [None]*(len(samples)-1)
    btag_up_hist = [None]*(len(samples)-1)
    btag_down_hist = [None]*(len(samples)-1)
    JER_up_hist = [None]*(len(samples)-1)
    JER_down_hist = [None]*(len(samples)-1)
    JUNC_up_hist = [None]*(len(samples)-1)
    JUNC_down_hist = [None]*(len(samples)-1)

    for i in range(len(mc_histograms)):
        open_file = uproot.open(inputdir + file_name)
        open_PU_up_file = uproot.open(inputdir + file_PU_up)
        open_PU_down_file = uproot.open(inputdir + file_PU_down)
        open_btag_up_file = uproot.open(inputdir + file_btag_up)
        open_btag_down_file = uproot.open(inputdir + file_btag_down)
        open_JER_up_file = uproot.open(inputdir + file_JER_up)
        open_JER_down_file = uproot.open(inputdir + file_JER_down)
        open_JUNC_up_file = uproot.open(inputdir + file_JUNC_up)
        open_JUNC_down_file = uproot.open(inputdir + file_JUNC_down)
 
        DY_histograms = open_file['DYJetsToLL_M-50_TuneCP5_13TeV-amcatnloFXFX-pythia8'].to_hist()
        EWK_histograms = open_file['EWK_LLJJ_MLL-50_MJJ-120_TuneCP5_13TeV-madgraph-pythia8_dipole'].to_hist()

        PU_up_hist[i] = open_PU_up_file[samples[i]].to_hist()
        PU_down_hist[i] = open_PU_down_file[samples[i]].to_hist()
        btag_up_hist[i] = open_btag_up_file[samples[i]].to_hist()
        btag_down_hist[i] = open_btag_down_file[samples[i]].to_hist()
        JER_up_hist[i] = open_JER_up_file[samples[i]].to_hist()
        JER_down_hist[i] = open_JER_down_file[samples[i]].to_hist()
        JUNC_up_hist[i] = open_JUNC_up_file[samples[i]].to_hist()
        JUNC_down_hist[i] = open_JUNC_down_file[samples[i]].to_hist()

        scaled_mc_signal= open_file['EWK_LLJJ_MLL-50_MJJ-120_TuneCP5_13TeV-madgraph-pythia8_dipole'].to_hist()*100
    """ 
    PU_up, PU_down = systematics(mc_histograms, PU_up_hist, PU_down_hist)
    btag_up, btag_down = systematics(mc_histograms, btag_up_hist, btag_down_hist)
    JER_up, JER_down = systematics(mc_histograms, JER_up_hist, JER_down_hist)
    JUNC_up, JUNC_down = systematics(mc_histograms, JUNC_up_hist, JUNC_down_hist)

    total_up_sys, total_down_sys = total_systematics([PU_up, btag_up, JER_up, JUNC_up], [PU_down, btag_down, JER_down, JUNC_down]) 
    yrr = [total_up_sys, total_down_sys]
    """
    # Set style
    plt.style.use(hep.style.ROOT)
    plt.rcParams.update({'font.size': 14})   
    plt.rcParams.update({'axes.titlesize':20})
    plt.rcParams.update({'axes.labelsize':20})

    # Set stack and ratio plot
    fig, ratio_axis = plt.subplots()
    # Calculate ratio Data/MC
    wratio, bins, werr = make_ratio(EWK_histograms, DY_histograms)
    print('ratio') 
    print(wratio)
    hep.histplot(np.sqrt(wratio), bins, yerr = (werr), stack=False, histtype='step', color='forestgreen', label = 'EWK Z / (QCD Z + jets)')
    plt.legend() 
    if file_name == 'Dijet_cut_Delta_R_jets_l_mu.root':
        ratio_axis.set_xlabel('$\\Delta R_{\\mu_{L} , jets}$')
    elif file_name == 'Dijet_cut_Delta_R_jets_s_mu.root':
        ratio_axis.set_xlabel('$\\Delta R_{\\mu_{S} , jets}$')
    elif file_name == 'Dijet_cut_pv_npvsGood.root':
        ratio_axis.set_xlabel('Number of good primary vertices')
    elif file_name == 'Dijet_cut_leading_muon_pt.root':
        ratio_axis.set_xlabel('Leading muon transverse momentum: $p_{T}$ [GeV]')   
    elif file_name == 'Dijet_cut_leading_muon_abseta.root':
        ratio_axis.set_xlabel('Leading muon $|\eta|$')
    elif file_name == 'Dijet_cut_Delta_R_l_mu_l_jet.root':
        ratio_axis.set_xlabel(' $\\Delta R_{\\mu_{L} , jet_{L}}$')
    elif file_name == 'Dijet_cut_Delta_R_l_mu_s_jet.root':
        ratio_axis.set_xlabel('$\\Delta R_{\\mu_{L} , jet_{S}}$')
    elif file_name == 'Dijet_cut_Delta_R_l_jet.root':
        ratio_axis.set_xlabel(' $\\Delta R_{j_{L} , \\mu \\mu}$')
    elif file_name == 'Dijet_cut_Delta_R_s_jet.root':
        ratio_axis.set_xlabel('$\\Delta R_{j_{S} , \\mu \\mu}$')
    elif file_name == 'Dijet_cut_Delta_phi_l_mu_l_jet.root':
        ratio_axis.set_xlabel(' $\\Delta \\phi_{\\mu_{L}, jet_{L}}$')
    elif file_name == 'Dijet_cut_Delta_phi_l_mu_s_jet.root':
        ratio_axis.set_xlabel('$\\Delta \\phi_{\\mu_{L}, jet_{S}}$')
    elif file_name == 'Dijet_cut_subleading_muon_pt.root':
        ratio_axis.set_xlabel('Subleading muon transverse momentum: $p_{T}$ [GeV]')
    elif file_name == 'Dijet_cut_subleading_muon_abseta.root':
        ratio_axis.set_xlabel('Subleading muon $|\eta|$')
    elif file_name == 'Dijet_cut_leading_jet_pt.root':
        ratio_axis.set_xlabel('Leading jet transverse momentum: $p_{T}$ [GeV]')
    elif file_name == 'Dijet_cut_Delta_R_s_mu_l_jet.root':
        ratio_axis.set_xlabel(' $\\Delta R_{\\mu_{S} , jet_{L}}$')
    elif file_name == 'Dijet_cut_Delta_R_s_mu_s_jet.root':
        ratio_axis.set_xlabel('$\\Delta R_{\\mu_{S} , jet_{S}}$')
    elif file_name == 'Dijet_cut_Delta_phi_s_mu_l_jet.root':
        ratio_axis.set_xlabel(' $\\Delta \\phi_{\\mu_{S}, jet_{L}}$')
    elif file_name == 'Dijet_cut_Delta_phi_s_mu_s_jet.root':
        ratio_axis.set_xlabel('$\\Delta \\phi_{\\mu_{S}, jet_{S}}$')
    elif file_name == 'Dijet_cut_leading_jet_abseta.root':
        ratio_axis.set_xlabel('Leading jet $|\eta|$')
    elif file_name == 'Dijet_cut_subleading_jet_pt.root':
        ratio_axis.set_xlabel('Subleading jet transverse momentum: $p_{T}$ [GeV]')
    elif file_name == 'Dijet_cut_subleading_jet_abseta.root':
        ratio_axis.set_xlabel('Subleading jet $|\eta|$')
    elif (file_name == 'Dijet_cut_mjj.root') or (file_name == 'Dijet_cut_Dijet_mass.root'):
        ratio_axis.set_xlabel('Dijet mass: $m_{jj}$ [GeV]', fontsize = 20)
    elif (file_name == 'Dijet_cut_pTjj.root') or (file_name == 'Dijet_cut_Dijet_pt.root'):
        ratio_axis.set_xlabel('$p_{Tjj}$ [GeV]', fontsize = 20)
    elif file_name == 'Dijet_cut_Dijet_phi.root':
        ratio_axis.set_xlabel(' $\\phi_{jj}$ ', fontsize = 20)
    elif file_name == 'Dijet_cut_Dijet_abseta.root':
        ratio_axis.set_xlabel(' $ | \\eta_{jj} | $ ', fontsize = 20)
    elif file_name == 'Dijet_cut_Zboson_mass.root':
        ratio_axis.set_xlabel('Z boson mass: $m_{l^{+}l^{-}}$ [GeV]', fontsize = 20)
    elif file_name == 'Dijet_cut_Zboson_pt.root':
        ratio_axis.set_xlabel('Z boson transverse momentum: $p_{T}$ [GeV]', fontsize = 20)
    elif file_name == 'Dijet_cut_Zboson_abseta.root':
        ratio_axis.set_xlabel('$|\\eta|_{l^{+}l^{-}}$ ', fontsize = 20)
    elif file_name == 'Dijet_cut_Zboson_phi.root':
        ratio_axis.set_xlabel('$|\\phi|_{l^{+}l^{-}}$ ', fontsize = 20)
    elif file_name == 'Dijet_cut_number_of_jets.root':
        ratio_axis.set_xlabel('Number of jets')
    elif file_name == 'Dijet_cut_number_of_low_pT_jets.root':
        ratio_axis.set_xlabel('Number of low pT jets')
    elif file_name == 'Dijet_cut_DphiZ_jets.root':
        ratio_axis.set_xlabel(' $\\Delta \\phi_{Z, jj}$ ', fontsize = 20)
    elif file_name == 'Dijet_cut_DphiZ_l_jet.root':
        ratio_axis.set_xlabel(' $\\Delta \\phi_{Z, j_{L}}$ ', fontsize = 20)
    elif file_name == 'Dijet_cut_DphiZ_s_jet.root':
        ratio_axis.set_xlabel(' $\\Delta \\phi_{Z, j_{S}}$ ', fontsize = 20)
    elif file_name == 'Dijet_cut_Delta_phi_l_jet_mus.root':
        ratio_axis.set_xlabel(' $\\Delta \\phi_{j_{L}, \\mu \\mu}$ ', fontsize = 20)
    elif file_name == 'Dijet_cut_Delta_phi_s_jet_mus.root':
        ratio_axis.set_xlabel(' $\\Delta \\phi_{j_{S}, \\mu \\mu}$ ', fontsize = 20)
    elif file_name == 'Dijet_cut_dphill.root':
        ratio_axis.set_xlabel(' $\\Delta \\phi_{\\mu \\mu}$ ', fontsize = 20)
    elif file_name == 'Dijet_cut_dphijj.root':
        ratio_axis.set_xlabel(' $\\Delta \\phi_{j_{L} j_{S}}$ ', fontsize = 20)
    elif file_name == 'Dijet_cut_detall.root':
        ratio_axis.set_xlabel(' $\\Delta \\eta_{\\mu \\mu}$ ', fontsize = 20)
    elif file_name == 'Dijet_cut_detajj.root':
        ratio_axis.set_xlabel(' $\\Delta \\eta_{j_{L} j_{S}}$ ',fontsize = 20)
    elif file_name == 'Dijet_cut_R_pT_hard.root':
        ratio_axis.set_xlabel('$R_{p_{T}}^{hard}$ ', fontsize = 20)
    elif file_name == 'Dijet_cut_z_star.root':
        ratio_axis.set_xlabel(' $z^*$ ')
    elif file_name == 'Dijet_cut_pv_npvs.root':
        ratio_axis.set_xlabel('Number of total reconstructed  primary vertices')
    elif file_name == 'Muon_SF_pv_npvs.root':
        ratio_axis.set_xlabel('Number of good primary vertices')
    elif file_name == 'Pileup reweighting_pv_npvs.root':
        ratio_axis.set_xlabel('Number of total reconstructed primary vertices')
    elif file_name == 'Dijet_cut_QGD.root':
        ratio_axis.set_xlabel('QGL Discriminant')
    elif file_name == 'Dijet_cut_Delta_R_jets_l_mu.root':
        ratio_axis.set_xlabel(' $R_{jets, \\mu_{L}}$ ')
    elif file_name == 'Dijet_cut_Delta_R_jets_s_mu.root':
        ratio_axis.set_xlabel(' $R_{jets, \\mu_{S}}$ ')
    elif file_name == 'Dijet_cut_leading_jet_QGL.root':
        ratio_axis.set_xlabel('Leading jet QGL', fontsize = 20)
    elif file_name == 'Dijet_cut_subleading_jet_QGL.root':
        ratio_axis.set_xlabel('Subleading jet QGL', fontsize = 20)
    #ratio_axis.set_xlabel(file_name+'[GeV]')
    ratio_axis.set_ylabel(r'EWK Z / (QCD Z + jets)')
    # ratio y_axis (0.8, 1.2)
    ratio_axis.set_ylim(0.0, 2.4)
    
    #plt.savefig('/afs/desy.de/user/m/moralfk/www/EWZjj-test-figures/NEW_DetectorLevelPlots/Pepper_muon_7thmay_SR_'+file_name+'.pdf')
    #plt.savefig('/afs/desy.de/user/m/moralfk/www/EWZjj-test-figures/NEW_DetectorLevelPlots/Pepper_muon_8thjune_goodPV_'+file_name+'.png')    
    plt.savefig('/afs/desy.de/user/m/moralfk/www/EWZjj-test-figures/NEW_DetectorLevelPlots/Pepper_muon_15thjune_MC_sqrt_ratio_24_'+file_name+'.png')
    plt.show() 
