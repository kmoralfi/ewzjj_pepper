import uproot 
import json
import numpy as np
def read_pu_file(file):
    pu_file = uproot.open(file)
    data_pu_hist = pu_file["pileup_nom"].values()
    data_pu_hist_up = pu_file["pileup_up"].values()
    data_pu_hist_down = pu_file["pileup_down"].values()
    return {"data_pu_hist":data_pu_hist, "data_pu_hist_up":data_pu_hist_up, "data_pu_hist_down":data_pu_hist_down}

pu = uproot.open("PileupHistogram-goldenJSON-13tev-2017-69200ub-99bins.root")["pileup"].to_hist()
pu_up = uproot.open("PileupHistogram-goldenJSON-13tev-2017-72400ub-99bins.root")["pileup"].to_hist()
pu_down = uproot.open("PileupHistogram-goldenJSON-13tev-2017-66000ub-99bins.root")["pileup"].to_hist()
pileup_17 = uproot.recreate("pileup_ul2017.root")
pileup_17["pileup_nom"] = pu
pileup_17["pileup_up"] = pu_up
pileup_17["pileup_down"] = pu_down
#print(repr(read_pu_file("/nfs/dust/cms/user/moralfk/PEPPER_test/pepper/EWZjj/EWZjj_files/PileupWeightsUL2018.root")["data_pu_hist"]))
#print(repr(read_pu_file("/nfs/dust/cms/user/moralfk/PEPPER_test/pepper/EWZjj/EWZjj_files/PileupWeightsUL2018.root")["data_pu_hist_up"]))
#print(repr(read_pu_file("/nfs/dust/cms/user/moralfk/PEPPER_test/pepper/EWZjj/EWZjj_files/PileupWeightsUL2018.root")["data_pu_hist_down"]))

