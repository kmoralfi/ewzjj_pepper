import pepper
import awkward as ak
import numpy as np
import coffea
from coffea import lookup_tools
import coffea.lumi_tools
import coffea.jetmet_tools
from functools import partial
from dataclasses import dataclass
from typing import Optional, Tuple
import uproot
import json
import logging
from pepper.scale_factors import PileupWeighter, ScaleFactors, MuonScaleFactor

def num_jets_num_muons_EWK(num_jets, num_muons, data):
    min_num_jets = ak.num(data["Jet"]) >= num_jets
    min_num_muons = ak.num(data["Muon"]) >= num_muons
    return (min_num_jets & min_num_muons)

