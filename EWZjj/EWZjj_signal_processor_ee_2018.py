# This file illustrates how to implement a processor, realizing the selection
# steps and outputting histograms and a cutflow with efficiencies.
# Here we create a very simplified version of the ttbar-to-dilep processor.
# One can run this processor using
# 'python3 -m pepper.runproc --debug example_processor.py example_config.json'
# Above command probably will need a little bit of time before all cuts are
# applied once. This is because a chunk of events are processed simultaneously.
# You change adjust the number of events in a chunk and thereby the memory
# usage by using the --chunksize parameter (the default value is 500000).

import pepper
import awkward as ak
import numpy as np
import coffea
from coffea import lookup_tools
import coffea.lumi_tools
import coffea.jetmet_tools
from functools import partial
from dataclasses import dataclass
from typing import Optional, Tuple
import uproot
import json
import logging
from copy import copy
from pepper.scale_factors import PileupWeighter, ScaleFactors, MuonScaleFactor
from EWZjj import num
import correctionlib 
from numba import vectorize, float64
import os.path
import pandas as pd
import io
from io import StringIO

# All processors should inherit from pepper.ProcessorBasicPhysics
class Processor(pepper.ProcessorBasicPhysics):
    # We use the ConfigTTbarLL instead of its base Config, to use some of its
    # predefined extras
    config_class = pepper.ConfigBasicPhysics

    def __init__(self, config, eventdir):
        # Initialize the class, maybe overwrite some config variables and
        # load additional files if needed
        # Can set and modify configuration here as well
        config["histogram_format"] = "root"
        # Need to call parent init to make histograms and such ready
        super().__init__(config, eventdir)
        # It is not recommended to put anything as member variable into a
        # a Processor because the Processor instance is sent as raw bytes
        # between nodes when running on HTCondor.

    def process_selection(self, selector, dsname, is_mc, filler):
        # Implement the selection steps: add cuts, define objects and/or
        # compute event weights
        #################################### SYSTEMATICS ################################################
        #if self.config["compute_systematics"] and is_mc:
        #    self.add_generator_uncertainies(dsname, selector)

        ##################################### LUMI MASK ##################################################
        # Add a cut only allowing events according to the golden JSON
        # The good_lumimask method is specified in pepper.ProcessorBasicPhysics
        # It also requires a lumimask to be specified in config
        if not is_mc:
            selector.add_cut("Lumi", partial(
                self.good_lumimask, is_mc, dsname))
         
        #################################### TRIGGER SELECTION ############################################
        # Only allow events that pass triggers specified in config
        # This also takes into account a trigger order to avoid triggering
        # the same event if it's in two different data datasets.
         
        pos_triggers, neg_triggers = pepper.misc.get_trigger_paths_for(
            dsname, is_mc, self.config["dataset_trigger_map"],
            self.config["dataset_trigger_order"])
        selector.add_cut("Trigger", partial(
            self.passing_trigger, pos_triggers, neg_triggers))
        
        ##################################### CROSS-SECTION SCALE FACTOR ####################################
        if is_mc:
            selector.add_cut(
                "Cross section", partial(self.crosssection_scale, dsname))        
         
        ########################################### PRIMARY VERTEX ##########################################
        
        selector.set_multiple_columns(self.pv_good_and_total)
        selector.add_cut("PV", self.primary_vertex_cond)
        
        ######################################## PILE-UP ####################################################
        if is_mc:
            selector.set_column("PU_vertices", self.PU_vertices)

        if is_mc and "pileup_reweighting" in self.config:
            selector.add_cut("Pileup reweighting", partial(
                self.do_pileup_reweighting, dsname))
        
        ################################ SINGLE ELECTRON SCALE FACTORS: RECO, ID, ISO, Trigger Efficiency SF  ##############################
        
        if "electron_sf" in self.config and is_mc:
            selector.add_cut("Electron_SF", self.compute_electron_sf)
        
        ############################################### OBJECT SELECTION: ELECTRONS #########################################################
        # Pick electrons
        selector.set_column("Electron", partial(self.pick_electron_EWK, is_mc, selector.rng))
        # Pick number of electrons
        selector.add_cut("Number_of_electrons", self.num_electrons_EWK)
        # Save dielectron system
        selector.set_multiple_columns(self.dielectron_EWK)
        # Only accept events that pass dielectron selection: signal reconstruction
        selector.add_cut("Dielectron_cut", self.dielectron_cond_EWK)
        ################################################ OBJECT SELECTION: JETS #########################################################
        # Define whether we reapply jet energy corrections or not (FALSE for us)
        reapply_jec = ("reapply_jec" in self.config and self.config["reapply_jec"])
        # Run with JER and JUNC systematics
        if (is_mc and self.config["compute_systematics"]
            and dsname not in self.config["dataset_for_systematics"]):
            if hasattr(filler, "sys_overwrite"):
                assert filler.sys_overwrite is None
            for variarg in self.get_jetmet_variation_args():
                selector_copy = copy(selector)
                filler.sys_overwrite = variarg.name
                self.process_selection_jet_part(selector_copy, is_mc, variarg, dsname, filler)
                if self.eventdir is not None:
                    logger.debug(f"Saving per event info for variation"
                                 f" {variarg.name}")
                    self.save_per_event_info(
                        dsname + "_" + variarg.name, selector_copy, False)
            filler.sys_overwrite = None

        # No-variation run, only pileup and btag systematics
        self.process_selection_jet_part(selector, is_mc, self.get_jetmet_nominal_arg(), dsname, filler)


    #######################################################################################################################################

    def process_selection_jet_part(self, selector, is_mc, variation, dsname, filler):
        # Pick jets(JEC, JUNC, JER, PU id)
        reapply_jec = ("reapply_jec" in self.config and self.config["reapply_jec"])
        selector.set_multiple_columns(partial(self.compute_jet_factors, is_mc, reapply_jec, variation.junc, variation.jer, selector.rng))
        # Pick jets 
        selector.set_column("Jet", self.pick_jets_EWK)
        selector.add_cut("b-tag_veto", partial(self.btag_cut, is_mc))
        # Pick number of jets
        selector.add_cut("Number_of_jets", self.num_jets_EWK)
        # Only accept events that pass dijet selections
        selector.add_cut("Dijet_cut", self.dijet_cond_EWK)
        selector.add_cut("MET filters", self.MET_filters_EWK)
        selector.set_multiple_columns(self.dijet_EWK)
        selector.set_multiple_columns(self.deltaR_phi_electrons_jets)
        #################################### ADITTIONAL VARIABLES ############################################
        selector.set_multiple_columns(self.z_star_R_pT_hard_PU_rho)
        selector.set_multiple_columns(self.QGD)
        #selector.set_column("PU_rho", self.PU_vertices)
       
    def compute_electron_sf(self, data):
        electrons = data["Electron"]
        weight = np.ones(len(data))
        systematics = {}
        # Electron identification and isolation efficiency
        for i, sffunc in enumerate(self.config["electron_sf"]):
            params = {}
            for dimlabel in sffunc.dimlabels:
                if dimlabel == "eta":
                    params["eta"] = abs(electrons.eta)
                else:
                    params[dimlabel] = getattr(electrons, dimlabel)
            central = ak.prod(sffunc(**params), axis=1)
            key = f"electronsf{i}"
            if self.config["compute_systematics"]:
                if ("split_electron_uncertainty" not in self.config
                        or not self.config["split_electron_uncertainty"]):
                    unctypes = ("",)
                else:
                    unctypes = ("stat ", "syst ")
                for unctype in unctypes:
                    up = ak.prod(sffunc(
                        **params, variation=f"{unctype}up"), axis=1)
                    down = ak.prod(sffunc(
                        **params, variation=f"{unctype}down"), axis=1)
                    systematics[key + unctype.replace(" ", "")] = (
                        up / central, down / central)
            weight = weight * central
        return weight, systematics
    
    def primary_vertex_cond(self, data):
        # We set the position and degrees of freedom for the primary vertex
        pv = data["PV"]
        pv_npvsGood = pv.npvsGood > 0
        return pv_npvsGood

    def pv_good_and_total(self, data):
        pv = data["PV"]
        # Number of good primary vertices
        pv_good = pv.npvsGood
        # Number of total reconstructed primary vertices
        pvs = pv.npvs
        return {"pv_good":pv_good, "pvs":pvs}

    def pick_electron_EWK(self, is_mc, rng, data):
        # We select electrons
        electrons = data["Electron"]

        # We ask for tight id, convVeto, DR03 < 0.15
        e_tight_cond = ((ak.all(electrons.mvaFall17V2Iso_WP90 == 1, axis = 1)) & (ak.all(electrons.convVeto == 1, axis = 1)) & (ak.all(electrons.pfRelIso03_all < 0.15, axis = 1)))
        # We ask for z and xy positions relative to the primary vertex depending on eta value following recommendation from:
        # https://twiki.cern.ch/twiki/bin/view/CMS/CutBasedElectronIdentificationRun2
        #has_dxy = electrons.dxy < 0.05
        #has_dz = electrons.dz < 0.2
        has_dz = ak.where(electrons.eta <= 1.479, electrons.dz < 0.1, electrons.dz < 0.2)
        has_dxy = ak.where(electrons.eta <= 1.479, electrons.dxy < 0.05, electrons.dxy < 0.1)
        is_good = (
            e_tight_cond
            & has_dz
            & has_dxy
            &  (abs(electrons.eta) < self.config["SR_abs_electron_eta_max"])
            )
        return electrons[is_good]

    def pick_jets_EWK(self, data):
        # We select jets, apply jet veto maps and apply JEC, JER and PU correction
        jets = data["Jet"]
        """
        # We fix HEM issue: FIRST OPTION: by correcting the energy
        jphi = jets.phi
        jphi = ak.where(jphi<-3.1415, 3.1415, ak.where(jphi>3.1415, 3.1415, jphi))
        HEM_veto_eta_strict = (jets.eta>-2.5) & (jets.eta<-1.3)
        HEM_veto_eta_lose = (jets.eta>-3.0) & (jets.eta<-2.5)
        HEM_veto_phi = (jphi > -1.57) & (jphi < -0.87)
        jets["energy"] = ak.where(HEM_veto_phi & HEM_veto_eta_lose, jets.energy*0.35, ak.where(HEM_veto_phi & HEM_veto_eta_strict, jets.energy*0.20, jets.energy))
        """
        # We fix HEM issue: SECOND OPTION: by thowing jets away
        jphi = jets.phi
        jphi = ak.where(jphi<-3.1415, 3.1415, ak.where(jphi>3.1415, 3.1415, jphi))
        HEM_veto_eta = (jets.eta<-3.0) | (jets.eta>-1.3)
        HEM_veto_phi = (jphi < -1.57) | (jphi > -0.87)
        has_no_HEM_issue = (HEM_veto_eta & HEM_veto_phi)
        
        # We implement jet veto maps
        sfs = correctionlib.CorrectionSet.from_file('/nfs/dust/cms/user/moralfk/EWZjj_Pepper/EWZjj/EWZjj_files/EWZjj_jet_corr/jet_veto_maps/jetvetomaps.json')
        eta = ak.to_numpy(ak.flatten(data["Jet"]["eta"]), False)
        jphi = data["Jet"]["phi"]
        jphi = ak.where(jphi<-3.1415, 3.1415, ak.where(jphi>3.1415, 3.1415, jphi))
        phi = ak.to_numpy(ak.flatten(jphi), False)
        num = ak.num(data["Jet"]["eta"])
        #jet_veto = ak.where(((jets.eta < 5.191) & (jets.eta > -5.191)), ak.unflatten(sfs["Summer19UL18_V1"].evaluate("jetvetomap", eta, phi), num), ak.unflatten([0]*len(eta), num))
        jet_veto = ak.unflatten(sfs["Summer19UL18_V1"].evaluate("jetvetomap", eta, phi), num)
        jets["veto_map_sf"]=jet_veto
        has_map_veto = (jets.veto_map_sf == 0)
         
        # We apply jer correction to jets
        #print('jet pt before jet corrections')
        init_pt = jets.pt
        #print(init_pt)
        if "jetfac" in ak.fields(data):
            #print("true")
            jets["pt"] = jets["pt"] * data["jetfac"]
            jets["mass"] = jets["mass"] * data["jetfac"]
            jets = jets[ak.argsort(jets["pt"], ascending=False)]
        #print('jet pt after corr')
        #print(jets.pt)
        #if (ak.all(init_pt == jets.pt)):
        #    print("same pt")
        
        has_delta_R = (jets.delta_r(data["Electron"][:,0]) > 0.4) & (jets.delta_r(data["Electron"][:,1]) > 0.4)
        has_id = jets.isTight
        is_good = (
            has_map_veto
            & has_no_HEM_issue 
            & has_delta_R
            & has_id
            & (abs(jets.eta) < self.config["SR_abs_jets_eta_max"])
            & ((jets.pt) > self.config["jets_pt_max_no_puID"] | (jets.puId == self.config["jets_puID_111_pass_loose_medium_tightID"])))
        jets = jets[is_good]
        
        # Evaluate b-tagging
        print('start b-tagging')
        tagger, wp = self.config["btag"].split(":")
        if tagger == "deepcsv":
            jets["btag"] = jets["btagDeepB"]
        elif tagger == "deepjet":
            jets["btag"] = jets["btagDeepFlavB"]
        else:
            raise pepper.config.ConfigError(
                "Invalid tagger name: {}".format(tagger))
        year = self.config["year"]
        wptuple = pepper.scale_factors.BTAG_WP_CUTS[tagger][year]
        if not hasattr(wptuple, wp):
            raise pepper.config.ConfigError(
                "Invalid working point \"{}\" for {} in year {}".format(
                    wp, tagger, year))
        jets["btagged"] = jets["btag"] > getattr(wptuple, wp)
        print('finish b-tagging')
        
        return jets

    def num_jets_EWK(self, data):
        min_num_jets = ak.num(data["Jet"]) >= self.config["min_number_jets"]
        return min_num_jets

    def num_electrons_EWK(self, data):
        min_num_electrons = (ak.num(data["Electron"]) == self.config["min_number_electrons"])
        return min_num_electrons

    def HEM_veto(self, data):
        jets = data["Jet"]
        jphi = jets.phi
        jphi = ak.where(jphi<-3.1415, 3.1415, ak.where(jphi>3.1415, 3.1415, jphi))
        
        HEM_veto_eta = (jets.eta<-2.5) | (jets.eta>-1.3)
        HEM_veto_phi = (jphi < -1.57) | (jphi > -0.87)
        
        return (ak.all((HEM_veto_eta & HEM_veto_phi), axis=1))

    def dijet_cond_EWK(self, data):
        # We only want events with excatly two leptons, thus look at our
        # electron and muon counts and pick events accordingly
        leading_jet = data["Jet"][:,0]
        subleading_jet = data["Jet"][:,1]
        dijet_pt_cond =  ((leading_jet.pt > self.config["SR_leading_jet_pt_min"]) & (subleading_jet.pt > self.config["SR_subleading_jet_pt_min"])) 
        dijet_mass_cond = (((leading_jet + subleading_jet).mass)  > self.config["SR_dijet_mass_min"])
        detajj_cond = (abs(leading_jet.eta - subleading_jet.eta) > self.config["SR_detajj_min"])
        return (dijet_pt_cond & dijet_mass_cond & detajj_cond)

    def dielectron_cond_EWK(self, data):
        # We select events with 2 electron and impose conditions over dielectron_pT, dielectron_mass, (dielectron-Z)_mass, dielectron_charge
        leading_electron = data["Electron"][:,0]
        subleading_electron = data["Electron"][:,1]

        dilepton_pt_cond =  ((leading_electron.pt > self.config["SR_leading_electron_pt_min"]) & (subleading_electron.pt > self.config["SR_subleading_electron_pt_min"]))
      
        dilepton_Zboson_mass_cond = (abs((leading_electron + subleading_electron).mass - self.config["Zboson_mass"]) < self.config["cond_dif_electron_Z_mass"])
        dilepton_charge_cond = ((leading_electron.charge + subleading_electron.charge) == 0)

        dphill_cond =  ((leading_electron.delta_phi(subleading_electron)) < self.config["dphill_max"])
        
        jet_eta_max = ak.max(ak.flatten(data["Jet"].eta, 1))
        jet_eta_min = ak.min(ak.flatten(data["Jet"].eta,1))
        Zeta_cond = ((leading_electron + subleading_electron).eta < jet_eta_max) & (jet_eta_min < (leading_electron + subleading_electron).eta)

        return (dilepton_pt_cond & dilepton_Zboson_mass_cond & dilepton_charge_cond & dphill_cond & Zeta_cond)

    def dijet_EWK(self, data):
        # We save the dijet 
        leading_jet = data["Jet"][:,0]
        subleading_jet = data["Jet"][:,1]
        dijet = leading_jet + subleading_jet
        
        dphijj = leading_jet.delta_phi(subleading_jet)
        detajj = (leading_jet.eta - subleading_jet.eta)
        num_lowpT_jets = ak.num(data["Jet"].pt < self.config["SR_leading_jet_pt_min"])
        return {"dijet":dijet, "dphijj":dphijj, "detajj":detajj, "num_lowpT_jets":num_lowpT_jets}

    def dielectron_EWK(self,data):
        # We save the reconstructed Z boson (dielectron)
        leading_electron = data["Electron"][:,0]
        subleading_electron = data["Electron"][:,1]
        dielectron = leading_electron + subleading_electron
        
        dphill = leading_electron.delta_phi(subleading_electron)
        detall = (leading_electron.eta - subleading_electron.eta)
        return {"dielectron":dielectron, "dphill":dphill, "detall":detall}
    
    def deltaR_phi_electrons_jets(self, data):
        # We save DeltaR((sub)leading_electron - (sub)leading_jet) and DeltaPhi((sub)leading_electron - (sub)leading_jet)
        leading_electron = data["Electron"][:,0]
        subleading_electron = data["Electron"][:,1]
        leading_jet = data["Jet"][:,0]
        subleading_jet = data["Jet"][:,1]
        jets = data["Jet"]
        dielectron = leading_electron + subleading_electron
        dijet = leading_jet + subleading_jet
        dR_jets_l_e = jets.delta_r(leading_electron) 
        dR_jets_s_e = jets.delta_r(subleading_electron)
        
        DphiZ_l_jet = dielectron.delta_phi(leading_jet)
        DphiZ_s_jet = dielectron.delta_phi(subleading_jet)
        DphiZ_jets = dielectron.delta_phi(dijet)
        return {"dR_jets_l_e":dR_jets_l_e, "dR_jets_s_e":dR_jets_s_e, "DphiZ_l_jet":DphiZ_l_jet, "DphiZ_s_jet":DphiZ_s_jet, "DphiZ_jets":DphiZ_jets}

    def z_star_R_pT_hard_PU_rho(self, data):
        leading_electron = data["Electron"][:,0]
        subleading_electron = data["Electron"][:,1]
        leading_jet = data["Jet"][:,0]
        subleading_jet = data["Jet"][:,1]
        dielectron = leading_electron + subleading_electron
        y_star = dielectron.eta - (leading_jet.eta + subleading_jet.eta)/2 
        z_star = (y_star / (leading_jet.eta - subleading_jet.eta))
        R_pT_hard = np.abs((leading_jet + subleading_jet + dielectron).pt)/(np.abs(leading_jet.pt)+np.abs(subleading_jet.pt)+np.abs(dielectron.pt))
        return {"z_star":z_star, 
                "R_pT_hard":R_pT_hard}

    def PU_vertices(self, data):
        PU_rho = data["Pileup"]["pudensity"]
        return PU_rho

    def QGD(self, data):
        leading_jet_QGL = data["Jet"][:,0].qgl
        subleading_jet_QGL = data["Jet"][:,1].qgl
        return{"leading_jet_QGL":leading_jet_QGL, "subleading_jet_QGL":subleading_jet_QGL}

    def MET_filters_EWK(self, data):
        MET_filters = ((data.Flag["goodVertices"] == True) & (data.Flag["globalSuperTightHalo2016Filter"] == True) & (data.Flag["HBHENoiseFilter"] == True) & 
                      (data.Flag["HBHENoiseIsoFilter"] == True) & (data.Flag["EcalDeadCellTriggerPrimitiveFilter"] == True) & (data.Flag["BadPFMuonFilter"] == True) & 
                      (data.Flag["BadPFMuonDzFilter"] == True) & (data.Flag["eeBadScFilter"] == True) & data.Flag["ecalBadCalibFilter"] == True)
        return MET_filters

