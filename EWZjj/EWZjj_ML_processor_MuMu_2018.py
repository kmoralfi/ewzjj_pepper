# This file illustrates how to implement a processor, realizing the selection
# steps and outputting histograms and a cutflow with efficiencies.
# Here we create a very simplified version of the ttbar-to-dilep processor.
# One can run this processor using
# 'python3 -m pepper.runproc --debug example_processor.py example_config.json'
# Above command probably will need a little bit of time before all cuts are
# applied once. This is because a chunk of events are processed simultaneously.
# You change adjust the number of events in a chunk and thereby the memory
# usage by using the --chunksize parameter (the default value is 500000).

import pepper
import awkward as ak
import numpy as np
import coffea
from coffea import lookup_tools
import coffea.lumi_tools
import coffea.jetmet_tools
from functools import partial
from dataclasses import dataclass
from typing import Optional, Tuple
import uproot
import json
import logging
from copy import copy
from pepper.scale_factors import PileupWeighter, ScaleFactors, MuonScaleFactor
from EWZjj import num
import correctionlib 
from numba import vectorize, float64
import os.path
import pandas as pd
import io
from io import StringIO

"""
@dataclass
class VariationArg:
    name: Optional[str] = None
    junc: Optional[Tuple[str, str]] = None
    jer: Optional[str] = "central"
logger = logging.getLogger(__name__)
"""
# All processors should inherit from pepper.ProcessorBasicPhysics
class Processor(pepper.ProcessorBasicPhysics):
    # We use the ConfigTTbarLL instead of its base Config, to use some of its
    # predefined extras
    config_class = pepper.ConfigBasicPhysics

    def __init__(self, config, eventdir):
        # Initialize the class, maybe overwrite some config variables and
        # load additional files if needed
        # Can set and modify configuration here as well
        config["histogram_format"] = "root"
        # Need to call parent init to make histograms and such ready
        super().__init__(config, eventdir)
        # It is not recommended to put anything as member variable into a
        # a Processor because the Processor instance is sent as raw bytes
        # between nodes when running on HTCondor.

    def process_selection(self, selector, dsname, is_mc, filler):
        # Implement the selection steps: add cuts, define objects and/or
        # compute event weights
        #################################### SYSTEMATICS ################################################
        if self.config["compute_systematics"] and is_mc:
            self.add_generator_uncertainies(dsname, selector)

        ##################################### LUMI MASK ##################################################
        # Add a cut only allowing events according to the golden JSON
        # The good_lumimask method is specified in pepper.ProcessorBasicPhysics
        # It also requires a lumimask to be specified in config
        if not is_mc:
            selector.add_cut("Lumi", partial(
                self.good_lumimask, is_mc, dsname))
         
        #################################### TRIGGER SELECTION ############################################
        # Only allow events that pass triggers specified in config
        # This also takes into account a trigger order to avoid triggering
        # the same event if it's in two different data datasets.
        pos_triggers, neg_triggers = pepper.misc.get_trigger_paths_for(
            dsname, is_mc, self.config["dataset_trigger_map"],
            self.config["dataset_trigger_order"])
        selector.add_cut("Trigger", partial(
            self.passing_trigger, pos_triggers, neg_triggers))
        
        ##################################### CROSS-SECTION SCALE FACTOR ####################################
        if is_mc:
            selector.add_cut(
                "Cross section", partial(self.crosssection_scale, dsname))        
         
        ########################################### PRIMARY VERTEX ##########################################
        
        selector.set_multiple_columns(self.pv_good_and_total)
        selector.add_cut("PV", self.primary_vertex_cond)
        
        ######################################## PILE-UP ####################################################
        if is_mc:
            selector.set_column("PU_vertices", self.PU_vertices)

        if is_mc and "pileup_reweighting" in self.config:
            selector.add_cut("Pileup reweighting", partial(
                self.do_pileup_reweighting, dsname))
        ################################ SINGLE MUON SCALE FACTORS: RECO, ID, ISO, Trigger Efficiency SF  ##############################
        if "muon_sf" in self.config and is_mc:
            selector.add_cut("Muon_SF", self.compute_muon_sf)
            
        ############################################### OBJECT SELECTION: MUONS #########################################################
        # Pick muons(rochester)
        selector.set_column("Muon", partial(self.pick_muons_EWK, is_mc, selector.rng))
        # Pick number of muons
        selector.add_cut("Number_of_muons", self.num_muons_EWK)
        # Save dimuon system
        selector.set_multiple_columns(self.dimuon_EWK)
        # Only accept events that pass dimuon selection: signal reconstruction
        selector.add_cut("Dimuon_cut", self.dimuon_cond_EWK)
        ################################################ OBJECT SELECTION: JETS #########################################################
        # Define whether we reapply jet energy corrections or not (FALSE for us)
        reapply_jec = ("reapply_jec" in self.config and self.config["reapply_jec"])
        # Run with JER and JUNC systematics
        if (is_mc and self.config["compute_systematics"]
            and dsname not in self.config["dataset_for_systematics"]):
            if hasattr(filler, "sys_overwrite"):
                assert filler.sys_overwrite is None
            for variarg in self.get_jetmet_variation_args():
                selector_copy = copy(selector)
                filler.sys_overwrite = variarg.name
                self.process_selection_jet_part(selector_copy, is_mc, variarg, dsname, filler)
                if self.eventdir is not None:
                    print(f"Saving per event info for variation"
                                 f" {variarg.name}")
                    self.save_per_event_info(
                        dsname + "_" + variarg.name, selector_copy, False)
            filler.sys_overwrite = None
        # No-variation run, only pileup and btag systematics
        self.process_selection_jet_part(selector, is_mc, self.get_jetmet_nominal_arg(), dsname, filler)


    #######################################################################################################################################

    def process_selection_jet_part(self, selector, is_mc, variation, dsname, filler):
        # Pick jets(JEC, JUNC, JER, PU id)
        reapply_jec = ("reapply_jec" in self.config and self.config["reapply_jec"])
        selector.set_multiple_columns(partial(self.compute_jet_factors, is_mc, reapply_jec, variation.junc, variation.jer, selector.rng))
        # Pick jets 
        selector.set_column("Jet", self.pick_jets_EWK)
        selector.add_cut("b-tag_veto", partial(self.btag_cut, is_mc))
        # Pick number of jets
        selector.add_cut("Number_of_jets", self.num_jets_EWK)
        # Only accept events that pass dijet selections
        selector.add_cut("Dijet_cut", self.dijet_cond_EWK)
        #selector.add_cut("Electrons_veto", self.electrons_veto_EWK)
        #selector.add_cut("MET_pT_cut", self.MET_pT_cut)
        selector.add_cut("MET filters", self.MET_filters_EWK)
        selector.set_multiple_columns(self.dijet_EWK)
        selector.set_multiple_columns(self.deltaR_phi_muons_jets)
        #################################### ADITTIONAL VARIABLES ############################################
        selector.set_multiple_columns(self.z_star_R_pT_hard_PU_rho)
        selector.set_multiple_columns(self.QGD)
        selector.set_column("alpha_jjZ_1j", self.alphajjZ_1j)
        #selector.set_multiple_columns(self.num_jets_pT)
        #selector.set_multiple_columns(self.MET_cols)
        #selector.set_multiple_columns(partial(self.BDT_cols, dsname)) 
        if dsname == 'EWK_LLJJ_MLL-50_MJJ-120_TuneCP5_13TeV-madgraph-pythia8_dipole':
            selector.add_cut("GenLepton_GenJet_num_cuts", self.GenLepton_GenJet_num)
            #selector.add_cut("GenLepton_cuts", self.GenLepton_cuts)
            #selector.add_cut("GenJet_cuts", self.GenJet_cuts)
            selector.set_multiple_columns(self.GenPart_variables)
        #selector.add_cut("Higher_number_of_jets", self.higher_num_jets_EWK)
        #selector.set_multiple_columns(self.alphajj)
        #selector.set_column("event_id", partial(self.save_pd_file, dsname))
        #selector.set_column("PU_rho", self.PU_vertices)
    
    def compute_muon_sf(self, data):
        muons = data["Muon"]
        weight = np.ones(len(data))
        systematics = {}
        # Muon identification and isolation efficiency
        for i, sffunc in enumerate(self.config["muon_sf"]):
            params = {}
            for dimlabel in sffunc.dimlabels:
                if dimlabel == "abseta":
                    params["abseta"] = abs(muons.eta)
                else:
                    params[dimlabel] = getattr(muons, dimlabel)
            central = ak.prod(sffunc(**params), axis=1)
            key = f"muonsf{i}"
            if self.config["compute_systematics"]:
                if ("split_muon_uncertainty" not in self.config
                        or not self.config["split_muon_uncertainty"]):
                    unctypes = ("",)
                else:
                    unctypes = ("stat ", "syst ")
                for unctype in unctypes:
                    up = ak.prod(sffunc(
                        **params, variation=f"{unctype}up"), axis=1)
                    down = ak.prod(sffunc(
                        **params, variation=f"{unctype}down"), axis=1)
                    systematics[key + unctype.replace(" ", "")] = (
                        up / central, down / central)
            weight = weight * central
        return weight, systematics

    def primary_vertex_cond(self, data):
        # We set the position and degrees of freedom for the primary vertex
        pv = data["PV"]
        pv_npvsGood = pv.npvsGood > 0
        return pv_npvsGood

    def pv_good_and_total(self, data):
        pv = data["PV"]
        # Number of good primary vertices
        pv_good = pv.npvsGood
        # Number of total reconstructed primary vertices
        pvs = pv.npvs
        return {"pv_good":pv_good, "pvs":pvs}

    def pick_muons_EWK(self, is_mc, rng, data):
        # We select muons
        muons = data["Muon"]
        # We apply rochester correction to muons
        if "muon_rochester" in self.config:
            muons = self.apply_rochester_corr(muons, rng, is_mc)

        # We apply isolation requirement pfRelIso04 is built using the particle flow (PF) algorithm, 
        # therefore we need to apply it before extracting the tuneP pT for high-pT muons
        has_iso = muons.pfRelIso04_all < self.config["muon_pfRelIso04_015"]
        
        """
        UPDATED recommendation: we do not apply this since we are using PF muons and this rec is based con CP5 algo
        # High pT muon recommendations for rull Run 2:https://twiki.cern.ch/twiki/bin/viewauth/CMS/SWGuideMuonIdRun2#HighPt_Muon 
        muons["pt"] = ak.where((muons.pt > self.config["high_pT_muon"]), (muons.tunepRelPt * muons.pt), muons.pt)
        has_pt_id = ((muons.pt < self.config["high_pT_muon"]) | (muons.highPtId == 2))
        """

        # We ask for medium ID 
        has_id = muons.mediumId
        # We ask for loose cuts in relative position to the primary vertex
        has_pv_pos = (muons.dxy < self.config["muon_dxy"]) & (muons.dz < self.config["muon_dz"])

        is_good = (
            has_id
            & has_iso
            & (abs(muons.eta) < self.config["SR_abs_muon_eta_max"])
            & has_pv_pos)
            #& has_pt_id)
        return muons[is_good]
    
    def pick_jets_EWK(self, data):
        # We select jets, apply jet veto maps and apply JEC, JER and PU correction
        jets = data["Jet"]
        """
        # We fix HEM issue: FIRST OPTION: by correcting the energy
        jphi = jets.phi
        jphi = ak.where(jphi<-3.1415, 3.1415, ak.where(jphi>3.1415, 3.1415, jphi))
        HEM_veto_eta_strict = (jets.eta>-2.5) & (jets.eta<-1.3)
        HEM_veto_eta_lose = (jets.eta>-3.0) & (jets.eta<-2.5)
        HEM_veto_phi = (jphi > -1.57) & (jphi < -0.87)
        jets["energy"] = ak.where(HEM_veto_phi & HEM_veto_eta_lose, jets.energy*0.35, ak.where(HEM_veto_phi & HEM_veto_eta_strict, jets.energy*0.20, jets.energy))
        """
        # We fix HEM issue: SECOND OPTION: by thowing jets away
        jphi = jets.phi
        jphi = ak.where(jphi<-3.1415, 3.1415, ak.where(jphi>3.1415, 3.1415, jphi))
        HEM_veto_eta = (jets.eta<-3.0) | (jets.eta>-1.3)
        HEM_veto_phi = (jphi < -1.57) | (jphi > -0.87)
        has_no_HEM_issue = (HEM_veto_eta & HEM_veto_phi)
        
        # We implement jet veto maps
        sfs = correctionlib.CorrectionSet.from_file('/nfs/dust/cms/user/moralfk/EWZjj_Pepper/EWZjj/EWZjj_files/EWZjj_jet_corr/jet_veto_maps/jetvetomaps.json')
        eta = ak.to_numpy(ak.flatten(data["Jet"]["eta"]), False)
        jphi = data["Jet"]["phi"]
        jphi = ak.where(jphi<-3.1415, 3.1415, ak.where(jphi>3.1415, 3.1415, jphi))
        phi = ak.to_numpy(ak.flatten(jphi), False)
        num = ak.num(data["Jet"]["eta"])
        jet_veto = ak.unflatten(sfs["Summer19UL18_V1"].evaluate("jetvetomap", eta, phi), num)
        jets["veto_map_sf"]=jet_veto
        has_map_veto = (jets.veto_map_sf == 0)
        
        # We apply jer correction to jets
        #print('jet pt before jet corrections')
        init_pt = jets.pt
        #print(init_pt)
        if "jetfac" in ak.fields(data):
            #print("true")
            jets["pt"] = jets["pt"] * data["jetfac"]
            jets["mass"] = jets["mass"] * data["jetfac"]
            jets = jets[ak.argsort(jets["pt"], ascending=False)]
        #print('jet pt after corr')
        #print(jets.pt)
        #if (ak.all(init_pt == jets.pt)):
        #    print("same pt")
        
        has_delta_R = (jets.delta_r(data["Muon"][:,0]) > 0.4) & (jets.delta_r(data["Muon"][:,1]) > 0.4)
        has_id = jets.isTight
        is_good = (
            has_map_veto
            & has_no_HEM_issue 
            & has_delta_R
            & has_id
            & (abs(jets.eta) < self.config["SR_abs_jets_eta_max"])
            & ((jets.pt) > self.config["jets_pt_max_no_puID"] | (jets.puId == self.config["jets_puID_111_pass_loose_medium_tightID"])))
        jets = jets[is_good]
        
        # Evaluate b-tagging
        print('start b-tagging')
        tagger, wp = self.config["btag"].split(":")
        if tagger == "deepcsv":
            jets["btag"] = jets["btagDeepB"]
        elif tagger == "deepjet":
            jets["btag"] = jets["btagDeepFlavB"]
        else:
            raise pepper.config.ConfigError(
                "Invalid tagger name: {}".format(tagger))
        year = self.config["year"]
        wptuple = pepper.scale_factors.BTAG_WP_CUTS[tagger][year]
        if not hasattr(wptuple, wp):
            raise pepper.config.ConfigError(
                "Invalid working point \"{}\" for {} in year {}".format(
                    wp, tagger, year))
        jets["btagged"] = jets["btag"] > getattr(wptuple, wp)
        print('finish b-tagging')
        return jets

    def num_jets_EWK(self, data):
        min_num_jets = ak.num(data["Jet"]) >= self.config["min_number_jets"]
        return min_num_jets

    def num_muons_EWK(self, data):
        min_num_muons = (ak.num(data["Muon"]) == self.config["min_number_muons"])
        return min_num_muons

    def electrons_veto_EWK(self, data):
        electrons_veto= (ak.num(data["Electron"]) == 0)
        return electrons_veto

    def HEM_veto(self, data):
        jets = data["Jet"]
        jphi = jets.phi
        jphi = ak.where(jphi<-3.1415, 3.1415, ak.where(jphi>3.1415, 3.1415, jphi))
        
        HEM_veto_eta = (jets.eta<-2.5) | (jets.eta>-1.3)
        HEM_veto_phi = (jphi < -1.57) | (jphi > -0.87)
        
        #HEM_veto_eta_strict = (jets.eta>-2.5) | (jets.eta<-1.3)
        #HEM_veto_eta_lose = (jets.eta>-3.0) | (jets.eta<-2.5)
        #HEM_veto_phi = (jphi > -1.57) | (jphi < -0.87)
        #jets["energy"] = ak.where(HEM_veto_eta_strict & HEM_veto_eta_lose, jets.energy*0.35, ak.where(HEM_veto_eta_strict & HEM_veto_eta_strict, jets.energy*0.20, jets.energy))

        return (ak.all((HEM_veto_eta & HEM_veto_phi), axis=1))

    def dijet_cond_EWK(self, data):
        # We only want events with excatly two leptons, thus look at our
        # electron and muon counts and pick events accordingly
        jets= data["Jet"]

        leading_jet = jets[:,0]
        subleading_jet = jets[:,1]
        
        leading_muon = data["Muon"][:,0]
        subleading_muon = data["Muon"][:,1]

        dijet_pt_cond =  ((leading_jet.pt > self.config["SR_leading_jet_pt_min"]) & (subleading_jet.pt > self.config["SR_subleading_jet_pt_min"]))
        dijet_mass_cond = (((leading_jet + subleading_jet).mass)  > self.config["SR_dijet_mass_min"])
        # Signal region
        detajj_cond = ((np.abs(leading_jet.eta - subleading_jet.eta)) > self.config["SR_detajj_min"])
        # DY CR
        #detajj_cond = ((np.abs(leading_jet.eta - subleading_jet.eta)) < self.config["SR_detajj_min"])
        return (dijet_pt_cond & dijet_mass_cond & detajj_cond)

    def dimuon_cond_EWK(self, data):
        # We select events with 2 muons and impose conditions over dimuon_pT, dimuon_mass, (dimuon-Z)_mass, dimuon_charge
        leading_muon = data["Muon"][:,0]
        subleading_muon = data["Muon"][:,1]
        dilepton = leading_muon + subleading_muon

        dilepton_pt_cond =  ((leading_muon.pt > self.config["SR_leading_muon_pt_min"]) & (subleading_muon.pt > self.config["SR_subleading_muon_pt_min"]))
        dilepton_Zboson_mass_cond = (abs((leading_muon + subleading_muon).mass - self.config["Zboson_mass"]) < self.config["cond_dif_mu_Z_mass"])
        dilepton_charge_cond = ((leading_muon.charge + subleading_muon.charge) == 0)
        #NEW 8th October
        dphill_cond =  ((leading_muon.delta_phi(subleading_muon)) < self.config["dphill_max"]) 
        # NEW 24th October
        dilepton_rap = np.arctanh(np.sqrt((dilepton.mass**2 - dilepton.pt**2)/(2*dilepton.mass**2)))
        jet_eta_max = ak.max(ak.flatten(data["Jet"].eta, 1))
        jet_eta_min = ak.min(ak.flatten(data["Jet"].eta,1))
        Zeta_cond = ((leading_muon + subleading_muon).eta < jet_eta_max) & (jet_eta_min < (leading_muon + subleading_muon).eta)
        return (dilepton_pt_cond & dilepton_Zboson_mass_cond & dilepton_charge_cond & dphill_cond & Zeta_cond)

    def dijet_EWK(self, data):
        # We save the dijet 
        leading_jet = data["Jet"][:,0]
        subleading_jet = data["Jet"][:,1]
        dijet = leading_jet + subleading_jet

        rapj_0 = np.arctanh(np.sqrt((leading_jet.mass**2 - leading_jet.pt**2)/(2*(leading_jet.mass**2))))
        rapj_1 = np.arctanh(np.sqrt((subleading_jet.mass**2 - subleading_jet.pt**2)/(2*(subleading_jet.mass**2))))

        dphijj = leading_jet.delta_phi(subleading_jet)
        detajj = (leading_jet.eta - subleading_jet.eta)
        rapjj = np.arctanh(np.sqrt((dijet.mass**2 - dijet.pt**2)/(2*(dijet.mass**2))))
        num_lowpT_jets = ak.num(data["Jet"].pt < self.config["SR_leading_jet_pt_min"])

        return {"dijet":dijet, "dphijj":dphijj, "detajj":detajj, "yj0":rapj_0, "yj1":rapj_1, "yjj":rapjj, "num_lowpT_jets":num_lowpT_jets,
                # For BDT cols in .ROOT format
                "dijet_pt":dijet.pt, "dijet_eta":dijet.eta, "dijet_abseta":np.abs(dijet.eta), "dijet_phi":dijet.phi, "dijet_mass":dijet.mass,
                "Jet_0_pt": leading_jet.pt, "Jet_0_eta": leading_jet.eta, "Jet_0_abseta": np.abs(leading_jet.eta), "Jet_0_phi": leading_jet.phi, "Jet_0_mass": leading_jet.mass,
                "Jet_1_pt": subleading_jet.pt, "Jet_1_eta": subleading_jet.eta, "Jet_1_abseta": np.abs(subleading_jet.eta), "Jet_1_phi": subleading_jet.phi, "Jet_1_mass": subleading_jet.mass}

    def dimuon_EWK(self,data):
        # We save the reconstructed Z boson (dimuon)
        leading_muon = data["Muon"][:,0]
        subleading_muon = data["Muon"][:,1]
        dimuon = leading_muon + subleading_muon
        dphill = leading_muon.delta_phi(subleading_muon)
        detall = (leading_muon.eta - subleading_muon.eta)
        rapll = np.arctanh(np.sqrt((dimuon.mass**2 - dimuon.pt**2)/(2*(dimuon.mass**2))))
        return {"dimuon":dimuon, "dphill":dphill, "detall":detall, "yll":rapll,
                # For BDT cols in .ROOT format
                "dimuon_pt":dimuon.pt, "dimuon_eta":dimuon.eta, "dimuon_abseta":np.abs(dimuon.eta), "dimuon_phi":dimuon.phi,"dimuon_mass":dimuon.mass,
                "Muon_0_pt":leading_muon.pt, "Muon_0_eta":leading_muon.eta, "Muon_0_abseta":np.abs(leading_muon.eta), "Muon_0_phi":leading_muon.phi, "Muon_0_mass":leading_muon.mass,
                "Muon_1_pt":subleading_muon.pt, "Muon_1_eta":subleading_muon.eta, "Muon_1_abseta":np.abs(subleading_muon.eta), "Muon_1_phi":subleading_muon.phi, "Muon_1_mass":subleading_muon.mass}
    
    def deltaR_phi_muons_jets(self, data):
        # We save DeltaR((sub)leading_muon - (sub)leading_jet) and DeltaPhi((sub)leading_muon - (sub)leading_jet)
        leading_muon = data["Muon"][:,0]
        subleading_muon = data["Muon"][:,1]
        leading_jet = data["Jet"][:,0]
        subleading_jet = data["Jet"][:,1]
        jets = data["Jet"]
        dimuon = leading_muon + subleading_muon
        dijet = leading_jet + subleading_jet
        dR_jets_l_mu = jets.delta_r(data["Muon"][:,0]) 
        dR_jets_s_mu = jets.delta_r(data["Muon"][:,1])
        
        DphiZ_l_jet = dimuon.delta_phi(leading_jet)
        DphiZ_s_jet = dimuon.delta_phi(subleading_jet)
        DphiZ_jets = dimuon.delta_phi(dijet)
        return {"dR_jets_l_mu":dR_jets_l_mu, "dR_jets_s_mu":dR_jets_s_mu, "DphiZ_l_jet":DphiZ_l_jet, "DphiZ_s_jet":DphiZ_s_jet, "DphiZ_jets":DphiZ_jets}

    def z_star_R_pT_hard_PU_rho(self, data):
        leading_muon = data["Muon"][:,0]
        subleading_muon = data["Muon"][:,1]
        leading_jet = data["Jet"][:,0]
        subleading_jet = data["Jet"][:,1]
        dimuon = leading_muon + subleading_muon
        event_id = data["event"]

        y_star = dimuon.eta - (leading_jet.eta + subleading_jet.eta)/2 
        z_star = (y_star / (leading_jet.eta - subleading_jet.eta))
        R_pT_hard = np.abs((leading_jet + subleading_jet + dimuon).pt)/(np.abs(leading_jet.pt)+np.abs(subleading_jet.pt)+np.abs(dimuon.pt))
        return {"z_star":z_star, 
                "R_pT_hard":R_pT_hard,
                "event_id":event_id}

    def PU_vertices(self, data):
        PU_rho = data["Pileup"]["pudensity"]
        return PU_rho

    def QGD(self, data):
        leading_jet_QGL = data["Jet"][:,0].qgl
        subleading_jet_QGL = data["Jet"][:,1].qgl
        return{"leading_jet_QGL":leading_jet_QGL, "subleading_jet_QGL":subleading_jet_QGL}

    def MET_filters_EWK(self, data):
        MET_filters = ((data.Flag["goodVertices"] == True) & (data.Flag["globalSuperTightHalo2016Filter"] == True) & (data.Flag["HBHENoiseFilter"] == True) & 
                      (data.Flag["HBHENoiseIsoFilter"] == True) & (data.Flag["EcalDeadCellTriggerPrimitiveFilter"] == True) & (data.Flag["BadPFMuonFilter"] == True) & 
                      (data.Flag["BadPFMuonDzFilter"] == True) & (data.Flag["eeBadScFilter"] == True) & data.Flag["ecalBadCalibFilter"] == True)
        return MET_filters

    def num_jets_pT(self, data):
        num_jets_30 = ak.num(data["Jet"].pt >= 30)
        num_jets_50 = ak.num(data["Jet"].pt >= 50)
        num_jets_70 = ak.num(data["Jet"].pt >= 70)
        num_jets_90 = ak.num(data["Jet"].pt >= 90)
        num_jets_150 = ak.num(data["Jet"].pt >= 150)
        num_jets_200 = ak.num(data["Jet"].pt >= 200)
        num_jets_250 = ak.num(data["Jet"].pt >= 250)
        return {"Njets30":num_jets_30, "Njets50":num_jets_50, "Njets70":num_jets_70, "Njets90":num_jets_90, "Njets150":num_jets_150, "Njets200":num_jets_200, "Njets250":num_jets_250}
 
    def alphajjZ_1j(self, data):
        leading_muon = data["Muon"][:,0]
        subleading_muon = data["Muon"][:,1]
        leading_jet = data["Jet"][:,0]
        dimuon = leading_muon + subleading_muon
        alpha_jjZ_1j = leading_jet.pt/dimuon.pt
        return alpha_jjZ_1j

    def higher_num_jets_EWK(self, data):
        higher_num_jets = ak.num(data["Jet"]) >= 3
        return higher_num_jets

    def alphajj(self, data):
        leading_muon = data["Muon"][:,0]
        subleading_muon = data["Muon"][:,1]
        leading_jet = data["Jet"][:,0]
        subleading_jet = data["Jet"][:,1]
        dimuon = leading_muon + subleading_muon
        third_jet = data["Jet"][:,2] 
        alpha_jj = third_jet.pt/(0.5*(leading_jet.pt + subleading_jet.pt))
        alpha_jjZ = third_jet.pt/(0.5*(leading_jet.pt + subleading_jet.pt) + dimuon.pt)

        return {"alpha_jj":alpha_jj, "alpha_jjZ":alpha_jjZ}
    
    def MET_pT_cut(slef, data):
        """
        Function to decrease DY in TT CR
        """
        MET = data["MET"] 
        MET_pT_cut = (MET.pt > 75)
        return MET_pT_cut

    def MET_cols(self, data):
        MET = data["MET"]
        chsMET = data["ChsMET"]
        return {"METpt":MET.pt, "METphi":MET.phi, "METsumEt":MET.sumEt, "METsignificance":MET.significance,
                "chsMETpt":chsMET.pt, "chsMETphi":chsMET.phi, "chsMETsumEt":chsMET.sumEt}
    
    def GenLepton_GenJet_num(self, data):
        # We select truth-level muons
        GenDressedLepton = data["GenDressedLepton"]
        GenDressedMuon_neg = GenDressedLepton[GenDressedLepton.pdgId == 13]
        GenDressedMuon_pos = GenDressedLepton[GenDressedLepton.pdgId == - 13]
        # We select the number of truth-level
        num_GenDressedMuon_neg = (ak.num(GenDressedMuon_neg) == self.config["min_number_truth_level_muon"])
        num_GenDressedMuon_pos = (ak.num(GenDressedMuon_pos) == self.config["min_number_truth_level_muon"])
        # We select the jets
        GenJet = data["GenJet"]
        # We select the number of  jets
        num_GenJets = (ak.num(GenJet) >= self.config["min_number_jets"])
        return (num_GenDressedMuon_neg & num_GenDressedMuon_pos & num_GenJets)

    def GenLepton_cuts(self, data):
        """
        Function to apply truth-level lepton cuts.
        """
        # We select truth-level muons
        GenDressedLepton = data["GenDressedLepton"]
        GenDressedMuon_neg = GenDressedLepton[GenDressedLepton.pdgId == 13]
        GenDressedMuon_pos = GenDressedLepton[GenDressedLepton.pdgId == - 13]
        # We select the jets
        GenJet = data["GenJet"]
        # We select the leading and subleading jets
        leading_GenJet = GenJet[:,0]
        subleading_GenJet = GenJet[:,1]
        # We select the leading and subleading muons
        leading_GenDressedMuon = ak.where(GenDressedMuon_neg[:,0].pt> GenDressedMuon_pos[:,0].pt, GenDressedMuon_neg[:,0], GenDressedMuon_pos[:,0])
        subleading_GenDressedMuon = ak.where(GenDressedMuon_neg[:,0].pt < GenDressedMuon_pos[:,0].pt, GenDressedMuon_neg[:,0], GenDressedMuon_pos[:,0])
        # We apply pT cuts
        GenDressedMuon_pT_cuts = ((leading_GenDressedMuon.pt >  self.config["SR_leading_muon_pt_min"]) & (subleading_GenDressedMuon.pt >  self.config["SR_subleading_muon_pt_min"]))
        # We apply eta cuts
        #GenDressedMuon_eta_cuts =  ((abs(leading_GenDressedMuon.eta) < self.config["SR_abs_muon_eta_max"]) &  (abs(subleading_GenDressedMuon.eta) < self.config["SR_abs_muon_eta_max"]))
        # We apply Delta R cuts
        #GenDressedMuon_has_delta_R = ((leading_GenJet.delta_r(leading_GenDressedMuon) > 0.4) & (leading_GenJet.delta_r(subleading_GenDressedMuon) > 0.4)
        #                              & (subleading_GenJet.delta_r(leading_GenDressedMuon) > 0.4) & (subleading_GenJet.delta_r(subleading_GenDressedMuon) > 0.4))
        # We apply Z boson mass cut
        GenDressedMuon_Zboson_mass_cut = (abs((leading_GenDressedMuon + subleading_GenDressedMuon).mass - self.config["Zboson_mass"]) < self.config["cond_dif_mu_Z_mass"])
        # We apply Delta phill cut
        #GenDressedMuon_DeltaPhill = ((leading_GenDressedMuon.delta_phi(subleading_GenDressedMuon)) < self.config["dphill_max"])
        # We apply Z eta cut
        """
        jet_eta_max = ak.max(ak.flatten(GenJet.eta, 1))
        jet_eta_min = ak.min(ak.flatten(GenJet.eta,1))
        GenZ_eta_cut = ((leading_GenDressedMuon + subleading_GenDressedMuon).eta < jet_eta_max) & (jet_eta_min < (leading_GenDressedMuon + subleading_GenDressedMuon).eta)
        """
        return (GenDressedMuon_pT_cuts & GenDressedMuon_Zboson_mass_cut) # & GenDressedMuon_eta_cuts & GenDressedMuon_has_delta_R & GenDressedMuon_DeltaPhill & GenZ_eta_cut)

    def GenJet_cuts(self, data):
        """
        Function to apply truth-level jet cuts
        """
        # We select the jets
        GenJet = data["GenJet"]
        # We select the leading and subleading jets
        leading_GenJet = GenJet[:,0]
        subleading_GenJet = GenJet[:,1]
        # We apply jet pT cuts
        GenJet_pT_cuts = ((leading_GenJet.pt > self.config["SR_leading_jet_pt_min"]) & (subleading_GenJet.pt > self.config["SR_subleading_jet_pt_min"]))
        # We apply eta cuts
        #GenJet_eta_cuts = ((abs(leading_GenJet.eta) < self.config["SR_abs_jets_eta_max"]) &  (abs(subleading_GenJet.eta) < self.config["SR_abs_jets_eta_max"]))
        # We apply mass cuts
        GenJet_mass_cuts = (((leading_GenJet + subleading_GenJet).mass > self.config["SR_dijet_mass_min"]))
        # We apply delta eta cuts
        GenJet_DeltaEta_cuts =  ((np.abs(leading_GenJet.eta - subleading_GenJet.eta)) > self.config["SR_detajj_min"]) 
        return (GenJet_pT_cuts & GenJet_mass_cuts & GenJet_DeltaEta_cuts) # & GenJet_eta_cuts

    def GenPart_variables(self, data):
        """
        Funtion to obtain truth-level informationi for the reconstructed Z boson. 
        """
        GenPart = data["GenPart"]
        # We select the muons 
        GenDressedLepton = data["GenDressedLepton"]
        GenDressedMuon_neg = GenDressedLepton[GenDressedLepton.pdgId == 13]
        GenDressedMuon_pos = GenDressedLepton[GenDressedLepton.pdgId == - 13]
        # We select the leading and subleading muons
        leading_GenDressedMuon = ak.where(GenDressedMuon_neg[:,0].pt> GenDressedMuon_pos[:,0].pt, GenDressedMuon_neg[:,0], GenDressedMuon_pos[:,0])
        subleading_GenDressedMuon = ak.where(GenDressedMuon_neg[:,0].pt < GenDressedMuon_pos[:,0].pt, GenDressedMuon_neg[:,0], GenDressedMuon_pos[:,0])
        truth_Zboson = leading_GenDressedMuon + subleading_GenDressedMuon
        
        # We select the jets
        GenJet = data["GenJet"]
        # We select the leading and subleading jets
        leading_GenJet = GenJet[:,0]
        subleading_GenJet = GenJet[:,1]
        GenDijet= leading_GenJet + subleading_GenJet
        # Additional variables
        # Delta R
        DeltaR_L_GenJet_L_GenMu = leading_GenJet.delta_r(leading_GenDressedMuon)
        DeltaR_L_GenJet_S_GenMu = leading_GenJet.delta_r(subleading_GenDressedMuon)
        DeltaR_S_GenJet_L_GenMu = subleading_GenJet.delta_r(leading_GenDressedMuon)
        DeltaR_S_GenJet_S_GenMu = subleading_GenJet.delta_r(subleading_GenDressedMuon)
        DeltaR_GenDijet_L_GenMu = GenDijet.delta_r(leading_GenDressedMuon)
        DeltaR_GenDijet_S_GenMu = GenDijet.delta_r(subleading_GenDressedMuon)
        # Delta phi between leading and subleading gen-lev muons
        Gen_Deltaphill = leading_GenDressedMuon.delta_phi(subleading_GenDressedMuon) 
        # Delta phi between leading and subleading gen-lev jets
        Gen_Deltaphijj = leading_GenJet.delta_phi(subleading_GenJet)
        # Delta eta between leading and subleading gen-lev muons
        Gen_AbsDeltaetall = (np.abs(leading_GenDressedMuon.eta - subleading_GenDressedMuon.eta))        
        # Delta eta between leading and subleading gen-lev jets
        Gen_AbsDeltaetajj = (np.abs(leading_GenJet.eta - subleading_GenJet.eta))

        # Test: we compare dilepton mass vs Z boson (from pdg id) mass
        #print("Truth-level dilepton mass: ", truth_Zboson.mass)   
        #truth_Zboson = GenPart[GenPart.pdgId == 23]  
        #print("Truth-level Z boson mass: ", truth_Zboson.mass)    
        return {"truth_Zboson": truth_Zboson, "leading_GenDressedMuon": leading_GenDressedMuon, "subleading_GenDressedMuon":subleading_GenDressedMuon,  
                "leading_GenJet":leading_GenJet, "subleading_GenJet":subleading_GenJet, "GenDijet": GenDijet,
                "DeltaR_L_GenJet_L_GenMu":DeltaR_L_GenJet_L_GenMu, "DeltaR_L_GenJet_S_GenMu":DeltaR_L_GenJet_S_GenMu, "DeltaR_S_GenJet_L_GenMu":DeltaR_S_GenJet_L_GenMu, "DeltaR_S_GenJet_S_GenMu":DeltaR_S_GenJet_S_GenMu,
                "DeltaR_GenDijet_L_GenMu":DeltaR_GenDijet_L_GenMu, "DeltaR_GenDijet_S_GenMu":DeltaR_GenDijet_S_GenMu,
                "Gen_Deltaphill":Gen_Deltaphill, "Gen_Deltaphijj":Gen_Deltaphijj, "Gen_AbsDeltaetall":Gen_AbsDeltaetall, "Gen_AbsDeltaetajj":Gen_AbsDeltaetajj,

                # For BDT in .ROOT file:
                "truth_Zboson_mass": truth_Zboson.mass, "GenDijet_mass": GenDijet.mass,
                "truth_Zboson_pt": truth_Zboson.pt, "leading_GenDressedMuon_pt": leading_GenDressedMuon.pt, "subleading_GenDressedMuon_pt":subleading_GenDressedMuon.pt,
                "truth_Zboson_eta": truth_Zboson.eta, "leading_GenDressedMuon_eta": leading_GenDressedMuon.eta, "subleading_GenDressedMuon_eta":subleading_GenDressedMuon.eta, 
                "truth_Zboson_abseta": np.abs(truth_Zboson.eta), "leading_GenDressedMuon_abseta": np.abs(leading_GenDressedMuon.eta), "subleading_GenDressedMuon_abseta": np.abs(subleading_GenDressedMuon.eta),
                "leading_GenJet_pt":leading_GenJet.pt, "subleading_GenJet_pt":subleading_GenJet.pt, "GenDijet_pt": GenDijet.pt,
                "leading_GenJet_eta":leading_GenJet.eta, "subleading_GenJet_eta":subleading_GenJet.eta, "GenDijet_eta": GenDijet.eta,
                "leading_GenJet_abseta": np.abs(leading_GenJet.eta), "subleading_GenJet_abseta": np.abs(subleading_GenJet.eta), "GenDijet_abseta": np.abs(GenDijet.eta)}
    def BDT_cols(self, data, dsname):
        events['label'] = ak.where(dsname == 'EWK_LLJJ_MLL-50_MJJ-120_TuneCP5_13TeV-madgraph-pythia8_dipole', 1, ak.where(dsname == 'SingleMuon', 2, 0) )
        """
        if dsname == 'EWK_LLJJ_MLL-50_MJJ-120_TuneCP5_13TeV-madgraph-pythia8_dipole':
            events['label'] = self.config["EWKZjj_label"]
            events['name'] = self.config["EWKZjj_name"]

        elif dsname == 'DYJetsToLL_M-50_TuneCP5_13TeV-amcatnloFXFX-pythia8':
            events['label'] = self.config["BCKG_label"]
            events['name'] = self.config["DY_name"]

        elif (dsname == 'TTToSemiLeptonic_TuneCP5_13TeV-powheg-pythia8') or (dsname == 'TTTo2L2Nu_TuneCP5_13TeV-powheg-pythia8'):
            events['label'] = self.config["BCKG_label"]
            events['name'] = self.config["TT_name"] 
        elif dsname == 'SingleMuon': 
            events['label'] = self.config["data_label"]
        else:
            events['label'] = self.config["BCKG_label"]
            events['name'] = self.config["others_name"]
        """
        return{"label": events['label']}
