import uproot 
import json
import numpy as np
def read_pu_file(file):
    pu_file = uproot.open(file)
    data_pu_hist = pu_file["pileup_nom"].values()
    data_pu_hist_up = pu_file["pileup_up"].values()
    data_pu_hist_down = pu_file["pileup_down"].values()
    return {"data_pu_hist":data_pu_hist, "data_pu_hist_up":data_pu_hist_up, "data_pu_hist_down":data_pu_hist_down}
print(uproot.open("/nfs/dust/cms/user/moralfk/PEPPER_test/pepper/EWZjj/EWZjj_files/PileupWeightsUL2018.root")["pileup_nom"].to_hist())
print(repr(read_pu_file("/nfs/dust/cms/user/moralfk/PEPPER_test/pepper/EWZjj/EWZjj_files/PileupWeightsUL2018.root")["data_pu_hist"]))
print(repr(read_pu_file("/nfs/dust/cms/user/moralfk/PEPPER_test/pepper/EWZjj/EWZjj_files/PileupWeightsUL2018.root")["data_pu_hist_up"]))
print(repr(read_pu_file("/nfs/dust/cms/user/moralfk/PEPPER_test/pepper/EWZjj/EWZjj_files/PileupWeightsUL2018.root")["data_pu_hist_down"]))

