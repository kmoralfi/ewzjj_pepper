# EWZjj Analysis
Analysis for the Electroweak production of the Z boson in  association with 2 jets implemented through Pepper (Coffea-based).

👽Author: K. Moral Figueroa

🌱 Version v.3

## Description

This is a CMS analysis based on the Pepper framework. The whole code is written in:

- Perl: functions oriented to computational performance, HTCondor and XRootD routines.

- Shell: parameters inside configuration files oriented to HTCondor

- Python: functions oriented to the main analysis / physics part.

## Getting started

### 🔨 Installation
To install this framework:

```bash
git clone https://gitlab.cern.ch/kmoralfi/ewzjj_pepper.git

cd ewzjj_pepper

source plugins/enviroment.sh

python3 -m pip install --upgrade --upgrade-strategy eager --editable . 

# Additionally only if on CentOS7 (e.g. DESY NAF in 2023):
python3 -m pip install "urllib3<2"

```

⚠️  CAUTION!

This last command will install a native version of pepper, which means, some scripts could be over-written. This analysis
does not run on a native version of pepper but a slightly hacked one, to re-create this:

```bash
cd pepper
vim processor_basic.py # go to btag_cut function and remplace >= for <= (since we want to EXCLUDE any btag jets)
accept = np.asarray(num_btagged <= self.config["num_atleast_btagged"])
```

### 🔧 Run the analysis
First setup your enviroment:

```bash
source plugins/enviroment.sh
```

If you want to work only with datasamples form the local DESY machine, you need to make sure in EWZjj/EWZZjj_config.json

```bash
file_mode: "local"
```

Then, if you want to perform a debug run (where only 1 datasample from each type: ZZ, WZ, SingleMuon etc is analysed):

```bash
python3 -m pepper.runproc --debug EWZjj/EWZjj_ML_processor_MuMu_2018.py EWZjj/EWZjj_ML_config_MuMu_2018.json 
```

If you want to perform a run with a specific dataset (x. ex. only SingleMuon). ⚠️  Careful! The dataset name must be the same as the one used in EWZjj/EWZjj_config.json :

```bash
python3 -m pepper.runproc EWZjj/EWZjj_processor.py EWZjj/EWZjj_config.json --dataset SingleMuon
```

If you want to run everything in HTCondor, since at the moment we have a total of 1607 samples, we submit 1607 jobs (--condor 1607), since it consumes a lot of memory we set a lower chunksize (--chunksize 20000) to avoid getting jobs on hold and finallly to be able to retake the jobs if they get removed of put on hold, we set up a state file (--statedata EWZjj_state_example.coffea)

```bash
python3 -m pepper.runproc EWZjj/EWZjj_JER_test_processor.py EWZjj/EWZjj_config.json --condor 1607 --chunksize 20000 --statedata EWZjj_state_example.coffea
```

If you want to run everything in HTCondor using datasamples from the local DESY machine + datasameples from XRootD:
datasamples from the local DESY machine, then you need to setup your VOMS proxy first:

```bash
voms-proxy-init --voms cms --out $X509_USER_PROXY
```

or

```bash
voms-proxy-init --voms cms -valid 192:00
```

Then, you need to make sure in EWZjj/EWZZjj_config.json

```bash
file_mode: "local+xrootd"
```
Then proceed to source the enviroment and run the analysis as explained before. After running the analysis you will obtain the histograms (by default in root format) in the hists folder and a file called cutflows.json where you can see the raw number of events after each cut for each MC and data sample, this might be useful if you want to know the Data/MC ratio. If you need to implement a BDT or a NN afterwards, you might be interested in saving the raw values for each column (aka. physics variable such as leading lepton pt etc). This might be done by fixing in the config file the information related to columns to save and their format, setting the entry "compute systematics: false" and running the analysis specifying the path where you want the events to be saved:

```bash
-- eventdir /absolute_path_to_save_columns
```

⚠️  CAUTION 

1. IMPORTANT! To avoid killing the jobs by mistake when loggin off the cms-naf or when the terminal dies, you should use tmux:

```bash
tmux new-session -s session_name
```

Press Ctr+b+d to detach the session (it keeps running on the background). To retach the session:

```bash
tmux attach-session -t session_name
```

2. IMPORTANT! In case your jobs get removed or put on hold and you have tried to remove them with  

```bash
condor_rm username 
```

And it is not removing your jobs, do:

```bash
condor_rm --forcex username
```
and alternate it with the usual condor_rm username until you get your condor_q console clean again.

3. IMPORTANT! If some of your jobs get removed (because some files are corrupted x. ex.) you can still get the histograms with the jobs which have been completed by executing:

```bash
python3 scripts/export_hists_from_state.py EWZjj_state_example.coffea root hists
```

## 📊 How to

### The analysis structure: general

The main processor: 
```bash
EWZjj/EWZjj_*_processor*.py
```
calls another more basic processor:
```bash
pepper/processor_basic.py
```

Which has all the cuts functions except:

- compute_muon_sf

- pick_muons_EWK

- pick_jets_EWK

- num_jets_num_muons_EWK

- dijet_cond_EWK

- dimuon_cond_EWK

- dijet_mass_EWK

- dilepton_mass_EWK

- dilepton_pt_EWK

Which are defined inside processor itself

All the functions take as arguments the parameters defined in EWZjj/EWZjj_config.json.

Finally, the histograms are saved in the hists directory after aplying each cut, so in the current analysis, the final histograms are the Dimuon_cuts_*.root

### The analysis structure: details

Following the EWZjj/EWZjj_processor.py code:

- [ ] The <mark>luminosity mask</mark> is applied only to data samples through the function <mark>good_lumimask</mark>, which is defined in pepper/processor_basic.py.
- [ ] The <mark>trigger</mark> is applied through the function <mark>passing_trigger</mark>, which is defined in pepper/processor_basic.py.
- [ ] The <mark>cross section scale factor</mark> is applied only to MC samples through the function <mark>crosssection_scale</mark>, which is defined in pepper/processor_basic.py.
- [ ] The <mark>pileup reweighting</mark> is applied only to MC samples thought the function <mark>do_pileup_reweighting</mark>, which is defined in pepper/processor_basic.py. 
- [ ] The <mark>muon ID, muon ISOlation and muon trigger efficiency</mark> are applied through the function <mark>compute_muon_sf</mark>, which is defined in EWZjj/EWZjj_processor.py. ⚠️  Careful! In this analysis the single muon trigger efficiency scale factor can be obtained though this function because we are using the <mark>HLT_IsoMu24</mark>, whose scale factors are provided directly by the MuonPOG, otherwise, we would need to use an alternative function, such as trigger_sf.
- [ ] The <mark>muons cuts and rochester correction</mark> are applied though the function <mark>pick_muons_EWK</mark>, which is defined in EWZjj/EWZjj_processor.py. This function calls the function <mark>apply_rochester_corr</mark>, which is defined in pepper/processor_basic.py
- [ ] The <mark>jets cuts, jet pile-up id, the jet energy correction (JEC) and jet resolution scale (JER) factor</mark> are aplied thought the function pick_jets_EWK, which is defined in EWZjj/EWZjj_processor.py. This function checks whether the entry <mark>jetfac</mark> exists in the data fields or columns. This entry has the JEC and JER information and it is obtained through the function <mark>compute_jet_factors</mark>, defined in pepper/processor_basic.py 
- [ ] The <mark>minimun number of jets and muons</mark> is applied thorugh the function <mark>num_jets_num_muons_EWK</mark>.
- [ ] The <mark>dijet condition</mark> is applied through the function dijet_cond_EWK.
- [ ] The <mark>dimuon condition</mark> (Z boson reconstruction condition) is applied through the function dimuon_cond_EWK.

Finally we save some information in additional data entries or columns to call them when filling histograms:

- [ ] The <mark>dijet mass column</mark> is set through the function <mark>dijet_mass_EWK</mark>.
- [ ] The <mark>dilepton mass column</mark> is set through the function <mark>dilepton_mass_EWK</mark>.
- [ ] The <mark>dilepton transverse momentum column</mark> is set though the function <mark>dilepton_pt_EWK</mark>.

### Anaysis pepper-based scale factors 
The cross-section scale factor is obtained as:

```bash
python scripts/compute_mc_lumifactors.py EWZjj/EWZjj_config.json EWZjj_mc_lumi.json
```

where the EWZjj/EWZjj_config.json file has the entry "crosssections": "$CONFDIR/EWZjj_files/EWZjj_xsec_corr/EWZjj_crosssections.json", which is used to calculate that SF in fb.

The b-tag veto scale factor is obtained from first running the analyis with histos in coffea format and the config file without the entry "btag_sf", then:

```bash
python scripts/generate_btag_efficiencies.py --cut b-tag_veto --histname btageff  hists/hists.json EWZjj_btag_eff.root
```

## ⌛ Project status
Currently under development (both muon and electron  channel and year=ul2018 for now):
- [x] Event reconstruction
- [x] Signal reconstruction
- [x] Normalization
- [x] Triggers
- [x] Dijet cuts
- [x] Pile-up corrections (PU reweighting and PU correction for jets)
- [x] Rochester correction
- [x] Single Muon Trigger Efficiency scale factor (MuonPOG)
- [x] Muon ID scale factor (MuonPOG) / Electron ID scale factor (EGammaPOG)
- [x] Muon Isolation scale factor (MuonPOG) / Electron Isolation scale factor (EGammaPOG)
- [x] Muon Reco scale factor (MuonPOG)
- [x] Luminosity mask
- [x] HEM issue
- [x] Jet veto maps
- [x] Jet resolution scale factor 
- [x] Jet energy correction
- [x] B-tag veto
- [x] QGL discriminant
- [x] MET filters
- [x] Systematic uncertainties
- [x] 3 different kinematic regions: nominal, signal, ML.
- [x] Cross-check between frameworks
- [x] Multivariable analysis: boosted gradient decision tree vs. NN
- [x] 4 different kinematic regions for unfolding: signal (tBDT > 1.7), validation (tBDT < 1.7), DY CR and TT CR.
- [x] Symmetrization, smoothing and normalization for SR systematics, symmetrization and normalization for CRs systematics
- [x] Combine: fits and data driven methods checks
- [ ] Combine: Unfolding
- [ ] Validation

## ⛳ Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Friendly remminder: how to submit your files to this Git repository
```
cd EWZjj_Pepper
git add file
git commit -m "commit_message"
git push origin master
```
## 📚  References

### ✏️ The useful stuff
- Python (version: 3.9):
  - [Pepper](https://gitlab.cern.ch/pepper/pepper/-/tree/master/)
  - [Awkard](https://pypi.org/project/awkward/) (version:)
  - [Numpy](https://numpy.org/) (version: )
  - [Coffea](https://coffeateam.github.io/coffea/) (version: )
  - [Matplotlib](https://matplotlib.org/) (version: )
  - [Mplhep](https://mplhep.readthedocs.io/en/latest/index.html)(version: )

    Fan of Jupyter notebooks? Here you can find [here](https://hub.gke2.mybinder.org/user/coffeateam-coffea-5v3zsm55/tree/binder) some Coffea tutorials (installation free!)
- [Uproot](https://uproot.readthedocs.io/en/latest/uproot.html) (version: )
- [JSON](https://www.json.org/json-en.html) (version: )
- [README](https://docs.readme.com/)
    
    How to make [colorblind friendly plots](http://www.cookbook-r.com/Graphs/Colors_(ggplot2)/#a-colorblind-friendly-palette)

[![forthebadge](https://forthebadge.com/images/badges/you-didnt-ask-for-this.svg)](https://forthebadge.com)

[![forthebadge](https://forthebadge.com/images/badges/made-with-python.svg)](https://forthebadge.com)


### 🐶 The cute stuff
- [Emojis](https://infinitbility.com/markdown-emoji-example-with-code/)
- [Badges](https://forthebadge.com/)

[![forthebadge](https://forthebadge.com/images/badges/powered-by-coffee.svg)](https://forthebadge.com) 
