#!/bin/bash

# Directory to search for .csv files
search_dir="/nfs/dust/cms/user/moralfk/EWZjj_Pepper/eventdir_TT_CR_MuMu18_full_sys_fixedMuonSF_sourcesJEC_CONTAINER/eventdir_TT_CR_MuMu18_full_sys_fixedMuonSF_sourcesJEC"
#search_dir="/nfs/dust/cms/user/moralfk/EWZjj_Pepper/"
# Destination directory where .csv files will be copied
destination_dir="davs://dcache-cms-webdav-wan.desy.de:2880/pnfs/desy.de/cms/tier2/store/user/kmoralfi/EWZjj_Pepper_new/eventdir_TT_CR_MuMu18_fixedMuon_SF_JEC_sources/"
#destination_dir="davs://dcache-cms-webdav-wan.desy.de:2880/pnfs/desy.de/cms/tier2/store/user/kmoralfi/EWZjj_Pepper_new/ALL_hists_and_coffea_files/"

# Ensure the destination directory exists
mkdir -p "$destination_dir"

# Find and copy all .csv files
for file in $(find "$search_dir" -type f -name "*.csv"); do
    gfal-copy "$file" "$destination_dir"
done

#for file in $(find "$search_dir" -type d -name "ALL*"); do
#    gfal-copy -r "$file" "$destination_dir"
#done

