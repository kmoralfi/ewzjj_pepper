# EWZjj Analysis files

## Description

## 📆  2018 analysis

### Muon data:

"SingleMuon": [
            "/SingleMuon/Run2018A-UL2018_MiniAODv2_NanoAODv9_GT36-v1/NANOAOD", -> Dataset size: 211623426941 (211.6GB) Number of blocks: 7 Number of events: 241596817 Number of files: 94 Creation time: 2022-05-24 20:46:38 Cross section: 0 Physics group: NoGroup Status: VALID Type: data
            "/SingleMuon/Run2018B-UL2018_MiniAODv2_NanoAODv9_GT36-v1/NANOAOD", -> Dataset size: 104257450149 (104.3GB) Number of blocks: 3 Number of events: 119918017 Number of files: 52 Creation time: 2022-03-26 23:31:16 Physics group: NoGroup Status: VALID Type: data
            "/SingleMuon/Run2018C-UL2018_MiniAODv2_NanoAODv9_GT36-v1/NANOAOD", -> Dataset size: 99005245291 (99.0GB) Number of blocks: 11 Number of events: 110032072 Number of files: 67 Creation time: 2022-03-06 15:37:41 Physics group: NoGroup Status: VALID Type: data
            "/SingleMuon/Run2018D-UL2018_MiniAODv2_NanoAODv9_GT36-v1/NANOAOD"  -> Dataset size: 449431360000 (449.4GB) Number of blocks: 10 Number of events: 513884680 Number of files: 208 Creation time: 2022-05-17 20:15:06 Cross section: 0 Physics group: NoGroup Status: VALID Type: data
        ]

### Electron data:

"EGama": [
            "/EGamma/Run2018A-UL2018_MiniAODv2_NanoAODv9-v1/NANOAOD", ->
            "/EGamma/Run2018B-UL2018_MiniAODv2_NanoAODv9-v1/NANOAOD", ->
            "/EGamma/Run2018C-UL2018_MiniAODv2_NanoAODv9-v1/NANOAOD", ->
            "/EGamma/Run2018D-UL2018_MiniAODv2_NanoAODv9-v1/NANOAOD"  ->
        ]

### Simulation (MC):

"EWK_LLJJ_MLL-50_MJJ-120_TuneCP5_13TeV-madgraph-pythia8_dipole":[
            "/EWK_LLJJ_MLL-50_MJJ-120_TuneCP5_13TeV-madgraph-pythia8_dipole/RunIISummer20UL18NanoAODv9-106X_upgrade2018_realistic_v16_L1v1-v1/NANOAODSIM"
        ] -> Dataset size: 14568681909 (14.6GB) Number of blocks: 3 Number of events: 7934000 Number of files: 10 Creation time: 2022-08-16 15:29:25 Cross section: 0 Physics group: NoGroup Status: VALID Type: mc

"DYJetsToLL_M-50_TuneCP5_13TeV-amcatnloFXFX-pythia8": [
            "/DYJetsToLL_M-50_TuneCP5_13TeV-amcatnloFXFX-pythia8/RunIISummer20UL18NanoAODv9-106X_upgrade2018_realistic_v16_L1v1-v2/NANOAODSIM"
        ] -> Dataset size: 257265865671 (257.3GB) Number of blocks: 39 Number of events: 195510810 Number of files: 204 Creation time: 2021-09-20 20:30:56 Physics group: NoGroup Status: VALID Type: mc

"TTTo2L2Nu_TuneCP5_13TeV-powheg-pythia8":[
            "/TTTo2L2Nu_TuneCP5_13TeV-powheg-pythia8/RunIISummer20UL18NanoAODv9-106X_upgrade2018_realistic_v16_L1v1-v1/NANOAODSIM"
        ] -> Dataset size: 309022244066 (309.0GB) Number of blocks: 39 Number of events: 145020000 Number of files: 155 Creation time: 2021-07-27 15:03:07 Physics group: NoGroup Status: VALID Type: mc

"TTToSemiLeptonic_TuneCP5_13TeV-powheg-pythia8":[
            "/TTToSemiLeptonic_TuneCP5_13TeV-powheg-pythia8/RunIISummer20UL18NanoAODv9-106X_upgrade2018_realistic_v16_L1v1-v1/NANOAODSIM"
        ] -> Dataset size: 1030792999916 (1.0TB) Number of blocks: 7 Number of events: 476408000 Number of files: 391 Creation time: 2021-07-30 06:34:37 Physics group: NoGroup Status: VALID Type: mc

"ST_tW_top_5f_inclusiveDecays_TuneCP5_13TeV-powheg-pythia8":[
            "/ST_tW_top_5f_inclusiveDecays_TuneCP5_13TeV-powheg-pythia8/RunIISummer20UL18NanoAODv9-106X_upgrade2018_realistic_v16_L1v1-v2/NANOAODSIM"
        ] -> Dataset size: 15685057180 (15.7GB) Number of blocks: 44 Number of events: 7956000 Number of files: 52 Creation time: 2021-09-04 00:49:03 Physics group: NoGroup Status: VALID Type: mc

"ST_tW_antitop_5f_inclusiveDecays_TuneCP5_13TeV-powheg-pythia8":[
            "/ST_tW_antitop_5f_inclusiveDecays_TuneCP5_13TeV-powheg-pythia8/RunIISummer20UL18NanoAODv9-106X_upgrade2018_realistic_v16_L1v1-v2/NANOAODSIM"
        ] -> Creation time: 2021-09-08 13:08:58 Physics group: NoGroup Status: VALID Type: mc Dataset size: 15186318776 (15.2GB) Number of blocks: 17 Number of events: 7749000 Number of files: 23

"ST_t-channel_top_4f_InclusiveDecays_TuneCP5_13TeV-powheg-madspin-pythia8":[
            "/ST_t-channel_top_4f_InclusiveDecays_TuneCP5_13TeV-powheg-madspin-pythia8/RunIISummer20UL18NanoAODv9-106X_upgrade2018_realistic_v16_L1v1-v1/NANOAODSIM"
        ] -> Dataset size: 301233146161 (301.2GB) Number of blocks: 6 Number of events: 178336000 Number of files: 149 Creation time: 2021-11-19 05:49:29 Physics group: NoGroup Status: VALID Type: mc

"ST_t-channel_antitop_4f_InclusiveDecays_TuneCP5_13TeV-powheg-madspin-pythia8":[
            "/ST_t-channel_antitop_4f_InclusiveDecays_TuneCP5_13TeV-powheg-madspin-pythia8/RunIISummer20UL18NanoAODv9-106X_upgrade2018_realistic_v16_L1v1-v1/NANOAODSIM"
        ] -> Dataset size: 162127100330 (162.1GB) Number of blocks: 36 Number of events: 95627000 Number of files: 130 Creation time: 2021-11-19 21:43:42 Physics group: NoGroup Status: VALID Type: mc

"ST_s-channel_4f_leptonDecays_TuneCP5_13TeV-amcatnlo-pythia8":[
            "/ST_s-channel_4f_leptonDecays_TuneCP5_13TeV-amcatnlo-pythia8/RunIISummer20UL18NanoAODv9-106X_upgrade2018_realistic_v16_L1v1-v1/NANOAODSIM"
        ] -> Dataset size: 31359845271 (31.4GB) Number of blocks: 6 Number of events: 19365999 Number of files: 19 Creation time: 2021-07-30 08:21:37 Physics group: NoGroup Status: VALID Type: mc

"WW_TuneCP5_13TeV-pythia8":[
            "/WW_TuneCP5_13TeV-pythia8/RunIISummer20UL18NanoAODv9-106X_upgrade2018_realistic_v16_L1v1-v1/NANOAODSIM"
        ] -> Creation time: 2021-07-22 07:54:29 Physics group: NoGroup Status: VALID Type: mc Dataset size: 18982542415 (19.0GB) Number of blocks: 27 Number of events: 15679000 Number of files: 31

"WZ_TuneCP5_13TeV-pythia8":[
            "/WZ_TuneCP5_13TeV-pythia8/RunIISummer20UL18NanoAODv9-106X_upgrade2018_realistic_v16_L1v1-v1/NANOAODSIM"
        ] -> Creation time: 2021-07-22 05:59:43 Physics group: NoGroup Status: VALID Type: mc Dataset size: 9716206542 (9.7GB) Number of blocks: 12 Number of events: 7940000 Number of files: 16

"ZZ_TuneCP5_13TeV-pythia8":[
            "/ZZ_TuneCP5_13TeV-pythia8/RunIISummer20UL18NanoAODv9-106X_upgrade2018_realistic_v16_L1v1-v1/NANOAODSIM"
        ] -> Creation time: 2022-03-13 07:49:34 Physics group: NoGroup Status: VALID Type: mc Dataset size: 4298534255 (4.3GB) Number of blocks: 5 Number of events: 3526000 Number of files: 6

