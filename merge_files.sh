#!/bin/bash
for dir in /nfs/dust/cms/user/moralfk/EWZjj_Pepper/eventdir_SR_MuMu18_full_sys_fixedMuonSF_sourcesJEC/*/     # list directories in the form "/tmp/dirname/"
do
    dir=${dir%*/}      # remove the trailing "/"
    echo "${dir##*/}"    # print everything after the final "/"

    #echo "${dir##*/}"/"${dir##*/}.root"
    #echo "${dir##*/}"/*".root"

    #hadd "${dir##*/}"/"${dir##*/}.root" "${dir##*/}"/*".root"

    current_directory="${dir##*/}"
    var="EWK_LLJJ_MLL-50_MJJ-120_TuneCP5_13TeV-madgraph-pythia8"
    
    if [[ "$current_directory" == *"$var"* ]]; then
	    gen="dipole"
	    #final_directory="${current_directory%%$gen*}$gen"
	    #mv "$current_directory"/"$current_directory.root" "$final_directory"/"$current_directory.root"
    else
	    gen="pythia8"
    fi

    final_directory="${current_directory%%$gen*}$gen"
    mv "$current_directory"/"$current_directory.root" "$final_directory"/"$current_directory.root" 
    
done


