import pandas as pd
import numpy as np
from sklearn.experimental import enable_hist_gradient_boosting
from numpy import loadtxt
from xgboost import XGBClassifier
from xgboost import plot_tree
import matplotlib.pyplot as plt
from sklearn.tree import plot_tree
from sklearn.model_selection import train_test_split
from sklearn.metrics import mean_absolute_error
from sklearn.ensemble import RandomForestRegressor, GradientBoostingRegressor, HistGradientBoostingRegressor, HistGradientBoostingClassifier
from sklearn.model_selection import cross_validate, RandomizedSearchCV
from sklearn.utils import shuffle
import random

EWK_MC = pd.read_csv("EWK_MC.csv")
signal = pd.DataFrame({"label": np.ones(len(EWK_MC))})
EWK_MC = EWK_MC.join(signal)
print(len(EWK_MC))
DY_MC = pd.read_csv("DY_MC.csv")
background = pd.DataFrame({"label": np.zeros(len(DY_MC))})
DY_MC = DY_MC.join(background)
print(DY_MC)
MC_dataset = pd.concat([EWK_MC, DY_MC], ignore_index=True, sort=False)
MC_dataset = random.shuffle(EWK_MC, DY_MC)
print(MC_dataset)

print('Size of data: {}'.format(MC_dataset.shape))
print('Number of events: {}'.format(MC_dataset.shape[0]))
print('Number of columns: {}'.format(MC_dataset.shape[1]))

print ('\nList of features in dataset:')
for col in MC_dataset.columns:
    print(col)

target_name = "label"
data = MC_dataset.drop(columns=[target_name, "event_id"])
target = MC_dataset[target_name]

data_train, data_test, target_train, target_test = train_test_split(data, target, random_state=0)
"""
# Random Forest dies
forest = RandomForestRegressor(n_estimators=200)
forest.fit(data_train, target_train)
target_predicted = forest.predict(data_test)
cv_results_forest = cross_validate(
    forest,
    data,
    target,
    scoring="neg_mean_absolute_error",
    n_jobs=2,
)
print("Random Forest")
print(
    "Mean absolute error via cross-validation: "
    f"{-cv_results_forest['test_score'].mean():.3f} ± "
    f"{cv_results_forest['test_score'].std():.3f} k$"
)
print(f"Average fit time: {cv_results_forest['fit_time'].mean():.3f} seconds")
print(
    f"Average score time: {cv_results_forest['score_time'].mean():.3f} seconds"
)

# Gradient Boosting Regressor (dies)
gradient_boosting = GradientBoostingRegressor(n_estimators=200)
cv_results_gbdt = cross_validate(
    gradient_boosting,
    data,
    target,
    scoring="neg_mean_absolute_error",
    n_jobs=2,
)
print("Gradient Boosting Decision Tree")
print(
    "Mean absolute error via cross-validation: "
    f"{-cv_results_gbdt['test_score'].mean():.3f} ± "
    f"{cv_results_gbdt['test_score'].std():.3f} k$"
)
print(f"Average fit time: {cv_results_gbdt['fit_time'].mean():.3f} seconds")
print(
    f"Average score time: {cv_results_gbdt['score_time'].mean():.3f} seconds"
)
"""
# Hist Gradient Boosting Regressor
#First strategy: default parameters
"""
Hist_GBT = HistGradientBoostingRegressor(max_iter=200, random_state=0)
Hist_GBT.fit(data_train, target_train)
target_predicted = Hist_GBT.predict(data_test)
target_predicted = np.round(target_predicted, 2)
print("target predicted:", target_predicted)
score_hist, counts = np.unique(target_predicted, return_counts=True)
score_counts_dict = dict(zip(score_hist, counts))
print(score_counts_dict)
cv_results_hgbdt = cross_validate(
    Hist_GBT,
    data,
    target,
    scoring="neg_mean_absolute_error",
    n_jobs=2,
)
print("Histogram Gradient Boosting Decision Tree")
print(
    "Mean absolute error via cross-validation: "
    f"{-cv_results_hgbdt['test_score'].mean():.3f} ± "
    f"{cv_results_hgbdt['test_score'].std():.3f}"
)
print(f"Average fit time: {cv_results_hgbdt['fit_time'].mean():.3f} seconds")
print(
    f"Average score time: {cv_results_hgbdt['score_time'].mean():.3f} seconds"
)
print()

#fig, axs = plt.subplots()
#axs.hist(counts, bins=score_hist)
#plt.show()

# Hist Gradient Boosting Classifier
#First strategy: default parameters
Hist_GBT = HistGradientBoostingClassifier(max_iter=200, random_state=0)
Hist_GBT.fit(data_train, target_train)
target_predicted = Hist_GBT.predict(data_test)
print("target predicted:", target_predicted)
if 0 in target_predicted:
    print("there is signal")
cv_results_hgbdt = cross_validate(
    Hist_GBT,
    data,
    target,
    scoring="neg_mean_absolute_error",
    n_jobs=2,
)
print("Histogram Gradient Boosting Decision Tree")
print(
    "Mean absolute error via cross-validation: "
    f"{-cv_results_hgbdt['test_score'].mean():.3f} ± "
    f"{cv_results_hgbdt['test_score'].std():.3f}"
)
print(f"Average fit time: {cv_results_hgbdt['fit_time'].mean():.3f} seconds")
print(
    f"Average score time: {cv_results_hgbdt['score_time'].mean():.3f} seconds"
)

# Visualization

viz = dtreeviz.model(Hist_GBT, tree_index = 3, data_train, target_train,
               feature_names=list(data_train.columns),
               target_name='label')
viz.save("tree_visualization.svg") 
viz.view()
"""
# Second strategy: random seach for hyperparameters
"""
param_distributions = {
    "max_leaf_nodes": [2, 5, 10, 20, 31,50, 100],
    "max_iter":[10, 50, 100, 150, 200],
    "max_depth": [2, 5, 8, 11, 14, 17],
    "learning_rate": loguniform(0.01, 1),
    "min_samples_leaf":[20, 50, 100, 200, 300, 400, 500, 600, 700, 800, 900, 1000, 2000, 5000]}
search_cv = RandomizedSearchCV(
    HistGradientBoostingRegressor(),
    param_distributions=param_distributions,
    scoring="neg_mean_absolute_error",
    random_state=0,
    n_iter=10,
    n_jobs=2
)
search_cv.fit(data_train, target_train)

columns = [f"param_{name}" for name in param_distributions.keys()]
columns += ["mean_test_error", "std_test_error"]
cv_results = pd.DataFrame(search_cv.cv_results_)
cv_results["mean_test_error"] = -cv_results["mean_test_score"]
cv_results["std_test_error"] = cv_results["std_test_score"]
cv_results[columns].sort_values(by="mean_test_error")
cv_results_hgbdt = cross_validate(
    search_cv,
    data,
    target,
    scoring="neg_mean_absolute_error",
    n_jobs=2,
)
print("Histogram Gradient Boosting Decision Tree")
print(
    "Mean absolute error via cross-validation: "
    f"{-cv_results_hgbdt['test_score'].mean():.3f} ± "
    f"{cv_results_hgbdt['test_score'].std():.3f}"
)
print(f"Average fit time: {cv_results_hgbdt['fit_time'].mean():.3f} seconds")
print(
    f"Average score time: {cv_results_hgbdt['score_time'].mean():.3f} seconds"
)
"""
