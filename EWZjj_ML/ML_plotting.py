import matplotlib.pyplot as plt
import mplhep as hep
import uproot
import matplotlib.gridspec as gridspec
import numpy as np
import matplotlib.patches as patches
import matplotlib.path as path
import hist
from hist import Hist
from sklearn.experimental import enable_hist_gradient_boosting
from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestClassifier, GradientBoostingClassifier, HistGradientBoostingClassifier
from sklearn.ensemble import RandomForestRegressor, GradientBoostingRegressor
from sklearn.model_selection import cross_validate, cross_val_score
from sklearn.metrics import mean_absolute_error
def make_ratio(hnum, hden):
  wnum, bins = hnum.to_numpy()
  wden, bins = hden.to_numpy()

  numerr = np.sqrt(hnum.variances())
  
  #wratio = np.divide(wnum, wden) - 1
  wratio = np.divide(wnum, wden)
  print('data')
  print(wnum)
  print('mc')
  print(wden)
  ratioerr = np.divide(numerr, wden)
  bincenters = (bins[:-1] + bins[1:]) / 2
  return wratio, bins, ratioerr

def systematics(mc_nom_file, sys_up_file, sys_down_file):
    sys_up_f = 0
    sys_down_f = 0
    for mc_nom, sys_up_value, sys_down_value in zip(mc_nom_file, sys_up_file, sys_down_file):
        mc_nom = hist.Hist(mc_nom)
        sys_up_value = hist.Hist(sys_up_value)
        sys_down_value = hist.Hist(sys_down_value)
        sys_up_temp = ((sys_up_value.values() - mc_nom.values())**2)
        sys_down_temp = ((sys_down_value.values() - mc_nom.values())**2)
        sys_up_f = sys_up_temp + sys_up_f
        sys_down_f = sys_down_temp + sys_down_f
    sys_up = np.sqrt(sys_up_f)
    sys_down = np.sqrt(sys_down_f)
    
    return sys_up, sys_down

def total_systematics(sys_up, sys_down):
    sys_up_f = 0
    sys_down_f = 0
    for sys_up_partial, sys_down_partial in zip(sys_up, sys_down):
        sys_up_temp = (sys_up_partial**2)
        sys_down_temp = (sys_down_partial**2)
        sys_up_f = sys_up_temp + sys_up_f
        sys_down_f = sys_down_temp + sys_down_f
    total_sys_up = np.sqrt(sys_up_f)
    total_sys_down = np.sqrt(sys_down_f)
    return total_sys_up, total_sys_down

def systematics_ratio(sys_up, sys_down, mc_nom_stacked):
    ratio_up_sys = 1 + sys_up/mc_nom_stacked  
    ratio_down_sys = 1 - sys_down/mc_nom_stacked
    return ratio_up_sys, ratio_down_sys

# Load histograms

# General results directory
inputdir =  '/nfs/dust/cms/user/moralfk/EWZjj_Pepper/hists/'

# New file names
file_names = ['Dijet_cut_pv_npvs.root', 'Dijet_cut_pv_npvsGood.root', 'Dijet_cut_leading_jet_QGL.root', 'Dijet_cut_subleading_jet_QGL.root', 'Dijet_cut_R_pT_hard.root', 'Dijet_cut_z_star.root',
              'Dijet_cut_leading_muon_pt.root', 'Dijet_cut_leading_muon_abseta.root', 'Dijet_cut_Delta_R_jets_l_mu.root', 'Dijet_cut_Delta_R_jets_s_mu.root',
              'Dijet_cut_subleading_muon_pt.root', 'Dijet_cut_subleading_muon_abseta.root',
              'Dijet_cut_leading_jet_pt.root', 'Dijet_cut_leading_jet_abseta.root', 'Dijet_cut_subleading_jet_pt.root', 'Dijet_cut_subleading_jet_abseta.root', 
              'Dijet_cut_Dijet_pt.root', 'Dijet_cut_Dijet_abseta.root', 'Dijet_cut_Dijet_mass.root', 'Dijet_cut_Dijet_phi.root', 
              'Dijet_cut_number_of_jets.root', 'Dijet_cut_number_of_low_pT_jets.root', 'Dijet_cut_dphijj.root', 'Dijet_cut_detajj.root', 'Dijet_cut_detall.root', 'Dijet_cut_dphill.root',
              'Dijet_cut_Zboson_pt.root', 'Dijet_cut_Zboson_mass.root', 'Dijet_cut_Zboson_abseta.root', 'Dijet_cut_Zboson_phi.root', 
              'Dijet_cut_DphiZ_l_jet.root', 'Dijet_cut_DphiZ_s_jet.root', 'Dijet_cut_DphiZ_jets.root']

#file_names = ['Dijet_cut_dphijj.root', 'Dijet_cut_dphill.root', 'Dijet_cut_DphiZ_l_jet.root', 'Dijet_cut_DphiZ_s_jet.root', 'Dijet_cut_DphiZ_jets.root']
#file_names = ['Dijet_cut_leading_jet_QGL.root', 'Dijet_cut_subleading_jet_QGL.root']
#file_names = ['Dijet_cut_Delta_R_jets_l_mu.root', 'Dijet_cut_Delta_R_jets_s_mu.root', 'Dijet_cut_Zboson_mass.root', 'Dijet_cut_pv_npvs.root', 'PV_pv_npvs.root', 'Dijet_cut_Dijet_pt.root']

samples = ['EWK_LLJJ_MLL-50_MJJ-120_TuneCP5_13TeV-madgraph-pythia8_dipole', 'WW_TuneCP5_13TeV-pythia8', 'WZ_TuneCP5_13TeV-pythia8', 'ZZ_TuneCP5_13TeV-pythia8',
           'ST_tW_top_5f_inclusiveDecays_TuneCP5_13TeV-powheg-pythia8', 'ST_t-channel_top_4f_InclusiveDecays_TuneCP5_13TeV-powheg-madspin-pythia8',
           'ST_tW_antitop_5f_inclusiveDecays_TuneCP5_13TeV-powheg-pythia8','ST_t-channel_antitop_4f_InclusiveDecays_TuneCP5_13TeV-powheg-madspin-pythia8',
           'ST_s-channel_4f_leptonDecays_TuneCP5_13TeV-amcatnlo-pythia8', 'TTTo2L2Nu_TuneCP5_13TeV-powheg-pythia8','TTToSemiLeptonic_TuneCP5_13TeV-powheg-pythia8', 
           'DYJetsToLL_M-50_TuneCP5_13TeV-amcatnloFXFX-pythia8', 'SingleMuon' ]

for file_name in file_names: 
    mc_background = [None]*(len(samples)-2) 
    for i in range(len(mc_background)):
        open_file = uproot.open(inputdir + file_name)
        if file_name != 'EWK_LLJJ_MLL-50_MJJ-120_TuneCP5_13TeV-madgraph-pythia8_dipole':
            mc_background[i] = open_file[samples[i]].to_hist()
        mc_signal = open_file['EWK_LLJJ_MLL-50_MJJ-120_TuneCP5_13TeV-madgraph-pythia8_dipole'].to_hist()
        data_histogram = open_file['SingleMuon'].to_hist()
   
    # (Predicted) physical observables: mjj, etajj, pTjj, QGL1, QGL2, z*, RpThard
    # Strategy: (should I compare first boosted decision tree to RF?) random forest for all observables -> check scores -> keep only meaningfull inputs (approx score > 0.92)
    #           I should run it with all the backgrounds and then compare it to considering DY only 
    """
    signal = mc_signal.values().astype('int').reshape(-1, 1)
    background = sum(mc_background).values().reshape(-1, 1)
    X = np.concatenate([signal,background])
    Y =  np.concatenate([np.ones(len(signal)),np.zeros(len(background))])
    # We use a background subset to test our Random forest because we want to predict the signal
    x_train, x_test, y_train, y_test = train_test_split(X, Y, random_state=0)
    """
    # Random forest
    signal = mc_signal.values()
    background = sum(mc_background).values()
    background_train, background_test, signal_train, signal_test = train_test_split(background, signal, random_state=0)
    random_forest = RandomForestRegressor(n_estimators=200, n_jobs=2, random_state=0)
    background_test = background_test.reshape(-1,1)
    random_forest.fit(background_train.reshape(-1,1), signal_train.astype('int'))
    signal_predicted = random_forest.predict(background_test)
    print("Predicted MC EWK Z signal for random forest:", signal_predicted)
    print("Test MC EWK Z signal for random forest:", signal_test)
    print(
    "Mean absolute error: "
    f"{mean_absolute_error(signal_test, signal_predicted):.3f}")
    
    cv_results_random_forest = cross_validate(random_forest, background_test.reshape(-1, 1), signal_test.astype('int'), cv = 2)
    print("Mean absolute error via cross-validation: "
    f"{-cv_results_random_forest['test_score'].mean():.3f} ± "
    f"{cv_results_random_forest['test_score'].std():.3f} k$")
   
    print(f"Average fit time: {cv_results_random_forest['fit_time'].mean():.3f} seconds")
    print(f"Average score time: {cv_results_random_forest['score_time'].mean():.3f} seconds")

    """ 
    # Random forest
    random_forest = RandomForestClassifier(n_estimators=200, n_jobs=2, random_state=0)
    random_forest.fit(background_train, signal_train)
    signal_predicted = random_forest.predict(background_test)
    print("Predicted MC EWK Z signal for random forest:", signal_predicted)
    print("Test MC EWK Z signal for random forest:", signal_test)
    print(
    "Mean absolute error: "
    f"{mean_absolute_error(signal_test, signal_predicted):.3f}")
    
    cv_results_random_forest = cross_validate(random_forest, background_test.reshape(-1, 1), signal_test.astype('int'), cv = 2)
    print("Mean absolute error via cross-validation: "
    f"{-cv_results_random_forest['test_score'].mean():.3f} ± "
    f"{cv_results_random_forest['test_score'].std():.3f} k$")
   
    print(f"Average fit time: {cv_results_random_forest['fit_time'].mean():.3f} seconds")
    print(f"Average score time: {cv_results_random_forest['score_time'].mean():.3f} seconds")
    
    # Boosted Gradient Decision Tree
    gradient_boosting = GradientBoostingClassifier(n_estimators=200)
    gradient_boosting.fit(background_train, signal_train)
    signal_predicted = gradient_boosting.predict(background_test)
    print("Predicted MC EWK Z signal for gradient boosting classifier:", signal_predicted)
    print("Test MC EWK Z signal for gradient boosting classifier:", signal_test)
    print(
    "Mean absolute error: "
    f"{mean_absolute_error(signal_test, signal_predicted):.3f}")
 
    # Histogram Boosted Gradient Decision Tree 
    histogram_gradient_boosting = GradientBoostingClassifier(max_depth=3) #max_iter=200, random_state=0, learning_rate=1)
    histogram_gradient_boosting.fit(x_train, y_train)
    predicted = histogram_gradient_boosting.predict(x_test)
    print(predicted)
    print(y_test)
    #print("Predicted MC EWK Z signal for histogram gradient boosting classifier:", signal_predicted)
    #print("Test MC EWK Z signal for histogram gradient boosting classifier:", signal_test)
    #print(
    #"Mean absolute error: "
    #f"{mean_absolute_error(signal_test, signal_predicted):.3f}")
    """
     
    # Set style
    plt.style.use(hep.style.ROOT)
    plt.rcParams.update({'font.size': 14})   
    plt.rcParams.update({'axes.titlesize':20})
    plt.rcParams.update({'axes.labelsize':20})

    # Set stack and ratio plot
    fig, ax = plt.subplots()

    # Make stack plot in upper axis
    stack_axis = ax
  
    hep.histplot(sum(mc_background), color = 'lightcoral', ax=stack_axis, binwnorm=1., stack=False, histtype='step', label = 'background')
    hep.histplot(mc_signal, color = 'firebrick', ax=stack_axis, binwnorm=1., stack=False, histtype='step', label = r'EWK $Z\rightarrow\mu^{+}\mu^{-}$')
    stack_axis.legend()
    stack_axis.set_yscale('log')
    stack_axis.set_ylabel(r'Events/bin')
    if file_name == 'Dijet_cut_Delta_R_jets_l_mu.root':
        stack_axis.set_xlabel('$\\Delta R_{\\mu_{L} , jets}$')
    if file_name == 'Dijet_cut_Delta_R_jets_s_mu.root':
        stack_axis.set_xlabel('$\\Delta R_{\\mu_{S} , jets}$')
    if file_name == 'Dijet_cut_pv_npvsGood.root':
        stack_axis.set_xlabel('Number of good primary vertices')
    if file_name == 'Dijet_cut_leading_muon_pt.root':
        stack_axis.set_xlabel('Leading muon transverse momentum: $p_{T}$ [GeV]')   
    elif file_name == 'Dijet_cut_leading_muon_abseta.root':
        stack_axis.set_xlabel('Leading muon $|\eta|$')
    elif file_name == 'Dijet_cut_Delta_R_l_mu_l_jet.root':
        stack_axis.set_xlabel(' $\\Delta R_{\\mu_{L} , jet_{L}}$')
    elif file_name == 'Dijet_cut_Delta_R_l_mu_s_jet.root':
        stack_axis.set_xlabel('$\\Delta R_{\\mu_{L} , jet_{S}}$')
    elif file_name == 'Dijet_cut_Delta_R_l_jet.root':
        stack_axis.set_xlabel(' $\\Delta R_{j_{L} , \\mu \\mu}$')
    elif file_name == 'Dijet_cut_Delta_R_s_jet.root':
        stack_axis.set_xlabel('$\\Delta R_{j_{S} , \\mu \\mu}$')
    elif file_name == 'Dijet_cut_Delta_phi_l_mu_l_jet.root':
        stack_axis.set_xlabel(' $\\Delta \\phi_{\\mu_{L}, jet_{L}}$')
    elif file_name == 'Dijet_cut_Delta_phi_l_mu_s_jet.root':
        stack_axis.set_xlabel('$\\Delta \\phi_{\\mu_{L}, jet_{S}}$')
    elif file_name == 'Dijet_cut_subleading_muon_pt.root':
        stack_axis.set_xlabel('Subleading muon transverse momentum: $p_{T}$ [GeV]')
    elif file_name == 'Dijet_cut_subleading_muon_abseta.root':
        stack_axis.set_xlabel('Subleading muon $|\eta|$')
    elif file_name == 'Dijet_cut_leading_jet_pt.root':
        stack_axis.set_xlabel('Leading jet transverse momentum: $p_{T}$ [GeV]')
    elif file_name == 'Dijet_cut_Delta_R_s_mu_l_jet.root':
        stack_axis.set_xlabel(' $\\Delta R_{\\mu_{S} , jet_{L}}$')
    elif file_name == 'Dijet_cut_Delta_R_s_mu_s_jet.root':
        stack_axis.set_xlabel('$\\Delta R_{\\mu_{S} , jet_{S}}$')
    elif file_name == 'Dijet_cut_Delta_phi_s_mu_l_jet.root':
        stack_axis.set_xlabel(' $\\Delta \\phi_{\\mu_{S}, jet_{L}}$')
    elif file_name == 'Dijet_cut_Delta_phi_s_mu_s_jet.root':
        stack_axis.set_xlabel('$\\Delta \\phi_{\\mu_{S}, jet_{S}}$')
    elif file_name == 'Dijet_cut_leading_jet_abseta.root':
        stack_axis.set_xlabel('Leading jet $|\eta|$')
    elif file_name == 'Dijet_cut_subleading_jet_pt.root':
        stack_axis.set_xlabel('Subleading jet transverse momentum: $p_{T}$ [GeV]')
    elif file_name == 'Dijet_cut_subleading_jet_abseta.root':
        stack_axis.set_xlabel('Subleading jet $|\eta|$')
    elif (file_name == 'Dijet_cut_mjj.root') or (file_name == 'Dijet_cut_Dijet_mass.root'):
        stack_axis.set_xlabel('Dijet mass: $m_{jj}$ [GeV]', fontsize = 20)
    elif (file_name == 'Dijet_cut_pTjj.root') or (file_name == 'Dijet_cut_Dijet_pt.root'):
        stack_axis.set_xlabel('$p_{Tjj}$ [GeV]', fontsize = 20)
    elif file_name == 'Dijet_cut_Dijet_phi.root':
        stack_axis.set_xlabel(' $\\phi_{jj}$ ', fontsize = 20)
    elif file_name == 'Dijet_cut_Dijet_abseta.root':
        stack_axis.set_xlabel(' $ | \\eta_{jj} | $ ', fontsize = 20)
    elif file_name == 'Dijet_cut_Zboson_mass.root':
        stack_axis.set_xlabel('Z boson mass: $m_{l^{+}l^{-}}$ [GeV]', fontsize = 20)
    elif file_name == 'Dijet_cut_Zboson_pt.root':
        stack_axis.set_xlabel('Z boson transverse momentum: $p_{T}$ [GeV]', fontsize = 20)
    elif file_name == 'Dijet_cut_Zboson_abseta.root':
        stack_axis.set_xlabel('$|\\eta|_{l^{+}l^{-}}$ ', fontsize = 20)
    elif file_name == 'Dijet_cut_Zboson_phi.root':
        stack_axis.set_xlabel('$|\\phi|_{l^{+}l^{-}}$ ', fontsize = 20)
    elif file_name == 'Dijet_cut_number_of_jets.root':
        stack_axis.set_xlabel('Number of jets')
    elif file_name == 'Dijet_cut_number_of_low_pT_jets.root':
        stack_axis.set_xlabel('Number of low pT jets')
    elif file_name == 'Dijet_cut_DphiZ_jets.root':
        stack_axis.set_xlabel(' $\\Delta \\phi_{Z, jj}$ ', fontsize = 20)
    elif file_name == 'Dijet_cut_DphiZ_l_jet.root':
        stack_axis.set_xlabel(' $\\Delta \\phi_{Z, j_{L}}$ ', fontsize = 20)
    elif file_name == 'Dijet_cut_DphiZ_s_jet.root':
        stack_axis.set_xlabel(' $\\Delta \\phi_{Z, j_{S}}$ ', fontsize = 20)
    elif file_name == 'Dijet_cut_Delta_phi_l_jet_mus.root':
        stack_axis.set_xlabel(' $\\Delta \\phi_{j_{L}, \\mu \\mu}$ ', fontsize = 20)
    elif file_name == 'Dijet_cut_Delta_phi_s_jet_mus.root':
        stack_axis.set_xlabel(' $\\Delta \\phi_{j_{S}, \\mu \\mu}$ ', fontsize = 20)
    elif file_name == 'Dijet_cut_dphill.root':
        stack_axis.set_xlabel(' $\\Delta \\phi_{\\mu \\mu}$ ', fontsize = 20)
    elif file_name == 'Dijet_cut_dphijj.root':
        stack_axis.set_xlabel(' $\\Delta \\phi_{j_{L} j_{S}}$ ', fontsize = 20)
    elif file_name == 'Dijet_cut_detall.root':
        stack_axis.set_xlabel(' $\\Delta \\eta_{\\mu \\mu}$ ', fontsize = 20)
    elif file_name == 'Dijet_cut_detajj.root':
        stack_axis.set_xlabel(' $\\Delta \\eta_{j_{L} j_{S}}$ ',fontsize = 20)
    elif file_name == 'Dijet_cut_R_pT_hard.root':
        stack_axis.set_xlabel('$R_{p_{T}}^{hard}$ ', fontsize = 20)
    elif file_name == 'Dijet_cut_z_star.root':
        stack_axis.set_xlabel(' $z^*$ ')
    elif file_name == 'Dijet_cut_pv_npvs.root':
        stack_axis.set_xlabel('Number of total reconstructed  primary vertices')
    elif file_name == 'Muon_SF_pv_npvs.root':
        stack_axis.set_xlabel('Number of good primary vertices')
    elif file_name == 'Pileup reweighting_pv_npvs.root':
        stack_axis.set_xlabel('Number of total reconstructed primary vertices')
    elif file_name == 'Dijet_cut_QGD.root':
        stack_axis.set_xlabel('QGL Discriminant')
    elif file_name == 'Dijet_cut_Delta_R_jets_l_mu.root':
        stack_axis.set_xlabel(' $R_{jets, \\mu_{L}}$ ')
    elif file_name == 'Dijet_cut_Delta_R_jets_s_mu.root':
        stack_axis.set_xlabel(' $R_{jets, \\mu_{S}}$ ')
    elif file_name == 'Dijet_cut_leading_jet_QGL.root':
        stack_axis.set_xlabel('Leading jet QGL', fontsize = 20)
    elif file_name == 'Dijet_cut_subleading_jet_QGL.root':
        stack_axis.set_xlabel('Subleading jet QGL', fontsize = 20)
    hep.cms.label(loc=0, data=True, ax=stack_axis, lumi=59.83, year=2018)
    
    #plt.savefig('/afs/desy.de/user/m/moralfk/www/EWZjj-test-figures/NEW_DetectorLevelPlots/Pepper_muon_ALL_20thjune_no_mjj_47eta_'+file_name+'.png')
    plt.show() 
