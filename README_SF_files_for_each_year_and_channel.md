# EWZjj Analysis SF files

## SF files: description and calculation

Scale Factors (SFs) files for each year / channel / selection. Details to take into account: 

- Cross-section SFs need to be calculated for each year because they are only applied to MC samples BEFORE any further selection (no dependence on either muon or electron channel).
- PU SFs also need to be calculated for each year without any dependence on either muon or electron channel.
- Btag SFs need to be calculated for each year, each channel and each selection if any cut has been modified in the workflow before applying the btag SF.
- The rest of SFs files (such as muon ID, rochester, trigger efficiency etc) are centrally-produced either by the POGs or have been derived by preious individuals for specific selections compatible with our analysis.


Notes about each year technical points:

- 2018:
    - Jet veto maps are applied
    - (strict) Btag veto: we requiere no b-produced jets and we implement a btag-scale factor produced with DeepJet. 

- 2017:
    - No jet veto maps are applied 
    - (strict) Btag veto: we requiere no b-produced jets and we implement a btag-scale factor produced with DeepJet.

- 2016:
    - In both Pre and Post, no jet veto maps are applied (after some discussion with experts, we know that there is no significant influence due to our selection across the 3 years)
    - (strict) Btag veto: we requiere no b-produced jets and we implement a btag-scale factor produced with DeepJet.        
        - PreVFP
        - PostVFP

# Btag SF files

## 2018 

### ML

   - Muon channel:
       - Currently:
           - Signal region: /EWZjj/EWZjj_files/EWZjj_btag/EWZjj_btag_ML_MuMu_2018_LeadMu40_nohighpTMuon.root
           - DY control region with inverted Delta eta_jj: /EWZjj/EWZjj_files/EWZjj_btag/EWZjj_btag_DY_CR_ML_MuMu_2018_LeadMu40_nohighpTMuon.root
           - TT control region with at least 1 b jet: /EWZjj/EWZjj_files/EWZjj_btag/EWZjj_btag_TT_CR_ML_MuMu_2018_LeadMu40_nohighpTMuon.root
       - Previously (high pT muon correction): /EWZjj/EWZjj_files/EWZjj_btag/EWZjj_btag_ML_MuMu_2018_LeadMu40.root 
   - Electron channel: /EWZjj/EWZjj_files/EWZjj_btag/EWZjj_btag_ML_ee_2018_HLT32.root

## 2017
  
### ML

   - Muon channel: /EWZjj/EWZjj_2017/EWZjj_btag/EWZjj_btag_ML_MuMu17_nohighpTMuon.root
   - Electron channel: /EWZjj/EWZjj_2017/EWZjj_btag/EWZjj_btag_ML_ee_2017_HLT35.root

## 2016

#### ML (Pre)
   
   - Muon channel: /EWZjj/EWZjj_2016/EWZjj_2016_preVFP/EWZjj_btag/EWZjj_btag_ee_UL16preVFP_noMuhighpT.root
   - Electron channel: /EWZjj/EWZjj_2016/EWZjj_2016_preVFP/EWZjj_btag/EWZjj_btag_ee_UL16preVFP.root

#### ML (Post) 

   - Muon channel: /EWZjj/EWZjj_2016/EWZjj_2016_postVFP/EWZjj_btag/EWZjj_btag_ML_MuMu16post_nohighpTMuon.root
   - Electron channel: /EWZjj/EWZjj_2016/EWZjj_2016_postVFP/EWZjj_btag/EWZjj_btag_ee_UL16postVFP_new.root



